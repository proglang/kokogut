#!@KO_PERL_PATH@

# This file a is part of Kokogut.
#
# Kokogut is a compiler of the Kogut programming language.
# Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
# (QrczakMK@gmail.com)
#
# Kokogut is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# Kokogut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

use strict;
$^W = 1;

# Algorithms for determining text boundaries are described in
# <http://www.unicode.org/reports/tr29/>.
# This version corresponds to revision 13.

my $default_data = {
   CAT         => 'Cn',
   CC          => 0,
   DECOMP      => [],
   NFD_SIZE    => 1,
   DECOMP_K    => [],
   NFKD_SIZE   => 1,
   COMP_EXCL   => 0,
   COMP_KEY    => -1,
   GCB         => 'Other',
   WB          => 'Other',
   CASED       => 0,
   SOFT_DOTTED => 0,
   UPPER       => 0,
   LOWER       => 0,
   TITLE       => 0,
   FOLD        => [],
   DIGIT       => -1,
};
my @data = ($default_data) x 0x110000;

sub char_range($) {
   my ($range) = @_;
   if ($range =~ /^([0-9A-Fa-f]+)\.\.([0-9A-Fa-f]+)$/) {
      return hex($1)..hex($2);
   }
   else {
      return hex $range;
   }
}

sub case_diff($$) {
   my ($char, $base) = @_;
   return !defined($char) || $char eq "" ? 0 : hex($char) - $base;
}

sub digit_value($) {
   my ($digit) = @_;
   return !defined($digit) || $digit eq "" ? -1 : $digit;
}

open DATA, "unidata/UnicodeData.txt"
   or die "Can't open unidata/UnicodeData.txt";
while (<DATA>) {
   chomp;
   my @fields = split /;/;
   my $char = hex $fields[0];
   my $cat = $fields[2];
   my $cc = $fields[3];
   my @decomp = ();
   my @decomp_k = ();
   my $decomp_str = $fields[5];
   if ($decomp_str ne '') {
      if ($decomp_str =~ s/^<\w+> //) {
         @decomp_k = map {hex} split(/ /, $decomp_str);
      }
      else {
         @decomp = map {hex} split(/ /, $decomp_str);
      }
   }
   my $cased = $cat =~ /^L[ult]$/ ? 1 : 0;
   my $upper = case_diff($fields[12], $char);
   my $lower = case_diff($fields[13], $char);
   my $title = case_diff($fields[14], $char + $upper);
   my $digit = digit_value($fields[6]);
   my $data = {
      CAT         => $cat,
      CC          => $cc,
      DECOMP      => \@decomp,
      NFD_SIZE    => 1,
      DECOMP_K    => \@decomp_k,
      NFKD_SIZE   => 1,
      COMP_EXCL   => 0,
      COMP_KEY    => -1,
      GCB         => 'Other',
      WB          => 'Other',
      CASED       => $cased,
      SOFT_DOTTED => 0,
      UPPER       => $upper,
      LOWER       => $lower,
      TITLE       => $title,
      FOLD        => [],
      DIGIT       => $digit,
   };
   if ($fields[1] =~ /First>$/) {
      my $first = $char;
      $_ = <DATA>;
      chomp;
      my @fields = split /;/;
      my $last = hex $fields[0];
      foreach $char ($first..$last) {
         $data[$char] = $data;
      }
   }
   else {
      $data[$char] = $data;
   }
}
close DATA;

open PROP, "unidata/PropList.txt"
   or die "Can't open unidata/PropList.txt";
while (<PROP>) {
   chomp;
   s/#.*$//;
   next unless /^\s*(\S+)\s*;\s*(\S+)\s*$/;
   my $range = $1;
   my $prop = $2;
   if ($prop eq 'Other_Lowercase' || $prop eq 'Other_Uppercase') {
      foreach my $char (char_range($range)) {
         $data[$char]{CASED} = 1;
      }
   }
   elsif ($prop eq 'Soft_Dotted') {
      foreach my $char (char_range($range)) {
         $data[$char]{SOFT_DOTTED} = 1;
      }
   }
}
close PROP;

my $SBase = 0xAC00;
my $LBase = 0x1100;
my $VBase = 0x1161;
my $TBase = 0x11A7;
my $LCount = 19;
my $VCount = 21;
my $TCount = 28;
my $NCount = $VCount * $TCount;
my $SCount = $LCount * $NCount;

sub nfd_size($) {
   my ($char) = @_;
   if ($char >= $SBase && $char < $SBase + $SCount) {
      return ($char - $SBase) % $TCount == 0 ? 2 : 3;
   }
   my $decomp = $data[$char]{DECOMP};
   if (@$decomp == 0) {return 1}
   my $total = 0;
   foreach my $decomp_char (@$decomp) {
      $total += nfd_size($decomp_char);
   }
   return $total;
}

foreach my $char (0..0x10FFFF) {
   my $this_data = $data[$char];
   $this_data->{NFD_SIZE} = nfd_size($char) if @{$this_data->{DECOMP}};
}

sub nfkd_size($) {
   my ($char) = @_;
   if ($char >= $SBase && $char < $SBase + $SCount) {
      return ($char - $SBase) % $TCount == 0 ? 2 : 3;
   }
   my $this_data = $data[$char];
   my $decomp = $this_data->{DECOMP};
   if (@$decomp == 0) {
      $decomp = $this_data->{DECOMP_K};
      if (@$decomp == 0) {return 1}
   }
   my $total = 0;
   foreach my $decomp_char (@$decomp) {
      $total += nfkd_size($decomp_char);
   }
   return $total;
}

foreach my $char (0..0x10FFFF) {
   my $this_data = $data[$char];
   $this_data->{NFKD_SIZE} = nfkd_size($char)
     if @{$this_data->{DECOMP}} || @{$this_data->{DECOMP_K}};
}

open COMP_EXCL, "unidata/CompositionExclusions.txt"
   or die "Can't open unidata/CompositionExclusions.txt";
while (<COMP_EXCL>) {
   chomp;
   s/#.*$//;
   next unless /^\s*(\S+)\s*$/;
   my $range = $1;
   foreach my $char (char_range($range)) {
      $data[$char]{COMP_EXCL} = 1;
   }
}
close COMP_EXCL;

my $comp_keys = 0;
foreach my $char (0..0xFFFF) {
   my $decomp = $data[$char]{DECOMP};
   next if @$decomp != 2 || $data[$char]{COMP_EXCL};
   if ($data[$decomp->[0]]{CC} != 0) {
      $data[$char]{COMP_EXCL} = 1;
      next;
   }
   foreach my $decomp_char (@$decomp) {
      my $this_data = $data[$decomp_char];
      next if $this_data->{COMP_KEY} != -1;
      $this_data->{COMP_KEY} = $comp_keys++;
   }
}

open GCB, "unidata/auxiliary/GraphemeBreakProperty.txt"
   or die "Can't open unidata/auxiliary/GraphemeBreakProperty.txt";
while (<GCB>) {
   chomp;
   s/#.*$//;
   next unless /^\s*(\S+)\s*;\s*(\S+)\s*$/;
   my $range = $1;
   my $value = $2;
   foreach my $char (char_range($range)) {
      $data[$char]{GCB} = $value;
   }
}
close GCB;

open WB, "unidata/auxiliary/WordBreakProperty.txt"
   or die "Can't open unidata/auxiliary/WorBreakProperty.txt";
while (<WB>) {
   chomp;
   s/#.*$//;
   next unless /^\s*(\S+)\s*;\s*(\S+)\s*$/;
   my $range = $1;
   my $value = $2;
   foreach my $char (char_range($range)) {
      $data[$char]{WB} = $value;
   }
}
close WB;

open FOLD, "unidata/CaseFolding.txt"
   or die "Can't open unidata/CaseFolding.txt";
while (<FOLD>) {
   chomp;
   s/#.*$//;
   next unless /^\s*(\S+)\s*;\s*(\S+)\s*;\s*(\S+(?: \S+)*)\s*;\s*$/;
   my $type = $2;
   if ($type eq 'C' || $type eq 'F') {
      my $char = hex $1;
      my $mapping = [map {hex} split(/ /, $3)];
      $mapping->[0] -= $char;
      $data[$char]{FOLD} = $mapping;
   }
}
close FOLD;

open TABLES, ">char-tables.c";
print TABLES <<END;
/* This file has been generated automatically by make-char-tables. */

#define SBase 0xAC00
#define LBase 0x1100
#define VBase 0x1161
#define TBase 0x11A7
#define LCount 19
#define VCount 21
#define TCount 28
#define NCount (VCount * TCount)
#define SCount (LCount * NCount)

enum ko_cat_block_kind {
   KO_ONE_CAT, KO_TWO_CATS, KO_MORE_CATS
};

struct ko_two_cats {
   enum ko_char_cat cat1, cat2;
   ko_uint8_t bits[256 / 8];
};

struct ko_cat_block {
   enum ko_cat_block_kind kind;
   const void *data;
};

struct ko_decomp_k_block {
   ko_nint_t max_len;
   const ko_uint16_t *data;
};

enum ko_gcb_block_kind {
   KO_OTHER_GCB, KO_TWO_GCBS, KO_MORE_GCBS
};

struct ko_gcb_block {
   enum ko_gcb_block_kind kind;
   const ko_uint8_t *data;
};

enum ko_wb_block_kind {
   KO_ONE_WB, KO_TWO_WBS, KO_MORE_WBS
};

struct ko_wb_block {
   enum ko_wb_block_kind kind;
   const ko_uint8_t *data;
};

enum ko_case_block_kind {
   KO_CASE_0, KO_CASE_8, KO_CASE_16, KO_CASE_32
};

struct ko_case_block {
   enum ko_case_block_kind kind;
   const void *data;
};

enum ko_fold_block_kind {
   KO_NO_FOLD, KO_FOLD_1, KO_FOLD_2, KO_FOLD_3
};

struct ko_fold_block {
   enum ko_fold_block_kind kind;
   const ko_uint16_t *data;
};
END

sub make_table($$$&) {
   my ($chars, $column, $header, $process_block) = @_;
   my @blocks = ();
   foreach my $block (0..($chars >> 8)-1) {
      my $start = $block * 256;
      my @block = map {$data[$_]{$column}} ($start..$start + 255);
      push @blocks,
         $process_block->(sprintf("%02X00", $block), $start, \@block);
   }

   printf TABLES "\n$header\[0x%04X / 256] = {\n", $chars;
   foreach my $block (@blocks) {
      print TABLES "   $block,\n";
   }
   print TABLES "};\n";
}

sub bitmap(&$) {
   my ($set, $block) = @_;
   my @bytes = ();
   foreach my $i (0..(@$block >> 3) - 1) {
      my $byte = 0;
      foreach my $j (0..7) {
         $_ = $block->[$i * 8 + $j];
         $byte |= 1 << $j if $set->();
      }
      push @bytes, $byte;
   }
   return \@bytes;
}

sub draw_table(&$$$) {
   my ($show_elem, $block, $indent, $width) = @_;
   my $indent_str = ' ' x $indent;
   foreach my $i (0..$#$block) {
      print TABLES $indent_str if $i % $width == 0;
      $_ = $block->[$i];
      print TABLES $show_elem->(), ",";
      print TABLES "\n" if ($i + 1) % $width == 0;
   }
}

sub intervals_after_table(&$$) {
   my ($show_elem, $from_char, $column) = @_;
   my $old = $data[$from_char]{$column};
   foreach my $char ($from_char + 1..0x10FFFF) {
      my $new = $data[$char]{$column};
      if ($new ne $old) {
         $_ = $old;
         printf TABLES "   if (ch < 0x%04X) return %s;\n",
            $char, $show_elem->();
         $old = $new;
      }
   }
   $_ = $old;
   printf TABLES "   return %s;\n", $show_elem->();
   print TABLES "}\n";
}

# Categories

print TABLES "\n";
make_table 0x20000, 'CAT',
   "static const struct ko_cat_block ko_cat_table", sub {
   my ($id, $start, $block) = @_;
   my $cat1 = $block->[0];
   my $cat2 = undef;
   my $two = 1;
   foreach my $cat (@$block) {
      if ($cat ne $cat1) {
         if (defined $cat2) {
            if ($cat ne $cat2) {$two = 0; last}
         }
         else {$cat2 = $cat}
      }
   }
   if (!defined $cat2) {
      return "{KO_ONE_CAT, (const void *)KO_CAT_$cat1}";
   }
   elsif ($two) {
      my $name = "ko_cat_data_$id";
      print TABLES "static const struct ko_two_cats $name = {\n";
      print TABLES "   KO_CAT_$cat1, KO_CAT_$cat2, {\n";
      my $bytes = bitmap {$_ ne $cat1} $block;
      draw_table {sprintf "0x%02X", $_} $bytes, 6, 8;
      print TABLES "   }\n";
      print TABLES "};\n";
      return "{KO_TWO_CATS, &$name}";
   }
   else {
      my $name = "ko_cat_data_$id";
      my $storage = $id eq '0000' ? "" : "static ";
      print TABLES "${storage}const ko_uint8_t $name\[256] = {\n";
      draw_table {"KO_CAT_$_"} $block, 3, 4;
      print TABLES "};\n";
      return "{KO_MORE_CATS, $name}";
   }
};

print TABLES <<END;

enum ko_char_cat
ko_cat_of_wchar(ko_wchar_t ch) {
   if (KO_LIKELY(ch < 0x20000)) {
      const struct ko_cat_block *block = &ko_cat_table[ch >> 8];
      switch (block->kind) {
         case KO_ONE_CAT:
            return (enum ko_char_cat)block->data;
         case KO_TWO_CATS:
            return (((const struct ko_two_cats *)block->data)
               ->bits[(ch & 255) >> 3] & (1 << (ch & 7))) == 0 ?
               ((const struct ko_two_cats *)block->data)->cat1 :
               ((const struct ko_two_cats *)block->data)->cat2;
         case KO_MORE_CATS:
            return ((const ko_uint8_t *)block->data)[ch & 255];
         default:
            ko_impossible_error("ko_cat_of_wchar");
      }
   }
END
intervals_after_table {"KO_CAT_$_"} 0x20000, 'CAT';

# Canonical combining classes

print TABLES "\nstatic const ko_uint8_t ko_cc_data_default[256] = {\n";
draw_table {$_} [(0) x 256], 3, 16;
print TABLES "};\n";

make_table 0x10000, 'CC',
   "static const ko_uint8_t *ko_cc_table", sub {
   my ($id, $start, $block) = @_;
   if (!grep {$_ != 0} @$block) {
      return "ko_cc_data_default";
   }
   else {
      my $name = "ko_cc_data_$id";
      print TABLES "static const ko_uint8_t $name\[256] = {\n";
      draw_table {$_} $block, 3, 16;
      print TABLES "};\n";
      return $name;
   }
};

print TABLES <<END;

int
ko_cc_of_wchar(ko_wchar_t ch) {
   if (KO_LIKELY(ch < 0x10000))
      return ko_cc_table[ch >> 8][ch & 255];
END
intervals_after_table {$_} 0x10000, 'CC';

# Canonical decomposition

print TABLES "\nstatic const ko_uint8_t ko_size_data_default[256] = {\n";
draw_table {$_} [("1") x 256], 3, 32;
print TABLES "};\n";

make_table 0x10000, 'NFD_SIZE',
   "static const ko_uint8_t *const ko_nfd_size_table", sub {
   my ($id, $start, $block) = @_;
   if (!grep {$_ != 1} @$block) {
      return "ko_size_data_default";
   }
   else {
      my $name = "ko_nfd_size_data_$id";
      my $storage = $id eq '0000' ? "" : "static ";
      print TABLES "${storage}const ko_uint8_t $name\[256] = {\n";
      draw_table {$_} $block, 3, 32;
      print TABLES "};\n";
      return $name;
   }
};
print TABLES <<END;

ko_nint_t
ko_nfd_size_of_wchar(ko_wchar_t ch) {
   if (KO_LIKELY(ch < 0x10000)) {
      if (KO_UNLIKELY(ch >= SBase && ch < SBase + SCount))
         return (ch - SBase) % TCount == 0 ? 2 : 3;
      return ko_nfd_size_table[ch >> 8][ch & 255];
   }
END
intervals_after_table {$_} 0x10000, 'NFD_SIZE';

print TABLES "\nstatic const ko_uint16_t ko_decomp_data_default[256 * 2] = {\n";
draw_table {$_} [("0,0") x 256], 3, 16;
print TABLES "};\n";

my @decomp_exceptions = ();

sub print_decomp($$) {
   my ($comp, $decomp) = @_;
   if ($decomp <= 0xFFFF) {
      return sprintf("0x%04X", $decomp);
   }
   else {
      push @decomp_exceptions, [$comp, $decomp];
      return "0xFFFF";
   }
}

make_table 0x10000, 'DECOMP',
   "static const ko_uint16_t *const ko_decomp_table", sub {
   my ($id, $start, $block) = @_;
   my $max_len = 0;
   foreach my $mapping (@$block) {
      my $len = @$mapping;
      if ($len > $max_len) {
         $max_len = $len;
      }
   }
   if ($max_len == 0) {
      return "ko_decomp_data_default";
   }
   else {
      my $name = "ko_decomp_data_$id";
      my $storage = $id eq '0000' ? "" : "static ";
      print TABLES "${storage}const ko_uint16_t $name\[256 * 2] = {\n";
      my $comp = $start;
      draw_table {
         my $decomp =
            @$_ == 0 ? "0,0" :
            @$_ == 1 ? print_decomp($comp, $_->[0]) . ",0" :
            sprintf("0x%04X,0x%04X", $_->[0], $_->[1]);
         ++$comp;
         $decomp
      } $block, 3, 8;
      print TABLES "};\n";
      return $name;
   }
};

print TABLES "\nstatic const ko_wchar_t ko_decomp_data_2F800[542] = {\n";
my @interval = map {$data[$_]{DECOMP}} (0x2F800..0x2FA1D);
draw_table {sprintf("0x%04X", $_->[0])} \@interval, 3, 8;
print TABLES "\n};\n";

print TABLES <<END;

ko_nint_t
ko_decomp_of_wchar(ko_wchar_t ch, ko_wchar_t *dest) {
   if (KO_LIKELY(ch < 0x10000)) {
      const ko_uint16_t *ptr = ko_decomp_table[ch >> 8] + (ch & 255) * 2;
      ko_uint16_t ch0 = ptr[0];
      ko_uint16_t ch1;
      if (KO_LIKELY(ch0 == 0)) return 0;
      dest[0] = ch0;
      ch1 = ptr[1];
      if (KO_UNLIKELY(ch1 == 0)) {
         if (KO_UNLIKELY(ch0 == 0xFFFF)) {
            switch (ch) {
END
foreach my $exception (@decomp_exceptions) {
   printf TABLES "               case 0x%04X: dest[0] = 0x%04X; break;\n",
      $exception->[0], $exception->[1];
}
print TABLES <<END;
            }
         }
         return 1;
      }
      dest[1] = ch1;
      return 2;
   }
   if (ch >= 0x2F800 && ch <= 0x2FA1D) {
      dest[0] = ko_decomp_data_2F800[ch - 0x2F800];
      return 1;
   }
   switch (ch) {
END
foreach my $char (0x10000..0x2F7FF, 0x2FA1E..0x10FFFF) {
   my $decomp = $data[$char]{DECOMP};
   if (@$decomp) {
      if (@$decomp == 1) {
         printf TABLES
           "      case 0x%04X: dest[0] = 0x%04X; return 1;\n",
           $char, $decomp->[0];
      }
      else {
         printf TABLES
           "      case 0x%04X: dest[0] = 0x%04X; dest[1] = 0x%04X; return 2;\n",
           $char, $decomp->[0], $decomp->[1];
      }
   }
}
print TABLES "      default: return 0;\n";
print TABLES "   }\n";
print TABLES "}\n";

# Compatibility decomposition

print TABLES "\n";
make_table 0x10000, 'NFKD_SIZE',
   "static const ko_uint8_t *const ko_nfkd_size_table", sub {
   my ($id, $start, $block) = @_;
   if (!grep {$_ != 1} @$block) {
      return "ko_size_data_default";
   }
   else {
      my $name = "ko_nfkd_size_data_$id";
      my $storage = $id eq '0000' ? "" : "static ";
      print TABLES "${storage}const ko_uint8_t $name\[256] = {\n";
      draw_table {$_} $block, 3, 32;
      print TABLES "};\n";
      return $name;
   }
};
print TABLES <<END;

ko_nint_t
ko_nfkd_size_of_wchar(ko_wchar_t ch) {
   if (KO_LIKELY(ch < 0x10000)) {
      if (KO_UNLIKELY(ch >= SBase && ch < SBase + SCount))
         return (ch - SBase) % TCount == 0 ? 2 : 3;
      return ko_nfkd_size_table[ch >> 8][ch & 255];
   }
END
intervals_after_table {$_} 0x10000, 'NFKD_SIZE';

print TABLES "\n";
make_table 0x10000, 'DECOMP_K',
   "static const struct ko_decomp_k_block ko_decomp_k_table", sub {
   my ($id, $start, $block) = @_;
   my $max_len = 0;
   foreach my $mapping (@$block) {
      my $len = @$mapping;
      if ($len > $max_len) {
         $max_len = $len;
      }
   }
   if ($max_len == 0) {
      return "{0, NULL}";
   }
   else {
      my $name = "ko_decomp_k_data_$id";
      my $storage = $id eq '0000' ? "" : "static ";
      print TABLES "${storage}const ko_uint16_t $name\[256 * $max_len] = {\n";
      my $width = 1;
      $width *= 2 while 2 * $width * 7 * $max_len < 77;
      draw_table {
         join(",", (map {sprintf("0x%04X", $_)} @$_), ("0") x ($max_len - @$_))
      } $block, 3, $width;
      print TABLES "};\n";
      return "{$max_len, $name}";
   }
};

print TABLES "\nstatic const ko_uint16_t ko_decomp_k_data_1D400[1024] = {\n";
my @interval = map {$data[$_]{DECOMP_K}} (0x1D400..0x1D7FF);
draw_table {@$_ == 0 ? "0" : sprintf("0x%04X", $_->[0])} \@interval, 3, 8;
print TABLES "};\n";

print TABLES <<END;

ko_nint_t
ko_decomp_k_of_wchar(ko_wchar_t ch, ko_wchar_t *dest) {
   if (KO_LIKELY(ch < 0x10000)) {
      const struct ko_decomp_k_block *block = &ko_decomp_k_table[ch >> 8];
      ko_nint_t max_len = block->max_len;
      const ko_uint16_t *ptr;
      ko_nint_t i;
      if (max_len == 0) return 0;
      ptr = block->data + (ch & 255) * max_len;
      for (i = 0; i < max_len; ++i) {
         ko_uint16_t ch = *ptr;
         if (ch == 0) return i;
         *dest = ch;
         ++ptr; ++dest;
      }
      return max_len;
   }
   if (ch >= 0x1D400 && ch <= 0x1D7FF) {
      ko_uint16_t ch0 = ko_decomp_k_data_1D400[ch - 0x1D400];
      if (ch0 == 0) return 0;
      dest[0] = ch0;
      return 1;
   }
   return 0;
}
END

foreach my $char (0x10000..0x1D3FF, 0x1D800..0x10FFFF) {
   if (@{$data[$char]{DECOMP_K}}) {
      die "DECOMP_K is present for $char";
   }
}

# Composition

print TABLES <<END;

static ko_bool_t
ko_comp_is_excluded(ko_wchar_t ch) {
   if (KO_LIKELY(ch < 0x100)) return ko_false;
   switch (ch) {
END
for my $char (0..0xFFFF) {
   if ($data[$char]{COMP_EXCL}) {
      printf TABLES "      case 0x%04X:\n", $char;
   }
}
print TABLES <<END;
         return ko_true;
      default:
         return ko_false;
   }
}
END

print TABLES "\nstatic const ko_int16_t ko_comp_key_data_default[256] = {\n";
draw_table {$_} [(-1) x 256], 3, 16;
print TABLES "};\n";

make_table 0x3100, 'COMP_KEY',
   "const ko_int16_t *const ko_comp_key_table", sub {
   my ($id, $start, $block) = @_;
   if (!grep {$_ != -1} @$block) {
      return "ko_comp_key_data_default";
   }
   else {
      my $name = "ko_comp_key_data_$id";
      print TABLES "static const ko_int16_t $name\[256] = {\n";
      draw_table {$_} $block, 3, 16;
      print TABLES "};\n";
      return $name;
   }
};

print TABLES <<END;

ko_uint16_t ko_comp_table[$comp_keys][$comp_keys];
static ko_bool_t ko_comp_table_initialized = ko_false;

void
ko_init_comp_table(void) {
   ko_wchar_t ch;
   if (KO_LIKELY(ko_comp_table_initialized)) return;
   for (ch = 0; ch <= 0xFFFF; ++ch) {
      ko_wchar_t decomp[2];
      ko_nint_t len = ko_decomp_of_wchar(ch, decomp);
      if (len == 2 && !ko_comp_is_excluded(ch)) {
         ko_int16_t key1 = ko_comp_key(decomp[0]);
         ko_int16_t key2 = ko_comp_key(decomp[1]);
         ko_comp_table[key1][key2] = (ko_uint16_t)ch;
      }
   }
   ko_comp_table_initialized = ko_true;
}
END

# Grapheme cluster boundaries

print TABLES "\n";
make_table 0x20000, 'GCB',
   "static const struct ko_gcb_block ko_gcb_table", sub {
   my ($id, $start, $block) = @_;
   my $one = 1;
   my $two = 1;
   foreach my $gcb (@$block) {
      if ($gcb ne 'Other') {
         $one = 0;
         if ($gcb ne 'Extend') {$two = 0; last}
      }
   }
   if ($one) {
      return "{KO_OTHER_GCB, NULL}";
   }
   elsif ($two) {
      my $name = "ko_gcb_data_$id";
      print TABLES "static const ko_uint8_t $name\[256 / 8] = {\n";
      my $bytes = bitmap {$_ eq 'Extend'} $block;
      draw_table {sprintf "0x%02X", $_} $bytes, 3, 8;
      print TABLES "};\n";
      return "{KO_TWO_GCBS, $name}";
   }
   else {
      my $name = "ko_gcb_data_$id";
      print TABLES "static const ko_uint8_t $name\[256] = {\n";
      draw_table {"KO_GCB_$_"} $block, 3, 4;
      print TABLES "};\n";
      return "{KO_MORE_GCBS, $name}";
   }
};

print TABLES <<END;

enum ko_gcb_type
ko_gcb_type_of_wchar(ko_wchar_t ch) {
   if (KO_LIKELY(ch < 0x20000)) {
      const struct ko_gcb_block *block;
      if (KO_UNLIKELY(ch >= SBase && ch < SBase + SCount)) {
         return (ch - SBase) % TCount == 0 ? KO_GCB_LV : KO_GCB_LVT;
      }
      block = &ko_gcb_table[ch >> 8];
      switch (block->kind) {
         case KO_OTHER_GCB:
            return KO_GCB_Other;
         case KO_TWO_GCBS:
            return (block->data[(ch & 255) >> 3] & (1 << (ch & 7))) == 0 ?
               KO_GCB_Other :
               KO_GCB_Extend;
         case KO_MORE_GCBS:
            return block->data[ch & 255];
         default:
            ko_impossible_error("ko_gcb_type_of_wchar");
      }
   }
END
intervals_after_table {"KO_GCB_$_"} 0x20000, 'GCB';

# Word boundaries

print TABLES "\n";
make_table 0x20000, 'WB',
   "static const struct ko_wb_block ko_wb_table", sub {
   my ($id, $start, $block) = @_;
   my $wb1 = $block->[0];
   my $one = 1;
   my $two = 1;
   foreach my $wb (@$block) {
      if ($wb ne $wb1) {$one = 0}
      if ($wb ne 'Other' && $wb ne 'ALetter') {$two = 0}
   }
   if ($one) {
      return "{KO_ONE_WB, (const ko_uint8_t *)KO_WB_$wb1}";
   }
   elsif ($two) {
      my $name = "ko_wb_data_$id";
      print TABLES "static const ko_uint8_t $name\[256 / 8] = {\n";
      my $bytes = bitmap {$_ eq 'ALetter'} $block;
      draw_table {sprintf "0x%02X", $_} $bytes, 3, 8;
      print TABLES "};\n";
      return "{KO_TWO_WBS, $name}";
   }
   else {
      my $name = "ko_wb_data_$id";
      my $storage = $id eq '0000' ? "" : "static ";
      print TABLES "${storage}const ko_uint8_t $name\[256] = {\n";
      draw_table {"KO_WB_$_"} $block, 3, 4;
      print TABLES "};\n";
      return "{KO_MORE_WBS, $name}";
   }
};

print TABLES <<END;

enum ko_wb_type
ko_wb_type_of_wchar(ko_wchar_t ch) {
   if (KO_LIKELY(ch < 0x20000)) {
      const struct ko_wb_block *block = &ko_wb_table[ch >> 8];
      switch (block->kind) {
         case KO_ONE_WB:
            return (enum ko_wb_type)block->data;
         case KO_TWO_WBS:
            return (block->data[(ch & 255) >> 3] & (1 << (ch & 7))) == 0 ?
               KO_WB_Other :
               KO_WB_ALetter;
         case KO_MORE_WBS:
            return block->data[ch & 255];
         default:
            ko_impossible_error("ko_wb_type_of_wchar");
      }
   }
END
intervals_after_table {"KO_WB_$_"} 0x20000, 'WB';

# Cased characters

print TABLES "\nstatic const ko_uint8_t ko_cased_data_default[256 / 8] = {\n";
draw_table {$_} [(0) x (256 >> 3)], 3, 16;
print TABLES "};\n";

make_table 0x20000, 'CASED',
   "const ko_uint8_t *const ko_cased_table", sub {
   my ($id, $start, $block) = @_;
   if (!grep {$_} @$block) {
      return "ko_cased_data_default";
   }
   else {
      my $name = "ko_cased_data_$id";
      print TABLES "static const ko_uint8_t $name\[256 / 8] = {\n";
      my $bytes = bitmap {$_} $block;
      draw_table {sprintf "0x%02X", $_} $bytes, 3, 8;
      print TABLES "};\n";
      return $name;
   }
};

# Soft-dotted characters

print TABLES <<END;

ko_bool_t
ko_is_soft_dotted(ko_wchar_t ch) {
   switch (ch) {
END
for my $char (0..0x10FFFF) {
   if ($data[$char]{SOFT_DOTTED}) {
      printf TABLES "      case 0x%04X:\n", $char;
   }
}
print TABLES <<END;
         return ko_true;
      default:
         return ko_false;
   }
}
END

# Upper case and lower case

foreach my $case ('UPPER', 'LOWER') {
   my $case_str = lc $case;
   print TABLES "\n";
   make_table 0x20000, $case,
      "static const struct ko_case_block ko_${case_str}_case_table", sub {
      my ($id, $start, $block) = @_;
      my $bits0 = 1;
      my $bits8 = 1;
      my $bits16 = 1;
      foreach my $diff (@$block) {
         if ($diff != 0) {
            $bits0 = 0;
            if ($diff < -0x80 || $diff > 0x7F) {$bits8 = 0}
            if ($diff < -0x8000 || $diff > 0x7FFF) {$bits16 = 0}
         }
      }
      if ($bits0) {
         return "{KO_CASE_0, NULL}";
      }
      else {
         my $type = $bits8 ? "ko_int8_t" :
            $bits16 ? "ko_int16_t" : "ko_int32_t";
         my $tag = $bits8 ? "KO_CASE_8" :
            $bits16 ? "KO_CASE_16" : "KO_CASE_32";
         my $name = "ko_${case_str}_case_data_$id";
         print TABLES "static const $type $name\[256] = {\n";
         draw_table {$_} $block, 3, 16;
         print TABLES "};\n";
         return "{$tag, $name}";
      }
   };

   print TABLES <<END;

ko_wchar_t
ko_basic_${case_str}_case_of_wchar(ko_wchar_t ch) {
   if (KO_LIKELY(ch < 0x20000)) {
      const struct ko_case_block *block = &ko_${case_str}_case_table[ch >> 8];
      switch (block->kind) {
         case KO_CASE_0:
            return ch;
         case KO_CASE_8:
            return (ko_wchar_t)((ko_int32_t)ch
               + ((const ko_int8_t *)block->data)[ch & 255]);
         case KO_CASE_16:
            return (ko_wchar_t)((ko_int32_t)ch
               + ((const ko_int16_t *)block->data)[ch & 255]);
         case KO_CASE_32:
            return (ko_wchar_t)((ko_int32_t)ch
               + ((const ko_int32_t *)block->data)[ch & 255]);
         default:
            ko_impossible_error("ko_basic_${case_str}_case_of_wchar");
      }
   }
   return ch;
}
END

   foreach my $char (0x20000..0x10FFFF) {
      if ($data[$char]{$case}) {
         die "$case is present for $char";
      }
   }
}

# Title case

print TABLES <<END;

ko_wchar_t
ko_basic_title_case_of_wchar(ko_wchar_t ch) {
   switch (ch) {
END
for my $char (0..0x10FFFF) {
   if ($data[$char]{TITLE} == 1) {
      printf TABLES "      case 0x%04X:\n", $char;
   }
}
print TABLES <<END;
         return ko_basic_upper_case_of_wchar(ch) + 1;
      default:
         return ko_basic_upper_case_of_wchar(ch);
   }
}
END

print TABLES <<END;

ko_nint_t
ko_estimate_fold_case_size_of_wstring(const ko_wchar_t *str, ko_nint_t size) {
   ko_nint_t destSize = 0;
   ko_nint_t index;
   for (index = 0; index < size; ++index) {
      switch (str[index]) {
END
foreach my $char (0..0x10FFFF) {
   if (@{$data[$char]{FOLD}} == 3) {
      printf TABLES "         case 0x%04X:\n", $char;
   }
}
print TABLES "            destSize += 3;\n";
print TABLES "            break;\n";
foreach my $char (0..0x10FFFF) {
   if (@{$data[$char]{FOLD}} == 2) {
      printf TABLES "         case 0x%04X:\n", $char;
   }
}
print TABLES <<END;
            destSize += 2;
            break;
         default:
            ++destSize;
      }
   }
   return destSize;
}
END

# Case folding

sub wrap_short($) {
   my ($value) = @_;
   return $value >= 0 ? $value : $value + 65536;
}

print TABLES "\n";
make_table 0x10000, 'FOLD',
   "static const struct ko_fold_block ko_fold_table", sub {
   my ($id, $start, $block) = @_;
   my $max_len = 0;
   foreach my $mapping (@$block) {
      my $len = @$mapping;
      if ($len > $max_len) {
         $max_len = $len;
      }
   }
   if ($max_len == 0) {
      return "{KO_NO_FOLD, NULL}";
   }
   elsif ($max_len == 1) {
      my $name = "ko_fold_data_$id";
      print TABLES "static const ko_uint16_t $name\[256] = {\n";
      draw_table {@$_ == 0 ? "0" : wrap_short($_->[0])} $block, 3, 16;
      print TABLES "};\n";
      return "{KO_FOLD_1, $name}";
   }
   elsif ($max_len == 2) {
      my $name = "ko_fold_data_$id";
      print TABLES "static const ko_uint16_t $name\[256 * 2] = {\n";
      draw_table {
         @$_ == 0 ? "0,0" :
         @$_ == 1 ? wrap_short($_->[0]) . ",0" :
         wrap_short($_->[0]) . ",$_->[1]"
      } $block, 3, 8;
      print TABLES "};\n";
      return "{KO_FOLD_2, $name}";
   }
   else {
      my $name = "ko_fold_data_$id";
      print TABLES "static const ko_uint16_t $name\[256 * 3] = {\n";
      draw_table {
         @$_ == 0 ? "0,0,0" :
         @$_ == 1 ? sprintf("%d,0,0", wrap_short($_->[0])) :
         @$_ == 2 ? sprintf("%d,0x%04X,0", wrap_short($_->[0]), $_->[1]) :
         sprintf("%d,0x%04X,0x%04X", wrap_short($_->[0]), $_->[1], $_->[2])
      } $block, 3, 4;
      print TABLES "};\n";
      return "{KO_FOLD_3, $name}";
   }
};

print TABLES <<END;

ko_nint_t
ko_fold_case_of_wchar(ko_wchar_t ch, ko_wchar_t *dest, ko_bool_t turk) {
   if (KO_LIKELY(ch < 0x10000)) {
      const struct ko_fold_block *block;
      if (KO_UNLIKELY(turk)) {
         if (KO_UNLIKELY(ch == 'I')) {dest[0] = 0x0131; return 1;}
         if (KO_UNLIKELY(ch == 0x0130)) {dest[0] = 'i'; return 1;}
      }
      block = &ko_fold_table[ch >> 8];
      switch (block->kind) {
         case KO_NO_FOLD:
            dest[0] = ch;
            return 1;
         case KO_FOLD_1:
            dest[0] = (ko_uint16_t)(ch + block->data[ch & 255]);
            return 1;
         case KO_FOLD_2: {
            int i = (ch & 255) * 2;
            ko_uint16_t ch1;
            dest[0] = (ko_uint16_t)(ch + block->data[i]);
            ch1 = block->data[i + 1];
            if (ch1 == 0) return 1;
            dest[1] = ch1;
            return 2;
         }
         case KO_FOLD_3: {
            int i = (ch & 255) * 3;
            ko_uint16_t ch1, ch2;
            dest[0] = (ko_uint16_t)(ch + block->data[i]);
            ch1 = block->data[i + 1];
            if (ch1 == 0) return 1;
            dest[1] = ch1;
            ch2 = block->data[i + 2];
            if (ch2 == 0) return 2;
            dest[2] = ch2;
            return 3;
         }
         default:
            ko_impossible_error("ko_fold_case_of_wchar");
      }
   }
END
my $old = $data[0x10000]{FOLD};
$old = @$old == 0 ? 0 : $old->[0];
foreach my $char (0x10001..0x10FFFF) {
   my $new = $data[$char]{FOLD};
   $new = @$new == 0 ? 0 : $new->[0];
   if ($new ne $old) {
      $_ = $old;
      printf TABLES "   if (ch < 0x%04X) {\n", $char;
      printf TABLES "      dest[0] = ch%s;\n", $old == 0 ? "" : " + $old";
      print TABLES "      return 1;\n";
      print TABLES "   }\n";
      $old = $new;
   }
}
printf TABLES "   dest[0] = ch%s;\n",
   $old == 0 ? "" : $old > 0 ? " + $old" : " - " . -$old;
print TABLES "   return 1;\n";
print TABLES "}\n";

# Digit values

print TABLES "\nstatic const ko_int8_t ko_digit_data_default[256] = {\n";
draw_table {$_} [(-1) x 256], 3, 16;
print TABLES "};\n";

make_table 0x10000, 'DIGIT',
   "static const ko_int8_t *const ko_digit_table", sub {
   my ($id, $start, $block) = @_;
   if (!grep {$_ != -1} @$block) {
      return "ko_digit_data_default";
   }
   else {
      my $name = "ko_digit_data_$id";
      print TABLES "static const ko_int8_t $name\[256] = {\n";
      draw_table {$_} $block, 3, 16;
      print TABLES "};\n";
      return $name;
   }
};

print TABLES <<END;

ko_nint_t
ko_digit_value(ko_wchar_t ch) {
   if (KO_LIKELY(ch < 0x10000))
      return ko_digit_table[ch >> 8][ch & 255];
END
foreach my $char (0x10000..0x10FFFF) {
   if ($data[$char]{DIGIT} == 0) {
      printf TABLES
        "   if (ch >= 0x%04X && ch <= 0x%04X) return (ko_nint_t)(ch - 0x%04X);\n",
        $char, $char + 9, $char;
   }
}
print TABLES "   return -1;\n";
print TABLES "}\n";

close TABLES;
