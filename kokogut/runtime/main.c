/*
 * This file is a part of the Kokogut library.
 *
 * Kokogut is a compiler of the Kogut programming language.
 * Copyright (C) 2004-2007 by Marcin 'Qrczak' Kowalczyk
 * (QrczakMK@gmail.com)
 *
 * The Kokogut library is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version;
 * with a "linking exception".
 *
 * The linking exception allows you to link a "work that uses the
 * Library" with a publicly distributed version of the Library to
 * produce an executable file containing portions of the Library,
 * and distribute that executable file under terms of your choice,
 * without any of the additional requirements listed in section 6
 * of LGPL version 2 or section 4 of LGPL version 3.
 *
 * Kokogut is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <kokogut/runtime.h>
#include <locale.h>
#include <signal.h>

void ko_initialize(char **argv);
int ko_run_module(ko_code_t code);
KO_CODE(Ko_Main_init);
void ko_shutdown(void);

int
main(int argc, char **argv) {
   int status;
   (void)argc;
   setlocale(LC_CTYPE, "");
   #ifdef LC_MESSAGES
      setlocale(LC_MESSAGES, "");
   #endif
   ko_initialize(argv);
   status = ko_run_module(Ko_Main_init);
   ko_shutdown();
   if (status >= 0)
      return status;
   else {
      raise(-status);
      return EXIT_FAILURE;
   }
}
