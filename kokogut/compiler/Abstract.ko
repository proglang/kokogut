// This file is a part of Kokogut.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// Kokogut is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either v1ersion 2
// of the License, or (at your option) any later version.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Abstract;

use KogutUtils;
use Names;

type A_MEANING {
   type AM_CONSTANT {
      type (private AM_CONSTANT_PROPER) AMConstant name;
      type AM_MODULE AMModule name exported {
         let dict = HashDict exported;
      };
   };
   type AM_VARIABLE AMVariable name;
   type AM_DEFINED  AMDefined name;
   type AM_CCON     AMCCon name;
};

let AMError = AMVariable ErrorName;

type A_CASE     ACase loc params def expr;
type A_GENERIC  AGeneric loc params arity keys;
type A_CON_CASE AConCase loc params thisName def extended exported;

type A_EXPR {
   type AE_ERROR        AEError;
   type AE_NAME         AEName loc name;
   type AE_MODULE       AEModule loc name exported dict;
   type AE_CCON         AECCon loc name;
   type AE_LIT          AELit loc value;
   type AE_LIST         AEList elems;
   type AE_FUN          AEFun loc thisName cases;
   type AE_MANY         AEMany loc arg;
   type AE_APPLY        AEApply loc fun args;
   type AE_IF           AEIf loc cond then else;
   type AE_END_IF       AEEndIf loc;
   type AE_CASE         AECase loc subjs cases;
   type AE_METHOD       AEMethod loc fun arity keys types superName method;
   type AE_MAKE_FINAL   AEMakeFinal loc type final;
   type AE_TRY          AETry loc body cases normal;
   type AE_HANDLED      AEHandled;
   type AE_HANDLE       AEHandle loc super cases body;
   type AE_RECEIVE      AEReceive loc name cases;
   type AE_RECEIVE_TIME AEReceiveTime loc name cases time timeBody;
   type AE_LOCAL        AELocal loc variable value body;
   type AE_STRUCT       AEStruct loc thisName def extended exported;
   type AE_STRUCT_PART  AEStructPart loc extended exported;
   type AE_USE          AEUse loc module;
   type AE_NOT          AENot loc arg;
   type AE_AND          AEAnd loc arg1 arg2;
   type AE_OR           AEOr loc arg1 arg2;
   type AE_PAIR         AEPair left right;
   type AE_DEF          AEDef def expr;
   type AE_CEXPR        AECExpr loc callback input code;
   type AE_CCOND        AECCond loc callback input code;
   type AE_CEVAL        AECEval loc callback result input aux code;
   type AE_CSTAT        AECStat loc callback input aux code;
   type AE_CCASE        AECCase loc callback output input aux code cases;
   type AE_DESCR_NAME   AEDescrName loc con case;
   type AE_ENTRY_NAME   AEEntryName obj arity;
};

type A_PAT {
   type AP_ERROR APError;
   type AP_NAME  APName loc name;
   type AP_ANY   APAny;
   type AP_LIST  APList elems;
   type AP_MANY  APMany pat;
   type AP_IF    APIf loc cond;
   type AP_MATCH APMatch subj pat;
   type AP_DEF   APDef def;
   type AP_NOT   APNot pat;
   type AP_AND   APAnd pat1 pat2;
   type AP_OR    APOr loc cases common;
   type AP_PAIR  APPair left right;
};

type A_DEF {
   type AD_EMPTY        ADEmpty;
   type AD_SEQ          ADSeq before after;
   type AD_EXPR         ADExpr loc expr;
   type AD_LET          ADLet loc pat expr;
   type AD_FUN          ADFun loc name cases;
   type AD_VAR          ADVar loc name value;
   type AD_DYNAMIC      ADDynamic loc name value;
   type AD_LAZY         ADLazy loc name value;
   type AD_FORWARD      ADForward loc name;
   type AD_TYPE         ADType loc name supertypes final;
   type AD_SINGLETON    ADSingleton loc name type;
   type AD_RECORD       ADRecord loc name type fields;
   type AD_RECORD_NEW   ADRecordNew loc name type fields;
   type AD_OBJECT       ADObject loc name type thisName def extended exported;
   type AD_STRUCT       ADStruct loc name type cases;
   type AD_CCON         ADCCon loc name type locMove move locFinalize finalize
      locSave save thisName cases;
   type AD_USE          ADUse loc module imported shouldBeUsed;
   type AD_CLET         ADCLet loc callback output input aux code;
   type AD_CDEFINE      ADCDefine loc input code;
   type AD_CDEFINE_NAME ADCDefineName loc name value;
   type AD_CEXPORT      ADCExport loc code key;
   type AD_CEXPORT_NAME ADCExportName loc name value module;
   type AD_CINCLUDE     ADCInclude file module;
};

type A_MAIN_MODULE AMainModule moduleName loc def;
type A_MODULE      AModule moduleName moduleNameLoc loc def name exported
   exportedC;
type A_INTERFACE   AInterface moduleName name exported exportedC;

def IsMainModuleA moduleA! {};
method IsMainModuleA _^A_MAIN_MODULE {True};
method IsMainModuleA _^A_MODULE      {False};

def ADThen defA1 defA2 {
   if [
      (defA1 %Is ADEmpty) {defA2}
      (defA2 %Is ADEmpty) {defA1}
      _                   {ADSeq defA1 defA2}
   ]
};

def ADSequence defAs {Fold defAs ADEmpty ADThen};

def AllowedArities caseAs {
   var allowed = 0;
   var fast = 0;
   Each caseAs ?caseA {
      let paramAs = caseA.params;
      if (Any paramAs (->HasType AP_MANY)) {
         allowed = allowed %BitOr (-1)->BitShift (Size paramAs - 1)
      } {
         let arity = Size paramAs;
         if (allowed->~TestBit arity) {fast = fast->SetBit arity};
         allowed = allowed->SetBit arity
      }
   };
   allowed, fast %BitAnd FastArityMask
};

def SimpleFunInfo name arity {
   let allowed = 1->BitShift arity;
   let fast = allowed %BitAnd FastArityMask;
   SetFunInfo name (IFunction name.symbol allowed fast Null)
};

private def ComputeFunInfo name symbol caseAs {
   let (allowed, fast) = AllowedArities caseAs;
   SetFunInfo name (IFunction symbol allowed fast Null)
};

private def SetPatInfo patA! info {};

method SetPatInfo patA^AP_NAME info {
   patA.name.info = info
};
method SetPatInfo patA^AP_NOT info {
   SetPatInfo patA.pat info
};
method SetPatInfo patA^AP_AND info {
   SetPatInfo patA.pat1 info; SetPatInfo patA.pat2 info
};
method SetPatInfo patA^AP_OR info {
   Each patA.cases ?(subpatA, _) {
      SetPatInfo subpatA info
   }
};
method SetPatInfo _ _ {};

def EachADefAux defA! fun {};

method EachADefAux _^AD_EMPTY _ {};
method EachADefAux defA^AD_SEQ fun {
   EachADefAux defA.before fun;
   EachADefAux defA.after fun
};
method EachADefAux defA^A_DEF fun {fun defA};

def EachADef modA fun {EachADefAux modA.def fun};

private def CaseIsGoodStatement caseA {IsGoodStatement caseA.expr};
def IsGoodStatement exprA! {};
method IsGoodStatement _^(AE_ERROR,AE_APPLY,AE_END_IF,AE_METHOD,AE_MAKE_FINAL,
   AE_HANDLED,AE_CSTAT) {True};
method IsGoodStatement exprA^AE_LIT {exprA.value %Is Null};
method IsGoodStatement exprA^AE_IF {
   IsGoodStatement exprA.then & IsGoodStatement exprA.else
};
method IsGoodStatement exprA^AE_CASE {
   Every exprA.cases CaseIsGoodStatement
};
method IsGoodStatement exprA^AE_TRY {
   exprA.normal->IfNull {exprA.body}->case [
      (bodyA->HasType AE_FUN) {
         Every bodyA.cases CaseIsGoodStatement
      }
      _ {True}
   ] & Every exprA.cases CaseIsGoodStatement
};
method IsGoodStatement exprA^AE_HANDLE {
   exprA.body->IfNull {True} (function [
      (bodyA->HasType AE_FUN) {
         Every bodyA.cases CaseIsGoodStatement
      }
      _ {True}
   ])
};
method IsGoodStatement exprA^AE_RECEIVE {
   Every exprA.cases CaseIsGoodStatement
};
method IsGoodStatement exprA^AE_RECEIVE_TIME {
   Every exprA.cases CaseIsGoodStatement &
   IsGoodStatement exprA.timeBody
};
method IsGoodStatement exprA^AE_LOCAL {
   case exprA.body [
      (bodyA->HasType AE_FUN) {
         Every bodyA.cases CaseIsGoodStatement
      }
      _ {True}
   ]
};
method IsGoodStatement exprA^AE_DEF {
   IsGoodStatement exprA.expr
};
method IsGoodStatement exprA^AE_CCASE {
   Every exprA.cases ?(_, caseA) {IsGoodStatement caseA}
};
method IsGoodStatement _ {False};

private def CaseIsGoodToApply caseA {IsGoodToApply caseA.expr};
def IsGoodToApply exprA! {};
method IsGoodToApply _^(AE_ERROR,AE_NAME,AE_END_IF,AE_MODULE,AE_FUN,AE_APPLY,
   AE_STRUCT,AE_STRUCT_PART,AE_USE,AE_CEXPR,AE_CCOND,AE_CEVAL) {True};
method IsGoodToApply exprA^AE_LIT {exprA.value->HasType SYMBOL};
method IsGoodToApply exprA^AE_IF {
   IsGoodToApply exprA.then & IsGoodToApply exprA.else
};
method IsGoodToApply exprA^AE_CASE {
   Every exprA.cases CaseIsGoodToApply
};
method IsGoodToApply exprA^AE_TRY {
   exprA.normal->IfNull {exprA.body}->case [
      (bodyA->HasType AE_FUN) {
         Every bodyA.cases CaseIsGoodToApply
      }
      _ {True}
   ] & Every exprA.cases CaseIsGoodToApply
};
method IsGoodToApply exprA^AE_HANDLE {
   exprA.body->IfNull {False} (function [
      (bodyA->HasType AE_FUN) {
         Every bodyA.cases CaseIsGoodToApply
      }
      _ {True}
   ])
};
method IsGoodToApply exprA^AE_RECEIVE {
   Every exprA.cases CaseIsGoodToApply
};
method IsGoodToApply exprA^AE_RECEIVE_TIME {
   Every exprA.cases CaseIsGoodToApply &
   IsGoodToApply exprA.timeBody
};
method IsGoodToApply exprA^AE_LOCAL {
   case exprA.body [
      (bodyA->HasType AE_FUN) {
         Every bodyA.cases CaseIsGoodToApply
      }
      _ {True}
   ]
};
method IsGoodToApply exprA^AE_DEF {
   IsGoodToApply exprA.expr
};
method IsGoodToApply exprA^AE_CCASE {
   Every exprA.cases ?(_, caseA) {IsGoodToApply caseA}
};
method IsGoodToApply _ {False};

def MarkRealNames x! {};
method MarkRealNames _^(NULL,NIL,SYMBOL,STRING) {};
method MarkRealNames x^CONS {Each x MarkRealNames};
method MarkRealNames (x,y)^PAIR {MarkRealNames x; MarkRealNames y};
method MarkRealNames x^NAME {x.real = True};
Each [
   (AM_CONSTANT,     [#name],                    Ignore)
   (AM_VARIABLE,     [#name],                    Ignore)
   (AM_MODULE,       [#name],                    Ignore)
   (AM_DEFINED,      [],                         Ignore)
   (AM_CCON,         [],                         Ignore)
   (A_CASE,          [#params #def #expr],       Ignore)
   (A_GENERIC,       [#params],                  Ignore)
   (A_CON_CASE,      [#params #def #extended #exported], Ignore)
   (AE_ERROR,        [],                         Ignore)
   (AE_NAME,         [#name],                    Ignore)
   (AE_MODULE,       [#name],                    Ignore)
   (AE_CCON,         [],                         Ignore)
   (AE_LIT,          [],                         Ignore)
   (AE_LIST,         [#elems],                   Ignore)
   (AE_FUN,          [#cases],                   ?x {
      ComputeFunInfo x.thisName Null x.cases
   })
   (AE_MANY,         [#arg],                     Ignore)
   (AE_APPLY,        [#fun #args],               Ignore)
   (AE_IF,           [#cond #then #else],        Ignore)
   (AE_END_IF,       [],                         Ignore)
   (AE_CASE,         [#subjs #cases],            Ignore)
   (AE_METHOD,       [#fun #types #method],      Ignore)
   (AE_MAKE_FINAL,   [#type #final],             Ignore)
   (AE_TRY,          [#body #cases #normal],     Ignore)
   (AE_HANDLED,      [],                         Ignore)
   (AE_HANDLE,       [#cases #body],             Ignore)
   (AE_RECEIVE,      [#cases],                   ?x {SimpleFunInfo x.name 0})
   (AE_RECEIVE_TIME, [#cases #time #timeBody],   ?x {SimpleFunInfo x.name 0})
   (AE_LOCAL,        [#variable #value #body],   Ignore)
   (AE_STRUCT,       [#def #extended #exported], Ignore)
   (AE_STRUCT_PART,  [#extended #exported],      Ignore)
   (AE_USE,          [],                         Ignore)
   (AE_NOT,          [#arg],                     Ignore)
   (AE_AND,          [#arg1 #arg2],              Ignore)
   (AE_OR,           [#arg1 #arg2],              Ignore)
   (AE_PAIR,         [#left #right],             Ignore)
   (AE_DEF,          [#def #expr],               Ignore)
   (AE_CEXPR,        [#input],                   Ignore)
   (AE_CCOND,        [#input],                   Ignore)
   (AE_CEVAL,        [#input],                   Ignore)
   (AE_CSTAT,        [#input],                   Ignore)
   (AE_CCASE,        [#input #cases],            Ignore)
   (AE_DESCR_NAME,   [#con],                     Ignore)
   (AE_ENTRY_NAME,   [#obj],                     Ignore)
   (AP_ERROR,        [],                         Ignore)
   (AP_NAME,         [],                         Ignore)
   (AP_ANY,          [],                         Ignore)
   (AP_LIST,         [#elems],                   Ignore)
   (AP_MANY,         [#pat],                     ?x {SetPatInfo x.pat IList})
   (AP_IF,           [#cond],                    Ignore)
   (AP_MATCH,        [#subj #pat],               Ignore)
   (AP_DEF,          [#def],                     Ignore)
   (AP_NOT,          [#pat],                     Ignore)
   (AP_AND,          [#pat1 #pat2],              Ignore)
   (AP_OR,           [#cases],                   Ignore)
   (AP_PAIR,         [#left #right],             Ignore)
   (AD_EMPTY,        [],                         Ignore)
   (AD_SEQ,          [#before #after],           Ignore)
   (AD_EXPR,         [#expr],                    Ignore)
   (AD_LET,          [#pat #expr],               ?x {
      case x.pat x.expr [
         (pat->HasType AP_NAME) (expr->HasType AE_NAME) {
            let patName = pat.name;
            // We should not copy info for public names, because in the case
            // of I_FUNCTION the alias would have MakeEntriesFor called in
            // importing modules; it does not have its own entries so this
            // would be wrong.
            if ~patName.isPublic =>
            let exprName = expr.name;
            patName.info = exprName.info
         }
         _ _ {}
      ]
   })
   (AD_FUN,          [#cases],                   ?x {
      let name = x.name;
      ComputeFunInfo name name.symbol x.cases
   })
   (AD_VAR,          [#value],                   ?x {x.name.info = IVar})
   (AD_DYNAMIC,      [#value],                   ?x {
      x.name.info = if (x.value %Is Null)
         {IDynamicMayFail x.name.symbol}
         {IDynamic}
   })
   (AD_LAZY,         [#value],                   Ignore)
   (AD_FORWARD,      [],                         ?x {
      x.name.info = IForward x.name.symbol
   })
   (AD_TYPE,         [#supertypes #final],       ?x {x.name.info = IType})
   (AD_SINGLETON,    [#type],                    Ignore)
   (AD_RECORD,       [#type],                    ?x {
      let name = x.name;
      SimpleFunInfo name (Size x.fields);
      SetConDescrs name 1;
      name.info = name.info->Change record:pure:(First name.conDescrs)
   })
   (AD_RECORD_NEW,   [#type],                    ?x {
      let name = x.name;
      SimpleFunInfo name (Size x.fields);
      SetConDescrs name 1;
      name.info = name.info->Change record:new:(First name.conDescrs)
   })
   (AD_OBJECT,       [#type #def #extended #exported], Ignore)
   (AD_STRUCT,       [#type #cases],             ?x {
      let name = x.name;
      ComputeFunInfo name name.symbol x.cases;
      SetConDescrs name (Size x.cases)
   })
   (AD_CCON,         [#type #cases],             ?x {
      if (x.thisName ~%Is Null) {ComputeFunInfo x.thisName Null x.cases}
   })
   (AD_USE,          [#imported],                Ignore)
   (AD_CLET,         [#input],                   Ignore)
   (AD_CDEFINE,      [#input],                   Ignore)
   (AD_CDEFINE_NAME, [#value],                   Ignore)
   (AD_CEXPORT,      [],                         Ignore)
   (AD_CEXPORT_NAME, [#value],                   Ignore)
   (AD_CINCLUDE,     [],                         Ignore)
   (A_MAIN_MODULE,   [#def],                     Ignore)
   (A_MODULE,        [#def #exported],           Ignore)
] ?(type, subforms, other) {
   case subforms [
      [] {
         method MarkRealNames x^type {other x}
      }
      [a] {
         method MarkRealNames x^type {
            MarkRealNames (x a);
            other x
         }
      }
      [a b] {
         method MarkRealNames x^type {
            MarkRealNames (x a);
            MarkRealNames (x b);
            other x
         }
      }
      _ {
         method MarkRealNames x^type {
            Each subforms ?field {MarkRealNames (x field)};
            other x
         }
      }
   ]
};
