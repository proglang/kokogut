// This file is a part of Kokogut.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// Kokogut is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Parser [ParseModule ParseSwitch];

ifDefined ~IfNotNull {use KogutUtils};
use Errors;
use Source;

dynamic Input;

def ParseError msg {Error Input.loc msg};
def Skip()         {Input = Input.next};
def LocSkip()      {let loc = Input.loc; Skip(); loc};

let TokensAfterArgs = HashSet [
   #'->' #'*' #'/' #'+' #'-' #'==' #'~=' #'<' #'>' #'<=' #'>='
   #'\\' #'&' #'|' #'=' #'!' #',' #';' #')' #']' #'}' #eof
];

def EndOfArgs() {
   case Input.token [
      (op, _) {op->match (#opL | #opR)}
      #'~' {Input.next.token->match (#opL | #opR, _)}
      token {token->IsIn TokensAfterArgs}
   ]
};

def Closed parser end beginStr endStr {
   let loc = LocSkip();
   let exprS = parser();
   if (Input.token %Is end) {Skip()} {
      Error loc (String "This '" beginStr "' has no matching '" endStr "' \
         below");
      ParseError (String "Missing '" endStr "' matching previous \
         '" beginStr "'")
   };
   exprS
};

def ParseAtom() {
   case Input.token [
      name:symbol {SName (LocSkip()) symbol}
      #any        {SAny (LocSkip())}
      lit:value   {SLit (LocSkip()) value}
      #'()'       {SEmpty (LocSkip())}
      #'('        {Closed ParseExpr #')' "(" ")"}
      #'['        {SList Input.loc (Closed ParseElems #']' "[" "]")}
      #'{'        {SFun Input.loc [] (Closed ParseBody #'}' "{" "}")}
      #'?'        {
         SFun (LocSkip()) (loop => case Input.token [
            (#'{' | #'=>' | ->if (EndOfArgs())) {[]}
            _                                   {ParseArg() \ again()}
         ]) (case Input.token [
            #'{'  {Closed ParseBody #'}' "{" "}"}
            #'=>' {Skip(); ParseBody()}
            _     {ParseError "Missing function body"; []}
         ])
      }
      #'=>'       {SFun (LocSkip()) [] (ParseBody())}
      _           {ParseError "Missing expression"; SError Input.loc}
   ]
};

def ParseAt() {
   ParseAtom()->loop ?exprS {
      case Input.token [
         field:symbol {
            let loc = LocSkip();
            SApply loc exprS [(SLit loc symbol)]->again
         }
         #'@' {SAt (LocSkip()) [exprS (ParseAtom())]->again}
         atLit:value {
            let loc = LocSkip();
            SAt loc [exprS (SLit loc value)]->again
         }
         _ {exprS}
      ]
   }
};

def ErrorMixParam op1 op2 {
   ParseError (String "An expression with '" op1 "' may not be an argument \
      of '" op2 "'")
};

def ParseParam() {
   ParseAt()->loop Null ?exprS nested {
      case Input.token [
         #'...' {
            nested->IfNotNull (->ErrorMixParam "...");
            SMany (LocSkip()) exprS->again "..."
         }
         #'!' {
            nested->IfNotNull (->ErrorMixParam "!");
            SDispatched (LocSkip()) exprS->again "!"
         }
         #'^' {
            nested->IfNotNull (->ErrorMixParam "^");
            SForType (LocSkip()) exprS (ParseAt())->again "^"
         }
         _ {exprS}
      ]
   }
};

def ParseArg() {
   case Input.token [
      #'~' {SNot (LocSkip()) (again())}
      label:symbol {
         let loc = LocSkip();
         SPair loc (SLit loc symbol) (again())
      }
      #'$' {SUnquote (LocSkip()) (again())}
      _    {ParseParam()}
   ]
};

def ParseArgs() {
   if (EndOfArgs()) {[]} {ParseArg() \ ParseArgs()}
};

def ParseElems() {
   case Input.token [
      (#')' | #']' | #'}' | #eof) {[]}
      _                           {
         if (EndOfArgs()) {
            ParseError "Uses of operators in a bracketed list \
               must be parenthesized";
            Skip();
            again()
         } {ParseArg() \ again()}
      }
   ]
};

def ParseApply() {
   let loc = Input.loc;
   let exprS = ParseArg();
   if (EndOfArgs())
      {exprS}
      {ApplyOrSpecial loc exprS (ParseArg() \ ParseArgs())}
};

def ParseArrow() {
   case Input.token [
      #'->' {SAny Input.loc}
      _     {ParseApply()}
   ]->loop ?exprS {
      case Input.token [
         #'->' {
            Skip();
            ApplyOrSpecial Input.loc (ParseArg()) (exprS \ ParseArgs())
            ->again
         }
         _ {exprS}
      ]
   }
};

def ErrorMixOp dir1 dir2 {
   ParseError (String "Mixing " dir1 "-associative and " dir2 "-associative \
      named operators requires parentheses to disambiguate")
};

def ParseNamedOpL exprS {
   case Input.token [
      opL:op {
         let loc = LocSkip();
         ApplyOrSpecial loc (SName loc op) [exprS (ParseArrow())]
         ->again
      }
      opR:op {
         ErrorMixOp "left" "right";
         let loc = LocSkip();
         ApplyOrSpecial loc (SName loc op) [exprS (ParseNamedOpR())]
      }
      #'~' {
         case Input.next.token [
            opL:op {
               let loc = LocSkip(); Skip();
               ApplyOrSpecial loc (SNot loc (SName loc op))
                  [exprS (ParseArrow())]
               ->again
            }
            opR:op {
               ErrorMixOp "left" "right";
               let loc = LocSkip(); Skip();
               ApplyOrSpecial loc (SNot loc (SName loc op))
                  [exprS (ParseNamedOpR())]
            }
            _ {exprS}
         ]
      }
      _ {exprS}
   ]
};

def ParseNamedOpR() {
   let exprS = ParseArrow();
   case Input.token [
      opL:op {
         ErrorMixOp "right" "left";
         let loc = LocSkip();
         ApplyOrSpecial loc (SName loc op) [exprS (ParseArrow())]
         ->ParseNamedOpL
      }
      opR:op {
         let loc = LocSkip();
         ApplyOrSpecial loc (SName loc op) [exprS (again())]
      }
      #'~' {
         case Input.next.token [
            opL:op {
               ErrorMixOp "right" "left";
               let loc = LocSkip(); Skip();
               ApplyOrSpecial loc (SNot loc (SName loc op))
                  [exprS (ParseArrow())]
               ->ParseNamedOpL
            }
            opR:op {
               let loc = LocSkip(); Skip();
               ApplyOrSpecial loc (SNot loc (SName loc op))
                  [exprS (again())]
            }
            _ {exprS}
         ]
      }
      _ {exprS}
   ]
};

def ParseNamedOp() {
   let exprS = case Input.token [
      opL:_ {SAny Input.loc}
      #'~'  {
         case Input.next.token [
            opL:_ {SAny Input.loc}
            _     {ParseArrow()}
         ]
      }
      _ {ParseArrow()}
   ];
   case Input.token [
      opL:op {
         let loc = LocSkip();
         ApplyOrSpecial loc (SName loc op) [exprS (ParseArrow())]
         ->ParseNamedOpL
      }
      opR:op {
         let loc = LocSkip();
         ApplyOrSpecial loc (SName loc op) [exprS (ParseNamedOpR())]
      }
      #'~' {
         case Input.next.token [
            opL:op {
               let loc = LocSkip(); Skip();
               ApplyOrSpecial loc (SNot loc (SName loc op))
                  [exprS (ParseArrow())]
               ->ParseNamedOpL
            }
            opR:op {
               let loc = LocSkip(); Skip();
               ApplyOrSpecial loc (SNot loc (SName loc op))
                  [exprS (ParseNamedOpR())]
            }
            _ {exprS}
         ]
      }
      _ {exprS}
   ]
};

def ParseMul() {
   case Input.token [
      #'/' {SApplyOp (LocSkip()) #'/' [(ParseNamedOp())]}
      _    {ParseNamedOp()}
   ]->loop ?exprS {
      case Input.token [
         (op & (#'*' | #'/')) {
            SApplyOp (LocSkip()) op [exprS (ParseNamedOp())]->again
         }
         _ {exprS}
      ]
   }
};

def ParseAdd() {
   case Input.token [
      #'-' {SApplyOp (LocSkip()) #'-' [(ParseMul())]}
      _    {ParseMul()}
   ]->loop ?exprS {
      case Input.token [
         (op & (#'+' | #'-')) {
            SApplyOp (LocSkip()) op [exprS (ParseMul())]->again
         }
         _ {exprS}
      ]
   }
};

def ParseNoRel() {
   let exprS = ParseAdd();
   case Input.token [
      (op & (#'==' | #'~=' | #'<' | #'>' | #'<=' | #'>=')) {
         ParseError "Relational operators are non-associative";
         SApplyOp (LocSkip()) op [exprS (again())]
      }
      _ {exprS}
   ]
};

def ParseRel() {
   case Input.token [
      (op & (#'==' | #'~=' | #'<' | #'>' | #'<=' | #'>=')) {
         let loc = LocSkip();
         SApplyOp loc op [(SAny loc) (ParseNoRel())]
      }
      _ {
         let exprS = ParseAdd();
         case Input.token [
            (op & (#'==' | #'~=' | #'<' | #'>' | #'<=' | #'>=')) {
               SApplyOp (LocSkip()) op [exprS (ParseNoRel())]
            }
            _ {exprS}
         ]
      }
   ]
};

def ParseCons() {
   let exprS = ParseRel();
   case Input.token [
      #'\\' {SCons (LocSkip()) exprS (again())}
      _     {exprS}
   ]
};

def ParseAnd() {
   let exprS = ParseCons();
   case Input.token [
      #'&' {SAnd (LocSkip()) exprS (again())}
      _    {exprS}
   ]
};

def ParseOr() {
   let exprS = ParseAnd();
   case Input.token [
      #'|' {SOr (LocSkip()) exprS (again())}
      _    {exprS}
   ]
};

def ParseAssign() {
   let exprS = ParseOr();
   case Input.token [
      #'=' {SAssign (LocSkip()) exprS (again())}
      _    {exprS}
   ]
};

def ParsePair() {
   let exprS = ParseAssign();
   case Input.token [
      #',' {SPair (LocSkip()) exprS (again())}
      _    {exprS}
   ]
};

def ParseDefs() {
   ParsePair() \ case Input.token [
      #';' {Skip(); ParseBody()}
      _    {[]}
   ]
};

def ParseBody() {
   case Input.token [
      (#')' | #']' | #'}' | #eof) {[]}
      _                           {ParseDefs()}
   ]
};

def ParseExpr() {
   let loc = Input.loc;
   let exprS = ParsePair();
   case Input.token [
      #';' {Skip(); SSeq loc (exprS \ ParseBody())}
      _    {exprS}
   ]
};

def ExtraClose open close {
   Error (LocSkip()) (String "Unexpected '" close "'; \
      perhaps matching '" open "' is missing above");
   ParseRestGlobalDefs()
};

def ParseRestGlobalDefs() {
   case Input.token [
      #';' {Skip(); ParseGlobalDefs()}
      #eof {[]}
      #')' {ExtraClose "(" ")"}
      #']' {ExtraClose "[" "]"}
      #'}' {ExtraClose "{" "}"}
      _    {ParseError "Missing ';'"; ParsePair() \ ParseRestGlobalDefs()}
   ]
};

def ParseGlobalDefs() {
   case Input.token [
      #eof {[]}
      #')' {ExtraClose "(" ")"}
      #']' {ExtraClose "[" "]"}
      #'}' {ExtraClose "{" "}"}
      _    {ParsePair() \ ParseRestGlobalDefs()}
   ]
};

def ParseModule tokens {
   local Input tokens =>
   SModule Input.loc (ParseGlobalDefs())
};

def ParseSwitch tokens {
   local Input tokens =>
   case (ParseGlobalDefs()) [
      [exprS] {exprS}
      defSs   {SSeq tokens.loc defSs}
   ]
};
