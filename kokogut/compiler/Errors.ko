// This file is a part of Kokogut.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// Kokogut is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Errors [
   InternalError InternalErrorNoAbort FatalError Error Warning Notice
   ShowLoc WereProblems WereErrors ABORT ShowMessages
];

var Messages = [];
var WereProblems = False;
var WereErrors = False;

type MESSAGE {
   type INTERNAL_ERROR MakeInternalError loc msg;
   type ERROR          MakeError loc msg;
   type WARNING        MakeWarning loc msg;
   type NOTE           MakeNotice loc msg;
};

type ABORT Abort;

def InternalError loc msg {
   Messages = MakeInternalError loc msg \ Messages;
   WereProblems = True;
   WereErrors = True;
   Fail Abort
};

def InternalErrorNoAbort loc msg {
   Messages = MakeInternalError loc msg \ Messages;
   WereProblems = True;
   WereErrors = True
};

def FatalError loc msg {
   Messages = MakeError loc msg \ Messages;
   WereProblems = True;
   WereErrors = True;
   Fail Abort
};

def Error [
   loc msg {again loc msg Null}
   loc msg result {
      Messages = MakeError loc msg \ Messages;
      WereProblems = True;
      WereErrors = True;
      result
   }
];

def Warning [
   loc msg {again loc msg Null}
   loc msg result {
      Messages = MakeWarning loc msg \ Messages;
      WereProblems = True;
      result
   }
];

def Notice loc msg {
   Messages = MakeNotice loc msg \ Messages
};

// TODO: Show the contents of the line as well.
def ShowLoc loc {
   if (loc %Is Null) {""} =>
   String " at " loc.file ":" loc.row "," loc.column
      (loc.context->IfNull {[]} ?context =>
         [" (" (ShowSymbol context) ")"])...
};

def ShowProblem p! {};

method ShowProblem p^INTERNAL_ERROR {
   WriteLineTo StdErr "Impossible Error" (ShowLoc p.loc) ": "
      p.msg
};
method ShowProblem p^ERROR {
   WriteLineTo StdErr "Error" (ShowLoc p.loc) ": " p.msg
};
method ShowProblem p^WARNING {
   WriteLineTo StdErr "Warning" (ShowLoc p.loc) ": " p.msg
};
method ShowProblem p^NOTE {
   WriteLineTo StdErr "Notice" (ShowLoc p.loc) ": " p.msg
};

def ShowMessages() {
   Reverse Messages->SortBy ?problem {
      problem.loc->IfNull {"", 0, 0} ?loc {loc.file, loc.row, loc.column}
   }->Each ShowProblem;
   Messages = []
};
