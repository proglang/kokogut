// This file is a part of Kokogut.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// Kokogut is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module RealPrinter [PrintRModuleHeader PrintRDef];

use KogutUtils;
use KogutPrinting;
use Names;
use Real;

let SEQ    = 1;
let COMMA  = 2;
let ASSIGN = 3;
let OR     = 4;
let AND    = 5;
let ARROW  = 6;
let APPLY  = 7;
let ARG    = APPLY+1;
let MANY   = 8;

def PrintMeaning meaningR! {};
method PrintMeaning meaningR^RM_CONSTANT {
   PrintRExpr meaningR.expr ARG
};
method PrintMeaning meaningR^RM_VARIABLE {
   Parens (ArgSeq ["ref" (PrintRExpr meaningR.expr ARG)])
};
method PrintMeaning meaningR^RM_MUTABLE {
   Parens (ArgSeq ["mutable" (ShowName meaningR.name)])
};
method PrintMeaning meaningR^RM_MODULE {
   Parens (ArgSeq ["module" (PrintRExpr meaningR.expr ARG)])
};

def PrintExported header extended exported {
   ArgSeq [
      header...
      (if (extended %Is Null) {[]} {[(PrintRExpr extended ARG)]})...
   ]->AndInBracketsArgSeq (Map exported ?(symbol, meaning) {
      ShowSymbol symbol, ":", PrintMeaning meaning
   })
};

def PrintRCaseParams header paramRs {
   ArgSeq [
      header...
      (case paramRs [
         [] {["()"]}
         _  {Map paramRs (->PrintRPat ARG)}
      ])...
   ]
};

def PrintRCase header caseR! {};
method PrintRCase header caseR^R_CASE {
   PrintRCaseParams header caseR.params
   ->AndInBraces (PrintRExpr caseR.then SEQ)
};
method PrintRCase header caseR^R_GENERIC {
   PrintRCaseParams header caseR.params
   ->AndInBraces (ArgSeq ["generic" (Map caseR.keys ?used {
      if used {"!"} {"_"}
   })...])
};

def PrintRCases header caseRs {
   ArgSeq header
   ->AndInBracketsVSeq (Map caseRs (PrintRCase [] _)) EDoc
};

def PrintCOutputs header outputs {
   header->AndInBracketsArgSeq (Map outputs ?(name, patR) {
      ShowSymbol (Symbol name), ":", PrintRPat patR ARG
   })
};

def PrintCOutputNames header outputs {
   header->AndInBracketsArgSeq (Map outputs ?(name, nameR) {
      ShowSymbol (Symbol name), ":", ShowName nameR
   })
};

def PrintCInputs header inputs {
   header->AndInBracketsArgSeq (Map inputs ?(name, exprR) {
      ShowSymbol (Symbol name), ":", PrintRExpr exprR ARG
   })
};

def PrintCAuxs header auxs {
   header->AndInBracketsArgSeq (Map auxs ?name {ShowSymbol (Symbol name)})
};

// Printing expressions

def PrintRExpr exprR! prec {};

method PrintRExpr _^RE_ERROR _ {"(ERROR)"};

method PrintRExpr exprR^RE_NAME _ {ShowName exprR.name};

method PrintRExpr exprR^RE_FORWARDED prec {
   ArgSeq
      ["forwarded" (ShowName exprR.forward) (ShowName exprR.name)]
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_SOON prec {
   ArgSeq ["soon" (ShowName exprR.forward) (ShowName exprR.name)]
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_GET prec {
   ArgSeq ["get" (ShowName exprR.name)]
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_SET prec {
   (ArgSeq ["set" (ShowName exprR.name)] Equal%
   PrintRExpr exprR.value ASSIGN)
   ->PrintParensIf (prec > ASSIGN)
};

method PrintRExpr exprR^RE_LIT _ {Show exprR.value PrecArg};

method PrintRExpr exprR^RE_LIST _ {
   Brackets (ArgSeq (Map exprR.elems (->PrintRExpr ARG)))
};

method PrintRExpr exprR^RE_FUN prec {
   PrintRCases ["function" (ShowName exprR.name)] exprR.cases
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_MANY prec {
   (PrintRExpr exprR.arg (MANY+1), "...")
   ->PrintParensIf (prec > MANY)
};

method PrintRExpr exprR^RE_APPLY prec {
   case exprR.args [
      []    {PrintRExpr exprR.fun ARG, "()"}
      argAs {ArgSeq (Map [exprR.fun argAs...] (->PrintRExpr ARG))}
   ]->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_OP prec {
   ArgSeq ["op" exprR.op.name (Map exprR.args (->PrintRExpr ARG))...]
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_SURE_BOOL prec {
   ArgSeq ["sureBool" (PrintRExpr exprR.expr ARG)]
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_EXPECT prec {
   ArgSeq ["expect" (PrintRExpr exprR.cond ARG) (Show exprR.value)]
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_FAIL prec {
   ArgSeq ["fail" (PrintRExpr exprR.exn ARG) (PrintRExpr exprR.loc ARG)]
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_FAIL_APPLY prec {
   ArgSeq ["failApply" (Map exprR.args (->PrintRExpr ARG))...]
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_IF prec {
   ArgSeq ["if" (PrintRExpr exprR.cond ARG)]
   ->AndInBraces (PrintRExpr exprR.then SEQ)
   ->AndInBraces (PrintRExpr exprR.else SEQ)
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr _^RE_END_IF _ {"endIf"};

method PrintRExpr exprR^RE_CASE prec {
   PrintRCases ["case" (Map exprR.subjs (->PrintRExpr ARG))...] exprR.cases
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr _^RE_HANDLED _ {"handled"};

method PrintRExpr exprR^RE_STRUCT prec {
   PrintExported ["struct" (ShowName exprR.descr)]
      exprR.extended exprR.exported
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_INCOMPLETE prec {
   PrintExported ["incomplete" (ShowName exprR.descr)]
      exprR.extended exprR.exported
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_COMPLETE prec {
   PrintExported
      ["complete" (PrintRExpr exprR.obj ARG) (ShowName exprR.descr)]
      exprR.extended exprR.exported
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_USE prec {
   ArgSeq ["module" (ShowQualifiedName exprR.module)]
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_NOT prec {
   ("~", PrintRExpr exprR.arg ARG)
   ->PrintParensIf (prec > ARG)
};

method PrintRExpr exprR^RE_AND prec {
   BinOpR " &" (PrintRExpr exprR.arg1 (AND+1)) (PrintRExpr exprR.arg2 AND)
   ->PrintParensIf (prec > AND)
};

method PrintRExpr exprR^RE_OR prec {
   BinOpR " |" (PrintRExpr exprR.arg1 (OR+1)) (PrintRExpr exprR.arg2 OR)
   ->PrintParensIf (prec > OR)
};

method PrintRExpr exprR^RE_PAIR prec {
   BinOpR "," (PrintRExpr exprR.left (COMMA+1)) (PrintRExpr exprR.right COMMA)
   ->PrintParensIf (prec > COMMA)
};

method PrintRExpr exprR^RE_DEF prec {
   (PrintRDef exprR.def, ";", NewLine,
   PrintRExpr exprR.expr SEQ)
   ->PrintParensIf (prec > SEQ)
};

method PrintRExpr exprR^RE_CEXPR prec {
   ArgSeq [
      (if exprR.callback {"c_exprCallback"} {"c_expr"}
      ->PrintCInputs exprR.input)
      (Show exprR.code)
   ]->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_CCOND prec {
   ArgSeq [
      (if exprR.callback {"c_condCallback"} {"c_cond"}
      ->PrintCInputs exprR.input)
      (Show exprR.code)
   ]->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_CEVAL prec {
   let op = if exprR.callback {"c_evalCallback"} {"c_eval"};
   ArgSeq [
      (exprR.result->IfNull {op} ?result {ArgSeq [op result]}
      ->PrintCInputs exprR.input
      ->PrintCAuxs exprR.aux)
      (Show exprR.code)
   ]->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_CSTAT prec {
   ArgSeq [
      (if exprR.callback {"c_statCallback"} {"c_stat"}
      ->PrintCInputs exprR.input
      ->PrintCAuxs exprR.aux)
      (Show exprR.code)
   ]->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_CCASE prec {
   ArgSeq [
      (if exprR.callback {"c_caseCallback"} {"c_case"}
      ->PrintCOutputNames exprR.output
      ->PrintCInputs exprR.input
      ->PrintCAuxs exprR.aux)
      (Show exprR.code)
   ]->AndInBracketsVSeq (Map exprR.cases ?(label, caseR) {
      (ShowSymbol (Symbol label), ":")->AndInBraces (PrintRExpr caseR SEQ)
   }) EDoc
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_DESCR_NAME prec {
   ArgSeq ["c_con" (ShowName exprR.name)]
   ->PrintParensIf (prec > APPLY)
};

method PrintRExpr exprR^RE_ENTRY_NAME prec {
   ArgSeq ["c_entry" (ShowName exprR.obj) (case exprR.arity [
      #list {[]}
      arity {[(Show arity)]}
   ])...]
   ->PrintParensIf (prec > APPLY)
};

// Printing patterns

def PrintRPat patR! prec {};

method PrintRPat _^RP_ERROR _ {"(ERROR)"};

method PrintRPat patR^RP_NAME _ {ShowName patR.name};

method PrintRPat _^RP_ANY _ {"_"};

method PrintRPat patR^RP_LIST _ {
   Brackets (ArgSeq (Map patR.elems (->PrintRPat ARG)))
};

method PrintRPat patR^RP_MANY prec {
   (PrintRPat patR.pat (MANY+1), "...")
   ->PrintParensIf (prec > MANY)
};

method PrintRPat patR^RP_IF prec {
   ArgSeq ["if" (PrintRExpr patR.cond ARG)]
   ->PrintParensIf (prec > APPLY)
};

method PrintRPat patR^RP_MATCH prec {
   ArgSeq ["match" (PrintRExpr patR.subj ARG) (PrintRPat patR.pat ARG)]
   ->PrintParensIf (prec > APPLY)
};

method PrintRPat patR^RP_DEF prec {
   "define"->AndInBraces (PrintRDef patR.def)
   ->PrintParensIf (prec > APPLY)
};

method PrintRPat patR^RP_NOT prec {
   ("~", PrintRPat patR.pat ARG)
   ->PrintParensIf (prec > ARG)
};

method PrintRPat patR^RP_AND prec {
   BinOpR " &" (PrintRPat patR.pat1 (AND+1)) (PrintRPat patR.pat2 AND)
   ->PrintParensIf (prec > AND)
};

method PrintRPat patR^RP_OR prec {
   case patR.common [
      [] {
         ArgSeq [
            "or"
            (Map patR.cases ?(subpatR, _) {PrintRPat subpatR ARG})...
         ]
      }
      commonNs {
         ArgSeq [
            "or"
            (Map patR.cases ?(subpatR, commonRs) {
               PrintRPat subpatR ARROW
               ->Arrow "with" (Map commonRs (->PrintRExpr ARG))...
               ->Parens
            })...
         ]->Arrow "with" (Map commonNs ShowName)...
      }
   ]->PrintParensIf (prec > APPLY)
};

method PrintRPat patR^RP_PAIR prec {
   BinOpR "," (PrintRPat patR.left (COMMA+1)) (PrintRPat patR.right COMMA)
   ->PrintParensIf (prec > COMMA)
};

// Printing definitions

def PrintRDef defR! {};

method PrintRDef _^RD_EMPTY {EDoc};

method PrintRDef defR^RD_SEQ {
   PrintRDef defR.before, ";", NewLine,
   PrintRDef defR.after
};

method PrintRDef defR^RD_FORWARD {
   ArgSeq ["forward" (ShowName defR.forward)]
};

method PrintRDef defR^RD_FILL {
   ArgSeq ["fill" (ShowName defR.forward)] Equal%
   PrintRExpr defR.expr ASSIGN
};

method PrintRDef defR^RD_LET {
   ArgSeq ["let" (PrintRPat defR.pat ARG)] Equal%
   PrintRExpr defR.expr ASSIGN
};

method PrintRDef defR^RD_NAMED_FUN {
   case defR.cases [
      [caseR] {PrintRCase ["def" (ShowName defR.name)] caseR}
      caseRs {PrintRCases ["def" (ShowName defR.name)] caseRs}
   ]
};

method PrintRDef defR^RD_FUN {
   case defR.cases [
      [caseR] {PrintRCase ["fun" (ShowName defR.name)] caseR}
      caseRs {PrintRCases ["fun" (ShowName defR.name)] caseRs}
   ]
};

method PrintRDef defR^RD_VAR {
   ArgSeq ["var" (ShowName defR.name)] Equal%
   PrintRExpr defR.value ASSIGN
};

method PrintRDef defR^RD_MUTABLE {
   ArgSeq ["mutable" (ShowName defR.name)] Equal%
   PrintRExpr defR.value ASSIGN
};

method PrintRDef defR^RD_DYNAMIC {
   let header = ArgSeq ["dynamic" (ShowName defR.name)];
   defR.value->IfNull {header} ?valueR =>
   header Equal% PrintRExpr valueR ASSIGN
};

method PrintRDef defR^RD_LAZY {
   ArgSeq ["lazy" (ShowName defR.name) (PrintRExpr defR.value ARG)]
};

method PrintRDef defR^RD_NAMED_FORWARD {
   ArgSeq ["namedForward" (ShowName defR.name)]
};

method PrintRDef defR^RD_MUTEX {
   ArgSeq ["mutex" (ShowName defR.name)]
};

method PrintRDef defR^RD_TYPE {
   ArgSeq [
      "type" (ShowName defR.name) (PrintRExpr defR.supertypes ARG)
      (PrintRExpr defR.final ARG)
   ]
};

method PrintRDef defR^RD_SINGLETON_DESCR {
   ArgSeq ["singletonDescr" (ShowName defR.name) (ShowName defR.type)]
};

method PrintRDef defR^RD_STRUCT_DESCR {
   PrintExported
      ["structDescr" (ShowName defR.name) (ShowName defR.type)]
      defR.extended defR.exported
};

method PrintRDef defR^RD_INCOMPLETE_DESCR {
   PrintExported
      ["incompleteDescr" (ShowName defR.name) (ShowName defR.type)]
      defR.extended defR.exported
};

method PrintRDef defR^RD_SINGLETON {
   ArgSeq ["singleton" (ShowName defR.name) (ShowName defR.descr)]
};

method PrintRDef defR^RD_CCON {
   ArgSeq [
      "c_con" (ShowName defR.type) (ShowName defR.name)
      (Show defR.move)
      (if (defR.finalize %Is Null) {[]} {[(Show defR.finalize)]})...
      (if (defR.save %Is Null) {[]} {[(Show defR.save)]})...
      (if (defR.cases %Is Null) {[]} {[
         (PrintRCases ["function" (ShowName defR.thisName)] defR.cases
         ->Parens)
      ]})...
   ]
};

method PrintRDef defR^RD_USE {
   let header = ArgSeq ["use" (ShowQualifiedName defR.module)];
   case defR.module [
      [#Prelude] {header->AndInBrackets "..."}
      _          {header->AndInBracketsArgSeq (Map defR.imported ShowName)}
   ]
};

method PrintRDef defR^RD_CLET {
   ArgSeq [
      (if defR.callback {"c_letCallback"} {"c_let"}
      ->PrintCOutputs defR.output
      ->PrintCInputs defR.input
      ->PrintCAuxs defR.aux)
      (Show defR.code)
   ]
};

method PrintRDef defR^RD_CDEFINE {
   ArgSeq [
      ("c_define"->PrintCInputs defR.input)
      (Show defR.code)
   ]
};

method PrintRDef defR^RD_CDEFINE_NAME {
   ArgSeq [
      "c_defineName"
      (ShowSymbol (Symbol defR.name), ":", PrintRExpr defR.value ARG)
   ]
};

method PrintRDef defR^RD_CEXPORT {
   ArgSeq ["c_export" (Show defR.code)]
};

method PrintRDef defR^RD_CEXPORT_NAME {
   ArgSeq [
      "c_exportName"
      (ShowSymbol (Symbol defR.name), ":", PrintRExpr defR.value ARG)
   ]
};

method PrintRDef defR^RD_CINCLUDE {
   ArgSeq ["c_include" (Show defR.file)]
};

def PrintRModuleHeader modR {
   ArgSeq ["module" (ShowQualifiedName modR.moduleName)]
};
