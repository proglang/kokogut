// This file is a part of Kokogut.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2009 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// Kokogut is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Lexer [Lex];

use KogutUtils;
use Errors;

type TOKEN_LIST MakeToken loc token next;

def IsDecDigit ch {ch->IsBetween "0" "9"};
def IsHexDigit ch {
   ch->IsBetween "0" "9" | ch->IsBetween "A" "F" | ch->IsBetween "a" "f"
};
def IsOctDigit ch {ch->IsBetween "0" "7"};
def IsBinDigit ch {ch->match ("0" | "1")};
def IsSpecialInString ch {ch->match ("\"" | "\\" | " " | "\t" | "\n")};
def IsSpecialInName   ch {ch->match ("'"  | "\\" | " " | "\t" | "\n")};

dynamic Text;
dynamic Filename;
dynamic Row;
dynamic Column;

ref Current = {Get Text 0 {}};
ref NextChar = {Get Text 1 {}};
ref Loc = {MakeLocation Filename Row Column Null};
def LexError msg {Error Loc msg};

def AfterSpace loc {
   case Current [
      (%Is Null | "\n") {Notice loc "Space at end of line"}
      _                 {}
   ]
};

def Skip() {
   DoRemoveFirst Text;
   Column = Column+1
};
def LocSkip() {
   let loc = Loc;
   Skip();
   loc
};
def SkipSpace() {
   LocSkip()->AfterSpace
};
def SkipTab() {
   let loc = Loc;
   DoRemoveFirst Text;
   Column = Column %Ceiling 8 + 1;
   AfterSpace loc
};
def SkipLF() {
   DoRemoveFirst Text;
   Row = Row+1;
   Column = 1
};

def SkipWhile cond {
   let ch = Current;
   if (ch ~%Is Null & cond ch) {
      case ch [
         " "  {SkipSpace()}
         "\t" {SkipTab()}
         _    {Skip()}
      ];
      SkipWhile cond
   }
};

def CutWhile pred {
   let iter = Iterate Text;
   iter {""} ?_ =>
   loop 1 ?i {
      let end = {Column = Column + i; DoCutPart Text 0 i->String};
      iter end ?elem =>
      if (~pred elem) {end()} {again (i+1)}
   }
};

let EscapeRuleTable = HashDict();
def StrangeEscape() {
   LexError (String "Strange escaped character " (Show Current));
   Skip(); ""
};

def EscapeRule char handler {EscapeRuleTable@char = handler};
def EscapeRules chars handler {Each chars (->EscapeRule handler)};

EscapeRule "a"  {Skip(); "\a"};
EscapeRule "b"  {Skip(); "\b"};
EscapeRule "t"  {Skip(); "\t"};
EscapeRule "n"  {Skip(); "\n"};
EscapeRule "v"  {Skip(); "\v"};
EscapeRule "f"  {Skip(); "\f"};
EscapeRule "r"  {Skip(); "\r"};
EscapeRule "s"  {Skip(); " "};
EscapeRule "\"" {Skip(); "\""};
EscapeRule "'"  {Skip(); "'"};
EscapeRule "\\" {Skip(); "\\"};
EscapeRule "c"  {
   Skip();
   case Current [
      (%Is Null)              {""}
      (ch->IsBetween "@" "_") {Skip(); ch - 64}
      (ch->IsBetween "a" "z") {Skip(); ch - 96}
      "?"                     {Skip(); "\127;"}
      ch {LexError (String "Strange control character " (Show ch)); ""}
   ]
};
def CloseCharCode code {
   case Current [
      ";" {Skip()}
      _   {LexError "Missing \";\" after character code"}
   ];
   if (code <= 0x10FFFF)
      {Char code}
      {LexError (String "Character code " code " out of range"); ""}
};
EscapeRules (Between "0" "9") {
   CutWhile IsDecDigit->Int->CloseCharCode
};
EscapeRule "x" {
   Skip();
   let ch = Current;
   if (ch ~%Is Null & IsHexDigit ch)
      {CutWhile IsHexDigit->Int base:16->CloseCharCode}
      {LexError "Missing character code"; ""}
};
EscapeRule "o" {
   Skip();
   let ch = Current;
   if (ch ~%Is Null & IsOctDigit ch)
      {CutWhile IsOctDigit->Int base:8->CloseCharCode}
      {LexError "Missing character code"; ""}
};
EscapeRules [" " "\t" "\n"] {
   SkipWhile (->match (" " | "\t"));
   case Current [
      "\n" {SkipLF(); SkipWhile (->match (" " | "\t"))}
      _    {LexError "\"\\\" must be at end of line"}
   ];
   ""
};
EscapeRule "_" {
   Skip();
   SkipWhile (->match (" " | "\t"));
   case Current [
      "\n" {SkipLF()}
      _    {LexError "\"\\_\" must be at end of line"}
   ];
   ""
};

def QuoteMessage [
   "\"" {"Matching '\"' is missing"}
   "'" {"Matching \"'\" is missing"}
];
def ReadQuoted loc isPlain end {
   loop {
      case Current [
         (%Is Null) {Error loc (QuoteMessage end); []}
         (%Is end) {Skip(); []}
         "\\" {
            Skip();
            let ch = Current;
            if (ch %Is Null)
               {Error loc (QuoteMessage end); []}
               {(Get EscapeRuleTable ch {StrangeEscape})() \ again()}
         }
         " "  {SkipSpace(); " " \ again()}
         "\t" {SkipTab(); "\t" \ again()}
         "\n" {SkipLF(); "\n" \ again()}
         _    {CutWhile isPlain \ again()}
      ]
   }->String
};

let RightContext = HashDict [
   ("_",  {if (IsIdent NextChar) {"a name"} {"\"_\""}})
   ("\"", {"'\"'"})
   ("#",  {"\"#\""})
   ("$",  {"\"$\""})
   ("%",  {"\"%\""})
   ("'",  {"\"'\""})
   ("(",  {if (NextChar %Is ")") {Null} {"\"(\" (except \"()\")"}})
   (".",  {if (IsDecDigit NextChar) {"a number"} {Null}})
   ("=",  {if (NextChar %Is ">") {"\"=>\""} {Null}})
   ("?",  {"\"?\""})
   ("[",  {"\"[\""})
   ("{",  {"\"{\""})
   ("~",  {if (NextChar %Is "=") {Null} {"\"~\""}})
];

def ForceSpace prev {
   let next = case Current [
      (%Is Null)       {Null}
      (->IsIdentFirst) {"a name"}
      (->IsDecDigit)   {"a number"}
      ch               {Get RightContext ch {Null} Apply}
   ];
   if (next ~%Is Null) {
      Error Loc (String "Missing space between " prev " and " next)
   }
};

def ReadName() {
   let (value, prev) = case Current [
      ((~%Is Null)->IsIdentFirst) {CutWhile IsIdent, "a name"}
      "'" {ReadQuoted (LocSkip()) (~IsSpecialInName _) "'", "\"'\""}
      _   {LexError "Missing name"; "", Null}
   ];
   prev->IfNotNull ForceSpace;
   Symbol value
};

def SkipComment loc {
   case Current [
      (%Is Null) {Error loc "Matching \"*/\" is missing below"}
      " "  {SkipSpace(); SkipComment loc}
      "\t" {SkipTab(); SkipComment loc}
      "\n" {SkipLF(); SkipComment loc}
      "*"  {Skip(); SkipCommentStar loc}
      "/"  {
         let loc' = LocSkip();
         case Current [
            "*" {Skip(); SkipComment loc'; SkipComment loc}
            _   {SkipComment loc}
         ]
      }
      _    {Skip(); SkipComment loc}
   ]
};
def SkipCommentStar loc {
   case Current [
      "*" {Skip(); SkipCommentStar loc}
      "/" {Skip()}
      _   {SkipComment loc}
   ]
};

let RuleTable = HashDict();
def Strange() {
   LexError (String "Strange character " (Show Current));
   Skip(); GetTokens()
};
def GetTokens() {
   case Current [
      (%Is Null) {MakeToken Loc #eof Null}
      (->IsIdentFirst) {
         let loc = Loc;
         let name = CutWhile IsIdent;
         case Current [
            "%" {Skip(); ForceSpace "\"%\""; NextToken loc opR:(Symbol name)}
            ":" {Skip(); NextToken loc label:(Symbol name)}
            _   {
               if (name %Is "_")
                  {ForceSpace "\"_\""; NextToken loc #any}
                  {ForceSpace "a name"; NextToken loc name:(Symbol name)}
            }
         ]
      }
      ch {Get RuleTable ch Strange Apply}
   ]
};
def NextToken loc token {MakeToken loc token (GetTokens())};

def Rule char handler {RuleTable@char = handler};
def Rules chars handler {Each chars (->Rule handler)};

def ExponentValue() {
   case Current [
      (->IsDecDigit) {CutWhile IsDecDigit->Int}
      _              {LexError "Missing exponent"; 0}
   ]
};

def Exponent m e {
   Skip();
   case Current [
      "+" {Skip(); RealValue False m (e + ExponentValue())}
      "-" {Skip(); RealValue False m (e - ExponentValue())}
      _   {RealValue False m (e + ExponentValue())}
   ]
};

def MaybeExponent m e {
   case Current [
      ("e" | "E") {Exponent m e}
      _           {RealValue False m e}
   ]
};

def IntOrReal value {
   case Current [
      "." {
         Skip();
         case Current [
            (->IsDigit) {
               let frac = CutWhile IsDecDigit;
               MaybeExponent (String value frac->Int) (-Size frac)
            }
            _ {MaybeExponent (Int value) 0}
         ]
      }
      ("e" | "E") {Exponent (Int value) 0}
      _ {Int value}
   ]
};

def After0 tokenSymbol decimal {
   let loc = Loc;
   Skip();
   let value = case Current [
      (->IsDecDigit) {CutWhile IsDecDigit->decimal}
      ("x" | "X") {
         Skip();
         if (IsHexDigit Current)
            {CutWhile IsHexDigit->Int base:16}
            {LexError "Bad integer syntax"; 0}
      }
      ("o" | "O") {
         Skip();
         if (IsOctDigit Current)
            {CutWhile IsOctDigit->Int base:8}
            {LexError "Bad integer syntax"; 0}
      }
      ("b" | "B") {
         Skip();
         if (IsBinDigit Current)
            {CutWhile IsBinDigit->Int base:2}
            {LexError "Bad integer syntax"; 0}
      }
      _ {decimal "0"}
   ];
   ForceSpace "a number";
   NextToken loc (tokenSymbol, value)
};

Rule "0" {After0 #lit IntOrReal};
Rules (Between "1" "9") {
   let value = CutWhile IsDecDigit->IntOrReal;
   ForceSpace "a number";
   NextToken Loc lit:value
};

Rule " "  {SkipSpace(); GetTokens()};
Rule "\t" {SkipTab();   GetTokens()};
Rule "\n" {SkipLF();    GetTokens()};

Rule "!" {let loc = LocSkip(); ForceSpace "\"!\""; NextToken loc #'!'};

Rule "\"" {
   let loc = LocSkip();
   let value = ReadQuoted loc (~IsSpecialInString _) "\"";
   ForceSpace "'\"'";
   NextToken loc lit:value
};

Rule "#" {NextToken (LocSkip()) lit:(ReadName())};

Rule "$" {NextToken (LocSkip()) #'$'};

Rule "%" {NextToken (LocSkip()) opL:(ReadName())};

Rule "&" {NextToken (LocSkip()) #'&'};

Rule "'" {
   let loc = LocSkip();
   let name = ReadQuoted loc (~IsSpecialInName _) "'"->Symbol;
   case Current [
      "%" {Skip(); ForceSpace "\"%\""; NextToken loc opR:name}
      ":" {Skip(); NextToken loc label:name}
      _   {ForceSpace "\"'\""; NextToken loc name:name}
   ]
};

Rule "(" {
   let loc = LocSkip();
   case Current [
      ")" {Skip(); ForceSpace "\"()\"";  NextToken loc #'()'}
      _   {NextToken loc #'('}
   ]
};

Rule ")" {let loc = LocSkip(); ForceSpace "\")\""; NextToken loc #')'};

Rule "*" {
   let loc = LocSkip();
   case Current [
      "/" {
         Error loc "Matching \"/*\" is missing above";
         Skip(); GetTokens()
      }
      _ {NextToken loc #'*'}
   ]
};


Rule "+" {NextToken (LocSkip()) #'+'};

Rule "," {NextToken (LocSkip()) #','};

Rule "-" {
   let loc = LocSkip();
   case Current [
      ">" {Skip(); NextToken loc #'->'}
      _   {NextToken loc #'-'}
   ]
};

Rule "." {
   let loc = LocSkip();
   case Current [
      ("."->if (NextChar %Is ".")) {
         Skip(); Skip();
         ForceSpace "\"...\"";
         NextToken loc #'...'
      }
      (->IsDecDigit) {
         let frac = CutWhile IsDecDigit;
         let value = MaybeExponent (Int frac) (-Size frac);
         ForceSpace "a number";
         NextToken loc lit:value
      }
      _ {NextToken loc field:(ReadName())}
   ]
};

Rule "/" {
   let loc = LocSkip();
   case Current [
      "*" {Skip(); SkipComment loc; GetTokens()}
      "/" {Skip(); SkipWhile (~%Is "\n"); GetTokens()}
      _   {NextToken loc #'/'}
   ]
};

Rule ";" {NextToken (LocSkip()) #';'};

Rule "<" {
   let loc = LocSkip();
   case Current [
      "=" {Skip(); NextToken loc #'<='}
      _   {NextToken loc #'<'}
   ]
};

Rule "=" {
   let loc = LocSkip();
   case Current [
      "=" {Skip(); NextToken loc #'=='}
      ">" {Skip(); NextToken loc #'=>'}
      _   {NextToken loc #'='}
   ]
};

Rule ">" {
   let loc = LocSkip();
   case Current [
      "=" {Skip(); NextToken loc #'>='}
      _   {NextToken loc #'>'}
   ]
};

Rule "?" {NextToken (LocSkip()) #'?'};

Rule "@" {
   let loc = LocSkip();
   case Current [
      "0" {After0 #atLit Int}
      (->IsBetween "1" "9") {
         let value = CutWhile IsDecDigit->Int;
         ForceSpace "a number";
         NextToken loc atLit:value
      }
      _ {NextToken loc #'@'}
   ]
};

Rule "[" {NextToken (LocSkip()) #'['};

Rule "\\" {NextToken (LocSkip()) #'\\'};

Rule "]" {let loc = LocSkip(); ForceSpace "\"]\""; NextToken loc #']'};

Rule "^" {NextToken (LocSkip()) #'^'};

Rule "{" {NextToken (LocSkip()) #'{'};

Rule "|" {NextToken (LocSkip()) #'|'};

Rule "}" {let loc = LocSkip(); ForceSpace "\"}\""; NextToken loc #'}'};

Rule "~" {
   let loc = LocSkip();
   case Current [
      "=" {Skip(); NextToken loc #'~='}
      _   {NextToken loc #'~'}
   ]
};

def Lex text filename {
   local Text text =>
   local Filename filename =>
   local Row 1 =>
   local Column 1 =>
   GetTokens()
};
