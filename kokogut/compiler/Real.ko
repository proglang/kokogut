// This file is a part of Kokogut.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// Kokogut is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Real;

use Names;

type R_MEANING {
   type RM_CONSTANT {
      type (private RM_CONSTANT_PROPER) RMConstant expr;
      type RM_MODULE                    RMModule expr;
   };
   type RM_VARIABLE RMVariable expr;
   type RM_MUTABLE  RMMutable name;
};

type R_CASE    RCase params then;
type R_GENERIC RGeneric params arity keys dict;

type R_EXPR {
   type RE_ERROR      REError;
   type RE_NAME       REName name;
   type RE_FORWARDED  REForwarded loc forward name;
   type RE_SOON       RESoon forward name;
   type RE_GET        REGet name;
   type RE_SET        RESet name value;
   type RE_LIT        RELit value;
   type RE_LIST       REList elems;
   type RE_FUN        REFun loc name cases {
      let free = case name.storage [
         #local {FreeLocals cases name}
         _      {[]}
      ];
   };
   type RE_MANY       REMany loc arg;
   type RE_APPLY      REApply loc fun args;
   type RE_OP         REOp op args;
   type RE_SURE_BOOL  RESureBool expr;
   type RE_EXPECT     REExpect cond value;
   type RE_FAIL       REFail exn loc;
   type RE_FAIL_APPLY REFailApply fun args;
   type RE_IF         REIf loc cond then else;
   type RE_END_IF     REEndIf loc;
   type RE_CASE       RECase loc subjs cases;
   type RE_HANDLED    REHandled;
   type RE_STRUCT     REStruct descr extended exported;
   type RE_INCOMPLETE REIncomplete descr extended exported;
   type RE_COMPLETE   REComplete obj descr extended exported;
   type RE_USE        REUse loc module;
   type RE_NOT        RENot loc arg;
   type RE_AND        REAnd loc arg1 arg2;
   type RE_OR         REOr loc arg1 arg2;
   type RE_PAIR       REPair left right;
   type RE_DEF        REDef def expr;
   type RE_CEXPR      RECExpr loc callback input code;
   type RE_CCOND      RECCond loc callback input code;
   type RE_CEVAL      RECEval loc callback result input aux code;
   type RE_CSTAT      RECStat loc callback input aux code;
   type RE_CCASE      RECCase loc callback output input aux code cases;
   type RE_DESCR_NAME REDescrName name;
   type RE_ENTRY_NAME REEntryName obj arity isPublic;
};

type R_PAT {
   type RP_ERROR RPError;
   type RP_NAME  RPName name;
   type RP_ANY   RPAny;
   type RP_LIST  RPList elems;
   type RP_MANY  RPMany pat;
   type RP_IF    RPIf loc cond;
   type RP_MATCH RPMatch subj pat;
   type RP_DEF   RPDef def;
   type RP_NOT   RPNot pat;
   type RP_AND   RPAnd pat1 pat2;
   type RP_OR    RPOr cases common;
   type RP_PAIR  RPPair left right;
};

type R_DEF {
   type RD_EMPTY            RDEmpty;
   type RD_SEQ              RDSeq before after;
   type RD_FORWARD          RDForward forward;
   type RD_FILL             RDFill forward expr;
   type RD_LET              RDLet loc pat expr;
   type RD_NAMED_FUN        RDNamedFun loc name cases {
      let generic = Any cases (->HasType R_GENERIC);
      let free = case name.storage [
         #local {FreeLocals cases name}
         _      {[]}
      ];
   };
   type RD_FUN              RDFun loc name cases {
      let free = case name.storage [
         #local {FreeLocals cases name}
         _      {[]}
      ];
   };
   type RD_VAR              RDVar name value;
   type RD_MUTABLE          RDMutable name value;
   type RD_DYNAMIC          RDDynamic name value;
   type RD_LAZY             RDLazy name value;
   type RD_NAMED_FORWARD    RDNamedForward name;
   type RD_MUTEX            RDMutex name;
   type RD_TYPE             RDType name supertypes final;
   type RD_SINGLETON_DESCR  RDSingletonDescr name type;
   type RD_STRUCT_DESCR     RDStructDescr loc name type extended exported;
   type RD_INCOMPLETE_DESCR RDIncompleteDescr name type extended exported;
   type RD_SINGLETON        RDSingleton name descr;
   type RD_CCON             RDCCon loc name type locMove move
      locFinalize finalize locSave save thisName cases;
   type RD_USE              RDUse loc module imported;
   type RD_CLET             RDCLet loc callback output input aux code;
   type RD_CDEFINE          RDCDefine loc input code;
   type RD_CDEFINE_NAME     RDCDefineName name value;
   type RD_CEXPORT          RDCExport loc code key;
   type RD_CEXPORT_NAME     RDCExportName name value;
   type RD_CINCLUDE         RDCInclude file module;
};

type R_MAIN_MODULE RMainModule moduleName loc def;
type R_MODULE      RModule moduleName loc def name exported;
// The expressions in meanings in the export list of a module are all
// RE_NAMEs.

def IsMainModuleR moduleR! {};
method IsMainModuleR _^R_MAIN_MODULE {True};
method IsMainModuleR _^R_MODULE      {False};

def RDThen defR1 defR2 {
   if [
      (defR1 %Is RDEmpty) {defR2}
      (defR2 %Is RDEmpty) {defR1}
      _                   {RDSeq defR1 defR2}
   ]
};

def REThen defR exprR {
   if (defR %Is RDEmpty)
      {exprR}
      {REDef defR exprR}
};

def RDSequence defRs {Fold defRs RDEmpty RDThen};

def REEq arg1 arg2 {REOp #'==' [arg1 arg2]};

private =>
def MarkUse x! free bound {};
method MarkUse x^NAME free _ {
   if (x.storage %Is #local) {DoAdd free x}
};
method MarkUse _^NULL _ _ {};
method MarkUse xs^CONS free bound {Each xs (->MarkUse free bound)};
method MarkUse _^NIL _ _ {};
method MarkUse (x, y)^PAIR free bound {
   MarkUse x free bound; MarkUse y free bound
};
method MarkUse _^STRING _ _ {};
method MarkUse _^SYMBOL _ _ {};
Each [
   (RM_CONSTANT,         [#expr],                           [])
   (RM_VARIABLE,         [#expr],                           [])
   (RM_MUTABLE,          [#expr],                           [])
   (RM_MODULE,           [#expr],                           [])
   (R_CASE,              [#params #then],                   [])
   (R_GENERIC,           [#params],                         [])
   (RE_ERROR,            [],                                [])
   (RE_NAME,             [#name],                           [])
   (RE_FORWARDED,        [#forward], /*no #name!*/          [])
   (RE_SOON,             [#forward], /*no #name!*/          [])
   (RE_GET,              [#name],                           [])
   (RE_SET,              [#name #value],                    [])
   (RE_LIT,              [],                                [])
   (RE_LIST,             [#elems],                          [])
   (RE_FUN,              [#free],                           [])
   (RE_MANY,             [#arg],                            [])
   (RE_APPLY,            [#fun #args],                      [])
   (RE_OP,               [#args],                           [])
   (RE_SURE_BOOL,        [#expr],                           [])
   (RE_EXPECT,           [#cond],                           [])
   (RE_FAIL,             [#exn #loc],                       [])
   (RE_FAIL_APPLY,       [#fun #args],                      [])
   (RE_IF,               [#cond #then #else],               [])
   (RE_END_IF,           [],                                [])
   (RE_CASE,             [#subjs #cases],                   [])
   (RE_HANDLED,          [],                                [])
   (RE_STRUCT,           [#descr #extended #exported],      [])
   (RE_INCOMPLETE,       [#descr],                          [])
   (RE_COMPLETE,         [#obj #descr #extended #exported], [])
   (RE_USE,              [],                                [])
   (RE_NOT,              [#arg],                            [])
   (RE_AND,              [#arg1 #arg2],                     [])
   (RE_OR,               [#arg1 #arg2],                     [])
   (RE_PAIR,             [#left #right],                    [])
   (RE_DEF,              [#def #expr],                      [])
   (RE_CEXPR,            [#input],                          [])
   (RE_CCOND,            [#input],                          [])
   (RE_CEVAL,            [#input],                          [])
   (RE_CSTAT,            [#input],                          [])
   (RE_CCASE,            [#input #cases],                   [#output])
   (RE_DESCR_NAME,       [],                                [])
   (RE_ENTRY_NAME,       [#obj],                            [])
   (RP_ERROR,            [],                                [])
   (RP_NAME,             [],                                [#name])
   (RP_ANY,              [],                                [])
   (RP_LIST,             [#elems],                          [])
   (RP_MANY,             [#pat],                            [])
   (RP_IF,               [#cond],                           [])
   (RP_MATCH,            [#subj #pat],                      [])
   (RP_DEF,              [#def],                            [])
   (RP_NOT,              [#pat],                            [])
   (RP_AND,              [#pat1 #pat2],                     [])
   (RP_OR,               [#cases],                          [#common])
   (RP_PAIR,             [#left #right],                    [])
   (RD_EMPTY,            [],                                [])
   (RD_SEQ,              [#before #after],                  [])
   (RD_FORWARD,          [],                                [#forward])
   (RD_FILL,             [#forward #expr],                  [])
   (RD_LET,              [#pat #expr],                      [])
   (RD_NAMED_FUN,        [#free],                           [#name])
   (RD_FUN,              [#free],                           [#name])
   (RD_VAR,              [#value],                          [#name])
   (RD_MUTABLE,          [#value],                          [#name])
   (RD_DYNAMIC,          [#value],                          [#name])
   (RD_LAZY,             [#value],                          [#name])
   (RD_NAMED_FORWARD,    [],                                [#name])
   (RD_MUTEX,            [],                                [#name])
   (RD_TYPE,             [#supertypes #final],              [#name])
   (RD_SINGLETON_DESCR,  [#type],                           [#name])
   (RD_STRUCT_DESCR,     [#type],                           [#name])
   (RD_INCOMPLETE_DESCR, [#type],                           [#name])
   (RD_SINGLETON,        [#descr],                          [#name])
   (RD_CCON,             [#cases],                          [])
   (RD_USE,              [],                                [])
   (RD_CLET,             [#output #input],                  [])
   (RD_CDEFINE,          [#input],                          [])
   (RD_CDEFINE_NAME,     [],                                [])
   (RD_CEXPORT,          [],                                [])
   (RD_CEXPORT_NAME,     [],                                [])
   (RD_CINCLUDE,         [],                                [])
] ?(type, uses, binds) {
   let use' = case uses [
      []     {?_ _ _ {}}
      [a]    {?x free bound {MarkUse (x a) free bound}}
      [a b]  {?x free bound {
         MarkUse (x a) free bound; MarkUse (x b) free bound
      }}
      fields {?x free bound {Each fields ?a {MarkUse (x a) free bound}}}
   ];
   case binds [
      [] {
         method MarkUse x^type free bound {use' x free bound}
      }
      [a] {
         method MarkUse x^type free bound {
            MarkBind (x a) free bound;
            use' x free bound
         }
      }
   ]
};

def MarkBind x! free bound {};
method MarkBind x^NAME _ bound {
   if (x.storage %Is #local) {DoAdd bound x}
};
method MarkBind xs^CONS free bound {Each xs (->MarkBind free bound)};
method MarkBind _^NIL _ _ {};
method MarkBind (x, y)^PAIR free bound {
   MarkBind x free bound; MarkBind y free bound
};
method MarkBind _^STRING _ _ {};

def FreeLocalsSet x {
   let free = HashSet();
   let bound = HashSet();
   MarkUse x free bound;
   DoDifference free bound;
   free
};

def FreeLocals x name {
   let free = FreeLocalsSet x;
   DoRemove free name;
   free->SortListBy _.id
};

public =>
def EachRDefAux defR! fun {};
method EachRDefAux _^RD_EMPTY _ {};
method EachRDefAux defR^RD_SEQ fun {
   EachRDefAux defR.before fun;
   EachRDefAux defR.after fun
};
method EachRDefAux defR^R_DEF fun {fun defR};

def EachRDef modL fun {EachRDefAux modL.def fun};

// Whether it doesn't matter when or how many times it's evaluated
// or if it's duplicated.
def IsImmediate exprR! {};
method IsImmediate _^(RE_NAME,RE_LIT) {True};
method IsImmediate _                  {False};

def IsSimplePat patR! {};
method IsSimplePat _^(RP_NAME,RP_ANY) {True};
method IsSimplePat _                  {False};

// Whether a pattern obviously matches all values. This includes those
// splicing patterns which match all lists of values.
def MatchesAll patR! {};
method MatchesAll _^(RP_NAME,RP_ANY) {True};
method MatchesAll patR^RP_MANY  {MatchesAll patR.pat};
method MatchesAll patR^RP_MATCH {MatchesAll patR.pat};
method MatchesAll _^RP_DEF      {True};
method MatchesAll patR^RP_AND   {MatchesAll patR.pat1 & MatchesAll patR.pat2};
method MatchesAll patR^RP_OR    {
   Any patR.cases ?(subpatR, _) {MatchesAll subpatR}
};
method MatchesAll _             {False};

// Check whether a list of patterns obviously matches all lists of values.
def MatchesAllLists [
   [paramR] {paramR->HasType RP_MANY & MatchesAll paramR.pat}
   _        {False}
];

def SureType exprR! {};
method SureType exprR^RE_NAME {exprR.name.info->HasType I_TYPE};
method SureType exprR^RE_OP   {exprR.op %Is #Type};
method SureType _             {False};

def EvalExprOnce expr loc cont {
   if (IsImmediate expr) {cont expr} =>
   let tempN = Name #temp;
   RDLet loc (RPName tempN) expr REDef%
   cont (REName tempN)
};

def EvalExprsOnce [
   (expr\exprs) loc cont {
      EvalExprOnce expr loc ?expr =>
      EvalExprsOnce exprs loc ?exprs =>
      cont (expr\exprs)
   }
   [] _ cont {cont []}
];
