// This file is a part of Kokogut.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// Kokogut is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Interfaces [
   SaveInterface GetInterface IsModuleDefined
   ModuleNameWithoutRoot ModuleFilename
   CopyName CopyMeaning CopyExported
];

use KogutUtils;
use Errors;
use KogutPrinting;
use Lexer;
use Names;
use Abstract;

def PrintStorage storage size {
   case storage [
      #global {["global"]}
      #static {["static" (Show size)]}
      #none   {["none"]}
   ]
};

def Extension [
   #global {"ptr"}
   #static {"obj"}
   #none   {"none"}
];

def PrintInfo info! {};

method PrintInfo _^I_UNKNOWN {[]};
method PrintInfo _^I_SMALL_INT {["smallInt"]};
method PrintInfo _^I_STATIC_INT {["staticInt"]};
method PrintInfo info^I_FUNCTION {
   [
      "fun" (info.symbol->IfNull {"_"} ShowSymbol)
      (Show info.allowed) (Show info.fast)
      (case info.record [
         (kind, descrN->_.isPublic) {
            [(kind.name, ":", ShowOrigin descrN.origin)]
         }
         _ {[]}
      ])...
   ]
};
method PrintInfo _^I_VAR {["var"]};
method PrintInfo _^I_DYNAMIC {["dynamic1"]};
method PrintInfo info^I_DYNAMIC_MAY_FAIL {
   let name = info.name;
   if (IsPrivateSymbol name) {[]} {["dynamic0" (ShowSymbol name)]}
};
method PrintInfo info^I_FORWARD {
   let name = info.name;
   if (IsPrivateSymbol name) {[]} {["forward" (ShowSymbol info.name)]}
};
method PrintInfo _^I_TYPE {["type"]};
method PrintInfo _^I_LIST {["list"]};

def PrintNameDetails name {
   [
      (ShowOrigin name.origin)
      (PrintStorage name.storage name.size)...
      (PrintInfo name.info)...
      (name.conDescrs->IfNull {[]} ?conDescrs {
         [("con:", Show (Size conDescrs))]
      })...
   ]
};

def PrintMeaning symbol meaning! {};

method PrintMeaning symbol meaning^AM_CONSTANT {
   ArgSeq [
      "let" (ShowSymbol symbol)
      (PrintNameDetails meaning.name)...
   ], ";"
};

method PrintMeaning symbol meaning^AM_VARIABLE {
   ArgSeq [
      "ref" (ShowSymbol symbol)
      (PrintNameDetails meaning.name)...
   ], ";"
};

method PrintMeaning symbol meaning^AM_MODULE {
   let name = meaning.name;
   ArgSeq [
      "module" (ShowSymbol symbol)
      (ShowQualifiedName name.origin)
   ]->AndInBraces (VSeq (Map meaning.exported ?(symbol', meaning') {
      PrintMeaning symbol' meaning'
   }) EDoc), ";"
};

method PrintMeaning symbol meaning^AM_DEFINED {
   let name = meaning.name;
   ArgSeq [
      "defined" (ShowSymbol symbol)
      (ShowOrigin name.origin)
   ], ";"
};

method PrintMeaning symbol meaning^AM_CCON {
   ArgSeq [
      "c_con" (ShowSymbol symbol)
      (PrintNameDetails meaning.name)...
   ], ";"
};

def PrintCValue value! {};

method PrintCValue value^(AE_NAME,AE_MODULE) {
   let name = value.name;
   [
      "name" (ShowOrigin name.origin)
      (case name.storage [
         #global {["global"]}
         #static {["static" (Show name.size)]}
         #none   {["none"]}
      ])...
   ]
};

method PrintCValue value^AE_DESCR_NAME {
   let name = value.con;
   [
      "c_con" (ShowOrigin name.origin)
      (name.conDescrs->IfNull {[]} ?conDescrs {
         [("con:", Show (Size conDescrs))]
      })...
      (Show value.case)
   ]
};

method PrintCValue value^AE_ENTRY_NAME {
   let name = value.obj;
   [
      "c_entry" (ShowOrigin name.origin)
      (PrintStorage name.storage name.size)...
      (PrintInfo name.info)...
      (case value.arity [
         #list {"_"}
         arity {Show arity}
      ])
   ]
};

def PrintCExport defA! {};

method PrintCExport defA^AD_CEXPORT {
   let (module, id) = defA.key;
   ArgSeq [
      "c_export"
      (if (CExportFile ~%Is Null | defA.code %Is Null)
         {"_"}
         {Show defA.code})
      (ShowQualifiedName module) (Show id)
   ], ";"
};

method PrintCExport defA^AD_CEXPORT_NAME {
   ArgSeq [
      "c_exportName" (ShowSymbol (Symbol defA.name))
      (if (CExportFile ~%Is Null | defA.value %Is Null)
         {["_"]}
         {PrintCValue defA.value})...
      (ShowQualifiedName defA.module)
   ], ";"
};

method PrintCExport defA^AD_CINCLUDE {
   ArgSeq [
      "c_include"
      (if (CExportFile ~%Is Null | defA.file %Is Null)
         {"_"}
         {Show defA.file})
      (ShowQualifiedName defA.module)
   ], ";"
};

def PrintInterface interface {
   VSeq [
      (ArgSeq ["interface" (ShowQualifiedName interface.moduleName)], ";")
      (Map interface.exported ?(symbol, meaning) {
         PrintMeaning symbol meaning
      })...
      (if (CExportFile %Is Null) {[]} {
         [(ArgSeq [
            "c_include" (Show CExportFile)
            (ShowQualifiedName interface.moduleName)
         ], ";")]
      })...
      (Map interface.exportedC PrintCExport)...
   ] EDoc
};

def SaveInterface interface filename {
   let text = local DefaultEncoding UTF8Encoding {PrintInterface interface}
      ->ShowDoc Inf Inf 3;
   let changed = try {
      text ~%IsEqual ReadLinesLazyFrom
         (OpenTextFile filename encoding:UTF8Encoding)
   } (->HasType IO_ERROR CODING_ERROR) {True};
   if changed {
      Using {CreateTextFile filename encoding:UTF8Encoding} ?file =>
      Each text (WriteLineTo file _)
   }
};

def ParseInterface tokens {
   var Input = tokens;
   def ParseError msg {FatalError Input.loc msg};
   def Skip() {Input = Input.next};

   var exportedC = [];

   def SkipSemicolon() {
      case Input.token [
         #';' {Skip()}
         _    {ParseError "Expected ';'"}
      ]
   };

   def ParseName() {
      case Input.token [
         name:name {Skip(); name}
         _         {ParseError "Expected a name"}
      ]
   };

   def ParseRestQualifiedName() {
      case Input.token [
         field:name {Skip(); name \ ParseRestQualifiedName()}
         _          {[]}
      ]
   };

   def ParseQualifiedName() {
      ParseName() \ ParseRestQualifiedName()
   };

   def ParseOrigin() {
      let [module... symbol] = ParseQualifiedName();
      symbol, module
   };

   def ParseNat() {
      case Input.token [
         lit:(value->HasType INT) {Skip(); value}
         _                        {ParseError "Expected an integer"}
      ]
   };

   def ParseInt() {
      case Input.token [
         #'-' {Skip(); -ParseNat()}
         _    {ParseNat()}
      ]
   };

   def ParseHeader() {
      case Input.token [
         name:#interface {
            Skip();
            let moduleName = ParseQualifiedName();
            SkipSemicolon();
            moduleName, Name #module
               origin:moduleName
               extension:"module"
               storage:#static
               isPublic:True
         }
         _ {ParseError "Expected interface header"}
      ]
   };

   def ParseStorage() {
      let storage = case Input.token [
         name:#static {Skip(); #static}
         name:#global {Skip(); #global}
         name:#none   {Skip(); #none}
         _            {ParseError "Expected storage kind"}
      ];
      let size = case storage [
         #static {ParseNat()}
         _       {Null}
      ];
      storage, size
   };

   def ParseRecord() {
      case Input.token [
         label:(kind & (#pure | #new)) {
            Skip();
            let (origin & (symbol, _)) = ParseOrigin();
            kind, Name symbol
               origin:origin
               extension:"descr"
               storage:#static
               isPublic:True
               size:#descr
         }
         _ {Null}
      ]
   };

   def ParseInfo() {
      case Input.token [
         name:type {
            case type [
               #smallInt    {Skip(); ISmallInt}
               #staticInt   {Skip(); IStaticInt}
               #fun {
                  Skip();
                  IFunction (if (Input.token %Is #any) {Null} {ParseName()})
                     (ParseInt()) (ParseNat()) (ParseRecord())
               }
               #var         {Skip(); IVar}
               #dynamic1    {Skip(); IDynamic}
               #dynamic0    {Skip(); IDynamicMayFail (ParseName())}
               #forward     {Skip(); IForward (ParseName())}
               #type        {Skip(); IType}
               #list        {Skip(); IList}
               _            {IUnknown}
            ]
         }
         _ {IUnknown}
      ]
   };

   def ParseNameDetails kind {
      let symbol = ParseName();
      let origin = ParseOrigin();
      let (storage, size) = ParseStorage();
      let info =  ParseInfo();
      let conDescrs = case Input.token [
         label:#con {Skip(); ParseNat()}
         _          {Null}
      ];
      SkipSemicolon();
      let name = Name symbol
         origin:origin
         extension:(Extension storage)
         storage:storage
         isPublic:True
         size:size
         info:info;
      if (info->HasType I_FUNCTION) {MakeEntriesFor name};
      conDescrs->IfNotNull (SetConDescrs name _);
      symbol, kind name
   };

   def ParseDef() {
      case Input.token [
         name:#let {Skip(); ParseNameDetails AMConstant}
         name:#ref {Skip(); ParseNameDetails AMVariable}
         name:#module {
            Skip();
            let symbol = ParseName();
            let origin = ParseQualifiedName();
            case Input.token [
               #'{' {Skip()}
               _    {ParseError "Expected '{'"}
            ];
            let exported = loop {
               case Input.token [
                  #'}' {Skip(); []}
                  _    {ParseDef() \ again()}
               ]
            };
            SkipSemicolon();
            let name = Name symbol
               origin:origin
               extension:"module"
               storage:#static
               isPublic:True
               size:(ModuleObjectSize exported);
            symbol, AMModule name exported
         }
         name:#defined {
            Skip();
            let symbol = ParseName();
            let origin = ParseQualifiedName();
            SkipSemicolon();
            let name = Name symbol
               origin:origin
               extension:"none"
               storage:#none
               isPublic:True;
            symbol, AMDefined name
         }
         name:#c_con {Skip(); ParseNameDetails AMCCon}
         name:#c_export {
            Skip();
            let code = case Input.token [
               lit:(code->HasType STRING) {Skip(); code}
               #any                       {Skip(); Null}
               _                          {ParseError "Expected C code"}
            ];
            let module = ParseQualifiedName();
            let id = ParseNat();
            SkipSemicolon();
            exportedC = ADCExport Null code (module, id) \ exportedC;
            Null
         }
         name:#c_exportName {
            Skip();
            let symbol = ParseName();
            let value = case Input.token [
               name:#name {
                  Skip();
                  let (origin & (symbol, _)) = ParseOrigin();
                  let (storage, size) = ParseStorage();
                  let name = Name symbol
                     origin:origin
                     extension:(Extension storage)
                     storage:storage
                     isPublic:True
                     size:size;
                  AEName Null name
               }
               name:#c_con {
                  Skip();
                  let (origin & (symbol, _)) = ParseOrigin();
                  let conDescrs = case Input.token [
                     label:#con {Skip(); ParseNat()}
                     _          {Null}
                  ];
                  let name = Name symbol
                     origin:origin
                     extension:"none"
                     storage:#none
                     isPublic:True;
                  conDescrs->IfNotNull (SetConDescrs name _);
                  let case = ParseNat();
                  AEDescrName Null name case
               }
               name:#c_entry {
                  Skip();
                  let (origin & (symbol, _)) = ParseOrigin();
                  let (storage, size) = ParseStorage();
                  let info =  ParseInfo();
                  let name = Name symbol
                     origin:origin
                     extension:(Extension storage)
                     storage:storage
                     isPublic:True
                     size:size
                     info:info;
                  if (info->HasType I_FUNCTION) {MakeEntriesFor name};
                  let arity = case Input.token [
                     #any {Skip(); #list}
                     _    {ParseNat()}
                  ];
                  AEEntryName name arity
               }
               #any {Skip(); Null}
               _ {ParseError "Expected a C value"}
            ];
            let module = ParseQualifiedName();
            SkipSemicolon();
            exportedC = ADCExportName Null (String symbol) value module \
               exportedC;
            Null
         }
         name:#c_include {
            Skip();
            let file = case Input.token [
               lit:(file->HasType STRING) {Skip(); file}
               #any                       {Skip(); Null}
               _                          {ParseError "Expected a filename"}
            ];
            let module = ParseQualifiedName();
            SkipSemicolon();
            exportedC = ADCInclude file module \ exportedC;
            Null
         }
         _ {ParseError "Expected a definition"}
      ]
   };

   def ParseDefs() {
      case Input.token [
         #eof {[]}
         _    {ParseDef()->IfNull ParseDefs ?def {def \ ParseDefs()}}
      ]
   };

   let (moduleName, name) = ParseHeader();
   let exported = ParseDefs();
   name.size = ModuleObjectSize exported;
   AInterface moduleName name exported (Reverse exportedC)
};

def EmptyInterface moduleName {
   let name = Name #module
      origin:moduleName
      extension:"module"
      storage:#static
      isPublic:True
      size:(ModuleObjectSize []);
   AInterface moduleName name [] []
};

def ModuleNameWithoutRoot moduleName root {
   if (moduleName->~BeginsWith root) {Null} =>
   case (Part moduleName (Size root)) [
      []              {Null}
      localModuleName {localModuleName}
   ]
};

def ModuleFilename [dir... file] ext path {
   [dir... (String file ext)]->SepBy "/"->String->FilenameInDir path
};

def FindInterface paths moduleName needed {
   loop paths [
      ((path, root)\rest) {
         moduleName->ModuleNameWithoutRoot root->IfNull {again rest}
            ?localModuleName =>
         let filenameKoi = ModuleFilename localModuleName ".koi" path;
         let file = try {OpenTextFile filenameKoi encoding:UTF8Encoding}
            (->HasType IO_ERROR) {Null};
         if [
            (file ~%Is Null) {
               if (Depend & needed) {AddDependency filenameKoi};
               let interface = Lex file filenameKoi->ParseInterface;
               Close file;
               interface
            }
            Depend {
               let filenameKo = ModuleFilename localModuleName ".ko" path;
               let file = try {OpenRawFile filenameKo}
                  (->HasType IO_ERROR) {Null};
               if (file ~%Is Null) {
                  if needed {AddDependency filenameKoi};
                  Close file;
                  EmptyInterface moduleName
               } {again rest}
            }
            _ {again rest}
         ]
      }
      [] {Null}
   ]
};

def CopyName name {
   Name name.symbol
      origin:name.origin
      extension:name.extension
      storage:name.storage
      isPublic:True
      size:name.size
      info:name.info
      real:False
      used:False
      descr:name.descr
      fastEntries:name.fastEntries
      listEntry:name.listEntry
      conDescrs:name.conDescrs
};

def CopyMeaning meaning! {};

method CopyMeaning meaning^AM_CONSTANT {
   AMConstant (CopyName meaning.name)
};
method CopyMeaning meaning^AM_VARIABLE {
   AMVariable (CopyName meaning.name)
};
method CopyMeaning meaning^AM_MODULE {
   AMModule (CopyName meaning.name) (CopyExported meaning.exported)
};
method CopyMeaning meaning^AM_DEFINED {
   AMDefined (CopyName meaning.name)
};
method CopyMeaning meaning^AM_CCON {
   AMCCon (CopyName meaning.name)
};

def CopyExported exported {
   Map exported ?(symbol, meaning) {
      symbol, CopyMeaning meaning
   }
};

let Modules = HashDict();

def GetInterface moduleName loc {
   GetOrSet Modules moduleName {
      FindInterface InterfaceDirs moduleName True
   }->IfNull {
      Error loc (String "Unknown module " (ShowQualifiedName moduleName));
      EmptyInterface moduleName
   }
};

def IsModuleDefined moduleName {
   GetOrSet Modules moduleName {
      FindInterface InterfaceDirs moduleName False
   }
};
