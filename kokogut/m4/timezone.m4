AC_DEFUN([KO_CHECK_TIMEZONE], [
   AC_CACHE_CHECK([for timezone variable],
      [ko_cv_have_timezone_var], [
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
         #include <time.h>
         extern long timezone;
      ]], [])],
         [ko_cv_have_timezone_var=yes],
         [ko_cv_have_timezone_var=no])
   ])
   if test "$ko_cv_have_timezone_var" = yes; then
      AC_DEFINE(KO_HAVE_TIMEZONE_VAR, 1,
         [Define to 1 if there is a timezone variable of type long.])
   fi
])
