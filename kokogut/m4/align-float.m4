AC_DEFUN([KO_CHECK_UNALIGNED_FLOATS], [
   AC_CHECK_SIZEOF(int)
   AC_CHECK_SIZEOF(long)
   AC_CHECK_SIZEOF(void *)
   AC_CACHE_CHECK([whether it's enough to align doubles to pointer size],
      [ko_cv_unaligned_floats], [
      AC_RUN_IFELSE([AC_LANG_PROGRAM([[
         #include <stdlib.h>
         #include <string.h>
         #if SIZEOF_VOID_P < SIZEOF_LONG
         typedef int ko_nint_t;
         #else
         typedef long ko_nint_t;
         #endif
         typedef double ko_float_t;
      ]], [[
         ko_nint_t *p1, *p2;
         p1 = malloc(sizeof(ko_nint_t) + sizeof(ko_float_t));
         if (p1 == NULL) return EXIT_FAILURE;
         *(ko_float_t *)(p1 + 1) = 42.0;
         p2 = malloc(sizeof(ko_nint_t) + sizeof(ko_float_t));
         if (p2 == NULL) return EXIT_FAILURE;
         memcpy(p2, p1, sizeof(ko_nint_t) + sizeof(ko_float_t));
         if (*(ko_float_t *)(p2 + 1) != 42.0) return EXIT_FAILURE;
         return EXIT_SUCCESS;
      ]])],
         [ko_cv_unaligned_floats=yes],
         [ko_cv_unaligned_floats=no],
         [ko_cv_unaligned_floats=unknown])
   ])
   if test "$ko_cv_unaligned_floats" = yes; then
      AC_DEFINE(KO_UNALIGNED_FLOATS, 1,
         [Define to 1 if it's enough to align doubles to pointer size])
   fi
])
