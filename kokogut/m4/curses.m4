AC_DEFUN([KO_CHECK_CURSES], [
   KO_HAVE_CURSES=no
   KO_CURSES_SWITCH=
   KO_CURSES_LINK=
   ko_save_CPPFLAGS=$CPPFLAGS
   if test -n "$KO_CURSES_INCLUDES"; then
      CPPFLAGS="$CPPFLAGS -I $KO_CURSES_INCLUDES"
   fi
   ko_save_LDFLAGS=$LDFLAGS
   if test -n "$KO_CURSES_LIBS"; then
      LDFLAGS="$LDFLAGS -L$KO_CURSES_LIBS"
   fi
   ko_have_curses_header=no
   if test "$KO_CURSES_CHARS" = narrow; then
      AC_CHECK_HEADERS([ncurses/curses.h curses.h],
         [ko_have_curses_header=yes])
   else
      AC_CHECK_HEADERS([ncursesw/curses.h ncurses/curses.h curses.h],
         [ko_have_curses_header=yes])
   fi
   if test "$ko_have_curses_header" = yes; then
      if test "$KO_CURSES_CHARS" != narrow; then
         AC_CHECK_LIB(ncursesw, wadd_wch, [
            KO_HAVE_CURSES=yes
            KO_CURSES_CHARS=maybe-wide
            KO_CURSES_LINK=-lncursesw
         ], [
            AC_CHECK_LIB(ncurses, wadd_wch, [
               KO_HAVE_CURSES=yes
               KO_CURSES_CHARS=maybe-wide
               KO_CURSES_LINK=-lncurses
            ], [
               AC_CHECK_LIB(curses, wadd_wch, [
                  KO_HAVE_CURSES=yes
                  KO_CURSES_CHARS=maybe-wide
                  KO_CURSES_LINK=-lcurses
               ])
            ])
         ])
      fi
      if test "$KO_CURSES_CHARS" = maybe-wide; then
         AC_CACHE_CHECK([whether wide character API has particular bugs fixed],
            [ko_cv_wide_chars_fixed], [
            AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
               #define _XOPEN_SOURCE_EXTENDED
               #if HAVE_NCURSESW_CURSES_H
                  #include <ncursesw/curses.h>
               #elif HAVE_NCURSES_CURSES_H
                  #include <ncurses/curses.h>
               #elif HAVE_CURSES_H
                  #include <curses.h>
               #else
                  #error Unknown curses header
               #endif
               #if NCURSES_VERSION_MAJOR == 5 && \
                  (NCURSES_VERSION_MINOR < 4 || \
                  (NCURSES_VERSION_MINOR == 4 && \
                  NCURSES_VERSION_PATCH < 20041002))
                  #error This version has broken wide character API
               #endif
            ]], [])],
               [ko_cv_wide_chars_fixed=yes],
               [ko_cv_wide_chars_fixed=no])
         ])
         if test "$ko_cv_wide_chars_fixed" = yes; then
            KO_CURSES_CHARS=wide
         fi
      fi
      if test "$KO_CURSES_CHARS" != wide; then
         AC_CHECK_LIB(ncurses, waddch, [
            KO_HAVE_CURSES=yes
            KO_CURSES_CHARS=narrow
            KO_CURSES_LINK=-lncurses
         ], [
            AC_CHECK_LIB(curses, waddch, [
               KO_HAVE_CURSES=yes
               KO_CURSES_CHARS=narrow
               KO_CURSES_LINK=-lcurses
            ])
         ])
      fi
   fi
   if test "$KO_HAVE_CURSES" = yes; then
      AC_CHECK_DECL(A_BOLD, , [KO_HAVE_CURSES=no], [
         #define _XOPEN_SOURCE_EXTENDED
         #if HAVE_NCURSESW_CURSES_H
            #include <ncursesw/curses.h>
         #elif HAVE_NCURSES_CURSES_H
            #include <ncurses/curses.h>
         #elif HAVE_CURSES_H
            #include <curses.h>
         #else
            #error Unknown curses header
         #endif
      ])
   fi
   if test "$KO_HAVE_CURSES" = yes; then
      ko_save_LIBS=$LIBS
      LIBS="$KO_CURSES_LINK $LIBS"
      AC_CACHE_CHECK([if keypad returns a result],
         [ko_cv_keypad_result], [
         AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
            #define _XOPEN_SOURCE_EXTENDED
            #if HAVE_NCURSESW_CURSES_H
               #include <ncursesw/curses.h>
            #elif HAVE_NCURSES_CURSES_H
               #include <ncurses/curses.h>
            #elif HAVE_CURSES_H
               #include <curses.h>
            #else
               #error Unknown curses header
            #endif
         ]], [[
            int result = keypad(stdscr, 0);
         ]])],
            [ko_cv_keypad_result=yes],
            [ko_cv_keypad_result=no])
      ])
      if test "$ko_cv_keypad_result" = yes; then
         AC_DEFINE(KO_KEYPAD_RESULT, 1,
            [Define to 1 if keypad returns a result.])
      fi
      AC_CACHE_CHECK([if nodelay returns a result],
         [ko_cv_nodelay_result], [
         AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
            #define _XOPEN_SOURCE_EXTENDED
            #if HAVE_NCURSESW_CURSES_H
               #include <ncursesw/curses.h>
            #elif HAVE_NCURSES_CURSES_H
               #include <ncurses/curses.h>
            #elif HAVE_CURSES_H
               #include <curses.h>
            #else
               #error Unknown curses header
            #endif
         ]], [[
            int result = nodelay(stdscr, 0);
         ]])],
            [ko_cv_nodelay_result=yes],
            [ko_cv_nodelay_result=no])
      ])
      if test "$ko_cv_nodelay_result" = yes; then
         AC_DEFINE(KO_NODELAY_RESULT, 1,
            [Define to 1 if nodelay returns a result.])
      fi
      AC_CHECK_FUNC([resize_term], [
         AC_CHECK_FUNC([ioctl], [
            AC_CHECK_DECL([TIOCGWINSZ], [
               AC_CHECK_TYPE([struct winsize], [
                  KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveResizeTerm"
               ], , [
                  #include <sys/ioctl.h>
                  #include <sys/termios.h>
               ])
            ], , [
               #include <sys/ioctl.h>
               #include <sys/termios.h>
            ])
         ])
      ])
      AC_CHECK_DECL(WA_BOLD, [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveExtendedAttr"
      ], , [
         #define _XOPEN_SOURCE_EXTENDED
         #if HAVE_NCURSESW_CURSES_H
            #include <ncursesw/curses.h>
         #elif HAVE_NCURSES_CURSES_H
            #include <ncurses/curses.h>
         #elif HAVE_CURSES_H
            #include <curses.h>
         #else
            #error Unknown curses header
         #endif
      ])
      AC_CHECK_FUNC([filter], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveFilter"
      ])
      AC_CHECK_FUNC([halfdelay], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveHalfDelay"
      ])
      AC_CHECK_FUNC([idcok], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveIDCOK"
      ])
      AC_CHECK_FUNC([immedok], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveImmedOK"
      ])
      AC_CHECK_FUNC([qiflush], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveQIFlush"
      ])
      AC_CHECK_FUNC([redrawwin], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveRedraw"
      ])
      AC_CHECK_FUNC([use_default_colors], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveDefaultColors"
      ])
      AC_CHECK_FUNC([assume_default_colors], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveAssumeDefaultColors"
      ])
      AC_CHECK_FUNC([can_change_color], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveCanChangeColor"
      ])
      AC_CHECK_FUNC([keyname], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveKeyName"
      ])
      AC_CHECK_FUNC([echochar], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveEchoChar"
      ])
      AC_CHECK_FUNC([addchnstr], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveAddChStr"
      ])
      AC_CHECK_FUNC([insnstr], [
         KO_CURSES_SWITCH="$KO_CURSES_SWITCH HaveInsStr"
      ])
      LIBS=$ko_save_LIBS
   fi
   CPPFLAGS=$ko_save_CPPFLAGS
   LDFLAGS=$ko_save_LDFLAGS
])
