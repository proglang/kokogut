AC_DEFUN([KO_CHECK_ATTR_REGPARM], [
   AC_CACHE_CHECK([if the regparm attribute is supported and effective],
      [ko_cv_have_attr_regparm], [
      ko_save_CFLAGS=$CFLAGS
      CFLAGS="$CFLAGS -Werror"
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
         __attribute__((regparm(1))) int test(int x) {
            return x + 1;
         }
      ]], [])],
         [ko_cv_have_attr_regparm=yes],
         [ko_cv_have_attr_regparm=no])
      CFLAGS=$ko_save_CFLAGS
   ])
   if test "$ko_cv_have_attr_regparm" = yes; then
      AC_DEFINE(KO_HAVE_ATTR_REGPARM, 1,
         [Define to 1 if the regparm attribute is supported and effective.])
   fi
])
