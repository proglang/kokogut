AC_DEFUN([KO_FIND_KOKOC_VERSION], [
   AC_CACHE_CHECK([Kogut compiler version], ko_cv_kokoc_version, [
      [ko_cv_kokoc_version=`$1 --version | sed 's/^[^-]*-//'`]
   ])
   KO_KOKOC_VERSION=$ko_cv_kokoc_version
   KO_KOKOC_MAJOR=`echo $KO_KOKOC_VERSION | sed ['s/\.[^.]*\.[^.]*$//']`
   KO_KOKOC_MINOR=`echo $KO_KOKOC_VERSION | sed ['s/^[^.]*\.//; s/\.[^.]*$//']`
   KO_KOKOC_MICRO=`echo $KO_KOKOC_VERSION | sed ['s/^[^.]*\.[^.]*\.//']`
])

AC_DEFUN([KO_CHECK_KOKOC_WORKS], [
   AC_CACHE_CHECK([if Kogut compiler works], ko_cv_kokoc_works, [
      echo 'WriteLine "Hello world";' >conftest.ko
      if _AC_EVAL_STDERR([$KO_COMPILER conftest.ko -o conftest]); then
         ko_cv_kokoc_works=yes
      else
         ko_cv_kokoc_works=no
      fi
   ])
   KO_KOKOC_WORKS=$ko_cv_kokoc_works
])
