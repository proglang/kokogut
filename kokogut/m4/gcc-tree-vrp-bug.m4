AC_DEFUN([KO_CHECK_GCC_TREE_VRP_BUG], [
   AC_CACHE_CHECK([whether $CC has a particular -ftree-vrp bug],
   ko_cv_gcc_tree_vrp_bug, [
      ko_save_CFLAGS=$CFLAGS
      CFLAGS="$CFLAGS -O -ftree-vrp"
      AC_RUN_IFELSE([AC_LANG_PROGRAM([[
         #include <stdlib.h>
         struct S {struct S *field;};
         struct S True, False, Z;
         static inline int f(void) {return 1;}
         static inline int g(struct S **obj) {
            return f() && *obj == &Z;
         }
         struct S **h(struct S **x) {
            if (x)
               return g(x) ? &True.field : &False.field;
            else
               return &True.field;
         }
         struct S **(*hptr)(struct S **x);
      ]], [[
         struct S obj;
         obj.field = 0;
         hptr = h;
         return hptr(&obj.field) == &True.field ? EXIT_SUCCESS : EXIT_FAILURE;
      ]])],
         [ko_cv_gcc_tree_vrp_bug=yes],
         [ko_cv_gcc_tree_vrp_bug=no],
         [ko_cv_gcc_tree_vrp_bug=unknown])
      CFLAGS=$ko_save_CFLAGS
   ])
   KO_GCC_TREE_VRP_BUG=$ko_cv_gcc_tree_vrp_bug
])
