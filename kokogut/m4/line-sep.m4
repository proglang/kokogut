AC_DEFUN([KO_CHECK_LINE_SEP], [
   AC_CACHE_CHECK([the default line separator], [ko_cv_line_sep], [
      ko_cv_line_sep=`$KO_PERL_PATH -e '
         open F, ">conftest.txt";
         print F "\n";
         close F;
         local $/ = undef;
         open F, "conftest.txt";
         binmode F;
         $newline = <F>;
         close F;
         if ($newline eq "\012") {print "lf"}
         elsif ($newline eq "\015") {print "cr"}
         elsif ($newline eq "\015\012") {print "crlf"}
         else {die "Strange line separator"}
         unlink "conftest.txt";
      '`
   ])
   if test -z "$ko_cv_line_sep"; then
      AC_MSG_ERROR([could not determine the default line separator])
   fi
   KO_LINE_SEP=$ko_cv_line_sep
])
