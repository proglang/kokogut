AC_DEFUN([KO_CHECK_BUILTIN], [
   AC_CACHE_CHECK([for __builtin_$1],
      [ko_cv_have_builtin_$1], [
      AC_LINK_IFELSE([AC_LANG_PROGRAM([], [[
         int x = __builtin_$1($2);
      ]])],
         [ko_cv_have_builtin_$1=yes],
         [ko_cv_have_builtin_$1=no])
   ])
   if test "$ko_cv_have_builtin_$1" = yes; then
      AC_DEFINE([KO_HAVE_BUILTIN_]translit([$1], a-z, A-Z), 1,
         [Define to 1 if the C compiler supports __bulitin_$1.])
   fi
])
