AC_DEFUN([KO_CHECK_BZIP2], [
   KO_HAVE_BZIP2=no
   KO_BZIP2_LINK=
   ko_save_CPPFLAGS=$CPPFLAGS
   if test -n "$KO_BZIP2_INCLUDES"; then
      CPPFLAGS="$CPPFLAGS -I $KO_BZIP2_INCLUDES"
   fi
   ko_save_LDFLAGS=$LDFLAGS
   if test -n "$KO_BZIP2_LIBS"; then
      LDFLAGS="$LDFLAGS -L$KO_BZIP2_LIBS"
   fi
   AC_CHECK_HEADERS([bzlib.h], [
      AC_CHECK_LIB(bz2, BZ2_bzCompress, [
         KO_HAVE_BZIP2=yes
         KO_BZIP2_LINK=-lbz2
      ], [
         AC_CHECK_LIB(bz2, bzCompress, [
            KO_HAVE_BZIP2=yes
            KO_BZIP2_LINK=-lbz2
            AC_DEFINE(KO_BZIP2_VERSION0, 1,
               [Define to 1 if bzip2 functions don't use the 'BZ2_' prefix])
         ])
      ])
   ])
   CPPFLAGS=$ko_save_CPPFLAGS
   LDFLAGS=$ko_save_LDFLAGS
])
