AC_DEFUN([KO_CHECK_SIGSAFE], [
   KO_HAVE_SIGSAFE=no
   KO_SIGSAFE_LINK=
   ko_save_CPPFLAGS=$CPPFLAGS
   if test -n "$KO_SIGSAFE_INCLUDES"; then
      CPPFLAGS="$CPPFLAGS -I $KO_SIGSAFE_INCLUDES"
   fi
   ko_save_LDFLAGS=$LDFLAGS
   if test -n "$KO_SIGSAFE_LIBS"; then
      LDFLAGS="$LDFLAGS -L$KO_SIGSAFE_LIBS"
   fi
   AC_CHECK_HEADERS([sigsafe.h], [
      AC_CHECK_LIB(sigsafe, sigsafe_install_handler, [
         KO_HAVE_SIGSAFE=yes
         KO_SIGSAFE_LINK=-lsigsafe
      ])
   ])
   CPPFLAGS=$ko_save_CPPFLAGS
   LDFLAGS=$ko_save_LDFLAGS
])
