AC_DEFUN([KO_FIND_PERL_VERSION], [
   AC_CACHE_CHECK([Perl major version], ko_cv_perl_major, [
      ko_cv_perl_major=`$KO_PERL_PATH -e '
         if (length $^V == 3) {
            print ord(substr($^V, 0, 1));
         }
         elsif ($^V =~ /^v?(\d+)\./) {
            print $[]1;
         }
         else {
            print "unknown";
         }
      '`
   ])
   KO_PERL_MAJOR=$ko_cv_perl_major
   AC_CACHE_CHECK([Python minor version], ko_cv_perl_minor, [
      ko_cv_perl_minor=`$KO_PERL_PATH -e '
         if (length $^V == 3) {
            print ord(substr($^V, 1, 1))
         }
         elsif ($^V =~ /^v?\d+\.(\d+)\./) {
            print $[]1;
         }
         else {
            print "unknown";
         }
      '`
   ])
   KO_PERL_MINOR=$ko_cv_perl_minor
])
