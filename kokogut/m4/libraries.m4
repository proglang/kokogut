AC_DEFUN([KO_C_LIBRARY_OPTIONS], [
   AC_ARG_WITH(translit([$1], A-Z, a-z),
      AS_HELP_STRING([--without-]translit([$1], A-Z, a-z), [$2]))
   AC_ARG_WITH(translit([$1], A-Z, a-z)[-includes],
      AS_HELP_STRING([--with-]translit([$1], A-Z, a-z)[-includes=DIR],
         translit([$1], A-Z, a-z)[ include directory]))
   KO_[]translit([$1], a-z, A-Z)_INCLUDES=$with_[]translit([$1], A-Z, a-z)_includes
   AC_ARG_WITH(translit([$1], A-Z, a-z)[-libs],
      AS_HELP_STRING([--with-]translit([$1], A-Z, a-z)[-libs=DIR],
         translit([$1], A-Z, a-z)[ library directory]))
   KO_[]translit([$1], a-z, A-Z)_LIBS=$with_[]translit([$1], A-Z, a-z)_libs
   AC_SUBST([KO_]translit([$1], a-z, A-Z)[_INCLUDES])
   AC_SUBST([KO_]translit([$1], a-z, A-Z)[_LIBS])
])

AC_DEFUN([KO_PKG_CONFIG], [
   AC_ARG_WITH(translit([$1], A-Z, a-z),
      AS_HELP_STRING([--without-]translit([$1], A-Z, a-z), [$2]))
   AC_SUBST([KO_]translit([$1], a-z, A-Z)[_CFLAGS])
   AC_SUBST([KO_]translit([$1], a-z, A-Z)[_LINK])
])

AC_DEFUN([KO_CHECK_PKG_CONFIG], [
   if pkg-config "$2"; then
      KO_HAVE_[]translit([$1], a-z, A-Z)=yes
      KO_[]translit([$1], a-z, A-Z)_CFLAGS=`pkg-config --cflags "$2"`
      KO_[]translit([$1], a-z, A-Z)_LINK=`pkg-config --libs "$2"`
   fi
])

AC_DEFUN([KO_BUILD_LIBRARY], [
   KO_[]translit([$1], a-z, A-Z)_SWITCH=
   if test "$with_[]translit([$1], A-Z, a-z)" = no; then
      AC_MSG_NOTICE([disabled building $1 library])
   else
      $2
      if test "$KO_HAVE_[]translit([$1], a-z, A-Z)" = yes; then
         AC_MSG_NOTICE([will build $1 library])
         KO_LIBRARIES="$KO_LIBRARIES $1"
         $3
      else
         if test "$with_[]translit([$1], A-Z, a-z)" = yes; then
            AC_MSG_ERROR([could not build requested $1 library])
         else
            AC_MSG_WARN([will not build $1 library])
         fi
      fi
   fi
   AC_SUBST([KO_HAVE_]translit([$1], a-z, A-Z))
   AC_SUBST([KO_]translit([$1], a-z, A-Z)[_LINK])
   AC_SUBST([KO_]translit([$1], a-z, A-Z)[_SWITCH])
])
