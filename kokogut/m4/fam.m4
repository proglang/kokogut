AC_DEFUN([KO_CHECK_FAM], [
   KO_HAVE_FAM=no
   KO_FAM_LINK=
   ko_save_CPPFLAGS=$CPPFLAGS
   if test -n "$KO_FAM_INCLUDES"; then
      CPPFLAGS="$CPPFLAGS -I $KO_FAM_INCLUDES"
   fi
   ko_save_LDFLAGS=$LDFLAGS
   if test -n "$KO_FAM_LIBS"; then
      LDFLAGS="$LDFLAGS -L$KO_FAM_LIBS"
   fi
   AC_CHECK_HEADERS([fam.h], [
      AC_CHECK_LIB([fam], [FAMOpen], [
         KO_HAVE_FAM=yes
         KO_FAM_LINK=-lfam
      ])
   ])
   CPPFLAGS=$ko_save_CPPFLAGS
   LDFLAGS=$ko_save_LDFLAGS
])

AC_DEFUN([KO_CHECK_GAMIN], [
   AC_CHECK_LIB([fam], [FAMNoExists], [
      KO_FAM_SWITCH="$KO_FAM_SWITCH HaveGamin"
   ])
])
