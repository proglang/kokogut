AC_DEFUN([KO_CHECK_ATTR_PACKED], [
   AC_CACHE_CHECK([for __attribute__((packed))],
      [ko_cv_have_attr_packed], [
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
         struct foo {
            int header;
            double value __attribute__((packed));
         };
      ]], [])],
         [ko_cv_have_attr_packed=yes],
         [ko_cv_have_attr_packed=no])
   ])
   if test "$ko_cv_have_attr_packed" = yes; then
      AC_DEFINE(KO_HAVE_ATTR_PACKED, 1,
         [Define to 1 if the C compiler supports __attribute__((packed)).])
   fi
])
