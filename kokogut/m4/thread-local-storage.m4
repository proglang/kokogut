AC_DEFUN([KO_CHECK_THREAD_LOCAL_STORAGE], [
   AC_CACHE_CHECK([whether __thread storage class is supported],
      [ko_cv_thread_local_storage], [
      AC_RUN_IFELSE([AC_LANG_PROGRAM([[
         #include <pthread.h>
         #include <stdlib.h>

         static __thread int x;

         static void *thread_body(void *arg) {
            (void)arg;
            x = 2;
            return NULL;
         }
      ]], [[
         pthread_t thread_id;
         x = 1;
         pthread_create(&thread_id, NULL, thread_body, NULL);
         pthread_join(thread_id, NULL);
         return x == 1 ? EXIT_SUCCESS : EXIT_FAILURE;
      ]])],
         [ko_cv_thread_local_storage=yes],
         [ko_cv_thread_local_storage=no],
         [ko_cv_thread_local_storage=unknown])
   ])
   KO_THREAD_LOCAL_STORAGE=$ko_cv_thread_local_storage
])
