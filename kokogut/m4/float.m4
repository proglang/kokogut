AC_DEFUN([KO_CHECK_MANTISSA_BITS], [
   AC_CACHE_CHECK([number of mantissa bits in $1],
      [AS_TR_SH(ko_cv_mantissa_bits_$1)], [
      AC_RUN_IFELSE([AC_LANG_PROGRAM([[
         #include <stdlib.h>
         #include <stdio.h>
      ]], [[
         $1 one = 1;
         $1 bit = 1;
         int num_bits = 0;
         FILE *f;
         for (;;) {
            bit /= 2;
            if (one + bit == one) break;
            ++num_bits;
         }
         f = fopen("conftestval", "w");
         if (f == NULL) return EXIT_FAILURE;
         fprintf(f, "%d\n", num_bits);
         fclose(f);
         return EXIT_SUCCESS;
      ]])],
         [AS_TR_SH(ko_cv_mantissa_bits_$1)=`cat conftestval`],
         [AS_TR_SH(ko_cv_mantissa_bits_$1)=52],
         [AS_TR_SH(ko_cv_mantissa_bits_$1)=52])
   ])
   AC_DEFINE_UNQUOTED(AS_TR_CPP(ko_c_$1_mantissa_bits),
      $AS_TR_SH(ko_cv_mantissa_bits_$1),
      [number of mantissa bits in $1, excluding the implicit highest bit])
])

AC_DEFUN([KO_CHECK_EXPONENT_BITS], [
   AC_CACHE_CHECK([number of exponent bits in $1],
      [AS_TR_SH(ko_cv_exponent_bits_$1)], [
      AC_RUN_IFELSE([AC_LANG_PROGRAM([[
         #include <stdlib.h>
         #include <stdio.h>
      ]], [[
         $1 bit = 2;
         int num_bits = 2;
         FILE *f;
         for (;;) {
            bit *= bit * 2;
            if (bit * 2 == bit) break;
            ++num_bits;
         }
         f = fopen("conftestval", "w");
         if (f == NULL) return EXIT_FAILURE;
         fprintf(f, "%d\n", num_bits);
         fclose(f);
         return EXIT_SUCCESS;
      ]])],
         [AS_TR_SH(ko_cv_exponent_bits_$1)=`cat conftestval`],
         [AS_TR_SH(ko_cv_exponent_bits_$1)=52],
         [AS_TR_SH(ko_cv_exponent_bits_$1)=52])
   ])
   AC_DEFINE_UNQUOTED(AS_TR_CPP(ko_c_$1_exponent_bits),
      $AS_TR_SH(ko_cv_exponent_bits_$1),
      [number of exponent bits in $1])
])

AC_DEFUN([KO_MANTISSA_TYPE], [
   [#]if AS_TR_CPP(ko_c_$1_mantissa_bits) > 64
      #error Mantissa larger than 64 bits is not supported
   #endif
   [#]if AS_TR_CPP(ko_c_$1_exponent_bits) > 31
      #error Exponent larger than 31 bits is not supported
   #endif
   /* The following mimics how ko_uint64_t is defined. */
   #if HAVE_INTTYPES_H
      #include <inttypes.h>
   #endif
   #if HAVE_INTTYPES_H && HAVE_INT64_T
      typedef uint64_t ko_float_mantissa_t;
   #elif SIZEOF_LONG == 8
      typedef unsigned long ko_float_mantissa_t;
   #elif HAVE_LONG_LONG && SIZEOF_LONG_LONG == 8
      typedef unsigned long long ko_float_mantissa_t;
   #elif HAVE_HAVE___INT64_T
      typedef unsigned long long ko_float_mantissa_t;
   #else
      #error A 64 bit integer type is required
   #endif
])

AC_DEFUN([KO_CHECK_KNOWN_FLOAT_REPR_LE], [
   AC_CACHE_CHECK([for IEEE-754 $1 representation (Little Endian)],
      [AS_TR_SH(ko_cv_known_float_repr_le_$1)], [
      AC_RUN_IFELSE([AC_LANG_PROGRAM([
         #include <stdlib.h>
         KO_MANTISSA_TYPE([$1])
         union ko_float_representation {
            $1 ko_value;
            struct {
               ko_float_mantissa_t ko_mantissa :
                  AS_TR_CPP(ko_c_$1_mantissa_bits);
               unsigned ko_exponent : AS_TR_CPP(ko_c_$1_exponent_bits);
               unsigned ko_sign : 1;
            } ko_repr;
         };
      ], [
         union ko_float_representation x;
         x.ko_value = 42;
         if (x.ko_repr.ko_mantissa == (ko_float_mantissa_t)(21 - (1 << 4))
               << (AS_TR_CPP(ko_c_$1_mantissa_bits) - 4) &&
            x.ko_repr.ko_exponent == 5
               + (1 << (AS_TR_CPP(ko_c_$1_exponent_bits) - 1)) - 1 &&
            x.ko_repr.ko_sign == 0)
            return EXIT_SUCCESS;
         else
            return EXIT_FAILURE;
      ])],
         [AS_TR_SH(ko_cv_known_float_repr_le_$1)=yes],
         [AS_TR_SH(ko_cv_known_float_repr_le_$1)=no],
         [AS_TR_SH(ko_cv_known_float_repr_le_$1)=no])
   ])
   if test "$AS_TR_SH(ko_cv_known_float_repr_le_$1)" = yes; then
      AC_DEFINE(AS_TR_CPP(ko_known_c_$1_repr_le), 1,
         [$1 has IEEE-754 representation (Little Endian)])
   fi
])

AC_DEFUN([KO_CHECK_KNOWN_FLOAT_REPR_BE], [
   AC_CACHE_CHECK([for IEEE-754 $1 representation (Big Endian)],
      [AS_TR_SH(ko_cv_known_float_repr_be_$1)], [
      AC_RUN_IFELSE([AC_LANG_PROGRAM([
         #include <stdlib.h>
         KO_MANTISSA_TYPE([$1])
         union ko_float_representation {
            $1 ko_value;
            struct {
               unsigned ko_sign : 1;
               unsigned ko_exponent : AS_TR_CPP(ko_c_$1_exponent_bits);
               ko_float_mantissa_t ko_mantissa :
                  AS_TR_CPP(ko_c_$1_mantissa_bits);
            } ko_repr;
         };
      ], [
         union ko_float_representation x;
         x.ko_value = 42;
         if (x.ko_repr.ko_mantissa == (ko_float_mantissa_t)(21 - (1 << 4))
               << (AS_TR_CPP(ko_c_$1_mantissa_bits) - 4) &&
            x.ko_repr.ko_exponent == 5
               + (1 << (AS_TR_CPP(ko_c_$1_exponent_bits) - 1)) - 1 &&
            x.ko_repr.ko_sign == 0)
            return EXIT_SUCCESS;
         else
            return EXIT_FAILURE;
      ])],
         [AS_TR_SH(ko_cv_known_float_repr_be_$1)=yes],
         [AS_TR_SH(ko_cv_known_float_repr_be_$1)=no],
         [AS_TR_SH(ko_cv_known_float_repr_be_$1)=no])
   ])
   if test "$AS_TR_SH(ko_cv_known_float_repr_be_$1)" = yes; then
      AC_DEFINE(AS_TR_CPP(ko_known_c_$1_repr_be), 1,
         [$1 has IEEE-754 representation (Big Endian)])
   fi
])

AC_DEFUN([KO_CHECK_KNOWN_FLOAT_REPR], [
   AC_CHECK_HEADERS([inttypes.h])
   AC_CHECK_TYPES([long long, int64_t, __int64_t])
   AC_CHECK_SIZEOF([long long])
   KO_CHECK_KNOWN_FLOAT_REPR_LE([$1])
   KO_CHECK_KNOWN_FLOAT_REPR_BE([$1])
])
