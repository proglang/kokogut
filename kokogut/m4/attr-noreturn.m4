AC_DEFUN([KO_CHECK_ATTR_NORETURN], [
   AC_CACHE_CHECK([for __attribute__((noreturn))],
      [ko_cv_have_attr_noreturn], [
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
         __attribute__((noreturn)) void (*foo)(void);
      ]], [])],
         [ko_cv_have_attr_noreturn=yes],
         [ko_cv_have_attr_noreturn=no])
   ])
   if test "$ko_cv_have_attr_noreturn" = yes; then
      AC_DEFINE(KO_HAVE_ATTR_NORETURN, 1,
         [Define to 1 if the C compiler supports __attribute__((noreturn)).])
   fi
])
