AC_DEFUN([KO_CHECK_NO_UNIT_AT_A_TIME], [
   AC_MSG_CHECKING([for $CC option -fno-unit-at-a-time])
   ko_save_CFLAGS=$CFLAGS
   CFLAGS="$CFLAGS -fno-unit-at-a-time"
   ko_save_c_werror_flag=$ac_c_werror_flag
   ac_c_werror_flag=yes
   AC_COMPILE_IFELSE([AC_LANG_PROGRAM([], [])], [
      AC_MSG_RESULT([yes])
      CKOFLAGS="$CKOFLAGS -fno-unit-at-a-time"
   ], [
      AC_MSG_RESULT([no])
   ])
   CFLAGS=$ko_save_CFLAGS
   ac_c_werror_flag=$ko_save_c_werror_flag
])
