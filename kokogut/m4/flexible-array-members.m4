AC_DEFUN([KO_CHECK_FLEXIBLE_ARRAY_MEMBERS], [
   AC_CACHE_CHECK([for statically allocated flexible array members],
   ko_cv_have_flexible_array_members, [
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
         struct {char header; char fields[];} obj = {0, {1, 2}};
         int check[(int)sizeof(obj) - 2];
      ]], [])],
         [ko_cv_have_flexible_array_members=yes],
         [ko_cv_have_flexible_array_members=no])
   ])
   if test "$ko_cv_have_flexible_array_members" = yes; then
      AC_DEFINE(KO_HAVE_FLEXIBLE_ARRAY_MEMBERS, 1,
         [Define to 1 if the C compiler supports statically allocated flexible array members.])
   fi
])
