AC_DEFUN([KO_CHECK_GZIP], [
   KO_HAVE_GZIP=no
   KO_GZIP_LINK=
   ko_save_CPPFLAGS=$CPPFLAGS
   if test -n "$KO_GZIP_INCLUDES"; then
      CPPFLAGS="$CPPFLAGS -I $KO_GZIP_INCLUDES"
   fi
   ko_save_LDFLAGS=$LDFLAGS
   if test -n "$KO_GZIP_LIBS"; then
      LDFLAGS="$LDFLAGS -L$KO_GZIP_LIBS"
   fi
   AC_CHECK_HEADERS([zlib.h], [
      AC_CHECK_LIB(z, inflate, [
         KO_HAVE_GZIP=yes
         KO_GZIP_LINK=-lz
      ])
   ])
   CPPFLAGS=$ko_save_CPPFLAGS
   LDFLAGS=$ko_save_LDFLAGS
])
