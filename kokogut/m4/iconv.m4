AC_DEFUN([KO_CHECK_ICONV], [
   KO_HAVE_ICONV=no
   KO_ICONV_LINK=
   ko_save_CPPFLAGS=$CPPFLAGS
   if test -n "$KO_ICONV_INCLUDES"; then
      CPPFLAGS="$CPPFLAGS -I $KO_ICONV_INCLUDES"
   fi
   ko_save_LDFLAGS=$LDFLAGS
   if test -n "$KO_ICONV_LIBS"; then
      LDFLAGS="$LDFLAGS -L$KO_ICONV_LIBS"
   fi
   AC_CHECK_HEADERS([iconv.h], [
      AC_MSG_CHECKING([for library containing iconv])
      AC_LINK_IFELSE([AC_LANG_PROGRAM([[
         #include <iconv.h>
      ]], [[
         iconv_open ("", "");
      ]])], [
         AC_MSG_RESULT([none required])
         KO_HAVE_ICONV=yes
      ], [
         ko_save_LIBS=$LIBS
         LIBS="-liconv $LIBS"
         AC_LINK_IFELSE([AC_LANG_PROGRAM([[
            #include <iconv.h>
         ]], [[
            iconv_open ("", "");
         ]])], [
            AC_MSG_RESULT([-liconv])
            KO_HAVE_ICONV=yes
            KO_ICONV_LINK=-liconv
         ], [
            AC_MSG_RESULT([not found])
         ])
         LIBS=$ko_save_LIBS
      ])
   ])
   CPPFLAGS=$ko_save_CPPFLAGS
   LDFLAGS=$ko_save_LDFLAGS
])

AC_DEFUN([KO_RECOGNIZE_ICONV_ENCODING], [
   ko_encoding_var=ko_cv_encoding_`echo $1 | tr - _`
   AC_CACHE_CHECK([what encoding iconv understands as $1],
      $ko_encoding_var, [
      AC_RUN_IFELSE([AC_LANG_PROGRAM([[
         #include <stdlib.h>
         #include <stdio.h>
         #include <iconv.h>
         #define SIZE 100
         static const char *iso_8859_1_names[] = {
            "ISO-8859-1", "ISO8859-1", NULL
         };

         static int found (const char *encoding) {
            FILE *f;
            f = fopen("conftestval", "w");
            if (!f) return EXIT_FAILURE;
            fprintf(f, "%s\n", encoding);
            fclose(f);
            return EXIT_SUCCESS;
         }
      ]], [[
         const char **name;
         iconv_t cd;
         char *inbuf = "\300";
         size_t inbytesleft = 1;
         char *outbuf = malloc(SIZE);
         #define OUT_WORD8 ((unsigned char *)outbuf)
         #define OUT_WORD32 ((unsigned int *)outbuf)
         size_t outbytesleft = SIZE;
         size_t res;

         for (name = iso_8859_1_names; *name != NULL; name++) {
            cd = iconv_open("$1", *name);
            if (cd == (iconv_t)-1) continue;
            res = iconv(cd, &inbuf, &inbytesleft, &outbuf, &outbytesleft);
            iconv_close(cd);
            if (res == (size_t)-1) continue;
            switch (SIZE - outbytesleft) {
               case 2:
                  if (OUT_WORD8[-2] == 0xC3 &&
                      OUT_WORD8[-1] == 0x80)
                     return found("UTF-8");
                  break;
               case 4:
                  if (OUT_WORD32[-1] == 0x000000C0)
                     return found("UCS-4-INTERNAL");
                  else
                  if (OUT_WORD32[-1] == 0xC0000000)
                     return found("UCS-4-SWAPPED");
                  break;
            }
         }
         return EXIT_FAILURE;
      ]])],
         [eval $ko_encoding_var=`cat conftestval`],
         [eval $ko_encoding_var=none],
         [eval $ko_encoding_var=none])
   ])
])

AC_DEFUN([KO_FIND_ICONV_NAME_UCS_4_INTERNAL], [
   AC_CACHE_VAL([ko_cv_iconv_name_ucs_4_internal], [
      ko_cv_iconv_name_ucs_4_internal=none
      ko_save_CPPFLAGS=$CPPFLAGS
      if test -n "$KO_ICONV_INCLUDES"; then
         CPPFLAGS="$CPPFLAGS -I $KO_ICONV_INCLUDES"
      fi
      ko_save_LDFLAGS=$LDFLAGS
      if test -n "$KO_ICONV_LIBS"; then
         LDFLAGS="$LDFLAGS -L$KO_ICONV_LIBS"
      fi
      ko_save_LIBS=$LIBS
      LIBS="$KO_ICONV_LINK $LIBS"
      for iconv_encoding in wchar_t UCS-4-INTERNAL UCS-4BE UCS-4LE UCS-4 \
        UTF-32BE UTF-32LE UTF-32; do
         KO_RECOGNIZE_ICONV_ENCODING($iconv_encoding)
         eval encoding=\$ko_cv_encoding_`echo $iconv_encoding | tr - _`
         if test "$encoding" == UCS-4-INTERNAL; then
            ko_cv_iconv_name_ucs_4_internal=$iconv_encoding
            break
         fi
      done
      CPPFLAGS=$ko_save_CPPFLAGS
      LDFLAGS=$ko_save_LDFLAGS
      LIBS=$ko_save_LIBS
   ])
   if test "$ko_cv_iconv_name_ucs_4_internal" != none; then
      AC_DEFINE_UNQUOTED(ICONV_NAME_UCS_4_INTERNAL,
         "$iconv_encoding",
         [Define to iconv name for UCS-4-INTERNAL encoding if supported.])
   fi
])

AC_DEFUN([KO_CHECK_ICONV_WITH_LANGINFO], [
   AC_CACHE_CHECK([whether iconv accepts encoding name returned by langinfo],
      [ko_cv_iconv_with_langinfo], [
      ko_cv_iconv_with_langinfo=none
      ko_save_CPPFLAGS=$CPPFLAGS
      if test -n "$KO_ICONV_INCLUDES"; then
         CPPFLAGS="$CPPFLAGS -I $KO_ICONV_INCLUDES"
      fi
      ko_save_LDFLAGS=$LDFLAGS
      if test -n "$KO_ICONV_LIBS"; then
         LDFLAGS="$LDFLAGS -L$KO_ICONV_LIBS"
      fi
      ko_save_LIBS=$LIBS
      LIBS="$KO_ICONV_LINK $LIBS"
      AC_RUN_IFELSE([AC_LANG_PROGRAM([[
         #include <stdlib.h>
         #include <locale.h>
         #include <langinfo.h>
         #include <iconv.h>

         static const char *iso_8859_1_names[] = {
            "ISO-8859-1", "ISO8859-1", NULL
         };
      ]], [[
         const char *encoding, **name;
         setlocale(LC_CTYPE, "");
         encoding = nl_langinfo(CODESET);
         for (name = iso_8859_1_names; *name != NULL; name++) {
            iconv_t cd = iconv_open(encoding, *name);
            if (cd == (iconv_t)-1) continue;
            iconv_close(cd);
            return EXIT_SUCCESS;
         }
         return EXIT_FAILURE;
      ]])],
         [ko_cv_iconv_with_langinfo=yes],
         [ko_cv_iconv_with_langinfo=no],
         [ko_cv_iconv_with_langinfo=yes])
      CPPFLAGS=$ko_save_CPPFLAGS
      LDFLAGS=$ko_save_LDFLAGS
      LIBS=$ko_save_LIBS
   ])
   KO_ICONV_WITH_LANGINFO=$ko_cv_iconv_with_langinfo
])
