AC_DEFUN([KO_CHECK_GMP], [
   KO_HAVE_GMP=no
   KO_GMP_LINK=
   ko_save_CPPFLAGS=$CPPFLAGS
   if test -n "$KO_GMP_INCLUDES"; then
      CPPFLAGS="$CPPFLAGS -I $KO_GMP_INCLUDES"
   fi
   ko_save_LDFLAGS=$LDFLAGS
   if test -n "$KO_GMP_LIBS"; then
      LDFLAGS="$LDFLAGS -L$KO_GMP_LIBS"
   fi
   AC_CHECK_HEADERS([gmp.h], [
      AC_CHECK_LIB(gmp, __gmpz_init, [
         KO_HAVE_GMP=yes
         KO_GMP_LINK=-lgmp
      ], [
         AC_CHECK_LIB(gmp, mpz_init, [
            KO_HAVE_GMP=yes
            KO_GMP_LINK=-lgmp
         ], [
            AC_CHECK_LIB(gmp2, mpz_init, [
               KO_HAVE_GMP=yes
               KO_GMP_LINK=-lgmp2
            ])
         ])
      ])
   ])
   CPPFLAGS=$ko_save_CPPFLAGS
   LDFLAGS=$ko_save_LDFLAGS
])

AC_DEFUN([KO_CHECK_MPZ_GETLIMBN], [
   AC_CACHE_CHECK([whether mpz_getlimbn works for negative numbers],
      [ko_cv_mpz_getlimbn], [
      AC_RUN_IFELSE([AC_LANG_PROGRAM([[
         #include <stdlib.h>
         #include <gmp.h>
      ]], [[
         mpz_t x;
         int result;
         mpz_init_set_si(x, -1);
         result = mpz_getlimbn(x, 0) == 1;
         mpz_clear(x);
         return result ? EXIT_SUCCESS : EXIT_FAILURE;
      ]])],
         [ko_cv_mpz_getlimbn=yes],
         [ko_cv_mpz_getlimbn=no],
         [ko_cv_mpz_getlimbn=unknown])
   ])
   if test "$ko_cv_mpz_getlimbn" != no; then
      AC_DEFINE(KO_HAVE_MPZ_GETLIMBN, 1,
         [Define to 1 if mpz_getlimbn works for negative numbers.])
   fi
])
