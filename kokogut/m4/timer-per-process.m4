AC_DEFUN([KO_CHECK_TIMER_CREATED_PER_PROCESS], [
   AC_CACHE_CHECK([whether timer_create works per process],
      [ko_cv_timer_created_per_process], [
      AC_RUN_IFELSE([AC_LANG_PROGRAM([[
         #include <pthread.h>
         #include <signal.h>
         #include <sys/time.h>
         #include <stdlib.h>

         #if KO_USE_PTHREADS && HAVE_PTHREAD_SIGMASK
         # define ko_sigmask pthread_sigmask
         #else
         # define ko_sigmask sigprocmask
         #endif

         static sigset_t timer_signal_set;
         static timer_t timer_id;
         static volatile int result = 0;

         static void handler(int sig) {result = 1;}

         static void *thread_body(void *arg) {
            struct itimerspec enabled_timer;
            struct timeval t0, t;

            enabled_timer.it_interval.tv_sec = 0;
            enabled_timer.it_interval.tv_nsec = 1000000;
            enabled_timer.it_value.tv_sec = 0;
            enabled_timer.it_value.tv_nsec = 1000000;
            timer_settime(timer_id, 0, &enabled_timer, NULL);

            ko_sigmask(SIG_UNBLOCK, &timer_signal_set, NULL);
            gettimeofday(&t0, NULL);
            ++t0.tv_sec;
            while (!result) {
               gettimeofday(&t, NULL);
               if (t.tv_sec > t0.tv_sec ||
                   (t.tv_sec == t0.tv_sec && t.tv_usec > t0.tv_usec))
                  break;
            }
            ko_sigmask(SIG_BLOCK, &timer_signal_set, NULL);
            return NULL;
         }
      ]], [[
         struct sigaction action;
         struct sigevent event;
         pthread_t thread_id;

         sigemptyset(&timer_signal_set);
         sigaddset(&timer_signal_set, SIGALRM);
         ko_sigmask(SIG_BLOCK, &timer_signal_set, NULL);

         action.sa_handler = handler;
         sigfillset(&action.sa_mask);
         action.sa_flags = 0;
         sigaction(SIGALRM, &action, NULL);

         event.sigev_notify = SIGEV_SIGNAL;
         event.sigev_signo = SIGALRM;
         timer_create(CLOCK_REALTIME, &event, &timer_id);

         pthread_create(&thread_id, NULL, thread_body, NULL);
         pthread_join(thread_id, NULL);
         return result ? EXIT_SUCCESS : EXIT_FAILURE;
      ]])],
         [ko_cv_timer_created_per_process=yes],
         [ko_cv_timer_created_per_process=no],
         [ko_cv_timer_created_per_process=unknown])
   ])
   KO_TIMER_CREATED_PER_PROCESS=$ko_cv_timer_created_per_process
])

AC_DEFUN([KO_CHECK_TIMER_ENABLED_PER_PROCESS], [
   AC_CACHE_CHECK([whether timer is enabled per process],
      [ko_cv_timer_enabled_per_process], [
      AC_RUN_IFELSE([AC_LANG_PROGRAM([[
         #include <pthread.h>
         #include <signal.h>
         #include <sys/time.h>
         #include <stdlib.h>

         #if KO_USE_PTHREADS && HAVE_PTHREAD_SIGMASK
         # define ko_sigmask pthread_sigmask
         #else
         # define ko_sigmask sigprocmask
         #endif

         static sigset_t timer_signal_set;
         static volatile int result = 0;

         static void handler(int sig) {result = 1;}

         static void *thread_body(void *arg) {
            struct timeval t0, t;
            ko_sigmask(SIG_UNBLOCK, &timer_signal_set, NULL);
            gettimeofday(&t0, NULL);
            ++t0.tv_sec;
            while (!result) {
               gettimeofday(&t, NULL);
               if (t.tv_sec > t0.tv_sec ||
                   (t.tv_sec == t0.tv_sec && t.tv_usec > t0.tv_usec))
                  break;
            }
            ko_sigmask(SIG_BLOCK, &timer_signal_set, NULL);
            return NULL;
         }
      ]], [[
         struct sigaction action;
         #if HAVE_TIMER_CREATE
            struct sigevent event;
            timer_t timer_id;
            struct itimerspec enabled_timer;
         #else
            struct itimerval enabled_timer;
         #endif
         pthread_t thread_id;

         sigemptyset(&timer_signal_set);
         sigaddset(&timer_signal_set, SIGALRM);
         ko_sigmask(SIG_BLOCK, &timer_signal_set, NULL);

         action.sa_handler = handler;
         sigfillset(&action.sa_mask);
         action.sa_flags = 0;
         sigaction(SIGALRM, &action, NULL);

         #if HAVE_TIMER_CREATE
            event.sigev_notify = SIGEV_SIGNAL;
            event.sigev_signo = SIGALRM;
            timer_create(CLOCK_REALTIME, &event, &timer_id);

            enabled_timer.it_interval.tv_sec = 0;
            enabled_timer.it_interval.tv_nsec = 1000000;
            enabled_timer.it_value.tv_sec = 0;
            enabled_timer.it_value.tv_nsec = 1000000;
            timer_settime(timer_id, 0, &enabled_timer, NULL);
         #else
            enabled_timer.it_interval.tv_sec = 0;
            enabled_timer.it_interval.tv_usec = 1000;
            enabled_timer.it_value.tv_sec = 0;
            enabled_timer.it_value.tv_usec = 1000;
            setitimer(ITIMER_REAL, &enabled_timer, NULL);
         #endif

         pthread_create(&thread_id, NULL, thread_body, NULL);
         pthread_join(thread_id, NULL);
         return result ? EXIT_SUCCESS : EXIT_FAILURE;
      ]])],
         [ko_cv_timer_enabled_per_process=yes],
         [ko_cv_timer_enabled_per_process=no],
         [ko_cv_timer_enabled_per_process=unknown])
   ])
   KO_TIMER_ENABLED_PER_PROCESS=$ko_cv_timer_enabled_per_process
])
