AC_DEFUN([KO_CHECK_SIGNED_WRAP], [
   AC_MSG_CHECKING([for $CC option -fwrapv])
   ko_save_CFLAGS=$CFLAGS
   CFLAGS="$CFLAGS -fwrapv"
   ko_save_c_werror_flag=$ac_c_werror_flag
   ac_c_werror_flag=yes
   AC_COMPILE_IFELSE([AC_LANG_PROGRAM([], [])], [
      AC_MSG_RESULT([yes])
   ], [
      AC_MSG_RESULT([no])
      CFLAGS=$ko_save_CFLAGS
   ])
   ac_c_werror_flag=$ko_save_c_werror_flag

   AC_MSG_CHECKING([for $CXX option -fwrapv])
   AC_LANG_PUSH([C++])
   ko_save_CXXFLAGS=$CXXFLAGS
   CXXFLAGS="$CXXFLAGS -fwrapv"
   ko_save_cxx_werror_flag=$ac_cxx_werror_flag
   ac_cxx_werror_flag=yes
   AC_COMPILE_IFELSE([AC_LANG_PROGRAM([], [])], [
      AC_MSG_RESULT([yes])
   ], [
      AC_MSG_RESULT([no])
      CXXFLAGS=$ko_save_CXXFLAGS
   ])
   ac_cxx_werror_flag=$ko_save_cxx_werror_flag
   AC_LANG_POP([C++])
])
