AC_DEFUN([KO_CHECK_UNSETENV_RESULT], [
   AC_CACHE_CHECK([if unsetenv returns a result],
      [ko_cv_unsetenv_result], [
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
         #include <stdlib.h>
      ]], [[
         int result = unsetenv("FOO");
      ]])],
         [ko_cv_unsetenv_result=yes],
         [ko_cv_unsetenv_result=no])
   ])
   if test "$ko_cv_unsetenv_result" = yes; then
      AC_DEFINE(KO_UNSETENV_RESULT, 1,
         [Define to 1 if unsetenv returns a result.])
   fi
])
