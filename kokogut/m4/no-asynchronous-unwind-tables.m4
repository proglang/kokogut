AC_DEFUN([KO_CHECK_NO_ASYNCHRONOUS_UNWIND_TABLES], [
   AC_MSG_CHECKING([for $CC option -fno-asynchronous-unwind-tables])
   ko_save_CFLAGS=$CFLAGS
   CFLAGS="$CFLAGS -fno-asynchronous-unwind-tables"
   ko_save_c_werror_flag=$ac_c_werror_flag
   ac_c_werror_flag=yes
   AC_COMPILE_IFELSE([AC_LANG_PROGRAM([], [])], [
      AC_MSG_RESULT([yes])
      CKOFLAGS="$CKOFLAGS -fno-asynchronous-unwind-tables"
   ], [
      AC_MSG_RESULT([no])
   ])
   CFLAGS=$ko_save_CFLAGS
   ac_c_werror_flag=$ko_save_c_werror_flag
])
