AC_DEFUN([KO_CHECK_TYPEOF], [
   AC_CACHE_CHECK([for typeof],
      [ko_cv_have_typeof], [
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM([], [[typeof(0) x;]])],
         [ko_cv_have_typeof=yes],
         [ko_cv_have_typeof=no])
   ])
   if test "$ko_cv_have_typeof" = yes; then
      AC_DEFINE(KO_HAVE_TYPEOF, 1,
         [Define to 1 if the C compiler supports typeof.])
   fi
])
