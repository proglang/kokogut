AC_DEFUN([KO_SHOW_OPTION], [
   ko_text="$1 "
   ko_width=`expr ${KO_MAX_OPTION_NAME_WIDTH-21} + 6`
   while test ${#ko_text} -lt $ko_width; do
      ko_text="$ko_text."
   done
   ko_text="$ko_text $2"
   if test $# -ge 3; then
      ko_width=`expr $ko_width + ${KO_MAX_OPTION_VALUE_WIDTH-8} + 1`
      while test ${#ko_text} -lt $ko_width; do
         ko_text="$ko_text "
      done
      ko_text="$ko_text (choices: $3)"
   fi
   echo "$ko_text"
])
