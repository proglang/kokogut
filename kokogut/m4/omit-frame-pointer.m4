AC_DEFUN([KO_CHECK_OMIT_FRAME_POINTER], [
   AC_MSG_CHECKING([for $CC option -fomit-frame-pointer])
   ko_save_CFLAGS=$CFLAGS
   CFLAGS="$CFLAGS -fomit-frame-pointer"
   ko_save_c_werror_flag=$ac_c_werror_flag
   ac_c_werror_flag=yes
   AC_COMPILE_IFELSE([AC_LANG_PROGRAM([], [])], [
      AC_MSG_RESULT([yes])
   ], [
      AC_MSG_RESULT([no])
      CFLAGS=$ko_save_CFLAGS
   ])
   ac_c_werror_flag=$ko_save_c_werror_flag
])
