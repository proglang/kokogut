AC_DEFUN([KO_FIND_PYTHON_VERSION], [
   AC_CACHE_CHECK([Python major version], ko_cv_python_major, [
      if $KO_PYTHON_PATH -c ['import sys; sys.exit(not hasattr(sys, "version_info"))']; then
         ko_cv_python_major=`$KO_PYTHON_PATH -c ['import sys; print(sys.version_info[0])']`
      else
         ko_cv_python_major=ancient
      fi
   ])
   KO_PYTHON_MAJOR=$ko_cv_python_major
   if test "$KO_PYTHON_MAJOR" != ancient; then
      AC_CACHE_CHECK([Python minor version], ko_cv_python_minor, [
         ko_cv_python_minor=`$KO_PYTHON_PATH -c ['import sys; print(sys.version_info[1])']`
      ])
      KO_PYTHON_MINOR=$ko_cv_python_minor
   fi
])

AC_DEFUN([KO_FIND_PYTHON_INCLUDES], [
   AC_CACHE_CHECK([Python include directory], ko_cv_python_includes, [
      ko_cv_python_includes=`$KO_PYTHON_PATH -c ['from distutils import sysconfig
print(sysconfig.get_python_inc())']`
   ])
   KO_PYTHON_INCLUDES=$ko_cv_python_includes
])

AC_DEFUN([KO_FIND_PYTHON_LIBS], [
   AC_CACHE_CHECK([Python library directory], ko_cv_python_libs, [
      ko_cv_python_libs=`$KO_PYTHON_PATH -c ['from distutils import sysconfig
if not sysconfig.get_config_var("Py_ENABLE_SHARED"):
   print(sysconfig.get_config_var("LIBPL"))']`
   ])
   KO_PYTHON_LIBS=$ko_cv_python_libs
])

AC_DEFUN([KO_FIND_PYTHON_LINK], [
   AC_CACHE_CHECK([Python libraries], ko_cv_python_link, [
      ko_cv_python_link=`$KO_PYTHON_PATH -c ['from distutils import sysconfig
print(" ".join(sysconfig.get_config_var("LIBS").split()
   + sysconfig.get_config_var("SYSLIBS").split()
   + ["-lpython"+sysconfig.get_config_var("VERSION")]))']`
   ])
   KO_PYTHON_LINK=$ko_cv_python_link
])

AC_DEFUN([KO_CHECK_PY_TYPE], [
   AS_VAR_PUSHDEF([ko_Symbol], [ko_cv_have_decl_$1])
   AC_CACHE_CHECK([whether $1 is declared], [ko_Symbol], [
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM(
         [AC_INCLUDES_DEFAULT([#include <Python.h>])], [
         void *p = & $1;
         return !p;
      ])],
         [AS_VAR_SET(ko_Symbol, yes)],
         [AS_VAR_SET(ko_Symbol, no)])])
   AS_IF([test AS_VAR_GET(ko_Symbol) = yes], [$2], [$3])
   AS_VAR_POPDEF([ko_Symbol])
])
