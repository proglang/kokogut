AC_DEFUN([KO_CHECK_RAW_FILES_UNIX], [
   AC_CACHE_CHECK([for Unix file I/O interface],
      [ko_cv_raw_files_unix], [
      AC_LINK_IFELSE([AC_LANG_PROGRAM([[
         #include <unistd.h>
         #include <fcntl.h>
         #include <sys/types.h>
         #include <sys/stat.h>
      ]], [[
         int fd;
         char buf[1024];
         fd = open("foo", O_RDONLY);
         read(fd, buf, 1024);
         lseek(fd, 0, SEEK_SET);
         write(fd, buf, 1024);
         close(fd);
      ]])],
         [ko_cv_raw_files_unix=yes],
         [ko_cv_raw_files_unix=no])
   ])
   KO_RAW_FILES_UNIX=$ko_cv_raw_files_unix
])
