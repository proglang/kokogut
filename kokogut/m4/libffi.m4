AC_DEFUN([KO_CHECK_LIBFFI], [
   KO_HAVE_LIBFFI=no
   KO_LIBFFI_LINK=
   ko_save_CPPFLAGS=$CPPFLAGS
   if test -n "$KO_LIBFFI_INCLUDES"; then
      CPPFLAGS="$CPPFLAGS -I $KO_LIBFFI_INCLUDES"
   fi
   ko_save_LDFLAGS=$LDFLAGS
   if test -n "$KO_LIBFFI_LIBS"; then
      LDFLAGS="$LDFLAGS -L$KO_LIBFFI_LIBS"
   fi
   AC_CHECK_HEADERS([ffi.h], [
      AC_CHECK_LIB(ffi, ffi_prep_cif, [
         KO_HAVE_LIBFFI=yes
         KO_LIBFFI_LINK=-lffi
      ])
   ])
   CPPFLAGS=$ko_save_CPPFLAGS
   LDFLAGS=$ko_save_LDFLAGS
])
