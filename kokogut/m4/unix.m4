AC_DEFUN([KO_CHECK_IS_UNIX], [
   AC_CACHE_CHECK([for basic Unix interfaces],
      [ko_cv_is_unix], [
      AC_LINK_IFELSE([AC_LANG_PROGRAM([[
         #include <unistd.h>
         #include <signal.h>
         #include <sys/types.h>
         #include <sys/stat.h>
         #include <sys/wait.h>
      ]], [[
         int fds[2];
         pid_t pid;
         static char *argv[] = {"foo", 0};
         int status;
         struct stat buf;
         pipe(fds);
         if ((pid = fork()) == 0) {
            dup2(fds[1], 1);
            execvp("foo", argv);
         }
         waitpid(pid, &status, 0);
         stat("bar", &buf);
         kill(getpid(), SIGTERM);
      ]])],
         [ko_cv_is_unix=yes],
         [ko_cv_is_unix=no])
   ])
   KO_HAVE_UNIX=$ko_cv_is_unix
])

AC_DEFUN([KO_CHECK_ERROR], [
   AC_CHECK_DECL(translit($1, a-z, A-Z), [
      KO_UNIX_SWITCH="$KO_UNIX_SWITCH Have$1"
   ], , [#include <errno.h>])
])

AC_DEFUN([KO_CHECK_SIGNAL], [
   AC_CHECK_DECL(translit($1, a-z, A-Z), [
      KO_UNIX_SWITCH="$KO_UNIX_SWITCH Have$1"
   ], , [#include <signal.h>])
])
