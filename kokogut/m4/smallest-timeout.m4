AC_DEFUN([KO_CHECK_SMALLEST_TIMEOUT], [
   AC_CACHE_CHECK([the smallest poll/epoll timeout (ms)],
      [ko_cv_smallest_timeout], [
      AC_RUN_IFELSE([AC_LANG_PROGRAM([[
         #if KO_USE_EPOLL
            #include <sys/epoll.h>
         #else
            #include <sys/poll.h>
         #endif
         #include <sys/time.h>
         #include <stdlib.h>
         #include <stdio.h>
         #include <unistd.h>

         #define SAMPLES 20
         #define SKIP_SHORT 3
         #define SKIP_LONG 7
         #define TRIES 50

         /*
          * We try to measure the smallest non-zero timeout provided by
          * poll/epoll. That is, the time they will actually wait when asked
          * to wait for 1ms, when started just after a timer tick (a previous
          * delay).
          *
          * We measure SAMPLES delays. They should be equal when rounded up to
          * milliseconds, excluding SKIP_SHORT shortest and SKIP_LONG longest
          * ones.
          *
          * If they are not equal, the results are not reliable, perhaps
          * because the system was busy. We wait for 1s and try again.
          * This is repeated up to TRIES times, when we finally give up.
          */

         static int cmp_int(const void *x, const void *y) {
            return *(const int *)x - *(const int *)y;
         }
      ]], [[
         struct timeval tmp;
         int try;
         #if KO_USE_EPOLL
            int epoll_fd;
            epoll_fd = epoll_create(16);
            if (epoll_fd == -1) return EXIT_FAILURE;
         #endif
         gettimeofday(&tmp, NULL);
         for (try = 0; try < TRIES; ++try) {
            #if KO_USE_EPOLL
               struct epoll_event event;
            #endif
            struct timeval tv[SAMPLES + 1];
            int diff[SAMPLES];
            int i;
            FILE *f;
            for (i = 0; i <= SAMPLES; ++i) {
               #if KO_USE_EPOLL
                  epoll_wait(epoll_fd, &event, 1, 1);
               #else
                  poll(NULL, 0, 1);
               #endif
               gettimeofday(&tv[i], NULL);
            }
            for (i = 0; i < SAMPLES; ++i) {
               tmp.tv_sec = tv[i + 1].tv_sec - tv[i].tv_sec;
               tmp.tv_usec = tv[i + 1].tv_usec - tv[i].tv_usec;
               if (tv[i + 1].tv_usec < tv[i].tv_usec) {
                  --tmp.tv_sec;
                  tmp.tv_usec += 1000000;
               }
               diff[i] = tmp.tv_sec * 1000 + (tmp.tv_usec + 999) / 1000;
            }
            qsort(diff, SAMPLES, sizeof(int), cmp_int);
            if (diff[SKIP_SHORT] != diff[SAMPLES-SKIP_LONG-1]) goto again;
            f = fopen("conftestval", "w");
            if (!f) return EXIT_FAILURE;
            fprintf(f, "%d\n", diff[SKIP_SHORT]);
            fclose(f);
            return EXIT_SUCCESS;
         again:
            sleep(1);
         }
         return EXIT_FAILURE;
      ]])],
         [ko_cv_smallest_timeout=`cat conftestval`],
         [ko_cv_smallest_timeout=1],
         [ko_cv_smallest_timeout=1])
   ])
   KO_SMALLEST_TIMEOUT=$ko_cv_smallest_timeout
])
