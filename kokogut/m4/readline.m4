AC_DEFUN([KO_CHECK_READLINE], [
   KO_HAVE_READLINE=no
   KO_READLINE_LINK=
   ko_save_CPPFLAGS=$CPPFLAGS
   if test -n "$KO_READLINE_INCLUDES"; then
      CPPFLAGS="$CPPFLAGS -I $KO_READLINE_INCLUDES"
   fi
   ko_save_LDFLAGS=$LDFLAGS
   if test -n "$KO_READLINE_LIBS"; then
      LDFLAGS="$LDFLAGS -L$KO_READLINE_LIBS"
   fi
   AC_CHECK_HEADERS([readline/readline.h], [
      AC_CHECK_LIB(readline, readline, [
         KO_HAVE_READLINE=yes
         KO_READLINE_LINK=-lreadline
      ], [
         AC_MSG_CHECKING([for readline in -lreadline -lncurses])
         ko_save_LIBS=$LIBS
         LIBS="-lreadline -lncurses $LIBS"
         AC_LINK_IFELSE([AC_LANG_CALL([], [readline])], [
            AC_MSG_RESULT([yes])
            KO_HAVE_READLINE=yes
            KO_READLINE_LINK="-lreadline -lncurses"
         ], [
            AC_MSG_RESULT([no])
         ])
         LIBS=$ko_save_LIBS
      ])
   ])
   CPPFLAGS=$ko_save_CPPFLAGS
   LDFLAGS=$ko_save_LDFLAGS
])
