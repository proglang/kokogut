AC_DEFUN([KO_CHECK_NETWORK], [
   KO_HAVE_NETWORK=no
   KO_NETWORK_SWITCH=
   KO_NETWORK_LINK=
   ko_save_CPPFLAGS=$CPPFLAGS
   if test -n "$KO_NETWORK_INCLUDES"; then
      CPPFLAGS="$CPPFLAGS -I $KO_NETWORK_INCLUDES"
   fi
   ko_save_LDFLAGS=$LDFLAGS
   if test -n "$KO_NETWORK_LIBS"; then
      LDFLAGS="$LDFLAGS -L$KO_NETWORK_LIBS"
   fi
   ko_save_LIBS=$LIBS
   AC_CHECK_HEADERS([sys/socket.h], [
      AC_CHECK_FUNC([socket], [KO_HAVE_NETWORK=yes], [
         AC_CHECK_LIB([socket], [socket], [
            KO_HAVE_NETWORK=yes
            KO_NETWORK_LINK=-lsocket
         ])
      ])
   ])
   if test "$KO_HAVE_NETWORK" = yes; then
      LIBS="$KO_NETWORK_LINK $LIBS"
      AC_CHECK_HEADERS([netdb.h], [
         AC_CHECK_FUNCS([getaddrinfo], , [KO_HAVE_NETWORK=no])
      ], [KO_HAVE_NETWORK=no])
   fi
   if test "$KO_HAVE_NETWORK" = yes; then
      KO_NETWORK_CCOPTS=
      AC_SUBST(KO_NETWORK_CCOPTS)
      AC_CHECK_TYPES([socklen_t], , , [
         #include <sys/types.h>
         #include <sys/socket.h>
      ])
      AC_CHECK_MEMBERS(struct sockaddr.sa_len, , , [
         #include <sys/types.h>
         #include <sys/socket.h>
      ])
      AC_CHECK_DECL([AF_INET6], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveInet6Family"
         # Sometimes IN6_IS_ADDR_UNSPECIFIED requires _BSD_SOURCE.
         AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
            #include <netinet/in.h>
         ]], [[
            struct in6_addr addr;
            int result = IN6_IS_ADDR_UNSPECIFIED(&addr);
         ]])], [], [
            AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
               #define _BSD_SOURCE
               #include <netinet/in.h>
            ]], [[
               struct in6_addr addr;
               int result = IN6_IS_ADDR_UNSPECIFIED(&addr);
            ]])], [
               KO_NETWORK_CCOPTS=-D_BSD_SOURCE
               AC_SUBST(KO_NETWORK_CCOPTS)
            ])
         ])
      ], , [
         #include <sys/types.h>
         #include <sys/socket.h>
      ])
      AC_CHECK_MEMBERS([struct sockaddr_in6.sin6_scope_id], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveScopeId"
      ] , , [#include <netinet/in.h>])
      AC_CHECK_DECL([SOCK_RAW], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveRawType"
      ], , [
         #include <sys/types.h>
         #include <sys/socket.h>
      ])
      AC_CHECK_DECL([SO_ACCEPTCONN], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveAcceptConn"
      ], , [
         #include <sys/types.h>
         #include <sys/socket.h>
      ])
      AC_CHECK_FUNCS([getnameinfo], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveGetNameInfo"
      ])
      AC_CHECK_DECL([AI_NUMERICSERV], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveNumericServ"
      ], , [#include <netdb.h>])
      AC_CHECK_DECL([AI_V4MAPPED], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveV4Mapped"
      ], , [#include <netdb.h>])
      AC_CHECK_DECL([AI_ALL], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveAll"
      ], , [#include <netdb.h>])
      AC_CHECK_DECL([AI_ADDRCONFIG], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveAddrConfig"
      ], , [#include <netdb.h>])
      AC_CHECK_DECL([NI_NUMERICSCOPE], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveNumericScope"
      ], , [#include <netdb.h>])
      AC_CHECK_DECL([EAI_OVERFLOW], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveEAIOverflow"
      ], , [#include <netdb.h>])
      AC_CHECK_DECL([IPPROTO_IPV6], [
         KO_NETWORK_SWITCH="$KO_NETWORK_SWITCH HaveIPProtoIPv6"
      ], , [#include <netinet/in.h>])
   fi
   CPPFLAGS=$ko_save_CPPFLAGS
   LDFLAGS=$ko_save_LDFLAGS
   LIBS=$ko_save_LIBS
])
