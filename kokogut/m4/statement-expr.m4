AC_DEFUN([KO_CHECK_STATEMENT_EXPR], [
   AC_CACHE_CHECK([for statement expressions],
      [ko_cv_have_statement_expr], [
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM([], [[
         int x = ({int y = 0; y;});
      ]])],
         [ko_cv_have_statement_expr=yes],
         [ko_cv_have_statement_expr=no])
   ])
   if test "$ko_cv_have_statement_expr" = yes; then
      AC_DEFINE(KO_HAVE_STATEMENT_EXPR, 1,
         [Define to 1 if the C compiler supports statement expressions.])
   fi
])
