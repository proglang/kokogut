<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="author" content="Marcin 'Qrczak' Kowalczyk" />
    <title>Overview of Kokogut Internals</title>
    <link rel="stylesheet" href="kogut.css" type="text/css" />
  </head>
  <body>
    <div class="navigation">
      <p>Kogut language</p>
      <ul>
        <li><a href="kogut.html">Overview</a></li>
        <li><a href="kogut/kogut.pdf">Reference</a></li>
        <li><a href="kogut-examples.html">Examples</a></li>
        <li><a href="http://sourceforge.net/mail/?group_id=110425">Mailing list</a></li>
        <li><a href="faq.html">FAQ</a></li>
      </ul>
      <p>Kokogut compiler</p>
      <ul>
        <li><a href="kokogut.html">Overview</a></li>
        <li><a href="http://sourceforge.net/project/showfiles.php?group_id=110425">Download</a></li>
        <li><a href="http://sourceforge.net/cvs/?group_id=110425">CVS</a></li>
        <li><a href="NEWS">NEWS</a></li>
        <li><a href="TODO">TODO</a></li>
        <li><em>Internals</em></li>
      </ul>
    </div>
    <div style="font-size: small">
      <p>
        Copyright &copy; 2004-2006,2008 by
        Marcin 'Qrczak' Kowalczyk
        (<a href="mailto:&#81;rczakMK@gmail.com">&#81;rczakMK@gmail.com</a>)
      </p>
      <p>
        Permission is granted to copy, distribute and/or modify this
        document under the terms of the
        <a href="http://www.gnu.org/copyleft/fdl.html">GNU Free
        Documentation License</a>, Version 1.2 or any later version
        published by the Free Software Foundation; with no Invariant
        Sections, no Front-Cover Texts, and no Back-Cover Texts.
        A copy of the license is included
        <a href="COPYING.FDL">here</a>.
      </p>
    </div>
    <h1>Kokogut Internals</h1>
    <p>
      Let&rsquo;s first introduce some concepts used by the generated
      code. All values except small integers are represented by
      pointers to objects, where the first word of each object is a
      pointer to its descriptor. Descriptors are usually allocated
      statically, except for objects of locally defined types. A
      descriptor contains:
    </p>
    <ul>
      <li>
        its own descriptor, so the garbage collector can treat
        descriptors as objects;
      </li>
      <li>
        the pointer to a C function which moves the object to a new
        address and marks its fields which point to other objects;
      </li>
      <li>
        an optional pointer to a finalization function;
      </li>
      <li>
        the type of the object;
      </li>
      <li>
        entries, pointers to code which implement application of the
        object; there are fast entries for a known number of arguments
        between 0 and 5, and a list entry which takes arguments in
        a list.
      </li>
    </ul>
    <p>
      For most types there is a single descriptor of objects of
      that type.  Some types use several descriptors, e.g. a lazy
      variable has separate descriptors depending on whether its value
      has been computed. Functions defined at different places in the
      source have separate descriptors (of course all local functions
      defined at one place share the same descriptor).
    </p>
    <p>
      Kokogut compiles a module at once. Compilation of a module is
      split into several stages. First the source of the module is
      read as a string.
    </p>
    <h2>Lexer: String &rarr; Tokens</h2>
    <p>
      The Lexer splits the string into a list of tokens. Each token
      is paired with its source location.
    </p>
    <h2>Parser: Tokens &rarr; Source</h2>
    <p>
      The Parser transforms the list of tokens into a Source syntax
      tree. It&rsquo;s a recursive descent one, implemented by hand.
    </p>
    <p>
      The Source represents names as symbols, without any contextual
      information. All nodes store a location. The tree doesn&rsquo;t
      distinguish between expressions, patterns and definitions,
      and doesn&rsquo;t enforce contextual invariants. Parentheses
      are not stored, and some basic cases of syntactic sugar are
      already expanded.
    </p>
    <h2>Expander: Source &rarr; Abstract</h2>
    <p>
      The Expander transforms the Source tree into an Abstract form
      by resolving names, interpreting compound expressions which use
      builtin macros, and expanding any remaining syntactic sugar.
      The Abstract form is designed to represent the core semantics
      of the language, no matter how it&rsquo;s represented in the
      source nor how it will be realized.
    </p>
    <p>
      The Expander produces the Abstract representation of the module
      and an interface to be written into an interface file. It reads
      interfaces of used modules when it needs them. The format of
      interfaces is internal to the compiler. Reading interfaces uses
      the same Lexer as the normal source but a different parser.
    </p>
    <p>
      An Abstract module consists of a module name and global
      definitions which bind global names and use expressions and
      patterns.
    </p>
    <p>
      Names, both those translated from the source and those introduced
      by the Expander for internal purposes, are stored as objects
      which many fields:
    </p>
    <ul>
      <li>
        unique identifier (serial number) for hashing;
      </li>
      <li>
        module name in case it&rsquo;s a global name;
      </li>
      <li>
        symbol, taken from the source if there is a corresponding
        source name;
      </li>
      <li>
        name extension, to distinguish several internal
        names supporting the given source name, such that
        (module,symbol,extension) uniquely identifies every global
        name;
      </li>
      <li>
        storage, which is one of:
        <ul>
          <li>
            <code>#local</code>, for locally named objects,
          </li>
          <li>
            <code>#global</code>, for global pointers to computed objects,
          </li>
          <li>
            <code>#static</code>, for objects allocated statically,
          </li>
          <li>
            <code>#fun</code>, for global functions,
          </li>
          <li>
            <code>#label</code>, for targets of jumps in later stages;
          </li>
        </ul>
      </li>
      <li>
        whether a global name is public, available from other modules;
      </li>
      <li>
        whether the name has been used in addition to being defined;
      </li>
      <li>
        some type information if we statically know the type;
      </li>
      <li>
        the descriptor name if we statically know the descriptor;
      </li>
      <li>
        entry names if we statically know the entries;
      </li>
      <li>
        whether the object is known to not need the self pointer
        on application.
      </li>
    </ul>
    <p>
      Each name in this sense has exactly one point of definition,
      i.e. they are never shadowed.
    </p>
    <h2>Realizer: Abstract &rarr; Real</h2>
    <p>
      The Realizer resolves forward references by introducing special
      objects, called forwards, which are allocated when a name needs
      a forward reference, and filled just after the corresponding
      definition executed. Code before a definition takes the value of
      the defined name from the forward.
    </p>
    <p>
      In some cases forwards can be avoided. If the definition has not
      executed yet, but we are sure that it will execute before the
      code we are currently processing (because we are inside a
      function body and the function will not be applied before the
      definition executes), we don't have to check whether the forward
      is filled. In addition if the name is global, we may refer to
      the name directly and a forward will not be needed at all.
    </p>
    <p>
      The Realizer translates some constructs to lower level code.
      Descriptors of types being defined are made explicit.
      Specializations of SingletonName, RecordConstructor and
      RecordFields are added. Specializations and <code>try</code>
      expressions are translated to applications of appropriate
      builtin functions.
    </p>
    <p>
      The Realizer reports warnings about mismatched arities, about
      definitions or imports of names which are never used, about
      forward references which will always fail, about applications
      which will always fail due to the type of the applied object,
      or when the result of an expression is ignored while the kind
      of the expression makes this suspicious.
    </p>
    <p>
      Some optimizations are performed, e.g. direct applications of
      anonymous functions which don't refer to themselves are
      translated to <code>case</code> expressions.
    </p>
    <p>
      The Real representation is quite similar to Abstract, except
      that forwards, descriptors etc. are explicit.
    </p>
    <h2>Optimizer: Real &rarr; Real</h2>
    <p>
      This stage is run optionally, when the <code>-O</code> option is
      turned on.
    </p>
    <p>
      Optimization is split into three phases:
    <p>
    <ol>
      <li>
        <p>
          Some standard functions are recognized and replaced with
          inline code. In particular arithmetic operators and
          comparison operators check for small ints before falling
          back to generic functions.
        </p>
        <p>
          Names with trivial definitions (defined as other names or
          literals) are optimized out within a module.
        </p>
      </li>
      <li>
        <p>
          We will perform two optimizations which are internally
          similar:
        </p>
        <ul>
          <li>
            lifting some local functions to the toplevel, transforming
            their free variables into additional parameters
          </li>
          <li>
            unboxing of some plain variables
          </li>
        </ul>
        <p>
          This phase scans definitions and usages of functions and
          variables, and decides which objects will be optimized in
          this way.
        </p>
        <p>
          Also, remaining substitutions are performed. The previous
          phase performed a substitution only for occurrences seen
          after it has decided about the substitution.
        </p>
      </li>
      <li>
        <p>
          Optimizations of functions and variables determined in the
          previous phase are performed.
        </p>
        <p>
          Functions and structure descriptors are lifted to the
          toplevel and allocated statically when possible.
        </p>
      </li>
    </ol>
    <h2>Planner: Real &rarr; Plan</h2>
    <p>
      The Planner transforms the Real module into a Plan form where
      the order of operations is explicit. A Plan module consists of
      unordered global definitions and an initialization statement.
      Definitions describe global and static objects, static
      descriptors, and functions.
    </p>
    <p>
      A function definition, besides the function name, contains a
      local name of the object being applied, names of its free local
      variables, local names of parameters, and the statement to
      execute. The Plan representation uses the same names as the
      Abstract and Real representations.
    </p>
    <p>
      A statement can be a proper statement or a pair of statements to
      be executed in order. Statements can bind local names which are
      used in the following statements. Execution of most statements
      is followed by the execution of the next statement in sequence,
      except a few special statements:
    </p>
    <ul>
      <li>
        <code>PS_RETURN</code> ends the execution of the function,
      </li>
      <li>
        <code>PS_CALL</code> calls another function and resumes
        execution when it returns,
      </li>
      <li>
        <code>PS_TAIL_CALL</code> ends this function and jumps to
        another function,
      </li>
      <li>
        <code>PS_FAIL</code> throws an exception,
      </li>
      <li>
        <code>PS_IF</code> and <code>PS_CCOND</code> conditionally
        execute one of two statements (which may either continue
        after the conditional or end the execution of the function),
      </li>
      <li>
        <code>PS_LABEL</code> is a target of jumps,
      </li>
      <li>
        <code>PS_JUMP</code> jumps to a label in the same function.
      </li>
      <li>
        <code>PS_CCASE</code> contains a list of pairs of a label
        and a statement.
      </li>
    </ul>
    <p>
      Statements use names and expressions. An expression evaluates
      to a value, never fails, doesn&rsquo;t use statements, and
      doesn&rsquo;t transfer control to other functions. A Sequential
      expression can always be implemented as a C expression.
    </p>
    <p>
      The same local name can be used in several functions, with an
      independent definition in each function.
    </p>
    <h2>CCoder: Plan &rarr; CCode</h2>
    <p>
      The CCoder transforms the Plan module into CCode form, an
      abstract syntax of C. In particular it allocates registers and
      stack offsets for local names. A subsequence of statements
      between calls is translated into a separate C function.
    </p>
    <p>
      C names corresponding to global Kogut names are derived from
      the (module,symbol,extension) triples, so the binary interface
      of a module changes only after predictable changes to the
      source. Other names include the unique identifier, but also
      the symbol and extension if they are available, so it&rsquo;s
      easier to relate them to the source.
    </p>
    <h2>Printing</h2>
    <p>
      Each internal representation (Source, Abstract, Real, Plan, and
      CCode) comes with its pretty-printer. The printers for Source
      and CCode use the actual syntax of Kogut and C respectively,
      other printers use some ad-hoc textual representation useful for
      debugging.
    </p>
  </body>
</html>
