<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="author" content="Marcin 'Qrczak' Kowalczyk" />
    <title>The Kogut Programming Language</title>
    <link rel="stylesheet" href="kogut.css" type="text/css" />
  </head>
  <body>
    <div class="navigation">
      <p>Kogut language</p>
      <ul>
        <li><em>Overview</em></li>
        <li><a href="kogut/kogut.pdf">Reference</a></li>
        <li><a href="kogut-examples.html">Examples</a></li>
        <li><a href="http://sourceforge.net/mail/?group_id=110425">Mailing list</a></li>
        <li><a href="faq.html">FAQ</a></li>
      </ul>
      <p>Kokogut compiler</p>
      <ul>
        <li><a href="kokogut.html">Overview</a></li>
        <li><a href="http://sourceforge.net/project/showfiles.php?group_id=110425">Download</a></li>
        <li><a href="http://sourceforge.net/cvs/?group_id=110425">CVS</a></li>
        <li><a href="NEWS">NEWS</a></li>
        <li><a href="TODO">TODO</a></li>
        <li><a href="kokogut-internals.html">Internals</a></li>
      </ul>
    </div>
    <div style="font-size: small">
      <p>
        Copyright &copy; 2004-2005,2008 by
        Marcin 'Qrczak' Kowalczyk
        (<a href="mailto:&#81;rczakMK@gmail.com">&#81;rczakMK@gmail.com</a>)
      </p>
      <p>
        Permission is granted to copy, distribute and/or modify this
        document under the terms of the
        <a href="http://www.gnu.org/copyleft/fdl.html">GNU Free
        Documentation License</a>, Version 1.2 or any later version
        published by the Free Software Foundation; with no Invariant
        Sections, no Front-Cover Texts, and no Back-Cover Texts.
        A copy of the license is included
        <a href="COPYING.FDL">here</a>.
      </p>
    </div>
    <h1>The Kogut Programming Language</h1>
    <p>
      <img src="kogut.png" width="252" height="230" style="float: right"
      alt="" />
      Kogut is an experimental programming language which supports
      impurely functional programming and a non-traditional flavor of
      object-oriented programming. Its semantics is most similar to
      Scheme or Dylan, but the syntax looks more like ML or Ruby.
    </p>
    <p>
      The name &ldquo;Kogut&rdquo; means &ldquo;Rooster&rdquo;
      (&ldquo;Cock&rdquo;) in Polish and is pronounced like
      [KOH-goot].
    </p>
    <h2>The paradigm</h2>
    <ol>
      <li>
        The language is dynamically typed: consistent usage of types
        is not enforced statically.
      </li>
      <li>
        The language is mostly functional: object contents and
        name bindings are immutable by default. You can request a
        variable binding explicitly. The standard library includes
        both immutable and mutable collections, with most important
        compound types (lists, strings, tuples) being immutable.
      </li>
      <li>
        The language doesn&rsquo;t prevent arbitrary side effects from
        occurring during evaluation of an expression (it&rsquo;s not
        purely functional) and the evaluation order is deterministic.
        It&rsquo;s not purely functional.
      </li>
      <li>
        Objects are deallocated implicitly when no longer referenced
        (garbage collection).
      </li>
    </ol>
    <h2>Names, definitions and scopes</h2>
    <ol>
      <li>
        The language is lexically scoped: an occurrence of a name
        refers to a definition determined statically from program
        source, not dynamically by control flow.
      </li>
      <li>
        The same syntax and semantics of definitions is used globally
        in a module scope, locally in a function, and for specifying
        the fields of an object.
      </li>
      <li>
        There is a single namespace: each identifier in a given scope
        has one meaning, independent of the context of usage.
      </li>
      <li>
        Definitions are evaluated and names are defined in the order
        the definitions are written. Expressions may refer to names
        defined above or below, as long as names defined below are
        used only inside functions which are not called before the
        names are defined.
      </li>
    </ol>
    <h2>Errors</h2>
    <ol>
      <li>
        There is no undefined behavior (an error can&rsquo;t trash
        memory nor make the processor execute unpredictable code),
        and there is a little unspecified behavior. The meaning of a
        program is almost deterministic. In particular strings and
        lists are immutable, so they can be freely shared without
        problems with modifying literals.
      </li>
      <li>
        On errors generally exceptions are thrown, instead of implicit
        conversion of an argument to another type, returning a null or
        unspecified value, ignoring excess arguments, or guessing what
        the programmer could possibly mean. In particular a condition
        must be either <code>True</code> or <code>False</code>, and
        trying to get a non-existent element of a collection throws
        an exception.
      </li>
    </ol>
    <h2>Execution</h2>
    <ol>
      <li>
        An object conceptually consists of three parts:
        <ul>
          <li>
            behavior, which is how it reacts to application to
            arguments,
          </li>
          <li>
            type, a tag which helps to identify behavioral protocols
            it uses,
          </li>
          <li>
            and sometimes some hidden type-specific magic.
          </li>
        </ul>
      </li>
      <li>
        A function (or generally any object) takes a list of
        arguments and either returns a single result or throws
        an exception. Keyword parameters and multiple results are
        simulated in terms of this model.
      </li>
      <li>
        Tail calls are properly implemented: before execution of a
        tail call, the memory which was implicitly allocated for the
        caller&rsquo;s execution state is dealloated.
      </li>
    </ol>
    <h2>Miscellaneous issues with functions</h2>
    <ol>
      <li>
        Data objects are primarily used by applying functions to them,
        rather than by sending them some messages. Objects themselves
        are applied only to access their core functionality, e.g. to
        access fields of a record.
      </li>
      <li>
        In case of a generic interface common to several types with
        different implementations, the functions specified by the
        interface are realized by generic functions, which dispatch
        their implementation on the types of arguments. In particular
        many operators are generic functions.
      </li>
      <li>
        New objects are constructed by applying functions which are
        designed to return new objects, rather than by using some
        distinct syntactic notion of constructors.
      </li>
      <li>
        There is a single most important equality operator
        <code>==</code>, which generally compares values of immutable
        objects and identity of mutable objects, and can also be
        defined manually for particular types.
      </li>
      <li>
        The comparison used for sorting, for dictionary lookup, and
        the corresponding hash function, are generally specified once
        per type, not once per sorting operation or once per dictionary.
        Instead, these operations take a transformation function which
        extracts or transforms the part of the key used for comparison.
      </li>
      <li>
        Locking and unlocking synchronization objects, blocking and
        unblocking asynchronous signals, changing values of dynamic
        variables (usually), installing signal handlers (usually)
        are done for the duration of execution of given code, not as
        a permanent effect of an imperative operation.
      </li>
    </ol>
    <h2>Syntax</h2>
    <ol>
      <li>
        Names are case-sensitive.
      </li>
      <li>
        Function application is denoted by separating the function
        from the arguments and the arguments from one another with
        spaces, but functions are not curried; all arguments are
        passed at once and the function can determine how many of
        them were given.
      </li>
      <li>
        Breaking of program text into lines and indentation are
        insignificant. Definitions and statements are separated
        by semicolons.
      </li>
      <li>
        Mutable variables are first-class objects. The meaning of
        accessing particular variables or accessing fields of an
        object can be programmed.
      </li>
      <li>
        The set of operators with their priorities is fixed, which
        makes possible to parse a module independently of the contents
        of other modules. You can make your own binary operators
        from ordinary names with a fixed priority (they look like
        <code>%Foo</code> or <code>Foo%</code>).
      </li>
      <li>
        The only keyword is the underscore. Other identifier-like
        names which are used in core syntactic constructs are macros,
        and these names can be redefined.
      </li>
      <li>
        Parentheses <code>()</code> are used for grouping
        subexpressions and subpatterns.  Brackets <code>[]</code> are
        used for making and matching lists. Braces <code>{}</code>
        are used for delimiting other parts of the syntax (function
        bodies, <code>if</code> and <code>case</code> branches,
        object definitions etc.).
      </li>
    </ol>
    <h2>Conventions</h2>
    <ol>
      <li>
        Most global names are written <code>LikeThis</code>, except
        type names which are <code>LIKE_THIS</code> (because they
        often coexist with a function or constant of similar name)
        and names or important macros like <code>let</code> and
        <code>if</code>. Local names and field names are usually
        written <code>likeThis</code>.
      </li>
      <li>
        In the author&rsquo;s opinion the indentation width of
        3 spaces looks nice. Since the standard tab width, 8, is
        not even divisible by 3, tabs are better avoided. Using a
        non-standard tab width would be evil.
      </li>
    </ol>
  </body>
</html>
