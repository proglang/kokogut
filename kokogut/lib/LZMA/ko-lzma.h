/*
 * This file is a part of the Kokogut library.
 *
 * Kokogut is a compiler of the Kogut programming language.
 * Copyright (C) 2006-2007 by Marcin 'Qrczak' Kowalczyk
 * (QrczakMK@gmail.com)
 *
 * The Kokogut library is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version;
 * with a "linking exception".
 *
 * The linking exception allows you to link a "work that uses the
 * Library" with a publicly distributed version of the Library to
 * produce an executable file containing portions of the Library,
 * and distribute that executable file under terms of your choice,
 * without any of the additional requirements listed in section 6
 * of LGPL version 2 or section 4 of LGPL version 3.
 *
 * Kokogut is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef KOKOGUT_LZMA_H
#define KOKOGUT_LZMA_H

#ifdef __cplusplus
extern "C" {
#endif

enum ko_lzma_match_finder {
   KO_LZMA_BT2,
   KO_LZMA_BT3,
   KO_LZMA_BT4,
   KO_LZMA_HC4
};

enum ko_lzma_status {
   KO_LZMA_OK,
   KO_LZMA_DATA_ERROR,
   KO_LZMA_ABORT,
   KO_LZMA_OUT_OF_MEMORY,
   KO_LZMA_USAGE_ERROR
};

void *ko_lzma_new_kogut_in_stream(ko_value_t read);
void *ko_lzma_new_kogut_out_stream(ko_value_t write);

ko_nint_t ko_lzma_estimate_compress_size(ko_uint32_t dictionarySize,
   ko_uint32_t litContextBits, ko_uint32_t litPosBits,
   ko_uint32_t numFastBytes, enum ko_lzma_match_finder matchFinder);

enum ko_lzma_status ko_lzma_compress(void *inStreamPtr, void *outStreamPtr,
   ko_uint32_t dictionarySize, ko_uint32_t posStateBits,
   ko_uint32_t litContextBits, ko_uint32_t litPosBits, ko_uint32_t algorithm,
   ko_uint32_t numFastBytes, ko_uint32_t matchFinderCycles,
   enum ko_lzma_match_finder matchFinder);

enum ko_lzma_status ko_lzma_decompress(void *inStreamPtr, void *outStreamPtr,
   struct ko_external *reservedMemory);

#ifdef __cplusplus
}
#endif

#endif
