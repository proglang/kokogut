// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2006-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

// Based on LzmaAlone.cpp

#include <kokogut/runtime.h>
#include <kokogut/prelude.h>

#include "CPP/Common/MyWindows.h"
#include "CPP/Common/MyInitGuid.h"
#include "CPP/Common/MyCom.h"
#include "CPP/7zip/Common/StreamUtils.h"
#include "CPP/7zip/Compress/LZMA/LZMADecoder.h"
#include "CPP/7zip/Compress/LZMA/LZMAEncoder.h"

#include "ko-lzma.h"

//// class CKogutInStream

class CKogutInStream: public ISequentialInStream, public CMyUnknownImp {
   struct ko_external read;
public:
   CKogutInStream(ko_value_t read) {
      ko_register_external(&this->read, read);
   }
   ~CKogutInStream();
   MY_UNKNOWN_IMP1(ISequentialInStream);
   STDMETHOD(Read)(void *data, UInt32 size, UInt32 *processedSize);
};

CKogutInStream::~CKogutInStream() {
   ko_bool_t foreign = ko_maybe_leave_foreign_no_calls();
   ko_unregister_external(&read);
   if (foreign) ko_enter_foreign_no_calls();
}

STDMETHODIMP
CKogutInStream::Read(void *data, UInt32 size, UInt32 *processedSize) {
   if (processedSize != NULL) *processedSize = 0;
   if (KO_UNLIKELY(size == 0)) return S_OK;
   ko_leave_foreign();
   if (KO_UNLIKELY(ko_failed)) {ko_enter_foreign(); return E_ABORT;}
   KO_R[0] = read.ko_value;
   KO_R[1] = ko_make_cptr(data);
   KO_R[2] = ko_int(size);
   KO_CALL_INDIRECT(2);
   if (KO_UNLIKELY(ko_failed || KO_R[1] == KO_Null)) {
      ko_enter_foreign(); return E_ABORT;
   }
   if (processedSize != NULL) *processedSize = ko_nint_of_sint(KO_R[1]);
   ko_enter_foreign();
   return S_OK;
}

extern "C" void *
ko_lzma_new_kogut_in_stream(ko_value_t read) {
   return new CKogutInStream(read);
}

//// class CKogutOutStream

class CKogutOutStream: public ISequentialOutStream, public CMyUnknownImp {
   struct ko_external write;
public:
   CKogutOutStream(ko_value_t write) {
      ko_register_external(&this->write, write);
   }
   ~CKogutOutStream();
   MY_UNKNOWN_IMP1(ISequentialOutStream);
   STDMETHOD(Write)(const void *data, UInt32 size, UInt32 *processedSize);
};

CKogutOutStream::~CKogutOutStream() {
   ko_bool_t foreign = ko_maybe_leave_foreign_no_calls();
   ko_unregister_external(&write);
   if (foreign) ko_enter_foreign_no_calls();
}

STDMETHODIMP
CKogutOutStream::Write(const void *data, UInt32 size, UInt32 *processedSize) {
   if (processedSize != NULL) *processedSize = 0;
   if (KO_UNLIKELY(size == 0)) return S_OK;
   ko_leave_foreign();
   if (KO_UNLIKELY(ko_failed)) {ko_enter_foreign(); return E_ABORT;}
   KO_R[0] = write.ko_value;
   KO_R[1] = ko_make_cptr(const_cast<void *>(data));
   KO_R[2] = ko_int(size);
   KO_CALL_INDIRECT(2);
   if (KO_UNLIKELY(ko_failed || KO_R[1] == KO_Null)) {
      ko_enter_foreign(); return E_ABORT;
   }
   if (processedSize != NULL) *processedSize = ko_nint_of_sint(KO_R[1]);
   ko_enter_foreign();
   return S_OK;
}

extern "C" void *
ko_lzma_new_kogut_out_stream(ko_value_t write) {
   return new CKogutOutStream(write);
}

//// ko_lzma_estimate_compress_size

extern "C" ko_nint_t
ko_lzma_estimate_compress_size(ko_uint32_t dictionarySize,
   ko_uint32_t litContextBits, ko_uint32_t litPosBits,
   ko_uint32_t numFastBytes, ko_lzma_match_finder matchFinder)
{
   ko_unint_t alloc1 = 1 << 20;
   ko_unint_t alloc2 = (1 << (litPosBits + litContextBits))
      * sizeof(NCompress::NLZMA::CLiteralEncoder2);
   ko_unint_t sum = dictionarySize + NCompress::NLZMA::kNumOpts + numFastBytes
      + NCompress::NLZMA::kMatchMaxLen+1;
   ko_unint_t alloc3 = sum + sum/2 + 256;
   ko_unint_t hs;
   if (matchFinder == KO_LZMA_BT2)
      hs = 0;
   else {
      hs = dictionarySize-1;
      hs |= (hs >> 1);
      hs |= (hs >> 2);
      hs |= (hs >> 4);
      hs |= (hs >> 8);
      hs >>= 1;
      hs |= 0xFFFF;
      if (hs > (1 << 24)) {
         if (matchFinder == KO_LZMA_BT4 || matchFinder == KO_LZMA_HC4)
            hs >>= 1;
         else
            hs = (1 << 24) - 1;
      }
      ++hs;
   }
   if (matchFinder != KO_LZMA_BT2) {
      hs += 1 << 10;
      if (matchFinder != KO_LZMA_BT3)
         hs += 1 << 16;
   }
   ko_unint_t cyclicBufferSize = dictionarySize+1;
   ko_unint_t alloc4 = (hs + (matchFinder == KO_LZMA_HC4 ?
      cyclicBufferSize : cyclicBufferSize * 2)) * 4;
   return alloc1+alloc2+alloc3+alloc4;
}

//// ko_lzma_compress

static ko_lzma_status
ko_hresult_to_lzma_status(HRESULT status) {
   switch (status) {
   case S_FALSE:       return KO_LZMA_DATA_ERROR;
   case E_ABORT:       return KO_LZMA_ABORT;
   case E_OUTOFMEMORY: return KO_LZMA_OUT_OF_MEMORY;
   default:            return KO_LZMA_USAGE_ERROR;
   }
}

#define KO_LZMA_CHECK_STATUS(status)               \
   KO_STAT_BEGIN {                                 \
      if (KO_UNLIKELY((status) != S_OK))           \
         return ko_hresult_to_lzma_status(status); \
   } KO_STAT_END

extern "C" ko_lzma_status
ko_lzma_compress(void *inStreamPtr, void *outStreamPtr,
   ko_uint32_t dictionary, ko_uint32_t posStateBits,
   ko_uint32_t litContextBits, ko_uint32_t litPosBits, ko_uint32_t algorithm,
   ko_uint32_t numFastBytes, ko_uint32_t matchFinderCycles,
   ko_lzma_match_finder matchFinder)
{
   CMyComPtr<CKogutInStream> inStream =
      static_cast<CKogutInStream *>(inStreamPtr);
   CMyComPtr<CKogutOutStream> outStream =
      static_cast<CKogutOutStream *>(outStreamPtr);
   CMyComPtr<NCompress::NLZMA::CEncoder> encoder =
      new NCompress::NLZMA::CEncoder;
   static const PROPID propIDs[] = {
      NCoderPropID::kDictionarySize,
      NCoderPropID::kPosStateBits,
      NCoderPropID::kLitContextBits,
      NCoderPropID::kLitPosBits,
      NCoderPropID::kAlgorithm,
      NCoderPropID::kNumFastBytes,
      NCoderPropID::kMatchFinder,
      NCoderPropID::kEndMarker,
      NCoderPropID::kMatchFinderCycles
   };
   PROPVARIANT properties[9];
   properties[0].vt = VT_UI4;
   properties[0].ulVal = dictionary;
   properties[1].vt = VT_UI4;
   properties[1].ulVal = posStateBits;
   properties[2].vt = VT_UI4;
   properties[2].ulVal = litContextBits;
   properties[3].vt = VT_UI4;
   properties[3].ulVal = litPosBits;
   properties[4].vt = VT_UI4;
   properties[4].ulVal = algorithm;
   properties[5].vt = VT_UI4;
   properties[5].ulVal = numFastBytes;
   properties[6].vt = VT_BSTR;
   switch (matchFinder) {
   case KO_LZMA_BT2: properties[6].bstrVal = const_cast<BSTR>(L"BT2"); break;
   case KO_LZMA_BT3: properties[6].bstrVal = const_cast<BSTR>(L"BT3"); break;
   case KO_LZMA_BT4: properties[6].bstrVal = const_cast<BSTR>(L"BT4"); break;
   case KO_LZMA_HC4: properties[6].bstrVal = const_cast<BSTR>(L"HC4"); break;
   default: ko_impossible_error("LZMA: bad matchFinder code: %d", matchFinder);
   }
   properties[7].vt = VT_BOOL;
   properties[7].boolVal = VARIANT_TRUE; // endMarker
   int numProps = 8;
   if (matchFinderCycles != 0) {
      properties[8].vt = VT_UI4;
      properties[8].ulVal = matchFinderCycles;
      numProps = 9;
   }
   HRESULT status = encoder->SetCoderProperties(propIDs, properties, numProps);
   KO_LZMA_CHECK_STATUS(status);
   status = encoder->WriteCoderProperties(outStream);
   KO_LZMA_CHECK_STATUS(status);
   Byte fileSize[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
   status = WriteStream(outStream, fileSize, 8, NULL);
   KO_LZMA_CHECK_STATUS(status);
   status = encoder->Code(inStream, outStream, 0, 0, 0);
   KO_LZMA_CHECK_STATUS(status);
   return KO_LZMA_OK;
}

//// ko_lzma_decompress

static ko_nint_t
ko_lzma_estimate_decompress_size(const Byte *properties) {
   ko_unint_t alloc1 = 0;
   for (int i = 0; i < 4; ++i)
      alloc1 += (UInt32)(properties[1 + i]) << (i * 8);
   if (KO_UNLIKELY(alloc1 > 1 << 30)) return -1;
   int lc = properties[0] % 9;
   int remainder = properties[0] / 9;
   int lp = remainder % 5;
   if (KO_UNLIKELY(remainder / 5
   > NCompress::NLZMA::NLength::kNumPosStatesBitsMax))
      return -1;
   ko_unint_t alloc2 = (1 << (lc+lp))
      * sizeof(NCompress::NLZMA::CLiteralDecoder2);
   ko_unint_t alloc3 = 1 << 20;
   return alloc1+alloc2+alloc3;
}

extern "C" ko_lzma_status
ko_lzma_decompress(void *inStreamPtr, void *outStreamPtr,
   struct ko_external *reservedMemory)
{
   CMyComPtr<CKogutInStream> inStream =
      static_cast<CKogutInStream *>(inStreamPtr);
   CMyComPtr<CKogutOutStream> outStream =
      static_cast<CKogutOutStream *>(outStreamPtr);
   CMyComPtr<NCompress::NLZMA::CDecoder> decoder =
      new NCompress::NLZMA::CDecoder;
   const UInt32 numProps = 5;
   Byte properties[numProps];
   UInt32 processedSize;
   HRESULT status = ReadStream(inStream, properties, numProps, &processedSize);
   KO_LZMA_CHECK_STATUS(status);
   if (KO_UNLIKELY(processedSize != numProps)) return KO_LZMA_DATA_ERROR;
   ko_nint_t estimatedSize = ko_lzma_estimate_decompress_size(properties);
   if (KO_UNLIKELY(estimatedSize < 0)) return KO_LZMA_DATA_ERROR;
   ko_leave_foreign_no_calls();
   ko_register_external(reservedMemory, ko_reserve_memory(estimatedSize));
   ko_enter_foreign_no_calls();
   status = decoder->SetDecoderProperties2(properties, numProps);
   KO_LZMA_CHECK_STATUS(status);
   Byte fileSizeBytes[8];
   status = ReadStream(inStream, fileSizeBytes, 8, &processedSize);
   KO_LZMA_CHECK_STATUS(status);
   if (KO_UNLIKELY(processedSize != 8)) return KO_LZMA_DATA_ERROR;
   UInt64 fileSize = 0;
   for (int i = 0; i < 8; ++i)
      fileSize += (UInt64)fileSizeBytes[i] << (8 * i);
   status = decoder->Code(inStream, outStream, 0, &fileSize, 0);
   KO_LZMA_CHECK_STATUS(status);
   return KO_LZMA_OK;
}
