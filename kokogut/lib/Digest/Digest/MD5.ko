// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2006-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Digest.MD5 =>

reexport Digest;
use Unsafe;

// This code is derived from RFC 1321:
//
// Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991.
// All rights reserved.
//
// License to copy and use this software is granted provided that it
// is identified as the "RSA Data Security, Inc. MD5 Message-Digest
// Algorithm" in all material mentioning or referencing this software
// or this function.
//
// License is also granted to make and use derivative works provided
// that such works are identified as "derived from the RSA Data
// Security, Inc. MD5 Message-Digest Algorithm" in all material
// mentioning or referencing the derived work.
//
// RSA Data Security, Inc. makes no representations concerning either
// the merchantability of this software or the suitability of this
// software for any particular purpose. It is provided "as is"
// without express or implied warranty of any kind.
//
// These notices must be retained in any copies of any part of this
// documentation and/or software.

/* MD5 context. */

c_define "
   #define KO_MD5_DIGEST_SIZE 16

   struct ko_md5_context {
      KO_OBJECT;
      ko_uint32_t state[4]; /* state (ABCD) */
      ko_uint32_t count[2]; /* number of bits, modulo 2^64 (lsb first) */
      ko_byte_t buffer[64]; /* input buffer */
   };

   KO_OBJECT_FIELD_ARRAY(md5_context, state, ko_uint32_t);
   KO_OBJECT_FIELD_ARRAY(md5_context, count, ko_uint32_t);
   KO_OBJECT_FIELD_ARRAY(md5_context, buffer, ko_byte_t);
";

c_type MD5_CONTEXT is:BYTE_OUTPUT (private MD5ContextCon)
   "KO_MOVE_OBJECT(sizeof(struct ko_md5_context));";

c_defineName [KO_MD5_CONTEXT:MD5_CONTEXT];
c_define [(c_con MD5ContextCon)] "
   KO_DESCR_CHECKER(md5_context, MD5ContextCon);

   #define KO_CHECK_IS_MD5_CONTEXT(obj) \\
      KO_CHECK_TYPE(obj, ko_is_md5_context(obj), KO_MD5_CONTEXT)
";

c_define "
   /* Constants for ko_md5_transform routine. */

   #define S11 7
   #define S12 12
   #define S13 17
   #define S14 22
   #define S21 5
   #define S22 9
   #define S23 14
   #define S24 20
   #define S31 4
   #define S32 11
   #define S33 16
   #define S34 23
   #define S41 6
   #define S42 10
   #define S43 15
   #define S44 21

   static ko_byte_t PADDING[64] = {
      0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
   };

   /* F, G, H and I are basic MD5 functions. */
   #define F(x, y, z) (((x) & (y)) | ((~x) & (z)))
   #define G(x, y, z) (((x) & (z)) | ((y) & (~z)))
   #define H(x, y, z) ((x) ^ (y) ^ (z))
   #define I(x, y, z) ((y) ^ ((x) | (~z)))

   /* ROTATE_LEFT rotates x left n bits. */
   #define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))

   /* FF, GG, HH, and II transformations for rounds 1, 2, 3, and 4.
    * Rotation is separate from addition to prevent recomputation. */
   #define FF(a, b, c, d, x, s, ac)                 \\
      KO_STAT_BEGIN {                               \\
         a += F(b, c, d) + (x) + (ko_uint32_t)(ac); \\
         a = ROTATE_LEFT(a, s);                     \\
         a += b;                                    \\
      } KO_STAT_END

   #define GG(a, b, c, d, x, s, ac)                 \\
      KO_STAT_BEGIN {                               \\
         a += G(b, c, d) + (x) + (ko_uint32_t)(ac); \\
         a = ROTATE_LEFT(a, s);                     \\
         a += b;                                    \\
      } KO_STAT_END

   #define HH(a, b, c, d, x, s, ac)                 \\
      KO_STAT_BEGIN {                               \\
         a += H(b, c, d) + (x) + (ko_uint32_t)(ac); \\
         a = ROTATE_LEFT(a, s);                     \\
         a += b;                                    \\
      } KO_STAT_END

   #define II(a, b, c, d, x, s, ac)                 \\
      KO_STAT_BEGIN {                               \\
         a += I(b, c, d) + (x) + (ko_uint32_t)(ac); \\
         a = ROTATE_LEFT(a, s);                     \\
         a += b;                                    \\
      } KO_STAT_END

   /* Encodes input (ko_uint32_t) into output (ko_byte_t). Assumes len is
    * a multiple of 4. */
   static void
   Encode(ko_byte_t *output, ko_uint32_t *input, unsigned len) {
      do {
         ko_uint32_t x = *input;
         output[0] = (ko_byte_t)x;
         output[1] = (ko_byte_t)(x >>  8);
         output[2] = (ko_byte_t)(x >> 16);
         output[3] = (ko_byte_t)(x >> 24);
         ++input; output += 4; len -= 4;
      } while (len != 0);
   }

   /* Decodes input (ko_byte_t) into output (ko_uint32_t). Assumes len is
    * a multiple of 4. */
   static void
   Decode(ko_uint32_t *output, ko_byte_t *input, unsigned len) {
      do {
         *output =
               (ko_uint32_t)input[0]
            + ((ko_uint32_t)input[1] <<  8)
            + ((ko_uint32_t)input[2] << 16)
            + ((ko_uint32_t)input[3] << 24);
         input += 4; ++output; len -= 4;
      } while (len != 0);
   }

   /* MD5 basic transformation. Transforms state based on block. */
   static void
   ko_md5_transform(ko_uint32_t state[4], ko_byte_t block[64]) {
      ko_uint32_t a = state[0], b = state[1], c = state[2], d = state[3],
         x[16];

      Decode(x, block, 64);

      /* Round 1 */
      FF(a, b, c, d, x[ 0], S11, 0xd76aa478); /* 1 */
      FF(d, a, b, c, x[ 1], S12, 0xe8c7b756); /* 2 */
      FF(c, d, a, b, x[ 2], S13, 0x242070db); /* 3 */
      FF(b, c, d, a, x[ 3], S14, 0xc1bdceee); /* 4 */
      FF(a, b, c, d, x[ 4], S11, 0xf57c0faf); /* 5 */
      FF(d, a, b, c, x[ 5], S12, 0x4787c62a); /* 6 */
      FF(c, d, a, b, x[ 6], S13, 0xa8304613); /* 7 */
      FF(b, c, d, a, x[ 7], S14, 0xfd469501); /* 8 */
      FF(a, b, c, d, x[ 8], S11, 0x698098d8); /* 9 */
      FF(d, a, b, c, x[ 9], S12, 0x8b44f7af); /* 10 */
      FF(c, d, a, b, x[10], S13, 0xffff5bb1); /* 11 */
      FF(b, c, d, a, x[11], S14, 0x895cd7be); /* 12 */
      FF(a, b, c, d, x[12], S11, 0x6b901122); /* 13 */
      FF(d, a, b, c, x[13], S12, 0xfd987193); /* 14 */
      FF(c, d, a, b, x[14], S13, 0xa679438e); /* 15 */
      FF(b, c, d, a, x[15], S14, 0x49b40821); /* 16 */

      /* Round 2 */
      GG(a, b, c, d, x[ 1], S21, 0xf61e2562); /* 17 */
      GG(d, a, b, c, x[ 6], S22, 0xc040b340); /* 18 */
      GG(c, d, a, b, x[11], S23, 0x265e5a51); /* 19 */
      GG(b, c, d, a, x[ 0], S24, 0xe9b6c7aa); /* 20 */
      GG(a, b, c, d, x[ 5], S21, 0xd62f105d); /* 21 */
      GG(d, a, b, c, x[10], S22,  0x2441453); /* 22 */
      GG(c, d, a, b, x[15], S23, 0xd8a1e681); /* 23 */
      GG(b, c, d, a, x[ 4], S24, 0xe7d3fbc8); /* 24 */
      GG(a, b, c, d, x[ 9], S21, 0x21e1cde6); /* 25 */
      GG(d, a, b, c, x[14], S22, 0xc33707d6); /* 26 */
      GG(c, d, a, b, x[ 3], S23, 0xf4d50d87); /* 27 */
      GG(b, c, d, a, x[ 8], S24, 0x455a14ed); /* 28 */
      GG(a, b, c, d, x[13], S21, 0xa9e3e905); /* 29 */
      GG(d, a, b, c, x[ 2], S22, 0xfcefa3f8); /* 30 */
      GG(c, d, a, b, x[ 7], S23, 0x676f02d9); /* 31 */
      GG(b, c, d, a, x[12], S24, 0x8d2a4c8a); /* 32 */

      /* Round 3 */
      HH(a, b, c, d, x[ 5], S31, 0xfffa3942); /* 33 */
      HH(d, a, b, c, x[ 8], S32, 0x8771f681); /* 34 */
      HH(c, d, a, b, x[11], S33, 0x6d9d6122); /* 35 */
      HH(b, c, d, a, x[14], S34, 0xfde5380c); /* 36 */
      HH(a, b, c, d, x[ 1], S31, 0xa4beea44); /* 37 */
      HH(d, a, b, c, x[ 4], S32, 0x4bdecfa9); /* 38 */
      HH(c, d, a, b, x[ 7], S33, 0xf6bb4b60); /* 39 */
      HH(b, c, d, a, x[10], S34, 0xbebfbc70); /* 40 */
      HH(a, b, c, d, x[13], S31, 0x289b7ec6); /* 41 */
      HH(d, a, b, c, x[ 0], S32, 0xeaa127fa); /* 42 */
      HH(c, d, a, b, x[ 3], S33, 0xd4ef3085); /* 43 */
      HH(b, c, d, a, x[ 6], S34,  0x4881d05); /* 44 */
      HH(a, b, c, d, x[ 9], S31, 0xd9d4d039); /* 45 */
      HH(d, a, b, c, x[12], S32, 0xe6db99e5); /* 46 */
      HH(c, d, a, b, x[15], S33, 0x1fa27cf8); /* 47 */
      HH(b, c, d, a, x[ 2], S34, 0xc4ac5665); /* 48 */

      /* Round 4 */
      II(a, b, c, d, x[ 0], S41, 0xf4292244); /* 49 */
      II(d, a, b, c, x[ 7], S42, 0x432aff97); /* 50 */
      II(c, d, a, b, x[14], S43, 0xab9423a7); /* 51 */
      II(b, c, d, a, x[ 5], S44, 0xfc93a039); /* 52 */
      II(a, b, c, d, x[12], S41, 0x655b59c3); /* 53 */
      II(d, a, b, c, x[ 3], S42, 0x8f0ccc92); /* 54 */
      II(c, d, a, b, x[10], S43, 0xffeff47d); /* 55 */
      II(b, c, d, a, x[ 1], S44, 0x85845dd1); /* 56 */
      II(a, b, c, d, x[ 8], S41, 0x6fa87e4f); /* 57 */
      II(d, a, b, c, x[15], S42, 0xfe2ce6e0); /* 58 */
      II(c, d, a, b, x[ 6], S43, 0xa3014314); /* 59 */
      II(b, c, d, a, x[13], S44, 0x4e0811a1); /* 60 */
      II(a, b, c, d, x[ 4], S41, 0xf7537e82); /* 61 */
      II(d, a, b, c, x[11], S42, 0xbd3af235); /* 62 */
      II(c, d, a, b, x[ 2], S43, 0x2ad7d2bb); /* 63 */
      II(b, c, d, a, x[ 9], S44, 0xeb86d391); /* 64 */

      state[0] += a;
      state[1] += b;
      state[2] += c;
      state[3] += d;
   }

   /* MD5 initialization. Begins an MD5 operation, writing a new context. */
   static void
   ko_md5_init(ko_value_t context) {
      ko_count_of_md5_context(context)[0] = 0;
      ko_count_of_md5_context(context)[1] = 0;
      /* Load magic initialization constants. */
      ko_state_of_md5_context(context)[0] = 0x67452301;
      ko_state_of_md5_context(context)[1] = 0xefcdab89;
      ko_state_of_md5_context(context)[2] = 0x98badcfe;
      ko_state_of_md5_context(context)[3] = 0x10325476;
   }

   /* MD5 block update operation. Continues an MD5 message-digest
    * operation, processing another message block, and updating the
    * context. */
   static void
   ko_md5_update(ko_value_t context, ko_byte_t *input, ko_nint_t inputLen) {
      ko_nint_t i, index, partLen;

      /* Compute number of bytes mod 64 */
      index = (ko_nint_t)((ko_count_of_md5_context(context)[0] >> 3) & 0x3F);

      /* Update number of bits */
      if (
         (ko_count_of_md5_context(context)[0] += ((ko_uint32_t)inputLen << 3))
         < ((ko_uint32_t)inputLen << 3)
      )
         ++ko_count_of_md5_context(context)[1];
      ko_count_of_md5_context(context)[1] += (ko_uint32_t)(inputLen >> 29);

      partLen = 64 - index;

      /* Transform as many times as possible. */
      if (inputLen >= partLen) {
         memcpy(ko_buffer_of_md5_context(context) + index, input, partLen);
         ko_md5_transform(ko_state_of_md5_context(context),
            ko_buffer_of_md5_context(context));

         for (i = partLen; i + 63 < inputLen; i += 64)
            ko_md5_transform(ko_state_of_md5_context(context), input + i);

         index = 0;
      }
      else
         i = 0;

      /* Buffer remaining input */
      memcpy(ko_buffer_of_md5_context(context) + index, input + i,
         inputLen - i);
   }

   /* MD5 finalization. Ends an MD5 message-digest operation,
    * writing the message digest. */
   static void
   ko_md5_result(ko_byte_t digest[KO_MD5_DIGEST_SIZE], ko_value_t context) {
      ko_byte_t bits[8];
      ko_nint_t index, padLen;

      /* Save number of bits */
      Encode(bits, ko_count_of_md5_context(context), 8);

      /* Pad out to 56 mod 64. */
      index = (ko_nint_t)((ko_count_of_md5_context(context)[0] >> 3) & 0x3F);
      padLen = (index < 56) ? (56 - index) : (120 - index);
      ko_md5_update(context, PADDING, padLen);

      /* Append length (before padding) */
      ko_md5_update(context, bits, 8);

      /* Store state in digest */
      Encode(digest, ko_state_of_md5_context(context), 16);
   }
";

Serialization "Digest.MD5.MD5_CONTEXT" MD5_CONTEXT ?value {
   c_stat [value] "KO_CHECK_IS_MD5_CONTEXT(value);";
   loop 0 ?i {
      if (i %UnsafeLT 4) =>
      c_let [byte0 byte1 byte2 byte3] [value i] "
         ko_uint32_t x = ko_state_of_md5_context(value)[ko_nint_of_sint(i)];
         byte0 = ko_sint(x & 0xFF);
         byte1 = ko_sint((x >> 8) & 0xFF);
         byte2 = ko_sint((x >> 16) & 0xFF);
         byte3 = ko_sint(x >> 24);
      ";
      SerializeByte byte0;
      SerializeByte byte1;
      SerializeByte byte2;
      SerializeByte byte3;
      again (i %UnsafeAdd 1)
   };
   c_let [byte0 byte1 byte2 byte3 byte4 byte5 byte6 byte7] [value] "
      ko_uint32_t x = ko_count_of_md5_context(value)[0];
      byte0 = ko_sint(x & 0xFF);
      byte1 = ko_sint((x >> 8) & 0xFF);
      byte2 = ko_sint((x >> 16) & 0xFF);
      byte3 = ko_sint(x >> 24);
      x = ko_count_of_md5_context(value)[1];
      byte4 = ko_sint(x & 0xFF);
      byte5 = ko_sint((x >> 8) & 0xFF);
      byte6 = ko_sint((x >> 16) & 0xFF);
      byte7 = ko_sint(x >> 24);
   ";
   SerializeByte byte0;
   SerializeByte byte1;
   SerializeByte byte2;
   SerializeByte byte3;
   SerializeByte byte4;
   SerializeByte byte5;
   SerializeByte byte6;
   SerializeByte byte7;
   let index = c_expr [value] "
      ko_sint((ko_count_of_md5_context(value)[0] >> 3) & 0x3F)
   ";
   loop 0 ?i {
      if (i %UnsafeLT index) =>
      SerializeByte (c_expr [value i] "
         ko_sint(ko_buffer_of_md5_context(value)[ko_nint_of_sint(i)])
      ");
      again (i %UnsafeAdd 1)
   };
   RegisterSerialized value
} {
   let value = c_eval [(c_con MD5ContextCon)] "
      KO_RESULT = ko_alloc_bytes(sizeof(struct ko_md5_context));
      ko_init_descr(KO_RESULT, MD5ContextCon);
   ";
   loop 0 ?i {
      if (i %UnsafeLT 4) =>
      let byte0 = DeserializeByte();
      let byte1 = DeserializeByte();
      let byte2 = DeserializeByte();
      let byte3 = DeserializeByte();
      c_stat [value i byte0 byte1 byte2 byte3] "
         ko_state_of_md5_context(value)[ko_nint_of_sint(i)] =
               (ko_uint32_t)ko_nint_of_sint(byte0)
            + ((ko_uint32_t)ko_nint_of_sint(byte1) << 8)
            + ((ko_uint32_t)ko_nint_of_sint(byte2) << 16)
            + ((ko_uint32_t)ko_nint_of_sint(byte3) << 24);
      ";
      again (i %UnsafeAdd 1)
   };
   let byte0 = DeserializeByte();
   let byte1 = DeserializeByte();
   let byte2 = DeserializeByte();
   let byte3 = DeserializeByte();
   let byte4 = DeserializeByte();
   let byte5 = DeserializeByte();
   let byte6 = DeserializeByte();
   let byte7 = DeserializeByte();
   let index = c_eval [value byte0 byte1 byte2 byte3 byte4 byte5 byte6 byte7] "
      ko_uint32_t index;
      ko_count_of_md5_context(value)[0] = index =
            (ko_uint32_t)ko_nint_of_sint(byte0)
         + ((ko_uint32_t)ko_nint_of_sint(byte1) << 8)
         + ((ko_uint32_t)ko_nint_of_sint(byte2) << 16)
         + ((ko_uint32_t)ko_nint_of_sint(byte3) << 24);
      ko_count_of_md5_context(value)[1] =
            (ko_uint32_t)ko_nint_of_sint(byte4)
         + ((ko_uint32_t)ko_nint_of_sint(byte5) << 8)
         + ((ko_uint32_t)ko_nint_of_sint(byte6) << 16)
         + ((ko_uint32_t)ko_nint_of_sint(byte7) << 24);
      KO_RESULT = ko_sint((index >> 3) & 63);
   ";
   loop 0 ?i {
      if (i %UnsafeLT index) =>
      c_stat [value i byte:(DeserializeByte())] "
         ko_buffer_of_md5_context(value)[ko_nint_of_sint(i)] =
            ko_nint_of_sint(byte);
      ";
      again (i %UnsafeAdd 1)
   };
   RegisterDeserialized value
};

def MD5Context [
   () {
      c_eval [(c_con MD5ContextCon)] "
         KO_RESULT = ko_alloc_bytes(sizeof(struct ko_md5_context));
         ko_init_descr(KO_RESULT, MD5ContextCon);
         ko_md5_init(KO_RESULT);
      "
   }
   data {
      let context = MD5Context();
      c_stat [context data] "
         KO_CHECK_IS_BYTE_ARRAY(data);
         ko_md5_update(context, ko_elems_of_byte_array(data),
            ko_nint_of_sint(ko_size_of_array(data)));
      ";
      context
   }
];

method WriteSomeTo context^MD5_CONTEXT data maxSize _flush {
   c_stat [context data maxSize] "
      ko_value_t size;
      KO_CHECK_IS_BYTE_ARRAY(data);
      KO_CHECK_ARRAY_IS_UNLOCKED(data);
      KO_NORMALIZE_BUFFER_SIZE(size, maxSize, ko_size_of_array(data));
      ko_md5_update(context, ko_elems_of_byte_array(data),
         ko_nint_of_sint(size));
      KO_REMOVE_FIRST_PART_BYTE_ARRAY(data, size);
   "
};

method WriteTo context^MD5_CONTEXT data^BYTE_ARRAY {
   c_stat [context data] "
      ko_md5_update(context, ko_elems_of_byte_array(data),
         ko_nint_of_sint(ko_size_of_array(data)));
   "
};

method FlushTo _^MD5_CONTEXT _ {};

method Digest context^MD5_CONTEXT {
   c_eval [context] "
      struct ko_md5_context copy;
      memcpy(&copy, context, sizeof(struct ko_md5_context));
      KO_ALLOC_BYTE_ARRAY(KO_RESULT, ko_sint(KO_MD5_DIGEST_SIZE));
      ko_md5_result(ko_elems_of_byte_array(KO_RESULT), KO_STATIC_PTR(copy));
   "
};
