// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Compression.BZip2Internal =>

c_define "#include <bzlib.h>";

subtypes PROGRAM_ERROR {
   type BZIP2_CONFIG_ERROR   BZip2ConfigError;
   type BZIP2_SEQUENCE_ERROR BZip2SequenceError;
   type BZIP2_PARAM_ERROR    BZip2ParamError;
   type BZIP2_UNKNOWN_ERROR  BZip2UnknownError;
};
subtypes EXTERNAL_ERROR {
   type BZIP2_DATA_ERROR {
      type BZIP2_DATA_ERROR_STREAM BZip2DataError;
      type BZIP2_DATA_ERROR_MAGIC  BZip2DataErrorMagic;
   };
   type BZIP2_IO_ERROR       BZip2IOError;
   type BZIP2_UNEXPECTED_EOF BZip2UnexpectedEOF;
};

method ShowException _^BZIP2_CONFIG_ERROR     {"BZip2: configuration error"};
method ShowException _^BZIP2_SEQUENCE_ERROR   {"BZip2: sequence error"};
method ShowException _^BZIP2_PARAM_ERROR      {"BZip2: parameter error"};
method ShowException _^BZIP2_DATA_ERROR       {"BZip2: data error"};
method ShowException _^BZIP2_DATA_ERROR_MAGIC {"BZip2: data error \
   (file not created by bzip2)"};
method ShowException _^BZIP2_IO_ERROR         {"BZip2: I/O error"};
method ShowException _^BZIP2_UNEXPECTED_EOF   {"BZip2: unexpected \
   end of file"};
method ShowException _^BZIP2_UNKNOWN_ERROR    {"BZip2: unknown error"};

SingletonSerialization "Compression.BZip2.BZIP2_CONFIG_ERROR"
   BZIP2_CONFIG_ERROR BZip2ConfigError;
SingletonSerialization "Compression.BZip2.BZIP2_SEQUENCE_ERROR"
   BZIP2_SEQUENCE_ERROR BZip2SequenceError;
SingletonSerialization "Compression.BZip2.BZIP2_PARAM_ERROR"
   BZIP2_PARAM_ERROR BZip2ParamError;
TypeSerialization "Compression.BZip2.BZIP2_DATA_ERROR"
   BZIP2_DATA_ERROR;
SingletonSerialization "Compression.BZip2.BZIP2_DATA_ERROR_STREAM"
   BZIP2_DATA_ERROR_STREAM BZip2DataError;
SingletonSerialization "Compression.BZip2.BZIP2_DATA_ERROR_MAGIC"
   BZIP2_DATA_ERROR_MAGIC BZip2DataErrorMagic;
SingletonSerialization "Compression.BZip2.BZIP2_IO_ERROR"
   BZIP2_IO_ERROR BZip2IOError;
SingletonSerialization "Compression.BZip2.BZIP2_UNEXPECTED_EOF"
   BZIP2_UNEXPECTED_EOF BZip2UnexpectedEOF;
SingletonSerialization "Compression.BZip2.BZIP2_UNKNOWN_ERROR"
   BZIP2_UNKNOWN_ERROR BZip2UnknownError;

c_export "ko_value_t ko_decode_bzip2_error(int status);";

c_define [
   BZip2ConfigError BZip2SequenceError BZip2ParamError OutOfMemory
   BZip2DataError BZip2DataErrorMagic BZip2IOError BZip2UnexpectedEOF
   BZip2UnknownError
] "
   ko_value_t
   ko_decode_bzip2_error(int status) {
      switch (status) {
         #ifdef BZ_CONFIG_ERROR
            case BZ_CONFIG_ERROR:
               return BZip2ConfigError;
         #endif
         case BZ_SEQUENCE_ERROR:
            return BZip2SequenceError;
         case BZ_PARAM_ERROR:
            return BZip2ParamError;
         case BZ_MEM_ERROR:
            return OutOfMemory;
         case BZ_DATA_ERROR:
            return BZip2DataError;
         case BZ_DATA_ERROR_MAGIC:
            return BZip2DataErrorMagic;
         case BZ_IO_ERROR:
            return BZip2IOError;
         case BZ_UNEXPECTED_EOF:
            return BZip2UnexpectedEOF;
         default:
            return BZip2UnknownError;
      }
   }
";
