// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Foreign.Perl.Call =>

use Foreign.Perl.Scalars;
use Foreign.Perl.KogutInPerl;
use Foreign.Perl.Subroutines []; // ko_is_perl_sub

private def ToPerlScalarOrSub value {
   if (c_cond [value] "ko_is_perl_scalar(value) || ko_is_perl_sub(value)")
      {value} =>
   let sv = PerlScalar value;
   c_stat [sv] "KO_CHECK_IS_PERL_SCALAR(sv);";
   sv
};

private def ToPerlScalars [
   (obj\objs) {
      case obj [
         (left, right) {ToPerlScalar left \ ToPerlScalars (right\objs)}
         _ {ToPerlScalar obj \ ToPerlScalars objs}
      ]
   }
   _ {[]}
];

// Subroutines

def ApplyVoid [
   sub {
      c_stat #callback [sub:(ToPerlScalarOrSub sub)] "
         SV *subPl = ko_to_perl_scalar(sub);
         dSP;
         PUSHMARK(SP);
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(subPl, G_VOID | G_EVAL);
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN();
      "
   }
   sub (arg1 & ~(_, _)) {
      c_stat #callback [sub:(ToPerlScalarOrSub sub) arg1:(ToPerlScalar arg1)] "
         SV *subPl = ko_to_perl_scalar(sub);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(arg1));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(subPl, G_VOID | G_EVAL);
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN();
      "
   }
   sub (arg1 & ~(_, _)) (arg2 & ~(_, _)) {
      c_stat #callback [
         sub:(ToPerlScalarOrSub sub) arg1:(ToPerlScalar arg1)
         arg2:(ToPerlScalar arg2)
      ] "
         SV *subPl = ko_to_perl_scalar(sub);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(arg1));
         XPUSHs(ko_to_perl_scalar(arg2));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(subPl, G_VOID | G_EVAL);
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN();
      "
   }
   sub (arg1 & ~(_, _)) (arg2 & ~(_, _)) (arg3 & ~(_, _)) {
      c_stat #callback [
         sub:(ToPerlScalarOrSub sub) arg1:(ToPerlScalar arg1)
         arg2:(ToPerlScalar arg2) arg3:(ToPerlScalar arg3)
      ] "
         SV *subPl = ko_to_perl_scalar(sub);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(arg1));
         XPUSHs(ko_to_perl_scalar(arg2));
         XPUSHs(ko_to_perl_scalar(arg3));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(subPl, G_VOID | G_EVAL);
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN();
      "
   }
   sub args... {
      c_stat #callback [
         sub:(ToPerlScalarOrSub sub) args:(ToPerlScalars args)
      ] "
         SV *subPl = ko_to_perl_scalar(sub);
         ko_value_t iter;
         dSP;
         PUSHMARK(SP);
         for (iter = args; iter != KO_Nil; iter = ko_rest(iter))
            XPUSHs(ko_to_perl_scalar(ko_first(iter)));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(subPl, G_VOID | G_EVAL);
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN();
      "
   }
];

def ApplyScalar [
   sub {
      c_eval #callback [sub:(ToPerlScalarOrSub sub)] "
         SV *subPl = ko_to_perl_scalar(sub);
         dSP;
         PUSHMARK(SP);
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(subPl, G_SCALAR | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            POPs; PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(POPs; PUTBACK;);
         KO_RESULT = ko_wrap_perl_scalar_inc(POPs);
         PUTBACK;
      "
   }
   sub (arg1 & ~(_, _)) {
      c_eval #callback [sub:(ToPerlScalarOrSub sub) arg1:(ToPerlScalar arg1)] "
         SV *subPl = ko_to_perl_scalar(sub);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(arg1));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(subPl, G_SCALAR | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            POPs; PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(POPs; PUTBACK;);
         KO_RESULT = ko_wrap_perl_scalar_inc(POPs);
         PUTBACK;
      "
   }
   sub (arg1 & ~(_, _)) (arg2 & ~(_, _)) {
      c_eval #callback [
         sub:(ToPerlScalarOrSub sub) arg1:(ToPerlScalar arg1)
         arg2:(ToPerlScalar arg2)
      ] "
         SV *subPl = ko_to_perl_scalar(sub);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(arg1));
         XPUSHs(ko_to_perl_scalar(arg2));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(subPl, G_SCALAR | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            POPs; PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(POPs; PUTBACK;);
         KO_RESULT = ko_wrap_perl_scalar_inc(POPs);
         PUTBACK;
      "
   }
   sub (arg1 & ~(_, _)) (arg2 & ~(_, _)) (arg3 & ~(_, _)) {
      c_eval #callback [
         sub:(ToPerlScalarOrSub sub) arg1:(ToPerlScalar arg1)
         arg2:(ToPerlScalar arg2) arg3:(ToPerlScalar arg3)
      ] "
         SV *subPl = ko_to_perl_scalar(sub);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(arg1));
         XPUSHs(ko_to_perl_scalar(arg2));
         XPUSHs(ko_to_perl_scalar(arg3));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(subPl, G_SCALAR | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            POPs; PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(POPs; PUTBACK;);
         KO_RESULT = ko_wrap_perl_scalar_inc(POPs);
         PUTBACK;
      "
   }
   sub args... {
      c_eval #callback [
         sub:(ToPerlScalarOrSub sub) args:(ToPerlScalars args)
      ] "
         SV *subPl = ko_to_perl_scalar(sub);
         ko_value_t iter;
         dSP;
         PUSHMARK(SP);
         for (iter = args; iter != KO_Nil; iter = ko_rest(iter))
            XPUSHs(ko_to_perl_scalar(ko_first(iter)));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(subPl, G_SCALAR | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            POPs; PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(POPs; PUTBACK;);
         KO_RESULT = ko_wrap_perl_scalar_inc(POPs);
         PUTBACK;
      "
   }
];

def ApplyNumber [
   sub                {ApplyScalar sub               ->Number}
   sub arg1           {ApplyScalar sub arg1          ->Number}
   sub arg1 arg2      {ApplyScalar sub arg1 arg2     ->Number}
   sub arg1 arg2 arg3 {ApplyScalar sub arg1 arg2 arg3->Number}
   sub args...        {ApplyScalar sub args...       ->Number}
];

def ApplyInt [
   sub                {ApplyScalar sub               ->Int}
   sub arg1           {ApplyScalar sub arg1          ->Int}
   sub arg1 arg2      {ApplyScalar sub arg1 arg2     ->Int}
   sub arg1 arg2 arg3 {ApplyScalar sub arg1 arg2 arg3->Int}
   sub args...        {ApplyScalar sub args...       ->Int}
];

def ApplyFloat [
   sub                {ApplyScalar sub               ->Float}
   sub arg1           {ApplyScalar sub arg1          ->Float}
   sub arg1 arg2      {ApplyScalar sub arg1 arg2     ->Float}
   sub arg1 arg2 arg3 {ApplyScalar sub arg1 arg2 arg3->Float}
   sub args...        {ApplyScalar sub args...       ->Float}
];

def ApplyString [
   sub                {ApplyScalar sub               ->String}
   sub arg1           {ApplyScalar sub arg1          ->String}
   sub arg1 arg2      {ApplyScalar sub arg1 arg2     ->String}
   sub arg1 arg2 arg3 {ApplyScalar sub arg1 arg2 arg3->String}
   sub args...        {ApplyScalar sub args...       ->String}
];

def ApplyBool [
   sub                {ApplyScalar sub               ->IsPerlTrue}
   sub arg1           {ApplyScalar sub arg1          ->IsPerlTrue}
   sub arg1 arg2      {ApplyScalar sub arg1 arg2     ->IsPerlTrue}
   sub arg1 arg2 arg3 {ApplyScalar sub arg1 arg2 arg3->IsPerlTrue}
   sub args...        {ApplyScalar sub args...       ->IsPerlTrue}
];

def ApplyUnwrap [
   sub                {ApplyScalar sub               ->UnwrapFromPerl}
   sub arg1           {ApplyScalar sub arg1          ->UnwrapFromPerl}
   sub arg1 arg2      {ApplyScalar sub arg1 arg2     ->UnwrapFromPerl}
   sub arg1 arg2 arg3 {ApplyScalar sub arg1 arg2 arg3->UnwrapFromPerl}
   sub args...        {ApplyScalar sub args...       ->UnwrapFromPerl}
];

def ApplyList [
   sub {
      c_eval #callback result [sub:(ToPerlScalarOrSub sub)] [elem] "
         SV *subPl = ko_to_perl_scalar(sub);
         ko_nint_t count;
         dSP;
         PUSHMARK(SP);
         PUTBACK;
         KO_ENTER_FOREIGN();
         count = call_sv(subPl, G_ARRAY | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            while (count != 0) {POPs; --count;}
            PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(
            while (count != 0) {POPs; --count;}
            PUTBACK;);
         result = KO_Nil;
         while (count != 0) {
            ko_value_t cons;
            elem = ko_wrap_perl_scalar_inc(POPs);
            cons = ko_alloc(3);
            ko_init_descr(cons, KO_ConsCon);
            ko_init_first(cons, elem);
            ko_init_rest(cons, result);
            result = cons;
            --count;
         }
         PUTBACK;
      "
   }
   sub (arg1 & ~(_, _)) {
      c_eval #callback result [
         sub:(ToPerlScalarOrSub sub) arg1:(ToPerlScalar arg1)
      ] [elem] "
         SV *subPl = ko_to_perl_scalar(sub);
         ko_nint_t count;
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(arg1));
         PUTBACK;
         KO_ENTER_FOREIGN();
         count = call_sv(subPl, G_ARRAY | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            while (count != 0) {POPs; --count;}
            PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(
            while (count != 0) {POPs; --count;}
            PUTBACK;);
         result = KO_Nil;
         while (count != 0) {
            ko_value_t cons;
            elem = ko_wrap_perl_scalar_inc(POPs);
            cons = ko_alloc(3);
            ko_init_descr(cons, KO_ConsCon);
            ko_init_first(cons, elem);
            ko_init_rest(cons, result);
            result = cons;
            --count;
         }
         PUTBACK;
      "
   }
   sub (arg1 & ~(_, _)) (arg2 & ~(_, _)) {
      c_eval #callback result [
         sub:(ToPerlScalarOrSub sub) arg1:(ToPerlScalar arg1)
         arg2:(ToPerlScalar arg2)
      ] [elem] "
         SV *subPl = ko_to_perl_scalar(sub);
         ko_nint_t count;
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(arg1));
         XPUSHs(ko_to_perl_scalar(arg2));
         PUTBACK;
         KO_ENTER_FOREIGN();
         count = call_sv(subPl, G_ARRAY | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            while (count != 0) {POPs; --count;}
            PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(
            while (count != 0) {POPs; --count;}
            PUTBACK;);
         result = KO_Nil;
         while (count != 0) {
            ko_value_t cons;
            elem = ko_wrap_perl_scalar_inc(POPs);
            cons = ko_alloc(3);
            ko_init_descr(cons, KO_ConsCon);
            ko_init_first(cons, elem);
            ko_init_rest(cons, result);
            result = cons;
            --count;
         }
         PUTBACK;
      "
   }
   sub (arg1 & ~(_, _)) (arg2 & ~(_, _)) (arg3 & ~(_, _)) {
      c_eval #callback result [
         sub:(ToPerlScalarOrSub sub) arg1:(ToPerlScalar arg1)
         arg2:(ToPerlScalar arg2) arg3:(ToPerlScalar arg3)
      ] [elem] "
         SV *subPl = ko_to_perl_scalar(sub);
         ko_nint_t count;
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(arg1));
         XPUSHs(ko_to_perl_scalar(arg2));
         XPUSHs(ko_to_perl_scalar(arg3));
         PUTBACK;
         KO_ENTER_FOREIGN();
         count = call_sv(subPl, G_ARRAY | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            while (count != 0) {POPs; --count;}
            PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(
            while (count != 0) {POPs; --count;}
            PUTBACK;);
         result = KO_Nil;
         while (count != 0) {
            ko_value_t cons;
            elem = ko_wrap_perl_scalar_inc(POPs);
            cons = ko_alloc(3);
            ko_init_descr(cons, KO_ConsCon);
            ko_init_first(cons, elem);
            ko_init_rest(cons, result);
            result = cons;
            --count;
         }
         PUTBACK;
      "
   }
   sub args... {
      c_eval #callback result [
         sub:(ToPerlScalarOrSub sub) args:(ToPerlScalars args)
      ] [elem] "
         SV *subPl = ko_to_perl_scalar(sub);
         ko_value_t iter;
         ko_nint_t count;
         dSP;
         PUSHMARK(SP);
         for (iter = args; iter != KO_Nil; iter = ko_rest(iter))
            XPUSHs(ko_to_perl_scalar(ko_first(iter)));
         PUTBACK;
         KO_ENTER_FOREIGN();
         count = call_sv(subPl, G_ARRAY | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            while (count != 0) {POPs; --count;}
            PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(
            while (count != 0) {POPs; --count;}
            PUTBACK;);
         result = KO_Nil;
         while (count != 0) {
            ko_value_t cons;
            elem = ko_wrap_perl_scalar_inc(POPs);
            cons = ko_alloc(3);
            ko_init_descr(cons, KO_ConsCon);
            ko_init_first(cons, elem);
            ko_init_rest(cons, result);
            result = cons;
            --count;
         }
         PUTBACK;
      "
   }
];

// Methods

def CallVoid [
   obj method {
      c_stat #callback [obj:(ToPerlScalar obj) method:(ToPerlScalar method)] "
         SV *methodPl = ko_to_perl_scalar(method);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(methodPl, G_METHOD | G_VOID | G_EVAL);
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN();
      "
   }
   obj method (arg1 & ~(_, _)) {
      c_stat #callback [
         obj:(ToPerlScalar obj) method:(ToPerlScalar method)
         arg1:(ToPerlScalar arg1)
      ] "
         SV *methodPl = ko_to_perl_scalar(method);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         XPUSHs(ko_to_perl_scalar(arg1));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(methodPl, G_METHOD | G_VOID | G_EVAL);
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN();
      "
   }
   obj method (arg1 & ~(_, _)) (arg2 & ~(_, _)) {
      c_stat #callback [
         obj:(ToPerlScalar obj) method:(ToPerlScalar method)
         arg1:(ToPerlScalar arg1) arg2:(ToPerlScalar arg2)
      ] "
         SV *methodPl = ko_to_perl_scalar(method);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         XPUSHs(ko_to_perl_scalar(arg1));
         XPUSHs(ko_to_perl_scalar(arg2));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(methodPl, G_METHOD | G_VOID | G_EVAL);
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN();
      "
   }
   obj method args... {
      c_stat #callback [
         obj:(ToPerlScalar obj) method:(ToPerlScalar method)
         args:(ToPerlScalars args)
      ] "
         SV *methodPl = ko_to_perl_scalar(method);
         ko_value_t iter;
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         for (iter = args; iter != KO_Nil; iter = ko_rest(iter))
            XPUSHs(ko_to_perl_scalar(ko_first(iter)));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(methodPl, G_METHOD | G_VOID | G_EVAL);
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN();
      "
   }
];

def CallScalar [
   obj method {
      c_eval #callback [obj:(ToPerlScalar obj) method:(ToPerlScalar method)] "
         SV *methodPl = ko_to_perl_scalar(method);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(methodPl, G_METHOD | G_SCALAR | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            POPs; PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(POPs; PUTBACK;);
         KO_RESULT = ko_wrap_perl_scalar_inc(POPs);
         PUTBACK;
      "
   }
   obj method (arg1 & ~(_, _)) {
      c_eval #callback [
         obj:(ToPerlScalar obj) method:(ToPerlScalar method)
         arg1:(ToPerlScalar arg1)
      ] "
         SV *methodPl = ko_to_perl_scalar(method);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         XPUSHs(ko_to_perl_scalar(arg1));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(methodPl, G_METHOD | G_SCALAR | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            POPs; PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(POPs; PUTBACK;);
         KO_RESULT = ko_wrap_perl_scalar_inc(POPs);
         PUTBACK;
      "
   }
   obj method (arg1 & ~(_, _)) (arg2 & ~(_, _)) {
      c_eval #callback [
         obj:(ToPerlScalar obj) method:(ToPerlScalar method)
         arg1:(ToPerlScalar arg1) arg2:(ToPerlScalar arg2)
      ] "
         SV *methodPl = ko_to_perl_scalar(method);
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         XPUSHs(ko_to_perl_scalar(arg1));
         XPUSHs(ko_to_perl_scalar(arg2));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(methodPl, G_METHOD | G_SCALAR | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            POPs; PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(POPs; PUTBACK;);
         KO_RESULT = ko_wrap_perl_scalar_inc(POPs);
         PUTBACK;
      "
   }
   obj method args... {
      c_eval #callback [
         obj:(ToPerlScalar obj) method:(ToPerlScalar method)
         args:(ToPerlScalars args)
      ] "
         SV *methodPl = ko_to_perl_scalar(method);
         ko_value_t iter;
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         for (iter = args; iter != KO_Nil; iter = ko_rest(iter))
            XPUSHs(ko_to_perl_scalar(ko_first(iter)));
         PUTBACK;
         KO_ENTER_FOREIGN();
         call_sv(methodPl, G_METHOD | G_SCALAR | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            POPs; PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(POPs; PUTBACK;);
         KO_RESULT = ko_wrap_perl_scalar_inc(POPs);
         PUTBACK;
      "
   }
];

def CallNumber [
   obj method           {CallScalar obj method          ->Number}
   obj method arg1      {CallScalar obj method arg1     ->Number}
   obj method arg1 arg2 {CallScalar obj method arg1 arg2->Number}
   obj method args...   {CallScalar obj method args...  ->Number}
];

def CallInt [
   obj method           {CallScalar obj method          ->Int}
   obj method arg1      {CallScalar obj method arg1     ->Int}
   obj method arg1 arg2 {CallScalar obj method arg1 arg2->Int}
   obj method args...   {CallScalar obj method args...  ->Int}
];

def CallFloat [
   obj method           {CallScalar obj method          ->Float}
   obj method arg1      {CallScalar obj method arg1     ->Float}
   obj method arg1 arg2 {CallScalar obj method arg1 arg2->Float}
   obj method args...   {CallScalar obj method args...  ->Float}
];

def CallString [
   obj method           {CallScalar obj method          ->String}
   obj method arg1      {CallScalar obj method arg1     ->String}
   obj method arg1 arg2 {CallScalar obj method arg1 arg2->String}
   obj method args...   {CallScalar obj method args...  ->String}
];

def CallBool [
   obj method           {CallScalar obj method          ->IsPerlTrue}
   obj method arg1      {CallScalar obj method arg1     ->IsPerlTrue}
   obj method arg1 arg2 {CallScalar obj method arg1 arg2->IsPerlTrue}
   obj method args...   {CallScalar obj method args...  ->IsPerlTrue}
];

def CallUnwrap [
   obj method           {CallScalar obj method          ->UnwrapFromPerl}
   obj method arg1      {CallScalar obj method arg1     ->UnwrapFromPerl}
   obj method arg1 arg2 {CallScalar obj method arg1 arg2->UnwrapFromPerl}
   obj method args...   {CallScalar obj method args...  ->UnwrapFromPerl}
];

def CallList [
   obj method {
      c_eval #callback result [
         obj:(ToPerlScalar obj) method:(ToPerlScalar method)
      ] [elem] "
         SV *methodPl = ko_to_perl_scalar(method);
         ko_nint_t count;
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         PUTBACK;
         KO_ENTER_FOREIGN();
         count = call_sv(methodPl, G_METHOD | G_ARRAY | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            while (count != 0) {POPs; --count;}
            PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(
            while (count != 0) {POPs; --count;}
            PUTBACK;);
         result = KO_Nil;
         while (count != 0) {
            ko_value_t cons;
            elem = ko_wrap_perl_scalar_inc(POPs);
            cons = ko_alloc(3);
            ko_init_descr(cons, KO_ConsCon);
            ko_init_first(cons, elem);
            ko_init_rest(cons, result);
            result = cons;
            --count;
         }
         PUTBACK;
      "
   }
   obj method (arg1 & ~(_, _)) {
      c_eval #callback result [
         obj:(ToPerlScalar obj) method:(ToPerlScalar method)
         arg1:(ToPerlScalar arg1)
      ] [elem] "
         SV *methodPl = ko_to_perl_scalar(method);
         ko_nint_t count;
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         XPUSHs(ko_to_perl_scalar(arg1));
         PUTBACK;
         KO_ENTER_FOREIGN();
         count = call_sv(methodPl, G_METHOD | G_ARRAY | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            while (count != 0) {POPs; --count;}
            PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(
            while (count != 0) {POPs; --count;}
            PUTBACK;);
         result = KO_Nil;
         while (count != 0) {
            ko_value_t cons;
            elem = ko_wrap_perl_scalar_inc(POPs);
            cons = ko_alloc(3);
            ko_init_descr(cons, KO_ConsCon);
            ko_init_first(cons, elem);
            ko_init_rest(cons, result);
            result = cons;
            --count;
         }
         PUTBACK;
      "
   }
   obj method (arg1 & ~(_, _)) (arg2 & ~(_, _)) {
      c_eval #callback result [
         obj:(ToPerlScalar obj) method:(ToPerlScalar method)
         arg1:(ToPerlScalar arg1) arg2:(ToPerlScalar arg2)
      ] [elem] "
         SV *methodPl = ko_to_perl_scalar(method);
         ko_nint_t count;
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         XPUSHs(ko_to_perl_scalar(arg1));
         XPUSHs(ko_to_perl_scalar(arg2));
         PUTBACK;
         KO_ENTER_FOREIGN();
         count = call_sv(methodPl, G_METHOD | G_ARRAY | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            while (count != 0) {POPs; --count;}
            PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(
            while (count != 0) {POPs; --count;}
            PUTBACK;);
         result = KO_Nil;
         while (count != 0) {
            ko_value_t cons;
            elem = ko_wrap_perl_scalar_inc(POPs);
            cons = ko_alloc(3);
            ko_init_descr(cons, KO_ConsCon);
            ko_init_first(cons, elem);
            ko_init_rest(cons, result);
            result = cons;
            --count;
         }
         PUTBACK;
      "
   }
   obj method args... {
      c_eval #callback result [
         obj:(ToPerlScalar obj) method:(ToPerlScalar method)
         args:(ToPerlScalars args)
      ] [elem] "
         SV *methodPl = ko_to_perl_scalar(method);
         ko_value_t iter;
         ko_nint_t count;
         dSP;
         PUSHMARK(SP);
         XPUSHs(ko_to_perl_scalar(obj));
         for (iter = args; iter != KO_Nil; iter = ko_rest(iter))
            XPUSHs(ko_to_perl_scalar(ko_first(iter)));
         PUTBACK;
         KO_ENTER_FOREIGN();
         count = call_sv(methodPl, G_METHOD | G_ARRAY | G_EVAL);
         SPAGAIN;
         if (KO_UNLIKELY(SvTRUE(ERRSV))) {
            while (count != 0) {POPs; --count;}
            PUTBACK;
            ko_leave_foreign_no_signals();
            KO_FAIL(ko_from_perl_exception(ERRSV));
         }
         KO_LEAVE_FOREIGN_CLEANUP(
            while (count != 0) {POPs; --count;}
            PUTBACK;);
         result = KO_Nil;
         while (count != 0) {
            ko_value_t cons;
            elem = ko_wrap_perl_scalar_inc(POPs);
            cons = ko_alloc(3);
            ko_init_descr(cons, KO_ConsCon);
            ko_init_first(cons, elem);
            ko_init_rest(cons, result);
            result = cons;
            --count;
         }
         PUTBACK;
      "
   }
];
