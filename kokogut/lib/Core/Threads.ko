// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2009 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Threads =>

use Prelude;
reexport Kokogut.Threads.Core [CurrentThread THREAD];
reexport Kokogut.Threads.Signals [
   ACTION_SIGNAL ActionSignal FORK_PROCESS_SIGNAL ForkProcessSignal
   SignalThread CancelThread
];
reexport Kokogut.Threads.SystemSignals;
reexport Kokogut.Threads.Scheduler;
reexport Kokogut.Threads.NewThread [Thread ThreadBlockSignals EachThread];
reexport Kokogut.Threads.Mutexes [
   NO_CONDITION_PREDICATE NoConditionPredicate
   CONDITION Condition Wait Notify Notify1
];
reexport Kokogut.Threads.Joining;
reexport Kokogut.Threads.SleepingForever;
reexport Kokogut.Finalizers [FINALIZER Finalizer Touch];
reexport Kokogut.GC;
reexport Kokogut.Threads.WithAsyncExit;
reexport Kokogut.Threads.Boxes;
reexport Kokogut.Threads.Queues;
reexport Kokogut.Threads.EventQueues;

// Showing exceptions

method ShowException _^NO_CONDITION_PREDICATE {
   "Condition predicate was not given when the condition was created, \
      so it must be given to Wait"
};

// Serialization

SingletonSerialization "Threads.NO_CONDITION_PREDICATE" NO_CONDITION_PREDICATE NoConditionPredicate;
TypeSerialization "Threads.ACTION_SIGNAL" ACTION_SIGNAL;
TypeSerialization "Threads.FORK_PROCESS_SIGNAL" FORK_PROCESS_SIGNAL;
TypeSerialization "Threads.THREAD" THREAD;
TypeSerialization "Threads.CONDITION" CONDITION;

// Cancellation of other threads and garbage collection at program exit

private def WaitForAllThreads() {
   c_case [thread] "
      thread = ko_young_threads;
      while (thread != KO_ABSENT) {
         if (thread != ko_current_thread) goto present;
         thread = ko_next_of_all_of_thread(thread);
      }
      thread = ko_old_threads;
      while (thread != KO_ABSENT) {
         if (thread != ko_current_thread) goto present;
         thread = ko_next_of_all_of_thread(thread);
      }
      goto absent;
   " [
      absent:  {}
      present: {
         UnblockSyncSignals {WaitForThread thread Ignore};
         again()
      }
   ]
};

Register AtExit {
   SystemSignalHandler = CurrentThread;
   let mutex = Mutex();
   var otherThreads = 0;
   var noOtherThreads = Condition mutex {otherThreads %Is 0};
   EachThread ?thread {
      CancelThread thread;
      Lock mutex =>
      otherThreads = otherThreads + 1
   } ?_thread {
      Lock mutex =>
      otherThreads = otherThreads - 1;
      if (otherThreads %Is 0) {Notify noOtherThreads}
   } {
      Lock mutex {
         UnblockSignals {Wait noOtherThreads}
      };
      loop =>
      GarbageCollect();
      Lock mutex {
         if (otherThreads %Is 0) {Ignore} =>
         UnblockSignals {Wait noOtherThreads};
         again
      }->Apply
   }
};
