// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Lazy =>

use Kokogut.ExceptionTypes;
use Kokogut.Exceptions;
use Kokogut.Threads.Signals;

c_export "
   /* For LazyComputeCon: */
   KO_RECORD_FIELD(lazy, 0, compute);
   KO_RECORD_FIELD(lazy, 1, dynamic);

   /* For LazyComputingCon: */
   KO_RECORD_FIELD(lazy, 0, thread);
   KO_RECORD_FIELD(lazy, 1, first);
   KO_RECORD_FIELD(lazy, 2, last);

   /* For LazyValueCon: */
   KO_RECORD_FIELD(lazy, 0, value);

   /* For LazyExceptionCon: */
   KO_RECORD_FIELD(lazy, 0, exception);

   #define KO_LINK_TO_LAZY(lazy)                                     \\
      KO_STAT_BEGIN {                                                \\
         ko_value_t last_ = ko_last_of_lazy(lazy);                   \\
         if (last_ == KO_ABSENT)                                     \\
            ko_set_first_of_lazy(lazy, ko_current_thread);           \\
         else {                                                      \\
            ko_set_next_related_of_thread(last_, ko_current_thread); \\
            ko_set_prev_related_of_thread(ko_current_thread, last_); \\
            ko_mark_changed_object(last_);                           \\
         }                                                           \\
         ko_set_last_of_lazy(lazy, ko_current_thread);               \\
      } KO_STAT_END

   #define KO_UNLINK_FIRST_FROM_LAZY(lazy, thread)             \\
      KO_STAT_BEGIN {                                          \\
         ko_value_t next_ = ko_next_related_of_thread(thread); \\
         if (next_ == KO_ABSENT) {                             \\
            ko_init_first_of_lazy(lazy, KO_ABSENT);            \\
            ko_init_last_of_lazy(lazy, KO_ABSENT);             \\
         }                                                     \\
         else {                                                \\
            ko_set_next_related_of_thread(thread, KO_ABSENT);  \\
            ko_set_prev_related_of_thread(next_, KO_ABSENT);   \\
            ko_set_first_of_lazy_nogc(lazy, next_);            \\
         }                                                     \\
      } KO_STAT_END

   #define KO_UNLINK_FROM_LAZY(lazy, thread)                   \\
      KO_STAT_BEGIN {                                          \\
         ko_value_t prev_ = ko_prev_related_of_thread(thread); \\
         ko_value_t next_ = ko_next_related_of_thread(thread); \\
         if (prev_ == KO_ABSENT)                               \\
            ko_set_first_of_lazy_nogc(lazy, next_);            \\
         else {                                                \\
            ko_set_next_related_of_thread(prev_, next_);       \\
            ko_set_prev_related_of_thread(thread, KO_ABSENT);  \\
            ko_mark_changed_object(prev_);                     \\
         }                                                     \\
         if (next_ == KO_ABSENT)                               \\
            ko_set_last_of_lazy_nogc(lazy, prev_);             \\
         else {                                                \\
            ko_set_next_related_of_thread(thread, KO_ABSENT);  \\
            ko_set_prev_related_of_thread(next_, prev_);       \\
            ko_mark_changed_object(next_);                     \\
         }                                                     \\
      } KO_STAT_END
";

c_type LAZY {

   // The size of the object in each state must be large enough to hold the
   // object in possible subsequent sizes.

   c_con LazyComputeCon "
      KO_MOVE_RECORD_BEGIN(3);
      ko_mark_count(2);
      ko_mark(ko_compute_ptr_of_lazy(dest));
      ko_mark(ko_dynamic_ptr_of_lazy(dest));
      KO_MOVE_RECORD_END(4); /* 4 for switching the constructor */
   " [{
      c_case [compute old] [
         this (c_con LazyComputeCon) (c_con LazyComputingCon)
      ] "
         if (KO_UNLIKELY(ko_descr_of_ptr(this) != LazyComputeCon)) goto retry;
         ko_blocked_all = ko_add1(ko_blocked_all);
         compute = ko_compute_of_lazy(this);
         old = ko_dynamic;
         ko_dynamic = ko_dynamic_of_lazy(this);
         ko_init_descr(this, LazyComputingCon);
         ko_set_thread_of_lazy(this, ko_current_thread);
         ko_init_first_of_lazy(this, KO_ABSENT);
         ko_init_last_of_lazy(this, KO_ABSENT);
         goto computing;
      " [
         retry: {this()}
         computing: {
            try compute exn {
               c_stat [this old (c_con LazyExceptionCon) exn] "
                  ko_value_t thread;
                  ko_dynamic = old;
                  thread = ko_first_of_lazy(this);
                  if (KO_UNLIKELY(thread != KO_ABSENT)) {
                     do {
                        ko_value_t next = ko_next_related_of_thread(thread);
                        ko_unblock_thread(thread);
                        ko_set_code_of_thread(thread, ko_fail);
                        ko_registers_of_thread(thread)[1] = exn;
                        ko_mark_changed_object(thread);
                        thread = next;
                     } while (thread != KO_ABSENT);
                     ko_init_first_of_lazy(this, KO_ABSENT);
                     ko_init_last_of_lazy(this, KO_ABSENT);
                  }
                  ko_init_descr(this, LazyExceptionCon);
                  ko_set_exception_of_lazy(this, exn);
               ";
               Fail exn
            } ?value {
               c_stat [this old (c_con LazyValueCon) value] "
                  ko_value_t thread;
                  ko_dynamic = old;
                  thread = ko_first_of_lazy(this);
                  if (KO_UNLIKELY(thread != KO_ABSENT)) {
                     do {
                        ko_value_t next = ko_next_related_of_thread(thread);
                        ko_unblock_thread(thread);
                        ko_registers_of_thread(thread)[1] = value;
                        ko_mark_changed_object(thread);
                        thread = next;
                     } while (thread != KO_ABSENT);
                     ko_init_first_of_lazy(this, KO_ABSENT);
                     ko_init_last_of_lazy(this, KO_ABSENT);
                  }
                  ko_init_descr(this, LazyValueCon);
                  ko_set_value_of_lazy(this, value);
               ";
               if (c_cond "
                  KO_UNLIKELY(ko_signal_pending()) &&
                  ko_blocked_all == ko_sint(1) &&
                  ko_blocked_async == ko_sint(0)
               ") {HandleSignals()};
               c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
               value
            }
         }
      ]
   }];

   c_con LazyComputingCon "
      KO_MOVE_RECORD_BEGIN(4);
      ko_mark_count(3);
      ko_mark(ko_thread_ptr_of_lazy(dest));
      ko_mark(ko_first_ptr_of_lazy(dest));
      ko_mark(ko_last_ptr_of_lazy(dest));
      KO_MOVE_RECORD_END(4);
   " [{
      c_case [] [this LockAlreadyLocked] "
         if (KO_UNLIKELY(ko_thread_of_lazy(this) == ko_current_thread))
            KO_FAIL(LockAlreadyLocked);
         ko_set_state_of_thread(ko_current_thread,
            &ko_thread_waiting_for_lazy);
         ko_set_value_of_thread(ko_current_thread, this);
         KO_LINK_TO_LAZY(this);
         KO_CONTEXT_SWITCH();
      " []
   }];

   c_con LazyValueCon "
      KO_MOVE_RECORD_BEGIN(2);
      ko_mark(ko_value_ptr_of_lazy(dest));
      KO_MOVE_RECORD_END(2);
   " [{c_expr [this] "ko_value_of_lazy(this)"}];

   c_con LazyExceptionCon "
      KO_MOVE_RECORD_BEGIN(2);
      ko_mark(ko_exception_ptr_of_lazy(dest));
      KO_MOVE_RECORD_END(2);
   " [{Fail (c_expr [this] "ko_exception_of_lazy(this)")}];
};

private def SignalWhileWaitingForLazy state {
   HandleSignals();
   c_case [] [state] "
      ko_value_t lazy = ko_value_of_signal_state(state);
      KO_SIGNALS_HANDLED(state, KO_R[0] = lazy;,
         ko_descr_of_ptr(KO_R[0])->ko_fast_entries[0]);
   " []
};

c_export "void ko_unlink_waiting_for_lazy(ko_value_t thread);";

c_define [(c_entry SignalWhileWaitingForLazy 1)] "
   static void
   ko_kick_waiting_for_lazy(ko_value_t thread, ko_value_t signalState) {
      ko_value_t lazy;
      (void)signalState;
      lazy = ko_value_of_thread(thread);
      KO_UNLINK_FROM_LAZY(lazy, thread);
      ko_set_restart(thread, SignalWhileWaitingForLazy1);
      ko_link_thread(thread);
   }

   void
   ko_unlink_waiting_for_lazy(ko_value_t thread) {
      ko_value_t lazy = ko_value_of_thread(thread);
      KO_UNLINK_FROM_LAZY(lazy, thread);
   }

   static struct ko_thread_state ko_thread_waiting_for_lazy = {
      ko_send_signal_by_queue,
      ko_handles_signals_async,
      ko_kick_waiting_for_lazy,
      ko_unlink_waiting_for_lazy,
      ko_true /* ko_signal_deadlock */
   };
";
