// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2009 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Time =>

use Kokogut.Core;
use Kokogut.ExceptionTypes;
use Kokogut.Exceptions;
use Kokogut.Numbers.Ints;

type TIME TimeCon ticks {};

c_export "KO_RECORD_FIELD(time, 0, ticks);";

c_exportName [KO_TIME:TIME KO_TimeCon:(c_con TimeCon)];
c_export "
   KO_DESCR_CHECKER(time, KO_TimeCon);

   #define KO_CHECK_IS_TIME(obj) \\
      KO_CHECK_TYPE(obj, ko_is_time(obj), KO_TIME)
";

let TicksPerSecond = 1000000000;

method Is time1^TIME time2 {
   time1.ticks %Is time2.ticks
};

method Hash time^TIME {Hash time.ticks};

method '==' time1^TIME time2^TIME {
   time1.ticks == time2.ticks
};
method '<' time1^TIME time2^TIME {
   time1.ticks < time2.ticks
};
method '<=' time1^TIME time2^TIME {
   time1.ticks <= time2.ticks
};
method Compare time1^TIME time2^TIME {
   time1.ticks %Compare time2.ticks
};

method '+' time^TIME seconds^REAL {
   let ticks = time.ticks + Int (seconds * TicksPerSecond);
   c_stat [ticks] "KO_CHECK_IS_INT(ticks);";
   TimeCon ticks
};
method '-' time^TIME seconds^REAL {
   let ticks = time.ticks - Int (seconds * TicksPerSecond);
   c_stat [ticks] "KO_CHECK_IS_INT(ticks);";
   TimeCon ticks
};
method '-' time1^TIME time2^TIME {
   (time1.ticks - time2.ticks) / TicksPerSecond
};

c_export "
   typedef ko_int32_t ko_tick_t;

   static inline void
   ko_ticks_of_seconds(mpz_t ticks, time_t second) {
      #if SIZEOF_TIME_T <= SIZEOF_LONG
         mpz_set_ui(ticks, second);
      #elif SIZEOF_TIME_T <= 2 * SIZEOF_LONG
         mpz_set_ui(ticks,
            (unsigned long)(second >> (SIZEOF_LONG * CHAR_BIT)));
         mpz_mul_2exp(ticks, ticks, SIZEOF_LONG * CHAR_BIT);
         mpz_add_ui(ticks, ticks, (unsigned long)second);
      #else
         #error Strange time_t size
      #endif
      mpz_mul_ui(ticks, ticks, 1000000000);
   }
   int ko_seconds_of_ticks(time_t *second, ko_tick_t *tick, ko_value_t ticks);

   static inline void
   ko_ticks_of_timespec(mpz_t ticks, const ko_timespec_t *tv) {
      ko_ticks_of_seconds(ticks, tv->tv_sec);
      mpz_add_ui(ticks, ticks, tv->tv_nsec);
   }

   /* ko_timespec_of_ticks rounds the time up, and returns the minimal
    * or maximal possible timespec on overflow, so it is appropriate for
    * computing the end of a timeout with guaranteed lower bound of the
    * length. */
   void ko_timespec_of_ticks(ko_timespec_t *tv, ko_value_t ticks);

   static inline ko_bool_t
   ko_lt_time(const ko_timespec_t *tv1, const ko_timespec_t *tv2) {
      return tv1->tv_sec != tv2->tv_sec ? tv1->tv_sec < tv2->tv_sec :
         tv1->tv_nsec < tv2->tv_nsec;
   }

   static inline void
   ko_add_time(ko_timespec_t *result, const ko_timespec_t *tv1,
      const ko_timespec_t *tv2)
   {
      result->tv_sec = tv1->tv_sec + tv2->tv_sec;
      result->tv_nsec = tv1->tv_nsec + tv2->tv_nsec;
      if (result->tv_nsec >= 1000000000) {
         result->tv_nsec -= 1000000000;
         ++result->tv_sec;
      }
   }

   static inline void
   ko_sub_time(ko_timespec_t *result, const ko_timespec_t *tv1,
      const ko_timespec_t *tv2)
   {
      result->tv_sec = tv1->tv_sec - tv2->tv_sec;
      result->tv_nsec = tv1->tv_nsec - tv2->tv_nsec;
      if (result->tv_nsec < 0) {
         result->tv_nsec += 1000000000;
         --result->tv_sec;
      }
   }

   static inline void
   ko_get_time(ko_timespec_t *tv) {
      #if HAVE_CLOCK_GETTIME
         clock_gettime(CLOCK_REALTIME, tv);
      #else
         struct timeval sys_tv;
         gettimeofday(&sys_tv, NULL);
         tv->tv_sec = sys_tv.tv_sec;
         tv->tv_nsec = (long)sys_tv.tv_usec * 1000;
      #endif
   }
";

c_define "
   int
   ko_seconds_of_ticks(time_t *second, ko_tick_t *tick, ko_value_t ticks) {
      if (ko_is_sint(ticks)) {
         ko_nint_t ticksv, sec;
         if (KO_UNLIKELY(ko_lt0(ticks))) return -1;
         ticksv = ko_nint_of_sint(ticks);
         sec = ticksv / 1000000000;
         *second = sec;
         #if SIZEOF_TIME_T < KO_SIZEOF_NINT
            if (KO_UNLIKELY(*second != sec)) return 1;
         #endif
         if (tick != NULL) *tick = ticksv % 1000000000;
      }
      else {
         mpz_t sec;
         #if !HAVE___GMP_TDIV_UI
            mpz_t nsec_big;
         #endif
         ko_tick_t tickv;
         if (KO_UNLIKELY(ko_sgn_of_bint(ticks) < 0)) return -1;
         mpz_init(sec);
         #if HAVE___GMP_TDIV_UI
            tickv = mpz_fdiv_q_ui(sec, ko_mpz_of_bint(ticks), 1000000000);
         #else
            mpz_init(nsec_big);
            tickv = mpz_fdiv_qr_ui(sec, nsec_big, ko_mpz_of_bint(ticks),
               1000000000);
            mpz_clear(nsec_big);
         #endif
         if (tick != NULL) *tick = tickv;
         #if SIZEOF_TIME_T == SIZEOF_INT
            if (KO_UNLIKELY(mpz_cmp_ui(sec, INT_MAX) > 0)) {
               mpz_clear(sec);
               return 1;
            }
            *second = mpz_get_ui(sec);
         #elif SIZEOF_TIME_T == SIZEOF_LONG
            if (KO_UNLIKELY(mpz_cmp_ui(sec, LONG_MAX) > 0)) {
               mpz_clear(sec);
               return 1;
            }
            *second = mpz_get_ui(sec);
         #elif SIZEOF_TIME_T == SIZEOF_LONG_LONG
            *second = mpz_get_ui(sec);
            mpz_tdiv_q_2exp(sec, sec, SIZEOF_LONG * CHAR_BIT);
            if (KO_UNLIKELY(mpz_cmp_ui(sec, LONG_MAX) > 0)) {
               mpz_clear(sec);
               return 1;
            }
            *second += (time_t)mpz_get_ui(sec) << (SIZEOF_LONG * CHAR_BIT)
         #else
            #error Strange time_t size
         #endif
         mpz_clear(sec);
      }
      return 0;
   }

   void
   ko_timespec_of_ticks(ko_timespec_t *tv, ko_value_t ticks) {
      time_t second;
      ko_tick_t tick;
      int overflow;
      overflow = ko_seconds_of_ticks(&second, &tick, ticks);
      if (KO_UNLIKELY(overflow != 0)) {
         if (overflow < 0) {
            tv->tv_sec = 0;
            tv->tv_nsec = 0;
         }
         else {
            #if SIZEOF_TIME_T == SIZEOF_INT
               tv->tv_sec = INT_MAX;
            #elif SIZEOF_TIME_T == SIZEOF_LONG
               tv->tv_sec = LONG_MAX;
            #elif SIZEOF_TIME_T == SIZEOF_LONG_LONG
               tv->tv_sec = LLONG_MAX;
            #else
               #error Strange time_t size
            #endif
            tv->tv_nsec = 999999999;
         }
         return;
      }
      tv->tv_sec = second;
      tv->tv_nsec = tick;
   }
";

def Time [
   () {
      c_eval "
         ko_timespec_t tv;
         mpz_t ticks;
         ko_get_time(&tv);
         mpz_init(ticks);
         ko_ticks_of_timespec(ticks, &tv);
         KO_RESULT = ko_int_of_mpz(ticks);
      "->TimeCon
   }
   time! {}
];

method Time ticks^INT {TimeCon ticks};

def TicksNow() {
   c_eval "
      ko_timespec_t tv;
      mpz_t ticks;
      ko_get_time(&tv);
      mpz_init(ticks);
      ko_ticks_of_timespec(ticks, &tv);
      KO_RESULT = ko_int_of_mpz(ticks);
   "
};

def AbsoluteTime timeout! {};

method AbsoluteTime time^TIME {time};

method AbsoluteTime seconds^REAL {
   c_eval [d:(Int (seconds * TicksPerSecond))] "
      ko_timespec_t tv;
      mpz_t ticks;
      KO_CHECK_IS_INT(d);
      ko_get_time(&tv);
      mpz_init(ticks);
      ko_ticks_of_timespec(ticks, &tv);
      if (ko_is_sint(d)) {
         ko_nint_t dv = ko_nint_of_sint(d);
         if (dv >= 0)
            mpz_add_ui(ticks, ticks, dv);
         else
            mpz_sub_ui(ticks, ticks, -dv);
      }
      else
         mpz_add(ticks, ticks, ko_mpz_of_bint(d));
      KO_RESULT = ko_int_of_mpz(ticks);
   "->TimeCon
};

def AbsoluteTicksInFuture timeout! {};

method AbsoluteTicksInFuture time^TIME {
   c_eval [ticks:time.ticks] "
      ko_timespec_t tv;
      mpz_t ticksNow;
      ko_get_time(&tv);
      mpz_init(ticksNow);
      ko_ticks_of_timespec(ticksNow, &tv);
      KO_RESULT = ko_is_sint(ticks) ?
         mpz_cmp_si(ticksNow, ko_nint_of_sint(ticks)) < 0 ? ticks : KO_Null :
         mpz_cmp   (ticksNow, ko_mpz_of_bint(ticks))  < 0 ? ticks : KO_Null;
      mpz_clear(ticksNow);
   "
};

method AbsoluteTicksInFuture seconds^REAL {
   c_eval [d:(Int (seconds * TicksPerSecond))] "
      ko_timespec_t tv;
      mpz_t ticks;
      KO_CHECK_IS_INT(d);
      ko_get_time(&tv);
      mpz_init(ticks);
      ko_ticks_of_timespec(ticks, &tv);
      if (ko_is_sint(d)) {
         ko_nint_t dv = ko_nint_of_sint(d);
         if (dv > 0) {
            mpz_add_ui(ticks, ticks, dv);
            KO_RESULT = ko_bint_of_mpz(ticks);
         }
         else {
            mpz_clear(ticks);
            KO_RESULT = KO_Null;
         }
      }
      else {
         if (ko_sgn_of_bint(d) >= 0) {
            mpz_add(ticks, ticks, ko_mpz_of_bint(d));
            KO_RESULT = ko_bint_of_mpz(ticks);
         }
         else {
            mpz_clear(ticks);
            KO_RESULT = KO_Null;
         }
      }
   "
};

method AbsoluteTicksInFuture time {
   AbsoluteTicksInFuture (AbsoluteTime time)
};

private def FailTimeOutOfRange value {
   Fail (TimeOutOfRange value) Null
};
c_exportName [KO_FailTimeOutOfRange1:(c_entry FailTimeOutOfRange 1)];
c_export "
   #define KO_TIME_OUT_OF_RANGE(time) \\
      KO_FAIL_BY_JUMP1(KO_FailTimeOutOfRange1, time)
";
