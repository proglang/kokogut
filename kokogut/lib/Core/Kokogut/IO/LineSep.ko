// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2005-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.IO.LineSep =>

use Kokogut.Core;
use Kokogut.ExceptionTypes;
use Kokogut.Exceptions;
use Kokogut.IO.Streams [];
use Kokogut.IO.Filters;

c_define "
   struct ko_line_sep_decoder_auto {
      KO_OBJECT;
      ko_bool_t cr;
   };

   KO_OBJECT_FIELD_RAW(line_sep_decoder_auto, cr, ko_bool_t);
";

private {
   type   LINE_SEP_FILTER_CR    is:CHAR_FILTER LineSepFilterCR;
   type   LINE_SEP_DECODER_CRLF is:CHAR_FILTER LineSepDecoderCRLF;
   c_type LINE_SEP_DECODER_AUTO is:CHAR_FILTER LineSepDecoderAutoCon
      "KO_MOVE_OBJECT(struct ko_line_sep_decoder_auto);";
   def LineSepDecoderAuto() {
      c_eval [(c_con LineSepDecoderAutoCon)] "
         KO_RESULT = ko_alloc_bytes(sizeof(struct ko_line_sep_decoder_auto));
         ko_init_descr(KO_RESULT, LineSepDecoderAutoCon);
         ko_init_cr_of_line_sep_decoder_auto(KO_RESULT, ko_false);
      "
   };
};

def LineSepDecodingInput [
   input #auto {input->FilteredCharInput (LineSepDecoderAuto())}
   input #lf   {input}
   input #crlf {input->FilteredCharInput LineSepDecoderCRLF}
   input #cr   {input->FilteredCharInput LineSepFilterCR}
   _ lineSep {Fail (BadArgument lineSep "lineSep" "#lf | #cr | #crlf | #auto")}
];

method FilterBlock _^LINE_SEP_FILTER_CR
   input maxInputSize output maxOutputSize _flush
{
   c_case [] [input maxInputSize output maxOutputSize] "
      ko_value_t inputSize, size, outputPos;
      const ko_wchar_t *src;
      ko_wchar_t *dest;
      ko_nint_t count;
      KO_CHECK_IS_CHAR_ARRAY(input);
      KO_CHECK_ARRAY_IS_UNLOCKED(input);
      KO_NORMALIZE_BUFFER_SIZE(inputSize, maxInputSize,
         ko_size_of_array(input));
      KO_CHECK_IS_CHAR_ARRAY(output);
      KO_CHECK_ARRAY_IS_UNLOCKED(output);
      KO_NORMALIZE_BUFFER_SIZE(size, maxOutputSize, inputSize);
      outputPos = ko_size_of_array(output);
      KO_ADD_LAST_PART_CHAR_ARRAY(output, size);
      src = ko_elems_of_char_array(input);
      dest = ko_ptr_at_char_array(output, outputPos);
      for (count = ko_nint_of_sint(size); count != 0; --count) {
         ko_wchar_t ch = *src;
         if (KO_UNLIKELY(ch == 10)) ch = 13;
         else if (KO_UNLIKELY(ch == 13)) ch = 10;
         *dest = ch;
         ++src; ++dest;
      }
      KO_REMOVE_FIRST_PART_CHAR_ARRAY(input, size);
      if (ko_lt(size, inputSize)) goto overflow;
      goto done;
   " [
      done:     {False}
      overflow: {True}
   ]
};

method FilterBlock _^LINE_SEP_DECODER_CRLF
   input maxInputSize output maxOutputSize flush
{
   c_case [] [input maxInputSize output maxOutputSize flush noneSym:#none] "
      ko_value_t inputSize, outputSize, outputPos;
      const ko_wchar_t *src;
      ko_wchar_t *dest;
      ko_nint_t inputCount, outputCount;
      KO_CHECK_IS_CHAR_ARRAY(input);
      KO_CHECK_ARRAY_IS_UNLOCKED(input);
      KO_NORMALIZE_BUFFER_SIZE(inputSize, maxInputSize,
         ko_size_of_array(input));
      KO_CHECK_IS_CHAR_ARRAY(output);
      KO_CHECK_ARRAY_IS_UNLOCKED(output);
      KO_NORMALIZE_BUFFER_SIZE(outputSize, maxOutputSize, inputSize);
      outputPos = ko_size_of_array(output);
      KO_ADD_LAST_PART_CHAR_ARRAY(output, outputSize);
      src = ko_elems_of_char_array(input);
      dest = ko_ptr_at_char_array(output, outputPos);
      inputCount = ko_nint_of_sint(inputSize);
      outputCount = ko_nint_of_sint(outputSize);
      while (inputCount != 0 && outputCount != 0) {
         ko_wchar_t ch = *src;
         if (KO_UNLIKELY(ch == 13)) {
            if (KO_UNLIKELY(inputCount == 1)) {
               if (flush == noneSym) break;
            }
            else if (KO_LIKELY(src[1] == 10)) {
               ++src; --inputCount;
               ch = 10;
            }
         }
         *dest = ch;
         ++src; --inputCount;
         ++dest; --outputCount;
      }
      inputSize = ko_sub_sn(inputSize, inputCount);
      KO_REMOVE_FIRST_PART_CHAR_ARRAY(input, inputSize);
      outputSize = ko_sub_sn(outputSize, outputCount);
      ko_set_size_of_array(output, ko_add(outputPos, outputSize));
      if (inputCount != 0 && outputCount == 0) goto overflow;
      goto done;
   " [
      done:     {False}
      overflow: {True}
   ]
};

method FilterBlock decoder^LINE_SEP_DECODER_AUTO
   input maxInputSize output maxOutputSize _flush
{
   c_case [] [decoder input maxInputSize output maxOutputSize] "
      ko_value_t inputSize, outputSize, outputPos;
      const ko_wchar_t *src;
      ko_wchar_t *dest;
      ko_nint_t inputCount, outputCount;
      KO_CHECK_IS_CHAR_ARRAY(input);
      KO_CHECK_ARRAY_IS_UNLOCKED(input);
      KO_NORMALIZE_BUFFER_SIZE(inputSize, maxInputSize,
         ko_size_of_array(input));
      KO_CHECK_IS_CHAR_ARRAY(output);
      KO_CHECK_ARRAY_IS_UNLOCKED(output);
      KO_NORMALIZE_BUFFER_SIZE(outputSize, maxOutputSize, inputSize);
      outputPos = ko_size_of_array(output);
      KO_ADD_LAST_PART_CHAR_ARRAY(output, outputSize);
      src = ko_elems_of_char_array(input);
      dest = ko_ptr_at_char_array(output, outputPos);
      inputCount = ko_nint_of_sint(inputSize);
      outputCount = ko_nint_of_sint(outputSize);
      if (
         KO_UNLIKELY(ko_cr_of_line_sep_decoder_auto(decoder)) &&
         inputCount != 0
      ) {
         if (*src == 10) {
            ++src; --inputCount;
         }
         ko_set_cr_of_line_sep_decoder_auto(decoder, ko_false);
      }
      while (inputCount != 0 && outputCount != 0) {
         ko_wchar_t ch = *src;
         if (KO_UNLIKELY(ch == 13)) {
            if (KO_UNLIKELY(inputCount == 1))
               ko_set_cr_of_line_sep_decoder_auto(decoder, ko_true);
            else if (src[1] == 10) {
               ++src; --inputCount;
            }
            ch = 10;
         }
         *dest = ch;
         ++src; --inputCount;
         ++dest; --outputCount;
      }
      inputSize = ko_sub_sn(inputSize, inputCount);
      KO_REMOVE_FIRST_PART_CHAR_ARRAY(input, inputSize);
      outputSize = ko_sub_sn(outputSize, outputCount);
      ko_set_size_of_array(output, ko_add(outputPos, outputSize));
      if (inputCount != 0 && outputCount == 0) goto overflow;
      goto done;
   " [
      done:     {False}
      overflow: {True}
   ]
};

dynamic DefaultLineSep = c_expr [lf:#lf cr:#cr crlf:#crlf]
   "KO_DEFAULT_LINE_SEP";

private {
   type LINE_SEP_ENCODER_CRLF is:CHAR_FILTER LineSepEncoderCRLF;
};

def LineSepEncodingOutput [
   output #lf     {output}
   output #crlf   {output->FilteredCharOutput LineSepEncoderCRLF}
   output #cr     {output->FilteredCharOutput LineSepFilterCR}
   _      lineSep {Fail (BadArgument lineSep "lineSep" "#lf | #cr | #crlf")}
];

method FilterBlock _^LINE_SEP_ENCODER_CRLF
   input maxInputSize output maxOutputSize _flush
{
   c_case [] [input maxInputSize output maxOutputSize] "
      ko_value_t inputSize, outputSize, outputSpace, outputPos;
      const ko_wchar_t *src;
      ko_wchar_t *dest;
      ko_nint_t inputCount, outputCount;
      ko_value_t inputShift, outputShift;
      KO_CHECK_IS_CHAR_ARRAY(input);
      KO_CHECK_ARRAY_IS_UNLOCKED(input);
      KO_NORMALIZE_BUFFER_SIZE(inputSize, maxInputSize,
         ko_size_of_array(input));
      if (inputSize == ko_sint(0)) goto done;
      KO_CHECK_IS_CHAR_ARRAY(output);
      KO_CHECK_ARRAY_IS_UNLOCKED(output);
      KO_NORMALIZE_OUTPUT_BUFFER_SIZE(outputSize, maxOutputSize);
      outputSpace = ko_add_sn(inputSize, 16);
   again:
      if (ko_gt(outputSpace, ko_mul_sn(inputSize, 2)))
         outputSpace = ko_mul_sn(inputSize, 2);
      if (ko_gt(outputSpace, outputSize)) outputSpace = outputSize;
      outputPos = ko_size_of_array(output);
      KO_ADD_LAST_PART_CHAR_ARRAY(output, outputSpace);
      src = ko_elems_of_char_array(input);
      dest = ko_ptr_at_char_array(output, outputPos);
      inputCount = ko_nint_of_sint(inputSize);
      outputCount = ko_nint_of_sint(outputSpace);
      while (inputCount != 0 && outputCount != 0) {
         ko_wchar_t ch = *src;
         if (KO_UNLIKELY(ch == 10)) {
            if (KO_UNLIKELY(outputCount == 1)) {
               outputSize = ko_sub1(outputSize);
               break;
            }
            *dest++ = 13;
            --outputCount;
         }
         *dest = ch;
         ++src; ++dest; --inputCount; --outputCount;
      }
      inputShift = ko_sub_sn(inputSize, inputCount);
      KO_REMOVE_FIRST_PART_CHAR_ARRAY(input, inputShift);
      outputShift = ko_sub_sn(outputSpace, outputCount);
      ko_set_size_of_array(output, ko_add(outputPos, outputShift));
      if (inputCount != 0) {
         inputSize = ko_sub(inputSize, inputShift);
         outputSize = ko_sub(outputSize, outputShift);
         if (outputSize != ko_sint(0)) goto again;
         goto overflow;
      }
      goto done;
   " [
      done:     {False}
      overflow: {True}
   ]
};
