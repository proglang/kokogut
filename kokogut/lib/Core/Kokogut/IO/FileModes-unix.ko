// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.IO.FileModes =>

use Kokogut.Core;
use Kokogut.ExceptionTypes;
use Kokogut.Strings [];
use Kokogut.Records;

c_define "
   #ifndef S_IFLNK
      #if defined(_S_IFLNK)
         #define S_IFLNK _S_IFLNK
      #elif defined(__S_IFLNK)
         #define S_IFLNK __S_IFLNK
      #else
         #error Cannot find S_IFLNK definition
      #endif
   #endif

   #ifndef S_IFSOCK
      #if defined(_S_IFSOCK)
         #define S_IFSOCK _S_IFSOCK
      #elif defined(__S_IFSOCK)
         #define S_IFSOCK __S_IFSOCK
      #else
         #error Cannot find S_IFSOCK definition
      #endif
   #endif

   #ifndef S_ISVTX
      #if defined(_S_ISVTX)
         #define S_ISVTX _S_ISVTX
      #elif defined(__S_ISVTX)
         #define S_ISVTX __S_ISVTX
      #else
         #error Cannot find S_ISVTX definition
      #endif
   #endif
";

private let BadFileType = ImpossibleError "Bad file type";

c_export "
   struct ko_file_mode {
      KO_OBJECT;
      mode_t value;
   };

   KO_OBJECT_FIELD_RAW(file_mode, value, mode_t);
";

c_define "
   #define KO_GET_MODE(mask) ((ko_value_of_file_mode(this) & (mask)) != 0)
";

c_type FILE_MODE (private FileModeCon) "KO_MOVE_OBJECT(struct ko_file_mode);" [
   #code {c_expr [this] "ko_int(ko_value_of_file_mode(this))"}
   #type {
      c_eval [this blkSym:#blk chrSym:#chr fifoSym:#fifo regSym:#reg
      dirSym:#dir lnkSym:#lnk sockSym:#sock BadFileType] "
         switch (ko_value_of_file_mode(this) & S_IFMT) {
            case S_IFBLK:
               KO_RESULT = blkSym;
               break;
            case S_IFCHR:
               KO_RESULT = chrSym;
               break;
            case S_IFIFO:
               KO_RESULT = fifoSym;
               break;
            case S_IFREG:
               KO_RESULT = regSym;
               break;
            case S_IFDIR:
               KO_RESULT = dirSym;
               break;
            case S_IFLNK:
               KO_RESULT = lnkSym;
               break;
            case S_IFSOCK:
               KO_RESULT = sockSym;
               break;
            default:
               KO_FAIL(BadFileType);
         }
      "
   }
   #rusr {c_cond [this] "KO_GET_MODE(S_IRUSR)"}
   #wusr {c_cond [this] "KO_GET_MODE(S_IWUSR)"}
   #xusr {c_cond [this] "KO_GET_MODE(S_IXUSR)"}
   #usr  {
      c_cond [this] "KO_GET_MODE(S_IRUSR)",
      c_cond [this] "KO_GET_MODE(S_IWUSR)",
      c_cond [this] "KO_GET_MODE(S_IXUSR)"
   }
   #rgrp {c_cond [this] "KO_GET_MODE(S_IRGRP)"}
   #wgrp {c_cond [this] "KO_GET_MODE(S_IWGRP)"}
   #xgrp {c_cond [this] "KO_GET_MODE(S_IXGRP)"}
   #grp  {
      c_cond [this] "KO_GET_MODE(S_IRGRP)",
      c_cond [this] "KO_GET_MODE(S_IWGRP)",
      c_cond [this] "KO_GET_MODE(S_IXGRP)"
   }
   #roth {c_cond [this] "KO_GET_MODE(S_IROTH)"}
   #woth {c_cond [this] "KO_GET_MODE(S_IWOTH)"}
   #xoth {c_cond [this] "KO_GET_MODE(S_IXOTH)"}
   #oth  {
      c_cond [this] "KO_GET_MODE(S_IROTH)",
      c_cond [this] "KO_GET_MODE(S_IWOTH)",
      c_cond [this] "KO_GET_MODE(S_IXOTH)"
   }
   #suid {c_cond [this] "KO_GET_MODE(S_ISUID)"}
   #sgid {c_cond [this] "KO_GET_MODE(S_ISGID)"}
   #svtx {c_cond [this] "KO_GET_MODE(S_ISVTX)"}
];

c_exportName [KO_FILE_MODE:FILE_MODE KO_FileModeCon:(c_con FileModeCon)];
c_export "
   KO_DESCR_CHECKER(file_mode, KO_FileModeCon);

   #define KO_CHECK_IS_FILE_MODE(obj) \\
      KO_CHECK_TYPE(obj, ko_is_file_mode(obj), KO_FILE_MODE)
";

c_export "ko_value_t ko_make_file_mode(mode_t value);";
c_define "
   ko_value_t
   ko_make_file_mode(mode_t mode) {
      ko_value_t result;
      result = ko_alloc_bytes(sizeof(struct ko_file_mode));
      ko_init_descr(result, KO_FileModeCon);
      ko_set_value_of_file_mode(result, mode);
      return result;
   }
";

c_define "
   #define KO_SET_MODE(mask) \\
      ko_set_value_of_file_mode(mode, ko_value_of_file_mode(mode) | (mask))
   #define KO_RESET_MODE(mask) \\
      ko_set_value_of_file_mode(mode, ko_value_of_file_mode(mode) & ~(mask))
";

private def SetFileModeFields mode changes {
   loop changes [
      ((name, value)\rest) {
         case name [
            #code {
               c_stat [mode value] "
                  mode_t modeC;
                  KO_INT_VALUE_GE0(modeC, value, \"file mode code\");
                  ko_set_value_of_file_mode(mode, modeC);
               "
            }
            #type {
               let valueC = case value [
                  #blk  {c_expr "ko_int(S_IFBLK)"}
                  #chr  {c_expr "ko_int(S_IFCHR)"}
                  #fifo {c_expr "ko_int(S_IFIFO)"}
                  #reg  {c_expr "ko_int(S_IFREG)"}
                  #dir  {c_expr "ko_int(S_IFDIR)"}
                  #lnk  {c_expr "ko_int(S_IFLNK)"}
                  #sock {c_expr "ko_int(S_IFSOCK)"}
               ];
               c_stat [mode value:valueC] "
                  mode_t modeC;
                  KO_UNSAFE_FROM_INT(modeC, value);
                  ko_set_value_of_file_mode(mode,
                     (ko_value_of_file_mode(mode) & ~S_IFMT) | modeC);
               "
            }
            #rusr {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_IRUSR);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IRUSR);"}
            }
            #wusr {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_IWUSR);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IWUSR);"}
            }
            #xusr {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_IXUSR);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IXUSR);"}
            }
            #usr {
               let (r, w, x) = value;
               if r
                  {c_stat [mode] "KO_SET_MODE(S_IRUSR);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IRUSR);"};
               if w
                  {c_stat [mode] "KO_SET_MODE(S_IWUSR);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IWUSR);"};
               if x
                  {c_stat [mode] "KO_SET_MODE(S_IXUSR);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IXUSR);"}
            }
            #rgrp {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_IRGRP);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IRGRP);"}
            }
            #wgrp {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_IWGRP);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IWGRP);"}
            }
            #xgrp {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_IXGRP);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IXGRP);"}
            }
            #grp {
               let (r, w, x) = value;
               if r
                  {c_stat [mode] "KO_SET_MODE(S_IRGRP);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IRGRP);"};
               if w
                  {c_stat [mode] "KO_SET_MODE(S_IWGRP);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IWGRP);"};
               if x
                  {c_stat [mode] "KO_SET_MODE(S_IXGRP);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IXGRP);"}
            }
            #roth {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_IROTH);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IROTH);"}
            }
            #woth {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_IWOTH);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IWOTH);"}
            }
            #xoth {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_IXOTH);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IXOTH);"}
            }
            #oth {
               let (r, w, x) = value;
               if r
                  {c_stat [mode] "KO_SET_MODE(S_IROTH);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IROTH);"};
               if w
                  {c_stat [mode] "KO_SET_MODE(S_IWOTH);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IWOTH);"};
               if x
                  {c_stat [mode] "KO_SET_MODE(S_IXOTH);"}
                  {c_stat [mode] "KO_RESET_MODE(S_IXOTH);"}
            }
            #suid {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_ISUID);"}
                  {c_stat [mode] "KO_RESET_MODE(S_ISUID);"}
            }
            #sgid {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_ISGID);"}
                  {c_stat [mode] "KO_RESET_MODE(S_ISGID);"}
            }
            #svtx {
               if value
                  {c_stat [mode] "KO_SET_MODE(S_ISVTX);"}
                  {c_stat [mode] "KO_RESET_MODE(S_ISVTX);"}
            }
         ];
         again rest
      }
      [] {mode}
   ]
};

def FileMode fields... {
   c_expr "ko_make_file_mode(0)"
   ->SetFileModeFields fields
};

dynamic DefaultFileMode = c_expr "
   ko_make_file_mode(
      S_IRUSR|S_IWUSR|
      S_IRGRP|S_IWGRP|
      S_IROTH|S_IWOTH)
";

dynamic DefaultDirectoryMode = c_expr "
   ko_make_file_mode(
      S_IRUSR|S_IWUSR|S_IXUSR|
      S_IRGRP|S_IWGRP|S_IXGRP|
      S_IROTH|S_IWOTH|S_IXOTH)
";

method Change mode^FILE_MODE changes... {
   c_expr [mode] "ko_make_file_mode(ko_value_of_file_mode(mode))"
   ->SetFileModeFields changes
};

method Is x^FILE_MODE y {
   c_cond [x y] "ko_value_of_file_mode(x) == ko_value_of_file_mode(y)"
};

method Hash x^FILE_MODE {
   c_expr [x] "ko_sint(ko_value_of_file_mode(x) & KO_MAX_HASH)"
};
