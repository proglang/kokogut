// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.IO.ArrayStreams =>

use Kokogut.Core;
use Kokogut.Strings;
use Kokogut.Types;
use Kokogut.Collections.Whole;
use Kokogut.Collections.Ends;
use Kokogut.Collections.Part;
use Kokogut.Data.Arrays.ByteArrays;
use Kokogut.Data.Arrays.CharArrays;
use Kokogut.IO.Streams;

private def MovePartByteArray input output maxSize {
   c_stat [input output maxSize] "
      ko_value_t size, outputPos;
      KO_CHECK_ARRAY_IS_UNLOCKED(input);
      KO_CHECK_ARRAY_IS_UNLOCKED(output);
      KO_NORMALIZE_BUFFER_SIZE(size, maxSize, ko_size_of_array(input));
      outputPos = ko_size_of_array(output);
      KO_ADD_LAST_PART_BYTE_ARRAY(output, size);
      ko_copy_bytes(
         ko_ptr_at_byte_array(output, outputPos),
         ko_elems_of_byte_array(input),
         size);
      KO_REMOVE_FIRST_PART_BYTE_ARRAY(input, size);
   "
};

private def MovePartCharArray input output maxSize {
   c_stat [input output maxSize] "
      ko_value_t size, outputPos;
      KO_CHECK_ARRAY_IS_UNLOCKED(input);
      KO_CHECK_ARRAY_IS_UNLOCKED(output);
      KO_NORMALIZE_BUFFER_SIZE(size, maxSize, ko_size_of_array(input));
      outputPos = ko_size_of_array(output);
      KO_ADD_LAST_PART_CHAR_ARRAY(output, size);
      ko_copy_wchars(
         ko_ptr_at_char_array(output, outputPos),
         ko_elems_of_char_array(input),
         size);
      KO_REMOVE_FIRST_PART_CHAR_ARRAY(input, size);
   "
};

def FindEndOfLine buffer begin absent present {
   c_case [index] [buffer begin] "
      ko_wchar_t *elems, *ptr;
      ko_nint_t n;
      elems = ko_elems_of_char_array(buffer);
      ptr = ko_shift_wchars(elems, begin);
      for (n = ko_nint_of_sint(ko_sub(ko_size_of_array(buffer), begin));
         n != 0; --n)
      {
         if (*ptr == 10) {
            index = ko_sint(ptr - elems);
            goto present;
         }
         ++ptr;
      }
      goto absent;
   " [
      absent:  {absent()}
      present: {present index}
   ]
};

def DoCutLine [
   buffer {
      c_eval [buffer] "
         ko_wchar_t *elems;
         KO_CHECK_ARRAY_IS_UNLOCKED(buffer);
         elems = ko_elems_of_char_array(buffer);
         KO_MAKE_STRING_OF_WCHARS(KO_RESULT, elems, ko_size_of_array(buffer));
         ko_set_size_of_array(buffer, ko_sint(0));
      "
   }
   buffer size {
      c_eval [buffer size] "
         ko_wchar_t *elems;
         ko_value_t oldSize;
         KO_CHECK_ARRAY_IS_UNLOCKED(buffer);
         elems = ko_elems_of_char_array(buffer);
         KO_MAKE_STRING_OF_WCHARS(KO_RESULT, elems, size);
         oldSize = ko_size_of_array(buffer);
         if (size == oldSize)
            ko_set_size_of_array(buffer, ko_sint(0));
         else
            KO_REMOVE_FIRST_PART_CHAR_ARRAY(buffer, ko_add1(size));
      "
   }
];

// Arrays as input streams

DeclareSupertype BYTE_ARRAY BYTE_INPUT;
DeclareSupertype CHAR_ARRAY CHAR_INPUT;

method ReadSomeFrom arr^BYTE_ARRAY output maxSize {
   c_stat [output] "KO_CHECK_IS_BYTE_ARRAY(output);";
   MovePartByteArray arr output maxSize;
   IsEmpty arr
};

method ReadSomeFrom arr^CHAR_ARRAY output maxSize {
   c_stat [output] "KO_CHECK_IS_CHAR_ARRAY(output);";
   MovePartCharArray arr output maxSize;
   IsEmpty arr
};

method ReadSomeAhead input^BYTE_ARRAY output offset maxSize {
   c_eval [input output offset maxSize] "
      ko_value_t inputSize, begin, available, partSize, outputPos;
      KO_CHECK_IS_BYTE_ARRAY(output);
      KO_CHECK_ARRAY_IS_UNLOCKED(input);
      KO_CHECK_ARRAY_IS_UNLOCKED(output);
      inputSize = ko_size_of_array(input);
      KO_NORMALIZE_BEGIN(begin, offset, inputSize);
      available = ko_sub(inputSize, begin);
      KO_NORMALIZE_BUFFER_SIZE(partSize, maxSize, available);
      outputPos = ko_size_of_array(output);
      KO_ADD_LAST_PART_BYTE_ARRAY(output, partSize);
      ko_copy_bytes(
         ko_ptr_at_byte_array(output, outputPos),
         ko_ptr_at_byte_array(input, begin),
         partSize);
      KO_RESULT = ko_bool(partSize == available);
   "
};

method ReadSomeAhead input^CHAR_ARRAY output offset maxSize {
   c_eval [input output offset maxSize] "
      ko_value_t inputSize, begin, available, partSize, outputPos;
      KO_CHECK_IS_CHAR_ARRAY(output);
      KO_CHECK_ARRAY_IS_UNLOCKED(input);
      KO_CHECK_ARRAY_IS_UNLOCKED(output);
      inputSize = ko_size_of_array(input);
      KO_NORMALIZE_BEGIN(begin, offset, inputSize);
      available = ko_sub(inputSize, begin);
      KO_NORMALIZE_BUFFER_SIZE(partSize, maxSize, available);
      outputPos = ko_size_of_array(output);
      KO_ADD_LAST_PART_CHAR_ARRAY(output, partSize);
      ko_copy_wchars(
         ko_ptr_at_char_array(output, outputPos),
         ko_ptr_at_char_array(input, begin),
         partSize);
      KO_RESULT = ko_bool(partSize == available);
   "
};

method ReadSomeAhead input^STRING output offset maxSize {
   c_eval [input output offset maxSize] "
      ko_value_t inputSize, begin, available, partSize, outputPos;
      KO_CHECK_IS_CHAR_ARRAY(output);
      KO_CHECK_ARRAY_IS_UNLOCKED(output);
      inputSize = ko_size_of_string(input);
      KO_NORMALIZE_BEGIN(begin, offset, inputSize);
      available = ko_sub(inputSize, begin);
      KO_NORMALIZE_BUFFER_SIZE(partSize, maxSize, available);
      outputPos = ko_size_of_array(output);
      KO_ADD_LAST_PART_CHAR_ARRAY(output, partSize);
      if (KO_LIKELY(ko_string_is_narrow(input)))
         ko_wchars_of_nchars(
            ko_ptr_at_char_array(output, outputPos),
            ko_ptr_at_nstring(input, begin),
            partSize);
      else
         ko_copy_wchars(
            ko_ptr_at_char_array(output, outputPos),
            ko_ptr_at_wstring(input, begin),
            partSize);
      KO_RESULT = ko_bool(partSize == available);
   "
};

method ReadByteFrom arr^BYTE_ARRAY {
   TryCutFirst arr Ignore
};

method PeekByteFrom arr^BYTE_ARRAY {
   GetFirst arr Ignore
};

method UnreadByteFrom arr^BYTE_ARRAY byte {
   DoAddFirst arr byte
};

method ReadCharFrom arr^CHAR_ARRAY {
   TryCutFirst arr Ignore
};

method PeekCharFrom arr^CHAR_ARRAY {
   GetFirst arr Ignore
};

method ReadLineFrom arr^CHAR_ARRAY {
   if (IsEmpty arr) {Null} =>
   FindEndOfLine arr 0 {DoCutLine arr} (DoCutLine arr _)
};

method SkipLineFrom arr^CHAR_ARRAY {
   FindEndOfLine arr 0 {Clear arr} ?pos {DoRemovePart arr 0 (pos+1)}
};

method UnreadFrom arr^BYTE_ARRAY obj {DoAddPart arr 0 obj};
method UnreadFrom arr^CHAR_ARRAY obj {DoAddPart arr 0 obj};

method CanReadFrom _^BYTE_ARRAY {};
method CanReadFrom _^CHAR_ARRAY {};

method CanReadAhead _^BYTE_ARRAY _ {};
method CanReadAhead _^CHAR_ARRAY _ {};

method CanReadLineFrom _^CHAR_ARRAY {};

// Arrays as output streams

DeclareSupertype BYTE_ARRAY BYTE_OUTPUT;
DeclareSupertype CHAR_ARRAY CHAR_OUTPUT;

method WriteSomeTo arr^BYTE_ARRAY input maxSize _flush {
   c_stat [input] "KO_CHECK_IS_BYTE_ARRAY(input);";
   MovePartByteArray input arr maxSize
};

method WriteSomeTo arr^CHAR_ARRAY input maxSize _flush {
   c_stat [input] "KO_CHECK_IS_CHAR_ARRAY(input);";
   MovePartCharArray input arr maxSize
};

method WriteByteTo arr^BYTE_ARRAY byte {DoAddLast arr byte};

method WriteTo arr^BYTE_ARRAY obj {DoAddPart arr obj};
method WriteTo arr^CHAR_ARRAY obj {DoAddPart arr obj};

method WriteLineTo arr^CHAR_ARRAY {DoAddPart arr "\n"};

method FlushTo _^BYTE_ARRAY _flush {};
method FlushTo _^CHAR_ARRAY _flush {};

method CanWriteTo _^BYTE_ARRAY {};
method CanWriteTo _^CHAR_ARRAY {};

method Truncate arr^BYTE_ARRAY size {DoRemovePart arr size};
method Truncate arr^CHAR_ARRAY size {DoRemovePart arr size};

method Truncate _^BYTE_ARRAY {};
method Truncate _^CHAR_ARRAY {};
