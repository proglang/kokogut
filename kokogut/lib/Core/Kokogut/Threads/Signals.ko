// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2007,2009 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Threads.Signals =>

use Kokogut.Core;
use Kokogut.ExceptionTypes;
use Kokogut.Exceptions;
use Kokogut.Time []; // ko_timespec_of_ticks
use Kokogut.Threads.Core []; // ko_context_switch etc.

// Some signal types

type ACTION_SIGNAL ActionSignal (var action);

method Close signal^ACTION_SIGNAL {signal.action = {}};

type FORK_PROCESS_SIGNAL ForkProcessSignal sync;

// Thread state saved for handling a signal

c_export "
   struct ko_signal_state {
      KO_OBJECT;
      ko_value_t value;
      ko_timespec_t wakeup;
      ko_code_t code;
      ko_value_t registers[KO_REGISTERS_IN_THREAD];
      ko_value_t blocked_all, blocked_async;
   };

   KO_OBJECT_FIELD(signal_state, value);
   KO_OBJECT_FIELD_PTR(signal_state, wakeup, ko_timespec_t);
   KO_OBJECT_FIELD_RAW(signal_state, code, ko_code_t);
   KO_OBJECT_FIELD_ARRAY(signal_state, registers, ko_value_t);
   KO_OBJECT_FIELD_RAW(signal_state, blocked_all, ko_value_t);
   KO_OBJECT_FIELD_RAW(signal_state, blocked_async, ko_value_t);
";

private c_type SIGNAL_STATE SignalStateCon "
   KO_MOVE_OBJECT_BEGIN(struct ko_signal_state);
   ko_mark_count(KO_REGISTERS_IN_THREAD + 1);
   ko_mark(ko_value_ptr_of_signal_state(dest));
   ko_mark_array(ko_registers_of_signal_state(dest), KO_REGISTERS_IN_THREAD);
   KO_MOVE_OBJECT_END(struct ko_signal_state);
";

c_exportName [KO_SignalStateCon:(c_con SignalStateCon)];

c_export "
   void ko_save_current_signal_state(ko_value_t state);
   void ko_save_signal_state(ko_value_t state, ko_value_t thread);
   void ko_restore_signal_state(ko_value_t state);
";

c_define "
   void
   ko_save_current_signal_state(ko_value_t state) {
      ko_init_code_of_signal_state(state, ko_restart);
      memcpy(ko_registers_of_signal_state(state), KO_R,
         KO_REGISTERS_IN_THREAD * KO_SIZEOF_PTR);
   }

   void
   ko_save_signal_state(ko_value_t state, ko_value_t thread) {
      ko_init_value_of_signal_state(state, ko_value_of_thread(thread));
      if (ko_current_of_thread(thread)) {
         ko_save_current_signal_state(state);
         ko_init_blocked_all_of_signal_state(state, ko_blocked_all);
         ko_init_blocked_async_of_signal_state(state, ko_blocked_async);
      }
      else {
         ko_init_code_of_signal_state(state, ko_code_of_thread(thread));
         memcpy(ko_registers_of_signal_state(state),
            ko_registers_of_thread(thread),
            KO_REGISTERS_IN_THREAD * KO_SIZEOF_PTR);
         ko_init_blocked_all_of_signal_state(state,
            ko_blocked_all_of_thread(thread));
         ko_init_blocked_async_of_signal_state(state,
            ko_blocked_async_of_thread(thread));
      }
   }

   void
   ko_restore_signal_state(ko_value_t state) {
      ko_restart = ko_code_of_signal_state(state);
      memcpy(KO_R, ko_registers_of_signal_state(state),
         KO_REGISTERS_IN_THREAD * KO_SIZEOF_PTR);
      ko_blocked_all = ko_blocked_all_of_signal_state(state);
      ko_blocked_async = ko_blocked_async_of_signal_state(state);
   }
";

// The list of pending signals of a thread

c_export "
   KO_RECORD_FIELD(signal_node, 0, signal);
   KO_RECORD_FIELD(signal_node, 1, next);
";

private c_type SIGNAL_NODE SignalNodeCon "
   KO_MOVE_RECORD_BEGIN(3);
   ko_mark_count(2);
   ko_mark(ko_signal_ptr_of_signal_node(dest));
   ko_mark(ko_next_ptr_of_signal_node(dest));
   KO_MOVE_RECORD_END(3);
";

c_exportName [KO_SignalNodeCon:(c_con SignalNodeCon)];

dynamic Signal = Fail;
// Will be changed to DefaultSignalHandler during initialization of the
// DefaultSignalHandler module.

// Sending signals

c_export "void ko_signal_thread(ko_value_t thread, ko_value_t signal);";

c_define "
   void
   ko_signal_thread(ko_value_t threadVal, ko_value_t signalVal) {
      struct ko_external thread, signal;
      ko_value_t node, last;
      ko_bool_t (*handlesSignals)(ko_value_t all, ko_value_t async);
      ko_value_t signalState;
      if (ko_state_of_thread(threadVal)->ko_send_signal(threadVal, signalVal))
         return;
      ko_register_external(&thread, threadVal);
      ko_register_external(&signal, signalVal);
      node = ko_alloc(3);
      ko_init_descr(node, KO_SignalNodeCon);
      ko_init_signal_of_signal_node(node, signal.ko_value);
      ko_init_next_of_signal_node(node, KO_ABSENT);
      last = ko_last_signal_of_thread(thread.ko_value);
      if (KO_LIKELY(last == KO_ABSENT))
         ko_set_first_signal_of_thread(thread.ko_value, node);
      else
         ko_set_next_of_signal_node_nogc(last, node);
      ko_set_last_signal_of_thread(thread.ko_value, node);
      ko_mark_changed_object(thread.ko_value);
      ko_unregister_external(&signal);
      handlesSignals = ko_state_of_thread(thread.ko_value)->ko_handles_signals;
      if (!(ko_current_of_thread(thread.ko_value) ?
         handlesSignals(ko_blocked_all, ko_blocked_async) :
         handlesSignals(ko_blocked_all_of_thread(thread.ko_value),
            ko_blocked_async_of_thread(thread.ko_value))))
      {
         ko_unregister_external(&thread);
         return;
      }
      signalState = ko_alloc_bytes(sizeof(struct ko_signal_state));
      ko_init_descr(signalState, KO_SignalStateCon);
      ko_save_signal_state(signalState, thread.ko_value);
      if (ko_current_of_thread(thread.ko_value))
         ko_blocked_all = ko_add1(ko_blocked_all);
      else
         ko_set_blocked_all_of_thread(thread.ko_value,
            ko_add1(ko_blocked_all_of_thread(thread.ko_value)));
      ko_state_of_thread(thread.ko_value)->ko_kick(thread.ko_value,
         signalState);
      ko_set_state_of_thread(thread.ko_value, &ko_thread_running);
      ko_set_value_of_thread(thread.ko_value, KO_ABSENT);
      if (ko_current_of_thread(thread.ko_value))
         KO_R[1] = signalState;
      else {
         ko_registers_of_thread(thread.ko_value)[1] = signalState;
         ko_mark_changed_object(thread.ko_value);
      }
      ko_unregister_external(&thread);
   }
";

def SignalThread thread signal {
   c_stat [thread] "KO_CHECK_IS_THREAD(thread);";
   c_case [] [thread signal] "
      ko_value_t thr = thread, sig = signal;
      KO_R[1] = KO_Null;
      KO_POP_FRAME();
      ko_restart = KO_RET;
      ko_signal_thread(thr, sig);
      KO_JUMP(ko_restart);
   " []
};

def CancelThread thread {
   SignalThread thread ThreadExit
};

// Handling signals

c_export "
   static inline ko_bool_t
   ko_sync_signal_pending(void) {
      return ko_signal_pending() && ko_blocked_all == ko_sint(0);
   }

   #define KO_PROCESS_SYNC_SIGNAL(value, args, entry)                       \\
      KO_STAT_BEGIN {                                                       \\
         if (KO_UNLIKELY(ko_sync_signal_pending())) {                       \\
            ko_value_t state = (ko_value_t)ko_alloc_bytes(                  \\
               sizeof(struct ko_signal_state));                             \\
            ko_init_descr(state, KO_SignalStateCon);                        \\
            ko_init_value_of_signal_state(state, value);                    \\
            ko_init_blocked_all_of_signal_state(state, ko_sint(0));         \\
            ko_init_blocked_async_of_signal_state(state, ko_blocked_async); \\
            args                                                            \\
            KO_POP_FRAME();                                                 \\
            ko_restart = KO_RET;                                            \\
            ko_save_current_signal_state(state);                            \\
            ko_blocked_all = ko_sint(1);                                    \\
            KO_R[1] = state;                                                \\
            KO_JUMP(entry);                                                 \\
         }                                                                  \\
      } KO_STAT_END

   #define KO_SIGNALS_HANDLED(state, args, entry) \\
      KO_STAT_BEGIN {                             \\
         ko_restore_signal_state(state);          \\
         args                                     \\
         KO_POP_FRAME();                          \\
         KO_RET = ko_restart;                     \\
         KO_JUMP(entry);                          \\
      } KO_STAT_END
";

def HandleSignal() {
   Signal (c_eval "
      ko_value_t node = ko_first_signal_of_thread(ko_current_thread);
      ko_value_t next = ko_next_of_signal_node(node);
      ko_set_first_signal_of_thread(ko_current_thread, next);
      if (KO_LIKELY(next == KO_ABSENT))
         ko_set_last_signal_of_thread(ko_current_thread, KO_ABSENT);
      KO_RESULT = ko_signal_of_signal_node(node);
   ")
};

def HandleSignals() {
   HandleSignal();
   if (c_cond "KO_UNLIKELY(ko_signal_pending())") {again()}
};

def HandleSignalsOrFirstSync state {
   HandleSignal();
   if (c_cond [state] "
      KO_UNLIKELY(ko_signal_pending()) &&
      ko_blocked_async_of_signal_state(state) == ko_sint(0)
   ") {HandleSignals()}
};

def HandleSyncSignal() {
   c_stat "ko_blocked_all = ko_sint(1);";
   HandleSignal();
   c_stat "ko_blocked_all = ko_sint(0);"
};

def Handle [
   fun body {local Signal (fun Signal) body}
   fun      {Signal = fun Signal}
];

// Blocking signals

def BlockSignals body {
   c_stat "ko_blocked_all = ko_add1(ko_blocked_all);";
   let result = body();
   if (c_cond "
      KO_UNLIKELY(ko_signal_pending()) &&
      ko_blocked_all == ko_sint(1) && ko_blocked_async == ko_sint(0)
   ") {HandleSignals()};
   c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
   result
};

def UnblockSignals body {
   c_stat [SignalsUnblocked] "
      if (KO_UNLIKELY(ko_blocked_all == ko_sint(0)))
         KO_FAIL(SignalsUnblocked);
   ";
   if (c_cond "
      KO_UNLIKELY(ko_signal_pending()) &&
      ko_blocked_all == ko_sint(1) && ko_blocked_async == ko_sint(0)
   ") {HandleSignals()};
   c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
   let result = body();
   c_stat "ko_blocked_all = ko_add1(ko_blocked_all);";
   result
};

def BlockAsyncSignals body {
   c_stat "ko_blocked_async = ko_add1(ko_blocked_async);";
   let result = body();
   if (c_cond "
      KO_UNLIKELY(ko_signal_pending()) &&
      ko_blocked_all == ko_sint(0) && ko_blocked_async == ko_sint(1)
   ") {
      c_stat "ko_blocked_all = ko_sint(1); ko_blocked_async = ko_sint(0);";
      HandleSignals();
      c_stat "ko_blocked_all = ko_sint(0);";
      result
   } =>
   c_stat "ko_blocked_async = ko_sub1(ko_blocked_async);";
   result
};

def UnblockAsyncSignals body {
   c_stat [SignalsUnblocked] "
      if (KO_UNLIKELY(ko_blocked_async == ko_sint(0)))
         KO_FAIL(SignalsUnblocked);
   ";
   if (c_cond "
      KO_UNLIKELY(ko_signal_pending()) &&
      ko_blocked_all == ko_sint(0) && ko_blocked_async == ko_sint(1)
   ") {
      c_stat "ko_blocked_all = ko_sint(1); ko_blocked_async = ko_sint(0);";
      HandleSignals();
      c_stat "ko_blocked_async = ko_sint(1); ko_blocked_all = ko_sint(0);"
   };
   c_stat "ko_blocked_async = ko_sub1(ko_blocked_async);";
   let result = body();
   c_stat "ko_blocked_async = ko_add1(ko_blocked_async);";
   result
};

def BlockSyncSignals body {
   c_stat [SignalsUnblocked] "
      if (KO_UNLIKELY(ko_blocked_async == ko_sint(0)))
         KO_FAIL(SignalsUnblocked);
   ";
   c_stat "
      ko_blocked_all = ko_add1(ko_blocked_all);
      ko_blocked_async = ko_sub1(ko_blocked_async);
   ";
   let result = body();
   c_stat "
      ko_blocked_async = ko_add1(ko_blocked_async);
      ko_blocked_all = ko_sub1(ko_blocked_all);
   ";
   result
};

def UnblockSyncSignals body {
   c_stat [SignalsUnblocked] "
      if (KO_UNLIKELY(ko_blocked_all == ko_sint(0)))
         KO_FAIL(SignalsUnblocked);
   ";
   c_stat "
      ko_blocked_async = ko_add1(ko_blocked_async);
      ko_blocked_all = ko_sub1(ko_blocked_all);
   ";
   let result = body();
   c_stat "
      ko_blocked_all = ko_add1(ko_blocked_all);
      ko_blocked_async = ko_sub1(ko_blocked_async);
   ";
   result
};

ref SignalBlockingState = {
   c_eval "
      KO_RESULT = ko_alloc(3);
      ko_init_descr(KO_RESULT, KO_PairCon);
      ko_init_left_of_pair(KO_RESULT, ko_blocked_all);
      ko_init_right_of_pair(KO_RESULT, ko_blocked_async);
   "
};

// Using, Ensure

def Using [
   obtain body {again obtain Close Close body}
   obtain release body {again obtain release release body}
   obtain commit rollback body {
      c_stat "ko_blocked_async = ko_add1(ko_blocked_async);";
      let resource = obtain();
      c_stat "
         ko_blocked_all = ko_add1(ko_blocked_all);
         ko_blocked_async = ko_sub1(ko_blocked_async);
      ";
      try {
         if (c_cond "
            KO_UNLIKELY(ko_signal_pending()) &&
            ko_blocked_all == ko_sint(1) && ko_blocked_async == ko_sint(0)
         ") {HandleSignals()};
         c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
         let result = body resource;
         c_stat "ko_blocked_all = ko_add1(ko_blocked_all);";
         result
      } exn {
         try {rollback resource} _ {};
         Fail exn
      } ?result {
         commit resource;
         if (c_cond "
            KO_UNLIKELY(ko_signal_pending()) &&
            ko_blocked_all == ko_sint(1) && ko_blocked_async == ko_sint(0)
         ") {HandleSignals()};
         c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
         result
      }
   }
];

def Ensure [
   release body {
      try {
         let result = body();
         c_stat "ko_blocked_all = ko_add1(ko_blocked_all);";
         result
      } exn {
         try release _ {};
         Fail exn
      } ?result {
         release();
         if (c_cond "
            KO_UNLIKELY(ko_signal_pending()) &&
            ko_blocked_all == ko_sint(1) && ko_blocked_async == ko_sint(0)
         ") {HandleSignals()};
         c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
         result
      }
   }
   obtain release body {again obtain release release body}
   obtain commit rollback body {
      c_stat "ko_blocked_async = ko_add1(ko_blocked_async);";
      obtain();
      c_stat "
         ko_blocked_all = ko_add1(ko_blocked_all);
         ko_blocked_async = ko_sub1(ko_blocked_async);
      ";
      try {
         if (c_cond "
            KO_UNLIKELY(ko_signal_pending()) &&
            ko_blocked_all == ko_sint(1) && ko_blocked_async == ko_sint(0)
         ") {HandleSignals()};
         c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
         let result = body();
         c_stat "ko_blocked_all = ko_add1(ko_blocked_all);";
         result
      } exn {
         try rollback _ {};
         Fail exn
      } ?result {
         commit();
         if (c_cond "
            KO_UNLIKELY(ko_signal_pending()) &&
            ko_blocked_all == ko_sint(1) && ko_blocked_async == ko_sint(0)
         ") {HandleSignals()};
         c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
         result
      }
   }
];

// WithExit

type EXIT_TOKEN is:SPECIAL_RESULT ExitToken();

def WithExit body {
   let token = ExitToken();
   var action = {Fail token};
   var result;
   let exit = function [
      ()           {action()}
      (set result) {action()}
   ];
   c_stat "ko_blocked_all = ko_add1(ko_blocked_all);";
   let result' = try {
      if (c_cond "
         KO_UNLIKELY(ko_signal_pending()) &&
         ko_blocked_all == ko_sint(1) && ko_blocked_async == ko_sint(0)
      ") {HandleSignals()};
      c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
      let result = body exit;
      c_stat "ko_blocked_all = ko_add1(ko_blocked_all);";
      result
   } exn {
      action = {Fail AlreadyExited};
      if (exn %IsSame token) {result} {Fail exn}
   } ?result {
      action = {Fail AlreadyExited};
      result
   };
   if (c_cond "
      KO_UNLIKELY(ko_signal_pending()) &&
      ko_blocked_all == ko_sint(1) && ko_blocked_async == ko_sint(0)
   ") {HandleSignals()};
   c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
   result'
};
