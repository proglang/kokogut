// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Threads.DefaultSignalHandler =>

use Kokogut.ExceptionTypes;
use Kokogut.Exceptions;
use Kokogut.Threads.Signals;
use Kokogut.IO.RawFiles []; // ko_{restore_,set_non}blocking_mode

def DefaultSignalHandler signal! {};

method DefaultSignalHandler signal^SYSTEM_SIGNAL {
   c_stat [signal] "
      ko_value_t code = ko_code_of_system_signal(signal);
      if (code == ko_sint(SIGTSTP)) {
         struct sigaction action;
         ko_restore_blocking_mode();
         sigaction(SIGTSTP, &ko_old_signal_actions[SIGTSTP], &action);
         raise(SIGTSTP);
         sigaction(SIGTSTP, &action, NULL);
         ko_set_non_blocking_mode();
      }
      else if (
         code != ko_sint(SIGCHLD) &&
         code != ko_sint(SIGCONT) &&
         code != ko_sint(SIGURG)
         #ifdef SIGWINCH
            && code != ko_sint(SIGWINCH)
         #endif
      )
         KO_FAIL(signal);
   "
};

method DefaultSignalHandler signal^ACTION_SIGNAL {signal.action()};

method DefaultSignalHandler signal^FORK_PROCESS_SIGNAL {signal.sync()};

method DefaultSignalHandler signal {Fail signal};

Signal = DefaultSignalHandler;
