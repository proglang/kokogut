// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Threads.SleepingForever =>

use Kokogut.Threads.Signals;

def SleepForever() {
   c_case [] [(c_entry SignalWhileSleepingForever 1)] "
      KO_INIT_POLLING();
      KO_PROCESS_SYNC_SIGNAL(KO_ABSENT, ;, SignalWhileSleepingForever1);
      ko_set_state_of_thread(ko_current_thread, &ko_thread_sleeping_forever);
      KO_CONTEXT_SWITCH();
   " []
};

private def SignalWhileSleepingForever state {
   HandleSignalsOrFirstSync state;
   c_case [] [state (c_entry SleepForever 0)] "
      KO_SIGNALS_HANDLED(state, ;, SleepForever0);
   " []
};

c_define [(c_entry SignalWhileSleepingForever 1)] "
   static void
   ko_kick_sleeping_forever(ko_value_t thread, ko_value_t signalState) {
      (void)signalState;
      ko_set_restart(thread, SignalWhileSleepingForever1);
      ko_link_thread(thread);
   }

   static struct ko_thread_state ko_thread_sleeping_forever = {
      ko_send_signal_by_queue,
      ko_handles_signals_wait,
      ko_kick_sleeping_forever,
      ko_unlink_none,
      ko_false /* ko_signal_deadlock */
   };
";
