// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.ObjectIds =>

use Kokogut.Core;
use Kokogut.Numbers.Ints []; // KO_MAX_HASH

//// Unique indices

c_define "
   static ko_nint_t *ko_unique_indices, ko_num_unique_indices,
      ko_first_free_unique_index, ko_allocated_unique_indices;
   /* ko_first_free_unique_index, if not -1, points to a chain of free
    * unique indices. Each contains the next free index, except the
    * last one which contains -1. The values at live indices are unused. */

   static void
   ko_init_unique_indices(void) {
      ko_allocated_unique_indices = 1024;
      ko_unique_indices = ko_malloc_block(ko_allocated_unique_indices
         * KO_SIZEOF_NINT);
      ko_num_unique_indices = 0;
      ko_first_free_unique_index = -1;
   }

   static void
   ko_free_unique_indices(void) {
      ko_free_block(ko_unique_indices, ko_allocated_unique_indices
         * KO_SIZEOF_NINT);
   }

   static ko_nint_t
   ko_new_unique_index(void) {
      ko_nint_t i = ko_first_free_unique_index;
      if (i != -1) {
         ko_first_free_unique_index = ko_unique_indices[i];
         return i;
      }
      i = ko_num_unique_indices++;
      if (KO_UNLIKELY(i == ko_allocated_unique_indices)) {
         ko_nint_t newAllocated = ko_allocated_unique_indices;
         newAllocated += newAllocated >> 1;
         ko_unique_indices = ko_realloc_block(ko_unique_indices,
            ko_allocated_unique_indices * KO_SIZEOF_PTR,
            newAllocated * KO_SIZEOF_PTR);
         ko_allocated_unique_indices = newAllocated;
      }
      return i;
   }

   static inline void
   ko_free_unique_index(ko_nint_t i) {
      ko_unique_indices[i] = ko_first_free_unique_index;
      ko_first_free_unique_index = i;
   }
";

//// Object ids

c_export "
   struct ko_object_id {
      KO_FOREIGN_OBJECT;
      ko_nint_t index;
   };

   KO_OBJECT_FIELD_RAW(object_id, index, ko_nint_t);
";

c_type OBJECT_ID (private ObjectIdCon) "KO_MOVE_OBJECT(struct ko_object_id);"
   "ko_free_unique_index(ko_index_of_object_id(obj));"
[
   #index {c_expr [this] "ko_sint(ko_index_of_object_id(this))"}
];

c_exportName [KO_OBJECT_ID:OBJECT_ID KO_ObjectIdCon:(c_con ObjectIdCon)];
c_export "
   KO_DESCR_CHECKER(object_id, KO_ObjectIdCon);

   #define KO_CHECK_IS_OBJECT_ID(obj) \\
      KO_CHECK_TYPE(obj, ko_is_object_id(obj), KO_OBJECT_ID)

   static inline ko_value_t
   ko_hash_of_object_id(ko_value_t id) {
      return ko_sint(ko_index_of_object_id(id) & KO_MAX_HASH);
   }
";

c_export "
   struct ko_object_ids {
      ko_value_t *entries;
      ko_unint_t size, mask;
   };

   extern struct ko_object_ids ko_young_object_ids, ko_old_object_ids;

   void ko_init_all_object_ids(void);
   void ko_free_all_object_ids(void);

   void ko_init_object_ids(struct ko_object_ids *dict, ko_unint_t size);
   void ko_free_object_ids(struct ko_object_ids *dict);
   void ko_new_object_id(struct ko_object_ids *dict, ko_value_t key,
      ko_value_t value);
";

c_define "
   struct ko_object_ids ko_young_object_ids, ko_old_object_ids;

   /*
    * Object ids associated with objects are in 'ko_young_object_ids'
    * and 'ko_old_object_ids' arrays, depending on the age of their keys.
    * Elements of these arrays come in pairs: the key and the object id.
    *
    * GC removes entries from these dictionaries when either an object id
    * or the corresponding key is dead.
    */

   static ko_value_t *
   ko_alloc_object_ids(ko_unint_t alloc) {
      ko_value_t *entries = ko_malloc_block(alloc * 2 * KO_SIZEOF_PTR);
      ko_value_t *ptr = entries;
      ko_unint_t n = alloc;
      do {
         ptr[0] = KO_ABSENT;
         ptr += 2;
      } while (--n);
      return entries;
   }

   void
   ko_init_object_ids(struct ko_object_ids *dict, ko_unint_t size) {
      ko_unint_t min_alloc = 2 * size;
      ko_unint_t alloc = 16;
      while (min_alloc >= alloc) alloc *= 2;
      dict->entries = ko_alloc_object_ids(alloc);
      dict->size = 0;
      dict->mask = alloc - 1;
   }

   void
   ko_init_all_object_ids(void) {
      ko_init_unique_indices();
      ko_init_object_ids(&ko_young_object_ids, 0);
      ko_init_object_ids(&ko_old_object_ids, 0);
   }

   void
   ko_free_object_ids(struct ko_object_ids *dict) {
      ko_free_block(dict->entries, (dict->mask + 1) * 2 * KO_SIZEOF_PTR);
   }

   void
   ko_free_all_object_ids(void) {
      ko_free_object_ids(&ko_young_object_ids);
      ko_free_object_ids(&ko_old_object_ids);
      ko_free_unique_indices();
   }

   static inline ko_unint_t
   ko_hash_ptr(ko_value_t ptr) {
      return (ko_unint_t)ptr / KO_SIZEOF_PTR;
   }

   /* The algorithm for resolving collisions is taken from Python. */
   static ko_value_t
   ko_get_object_id(struct ko_object_ids *dict, ko_value_t key) {
      ko_unint_t hash = ko_hash_ptr(key);
      ko_unint_t i = hash & dict->mask;
      ko_value_t *found = dict->entries + i * 2;
      ko_unint_t perturb;
      if (found[0] == key) return found[1];
      if (found[0] == KO_ABSENT) return KO_ABSENT;
      perturb = hash;
      for(;;) {
         i = (5 * i + perturb + 1) & dict->mask;
         found = dict->entries + i * 2;
         if (found[0] == key) return found[1];
         if (found[0] == KO_ABSENT) return KO_ABSENT;
         perturb >>= 5;
      }
   }

   static ko_value_t *
   ko_find_free_object_id(ko_value_t *entries, ko_unint_t mask,
      ko_value_t key)
   {
      ko_unint_t hash = ko_hash_ptr(key);
      ko_unint_t i = hash & mask;
      ko_value_t *found = entries + i * 2;
      ko_unint_t perturb;
      if (found[0] == KO_ABSENT) return found;
      perturb = hash;
      for(;;) {
         i = (5 * i + perturb + 1) & mask;
         found = entries + i * 2;
         if (found[0] == KO_ABSENT) return found;
         perturb >>= 5;
      }
   }

   void
   ko_new_object_id(struct ko_object_ids *dict, ko_value_t key,
      ko_value_t value)
   {
      ko_value_t *ptr = ko_find_free_object_id(dict->entries, dict->mask, key);
      ko_unint_t size, oldAlloc;
      ptr[0] = key;
      ptr[1] = value;
      size = ++dict->size;
      oldAlloc = dict->mask + 1;
      if (KO_UNLIKELY(2 * size >= oldAlloc)) {
         ko_unint_t newAlloc = 2 * oldAlloc;
         ko_value_t *newEntries = ko_alloc_object_ids(newAlloc);
         ko_unint_t newMask = newAlloc - 1;
         ko_value_t *oldPtr = dict->entries;
         ko_unint_t n = oldAlloc;
         do {
            if (oldPtr[0] != KO_ABSENT) {
               ko_value_t *newPtr = ko_find_free_object_id(
                  newEntries, newMask, oldPtr[0]);
               newPtr[0] = oldPtr[0];
               newPtr[1] = oldPtr[1];
            }
            oldPtr += 2;
         } while (--n);
         ko_free_object_ids(dict);
         dict->entries = newEntries;
         dict->mask = newMask;
      }
   }
";

method Is _^OBJECT_ID _ {False};

method Hash id^OBJECT_ID {c_expr [id] "ko_hash_of_object_id(id)"};

method '==' id1^OBJECT_ID id2^OBJECT_ID {id1 %IsSame id2};

method '<' id1^OBJECT_ID id2^OBJECT_ID {
   c_cond [id1 id2] "ko_index_of_object_id(id1) < ko_index_of_object_id(id2)"
};

method '<=' id1^OBJECT_ID id2^OBJECT_ID {
   c_cond [id1 id2] "ko_index_of_object_id(id1) <= ko_index_of_object_id(id2)"
};

method Compare id1^OBJECT_ID id2^OBJECT_ID {
   c_eval [id1 id2] "
      ko_nint_t i1 = ko_index_of_object_id(id1);
      ko_nint_t i2 = ko_index_of_object_id(id2);
      KO_RESULT = i1 < i2 ? KO_LT : i1 == i2 ? KO_EQ : KO_GT;
   "
};

def ObjectId [
   () {
      c_eval "
         KO_RESULT = ko_alloc_bytes(sizeof(struct ko_object_id));
         ko_init_descr(KO_RESULT, KO_ObjectIdCon);
         ko_init_index_of_object_id(KO_RESULT, ko_new_unique_index());
         ko_register_foreign_object(KO_RESULT);
      "
   }
   obj {
      c_eval [obj] "
         KO_RESULT = ko_get_object_id(&ko_young_object_ids, obj);
         if (KO_RESULT == KO_ABSENT) {
            KO_RESULT = ko_get_object_id(&ko_old_object_ids, obj);
            if (KO_RESULT == KO_ABSENT) {
               KO_RESULT = ko_alloc_bytes(sizeof(struct ko_object_id));
               ko_init_descr(KO_RESULT, KO_ObjectIdCon);
               ko_init_index_of_object_id(KO_RESULT, ko_new_unique_index());
               ko_register_foreign_object(KO_RESULT);
               ko_new_object_id(&ko_young_object_ids, obj, KO_RESULT);
            }
         }
      "
   }
];
