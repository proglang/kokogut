/*
 * This file is a part of the Kokogut library.
 *
 * Kokogut is a compiler of the Kogut programming language.
 * Copyright (C) 2004-2009 by Marcin 'Qrczak' Kowalczyk
 * (QrczakMK@gmail.com)
 *
 * The Kokogut library is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version;
 * with a "linking exception".
 *
 * The linking exception allows you to link a "work that uses the
 * Library" with a publicly distributed version of the Library to
 * produce an executable file containing portions of the Library,
 * and distribute that executable file under terms of your choice,
 * without any of the additional requirements listed in section 6
 * of LGPL version 2 or section 4 of LGPL version 3.
 *
 * Kokogut is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

void
KO_SAVE_OBJECT(ko_value_t *ptr) {
   for(;;) {
      ko_value_t src = *ptr;
      if (KO_OBJECT_TO_SAVE(src)) {
         if (ko_descr_of_ptr(src) == KO_OBJECT_MOVED)
            *ptr = ko_new_address(src);
         else {
            ko_value_t dest = ko_old_top;
            ko_old_top = ko_descr_of_ptr(src)->ko_move(src, dest);
            ko_init_descr(src, KO_OBJECT_MOVED);
            ko_set_new_address(src, dest);
            *ptr = dest;
         }
      }
      if (KO_UNLIKELY(ko_marked_begin == ko_marked_top)) break;
      ptr = *--ko_marked_top;
   }
}

static void
KO_SAVE_IN_REGISTERS(void) {
   int reg;
   for (reg = 0; reg < KO_NUM_REGISTERS; ++reg)
      KO_SAVE_OBJECT(&KO_R[reg]);
}

void
KO_SAVE_ON_STACK(ko_stack_elem_t *begin, ko_stack_elem_t *frame) {
   while (frame != begin) {
      ko_frame_descr_t *descr = frame[-1].ko_frame_descr;
      ko_nint_t n = ko_unmark_frame_descr(descr)->ko_size;
      ko_stack_elem_t *ptr;
      frame[-1].ko_frame_descr = ko_mark_frame_descr(descr);
      frame -= n;
      ptr = frame;
      for (n -= 2; n != 0; --n) {
         KO_SAVE_OBJECT(&ptr->ko_value);
         ++ptr;
      }
      if (!KO_IS_MAJOR && ko_frame_descr_is_marked(descr)) break;
   }
}

static void
KO_SAVE_PROTECTED(ko_protected_t_0 *prot) {
   ko_protected_t *block;
   for (block = (ko_protected_t *)prot->ko_next;
      block != (ko_protected_t *)prot;
      block = (ko_protected_t *)block->ko_next)
   {
      ko_value_t **ptr;
      ko_nint_t n;
      ptr = block->ko_vars;
      for (n = block->ko_count; n != 0; --n) {
         KO_SAVE_OBJECT(*ptr);
         ++ptr;
      }
   }
}

static void
KO_SAVE_EXTERNAL(struct ko_external *ext) {
   struct ko_external *var;
   for (var = ext->ko_next; var != ext; var = var->ko_next)
      KO_SAVE_OBJECT(&var->ko_value);
}

static void
KO_SAVE_THREADS(void) {
   if (KO_LIKELY(ko_current_thread != KO_ABSENT)) {
      KO_SAVE_OBJECT(&ko_current_thread);
      ko_mark_changed_object(ko_current_thread);
   }
   KO_SAVE_OBJECT(&ko_main_thread);
   KO_SAVE_OBJECT(&ko_receives_signals);
   KO_SAVE_OBJECT(&ko_sleeping_threads);
   KO_SAVE_OBJECT(&ko_watched_files);
}

static void
KO_SAVE_CHANGED_OBJECTS(void) {
   while (ko_changed_objects != KO_LAST_CHANGED) {
      ko_value_t obj;
      KO_SAVE_OBJECT(&ko_changed_objects);
      obj = ko_changed_objects;
      ko_changed_objects = ko_next_of_changed_object(obj);
      ko_set_next_of_changed_object(obj, KO_NOT_CHANGED);
      ko_descr_of_ptr(obj)->KO_SAVE_CHANGED(obj);
   }
}

static ko_value_t
KO_FIND_DEAD_THREADS(ko_value_t *threads, ko_value_t *dead,
   ko_nint_t *numDead)
{
   ko_value_t thread, prev;
   thread = *threads;
   if (thread == KO_ABSENT) return KO_ABSENT;
   prev = KO_ABSENT;
   do {
      if (ko_descr_of_ptr(thread) == KO_OBJECT_MOVED) {
         thread = ko_new_address(thread);
      live:
         if (prev == KO_ABSENT)
            *threads = thread;
         else
            ko_set_next_of_all_of_thread(prev, thread);
         ko_set_prev_of_all_of_thread(thread, prev);
         prev = thread;
         thread = ko_next_of_all_of_thread(thread);
      }
      else {
         ko_unwatch_object(thread);
         if (ko_state_of_thread(thread)->ko_handles_signals(
            ko_blocked_all_of_thread(thread),
            ko_blocked_async_of_thread(thread)))
         {
            KO_SAVE_OBJECT(&thread);
            ko_set_next_of_thread(thread, *dead);
            *dead = thread;
            ++*numDead;
            goto live;
         }
         else {
            ko_value_t next = ko_next_of_all_of_thread(thread);
            ko_set_prev_of_all_of_thread(thread, KO_ABSENT);
            ko_set_next_of_all_of_thread(thread, KO_ABSENT);
            --ko_num_all_threads;
            thread = next;
         }
      }
   } while (thread != KO_ABSENT);
   if (prev == KO_ABSENT)
      *threads = KO_ABSENT;
   else
      ko_set_next_of_all_of_thread(prev, KO_ABSENT);
   return prev;
}

static void
KO_SPLIT_WEAK_REFS(ko_value_t weakRef, ko_value_t *livePtr) {
   ko_value_t live;
   if (weakRef == KO_ABSENT) return;
   live = *livePtr;
   do {
      if (ko_descr_of_ptr(weakRef) == KO_OBJECT_MOVED) {
         ko_value_t next, key;
         weakRef = ko_new_address(weakRef);
         next = ko_next_of_weak_ref(weakRef);
         key = ko_key_of_weak_ref(weakRef);
         if (KO_OBJECT_TO_SAVE(key)) {
            if (ko_descr_of_ptr(key) == KO_OBJECT_MOVED) {
               ko_set_key_of_weak_ref(weakRef, ko_new_address(key));
            live:
               ko_set_next_of_weak_ref(weakRef, live);
               live = weakRef;
            }
            else {
               ko_value_t dead = ko_dead_of_weak_ref(weakRef);
               if (dead == KO_ABSENT)
                  ko_init_descr(weakRef, KO_DeadWeakRefCon);
               else {
                  ko_init_descr(weakRef, KO_ScheduledWeakRefCon);
                  ko_set_next_of_weak_ref(weakRef,
                     ko_first_of_dead_weak_refs(dead));
                  ko_set_first_of_dead_weak_refs(dead, weakRef);
               }
            }
         }
         else
            goto live;
         weakRef = next;
      }
      else {
         ko_unwatch_dead_object(weakRef);
         weakRef = ko_next_of_weak_ref(weakRef);
      }
   } while (weakRef != KO_ABSENT);
   *livePtr = live;
}

static ko_value_t
KO_SPLIT_FINALIZERS(ko_value_t finalizer, ko_value_t dead,
   ko_value_t *livePtr)
{
   ko_value_t live;
   if (finalizer == KO_ABSENT) return dead;
   live = *livePtr;
   do {
      ko_value_t next = ko_next_of_finalizer(finalizer);
      if (KO_LIKELY(ko_descr_of_ptr(finalizer) == KO_FinalizerCon)) {
         ko_value_t key = ko_key_of_finalizer(finalizer);
         if (KO_OBJECT_TO_SAVE(key)) {
            if (ko_descr_of_ptr(key) == KO_OBJECT_MOVED) {
               ko_set_key_of_finalizer(finalizer, ko_new_address(key));
            live:
               ko_set_next_of_finalizer(finalizer, live);
               live = finalizer;
            }
            else {
               ko_set_next_of_finalizer(finalizer, dead);
               dead = finalizer;
            }
         }
         else
            goto live;
      }
      finalizer = next;
   } while (finalizer != KO_ABSENT);
   *livePtr = live;
   return dead;
}

static void
KO_FIND_LIVE_TYPE_DICT_ENTRIES(ko_value_t entries, ko_value_t *livePtr) {
   ko_value_t live;
   if (entries == KO_ABSENT) return;
   live = *livePtr;
   do {
      if (ko_descr_of_ptr(entries) == KO_OBJECT_MOVED) {
         ko_value_t next;
         ko_nint_t size;
         ko_value_t *ptr;
         entries = ko_new_address(entries);
         next = ko_next_of_type_dict_entries(entries);
         size = ko_nint_of_sint(ko_mask_of_type_dict_entries(entries)) + 1;
         ptr = ko_elems_of_type_dict_entries(entries);
         for (; size != 0; --size) {
            ko_value_t key = ptr[0];
            if (KO_OBJECT_TO_SAVE(key)) {
               if (KO_LIKELY(ko_descr_of_ptr(key) == KO_OBJECT_MOVED))
                  ptr[0] = ko_new_address(key);
               else {
                  ptr[0] = KO_REMOVED_TYPE_DICT_ENTRY;
                  ptr[1] = KO_ABSENT;
                  ko_set_removed_of_type_dict_entries(entries,
                     ko_add1(ko_removed_of_type_dict_entries(entries)));
               }
            }
            ptr += 2;
         }
         ko_set_next_of_type_dict_entries(entries, live);
         live = entries;
         entries = next;
      }
      else {
         ko_unwatch_dead_object(entries);
         entries = ko_next_of_type_dict_entries(entries);
      }
   } while (entries != KO_ABSENT);
   *livePtr = live;
}

static void
KO_REHASH_OBJECT_IDS(struct ko_object_ids *src, struct ko_object_ids *dest) {
   ko_value_t *ptr = src->entries;
   ko_unint_t n = src->mask + 1;
   do {
      ko_value_t key = ptr[0];
      if (key != KO_ABSENT) {
         ko_value_t value = ptr[1];
         if (ko_descr_of_ptr(value) == KO_OBJECT_MOVED) {
            value = ko_new_address(value);
            if (KO_OBJECT_TO_SAVE(key)) {
               if (ko_descr_of_ptr(key) == KO_OBJECT_MOVED) {
                  key = ko_new_address(key);
               live:
                  ko_new_object_id(dest, key, value);
               }
            }
            else
               goto live;
         }
         ptr[0] = KO_ABSENT;
      }
      ptr += 2;
   } while (--n);
   src->size = 0;
}
