// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Collections.Ends =>

use Kokogut.Core;
use Kokogut.Unsafe;
use Kokogut.ExceptionTypes;
use Kokogut.Exceptions;
use Kokogut.Lists;
use Kokogut.Collections.Core;
use Kokogut.Collections.Reverse;
use Kokogut.Collections.Single;
use Kokogut.Collections.Part;
use Kokogut.Collections.Whole;

// First, Last

def First coll! {};

method First list^CONS {UnsafeFirst list};
method First _^NIL  {Fail EmptyCollection};

method First seq^FLAT_SEQUENCE {
   if (IsEmpty seq) {Fail EmptyCollection} =>
   seq@0
};

method First coll^COLLECTION {
   let iter = Iterate coll;
   iter {Fail EmptyCollection} Identity
};

def Last seq! {};

method Last list^CONS {
   c_eval [list] "
      ko_value_t iter = list;
      while (ko_rest(iter) != KO_Nil)
         iter = ko_rest(iter);
      KO_RESULT = ko_first(iter);
   "
};
method Last _^NIL {Fail EmptyCollection};

method Last seq^FLAT_SEQUENCE {
   let size = Size seq;
   if (IsSmallInt size) {
      if (size %UnsafeGT 0) {seq@(size %UnsafeSub 1)} =>
      if (size %Is 0) {Fail EmptyCollection} =>
      Fail (NegativeSize size)
   } {
      c_stat [size] "KO_CHECK_IS_BINT(size);";
      if (size %UnsafeGTInt 0) {seq@(size - 1)} =>
      if (size %Is 0) {Fail EmptyCollection} =>
      Fail (NegativeSize size)
   }
};

method Last seq^SEQUENCE {
   let iter = Iterate seq;
   iter {Fail EmptyCollection} (function ?last {iter {last} again})
};

// GetFirst, GetLast

def GetFirst [
   coll absent {again coll absent Identity}
   coll! absent present {}
];

method GetFirst list^CONS _absent present {present (UnsafeFirst list)};
method GetFirst _^NIL  absent _present {absent()};

method GetFirst seq^FLAT_SEQUENCE absent present {
   if (IsEmpty seq) {absent()} {present seq@0}
};

method GetFirst coll^COLLECTION absent present {
   let iter = Iterate coll;
   iter absent present
};

def GetLast [
   seq absent {again seq absent Identity}
   seq! absent present {}
];

method GetLast list^CONS _absent present {
   c_eval [list] "
      ko_value_t iter = list;
      while (ko_rest(iter) != KO_Nil)
         iter = ko_rest(iter);
      KO_RESULT = ko_first(iter);
   "->present
};
method GetLast _^NIL absent _present {absent()};

method GetLast seq^FLAT_SEQUENCE absent present {
   let size = Size seq;
   if (IsSmallInt size) {
      if (size %UnsafeGT 0) {present seq@(size %UnsafeSub 1)} =>
      if (size %Is 0) {absent()} =>
      Fail (NegativeSize size)
   } {
      c_stat [size] "KO_CHECK_IS_BINT(size);";
      if (size %UnsafeGTInt 0) {present seq@(size - 1)} =>
      if (size %Is 0) {absent()} =>
      Fail (NegativeSize size)
   }
};

method GetLast seq^SEQUENCE absent present {
   let iter = IterateBack seq;
   iter absent present
};

// AddFirst, AddLast

def AddFirst seq! elem {};

method AddFirst list^CONS elem {elem\list};
method AddFirst _^NIL elem {[elem]};

method AddFirst seq^SEQUENCE elem {AddPart seq 0 [elem]};

def AddLast seq! elem {};

method AddLast list^CONS elem {[list... elem]};
method AddLast _^NIL elem {[elem]};

method AddLast seq^SEQUENCE elem {AddPart seq [elem]};

// RemoveFirstGen, RemoveLastGen

def RemoveFirstGen coll! {};

method RemoveFirstGen list^CONS {
   let rest = UnsafeRest list;
   Generate {IterateList rest}
};
method RemoveFirstGen _^NIL {EmptyGenerator};

method RemoveFirstGen gen^GENERATOR {
   Generate {
      let iter = gen();
      iter {EmptyIterator} ?_ =>
      iter
   }
};

method RemoveFirstGen seq^FLAT_SEQUENCE {PartGen seq 1};

method RemoveFirstGen coll^COLLECTION {
   Generate {
      let iter = Iterate coll;
      iter {EmptyIterator} ?_ =>
      iter
   }
};

def RemoveLastGen seq! {};

method RemoveLastGen list^CONS {
   Generate {
      var iter = list;
      ?absent present {
         case iter [
            (elem\(rest & ~[])) {iter = rest; present elem}
            _                   {absent()}
         ]
      }
   }
};
method RemoveLastGen _^NIL {EmptyGenerator};

method RemoveLastGen gen^GENERATOR {
   Generate {
      let iter = gen();
      iter {EmptyIterator} ?(var elem) =>
      ?absent present {
         iter absent ?next =>
         let prev = elem;
         elem = next;
         present prev
      }
   }
};

method RemoveLastGen seq^FLAT_SEQUENCE {PartGen seq 0 (Size seq - 1)};

method RemoveLastGen seq^SEQUENCE {
   Generate {
      let iter = Iterate seq;
      iter {EmptyIterator} ?(var elem) =>
      ?absent present {
         iter absent ?next =>
         let prev = elem;
         elem = next;
         present prev
      }
   }
};

// RemoveFirst, RemoveLast

def RemoveFirst coll! {};

method RemoveFirst list^CONS {UnsafeRest list};
method RemoveFirst list^NIL  {list};

method RemoveFirst gen^GENERATOR {RemoveFirstGen gen};
method RemoveFirst seq^FLAT_SEQUENCE {Part seq 1};
method RemoveFirst seq^SEQUENCE {RemoveFirstGen seq->Like seq};

method RemoveFirst set^SET {GetFirst set {set} (Remove set _)};
method RemoveFirst dict^DICTIONARY {
   GetFirst dict {dict} ?(key, _) {Remove dict key}
};

def RemoveLast seq! {};

method RemoveLast [list... _]^CONS {list};
method RemoveLast list^NIL         {list};

method RemoveLast gen^GENERATOR {RemoveLastGen gen};
method RemoveLast seq^FLAT_SEQUENCE {Part seq 0 (Size seq - 1)};
method RemoveLast seq^SEQUENCE {RemoveLastGen seq->Like seq};

// DoAddFirst, DoAddLast

def DoAddFirst seq! elem {};

method DoAddFirst seq^SEQUENCE elem {
   DoAddPart seq 0 [elem]
};

def DoAddLast seq! elem {};

method DoAddLast seq^SEQUENCE elem {
   DoAddPart seq [elem]
};

// DoRemoveFirst, DoRemoveLast

def DoRemoveFirst coll! {};

method DoRemoveFirst seq^SEQUENCE {
   DoRemovePart seq 0 1
};

method DoRemoveFirst set^SET {
   GetFirst set {} (DoRemove set _)
};

method DoRemoveFirst dict^DICTIONARY {
   GetFirst dict {} ?(key, _) {DoRemove dict key}
};

def DoRemoveLast seq! {};

method DoRemoveLast seq^SEQUENCE {
   DoRemovePart seq (Size seq - 1)
};

// DoCutFirst, DoCutLast

def DoCutFirst coll! {};

method DoCutFirst seq^SEQUENCE {
   let elem = First seq;
   DoRemoveFirst seq;
   elem
};

method DoCutFirst set^SET {
   let elem = First set;
   DoRemove set elem;
   elem
};

method DoCutFirst dict^DICTIONARY {
   let (elem & (key, _)) = First dict;
   DoRemove dict key;
   elem
};

def DoCutLast seq! {};

method DoCutLast seq^SEQUENCE {
   let elem = Last seq;
   DoRemoveLast seq;
   elem
};

// TryCutFirst, TryCutLast

def TryCutFirst [
   coll absent {again coll absent Identity}
   coll! absent present {}
];

method TryCutFirst coll^COLLECTION absent present {
   GetFirst coll absent ?value =>
   DoRemoveFirst coll;
   present value
};

def TryCutLast [
   seq absent {again seq absent Identity}
   seq! absent present {}
];

method TryCutLast seq^SEQUENCE absent present {
   GetLast seq absent ?value =>
   DoRemoveLast seq;
   present value
};
