// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Collections.Unique =>

use Kokogut.Core;
use Kokogut.Unsafe;
use Kokogut.Lists;
use Kokogut.Collections.Core;

// UniqueGen, UniqueGenBy

def UniqueGen coll! {};

method UniqueGen list^CONS {
   Generate {
      var iter = list;
      var state = #_start;
      function ?absent present {
         case iter [
            (elem\rest) {
               iter = rest;
               let prev = state;
               state = elem;
               if (prev %Is #_start | prev ~%Is elem)
                  {present elem}
                  {again absent present}
            }
            _ {absent()}
         ]
      }
   }
};
method UniqueGen _^NIL {EmptyGenerator};

method UniqueGen coll^COLLECTION {
   Generate {
      let iter = Iterate coll;
      var state = #_start;
      function ?absent present {
         iter absent ?elem =>
         let prev = state;
         state = elem;
         if (prev %Is #_start | prev ~%Is elem)
            {present elem}
            {again absent present}
      }
   }
};

def UniqueGenBy coll! getKey {};

method UniqueGenBy list^CONS getKey {
   Generate {
      var iter = list;
      var state = #_start;
      function ?absent present {
         case iter [
            (elem\rest) {
               iter = rest;
               let prev = state;
               let key = getKey elem;
               state = key;
               if (prev %Is #_start | prev ~%Is key)
                  {present elem}
                  {again absent present}
            }
            _ {absent()}
         ]
      }
   }
};
method UniqueGenBy _^NIL _ {EmptyGenerator};

method UniqueGenBy coll^COLLECTION getKey {
   Generate {
      let iter = Iterate coll;
      var state = #_start;
      function ?absent present {
         iter absent ?elem =>
         let prev = state;
         let key = getKey elem;
         state = key;
         if (prev %Is #_start | prev ~%Is key)
            {present elem}
            {again absent present}
      }
   }
};

// UniqueList, UniqueListBy

def UniqueList coll! {};

method UniqueList list^CONS {
   let elem0 = UnsafeFirst list;
   elem0 \
   loop elem0 (UnsafeRest list) [
      prev (elem\rest) {
         if (prev %Is elem)
            {again elem rest}
            {elem \ again elem rest}
      }
      _ _ {[]}
   ]
};
method UniqueList list^NIL {list};

method UniqueList coll^COLLECTION {
   let iter = Iterate coll;
   iter {[]} ?elem0 =>
   elem0 \
   loop elem0 ?prev {
      iter {[]} ?elem =>
      if (prev %Is elem) {again elem} {elem \ again elem}
   }
};

def UniqueListBy coll! getKey {};

method UniqueListBy list^CONS getKey {
   let elem0 = UnsafeFirst list;
   elem0 \
   loop (getKey elem0) (UnsafeRest list) [
      prev (elem\rest) {
         let key = getKey elem;
         if (prev %Is key) {again key rest} {elem \ again key rest}
      }
      _ _ {[]}
   ]
};
method UniqueListBy list^NIL _ {list};

method UniqueListBy coll^COLLECTION getKey {
   let iter = Iterate coll;
   iter {[]} ?elem0 =>
   elem0 \
   loop (getKey elem0) ?prev {
      iter {[]} ?elem =>
      let key = getKey elem;
      if (prev %Is key) {again key} {elem \ again key}
   }
};

// Unique, UniqueBy

def Unique seq! {};

method Unique list^CONS {
   let elem0 = UnsafeFirst list;
   elem0 \
   loop elem0 (UnsafeRest list) [
      prev (elem\rest) {
         if (prev %Is elem) {again elem rest} {elem \ again elem rest}
      }
      _ _ {[]}
   ]
};
method Unique list^NIL {list};

method Unique gen^GENERATOR {
   UniqueGen gen
};

method Unique seq^COLLECTION {
   UniqueGen seq->Like seq
};

def UniqueBy seq! getKey {};

method UniqueBy list^CONS getKey {
   let elem0 = UnsafeFirst list;
   elem0 \
   loop (getKey elem0) (UnsafeRest list) [
      prev (elem\rest) {
         let key = getKey elem;
         if (prev %Is key) {again key rest} {elem \ again key rest}
      }
      _ _ {[]}
   ]
};
method UniqueBy list^NIL _ {list};

method UniqueBy gen^GENERATOR getKey {
   UniqueGenBy gen getKey
};

method UniqueBy seq^COLLECTION getKey {
   UniqueGenBy seq getKey->Like seq
};
