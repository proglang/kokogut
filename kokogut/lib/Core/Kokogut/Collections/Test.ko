// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Collections.Test =>

use Kokogut.Core;
use Kokogut.Unsafe;
use Kokogut.Lists;
use Kokogut.Collections.Core;
use Kokogut.Collections.Whole;
use Kokogut.Collections.Single;

// SelectGen

def SelectGen [
   coll! pred {}
   coll1 coll2 pred {
      Generate {
         let iter1 = Iterate coll1;
         let iter2 = Iterate coll2;
         function ?absent present {
            iter1 absent ?elem1 =>
            iter2 absent ?elem2 =>
            if (pred elem1 elem2)
               {present elem1}
               {again absent present}
         }
      }
   }
   (colls & ~[])... pred {
      Generate {
         let iter = Iterate colls...;
         function ?absent present {
            iter absent ?(elems & elem\_)... =>
            if (pred elems...)
               {present elem}
               {again absent present}
         }
      }
   }
];

method SelectGen list^CONS pred {
   Generate {
      var iter = list;
      function ?absent present {
         case iter [
            (elem\rest) {
               iter = rest;
               if (pred elem)
                  {present elem}
                  {again absent present}
            }
            _ {absent()}
         ]
      }
   }
};
method SelectGen _^NIL _ {EmptyGenerator};

method SelectGen coll^COLLECTION pred {
   Generate {
      let iter = Iterate coll;
      function ?absent present {
         iter absent ?elem =>
         if (pred elem)
            {present elem}
            {again absent present}
      }
   }
};

// RejectGen

def RejectGen [
   coll! pred {}
   coll1 coll2 pred {
      Generate {
         let iter1 = Iterate coll1;
         let iter2 = Iterate coll2;
         function ?absent present {
            iter1 absent ?elem1 =>
            iter2 absent ?elem2 =>
            if (pred elem1 elem2)
               {again absent present}
               {present elem1}
         }
      }
   }
   (colls & ~[])... pred {
      Generate {
         let iter = Iterate colls...;
         function ?absent present {
            iter absent ?(elems & elem\_)... =>
            if (pred elems...)
               {again absent present}
               {present elem}
         }
      }
   }
];

method RejectGen list^CONS pred {
   Generate {
      var iter = list;
      function ?absent present {
         case iter [
            (elem\rest) {
               iter = rest;
               if (pred elem)
                  {again absent present}
                  {present elem}
            }
            _ {absent()}
         ]
      }
   }
};
method RejectGen _^NIL _ {EmptyGenerator};

method RejectGen coll^COLLECTION pred {
   Generate {
      let iter = Iterate coll;
      function ?absent present {
         iter absent ?elem =>
         if (pred elem)
            {again absent present}
            {present elem}
      }
   }
};

// SelectList

def SelectList [
   coll! pred {}
   coll1 coll2 pred {
      let iter1 = Iterate coll1;
      let iter2 = Iterate coll2;
      loop {
         iter1 {[]} ?elem1 =>
         iter2 {[]} ?elem2 =>
         if (pred elem1 elem2)
            {elem1 \ again()}
            {again()}
      }
   }
   (colls & ~[])... pred {
      let iter = Iterate colls...;
      loop {
         iter {[]} ?(elems & elem\_)... =>
         if (pred elems...)
            {elem \ again()}
            {again()}
      }
   }
];

method SelectList list^CONS pred {
   loop list [
      (elem\rest) {if (pred elem) {elem \ again rest} {again rest}}
      _           {[]}
   ]
};
method SelectList list^NIL _ {list};

method SelectList coll^COLLECTION pred {
   let iter = Iterate coll;
   loop {
      iter {[]} ?elem =>
      if (pred elem)
         {elem \ again()}
         {again()}
   }
};

// RejectList

def RejectList [
   coll! pred {}
   coll1 coll2 pred {
      let iter1 = Iterate coll1;
      let iter2 = Iterate coll2;
      loop {
         iter1 {[]} ?elem1 =>
         iter2 {[]} ?elem2 =>
         if (pred elem1 elem2)
            {again()}
            {elem1 \ again()}
      }
   }
   (colls & ~[])... pred {
      let iter = Iterate colls...;
      loop {
         iter {[]} ?(elems & elem\_)... =>
         if (pred elems...)
            {again()}
            {elem \ again()}
      }
   }
];

method RejectList list^CONS pred {
   loop list [
      (elem\rest) {if (pred elem) {again rest} {elem \ again rest}}
      _           {[]}
   ]
};
method RejectList list^NIL _ {list};

method RejectList coll^COLLECTION pred {
   let iter = Iterate coll;
   loop {
      iter {[]} ?elem =>
      if (pred elem)
         {again()}
         {elem \ again()}
   }
};

// Select

def Select [
   coll! pred {}
   (args & coll\_)... {
      SelectGen args...->Like coll
   }
];

method Select list^CONS pred {
   loop list [
      (elem\rest) {if (pred elem) {elem \ again rest} {again rest}}
      _           {[]}
   ]
};
method Select list^NIL _ {list};

method Select gen^GENERATOR pred {
   SelectGen gen pred
};

method Select coll^COLLECTION pred {
   SelectGen coll pred->Like coll
};

// Reject

def Reject [
   coll! pred {}
   (args & coll\_)... {
      RejectGen args...->Like coll
   }
];

method Reject list^CONS pred {
   loop list [
      (elem\rest) {if (pred elem) {again rest} {elem \ again rest}}
      _           {[]}
   ]
};
method Reject list^NIL _ {list};

method Reject gen^GENERATOR pred {
   RejectGen gen pred
};

method Reject coll^COLLECTION pred {
   RejectGen coll pred->Like coll
};

// DoReject

def DoReject coll! pred {};

method DoReject coll^COLLECTION pred {
   Replace coll (RejectGen coll pred)
};

// Every

def Every [
   coll! pred {}
   coll1 coll2 pred {
      let iter1 = Iterate coll1;
      let iter2 = Iterate coll2;
      loop {
         iter1 {True} ?elem1 =>
         iter2 {True} ?elem2 =>
         pred elem1 elem2 & again()
      }
   }
   colls... pred {
      let iter = Iterate colls...;
      loop {
         iter {True} ?elems... =>
         pred elems... & again()
      }
   }
];

method Every list^CONS pred {
   pred (UnsafeFirst list) & loop (UnsafeRest list) [
      (elem\rest) {pred elem & again rest}
      _           {True}
   ]
};
method Every _^NIL _ {True};

method Every coll^COLLECTION pred {
   let iter = Iterate coll;
   loop {
      iter {True} ?elem =>
      pred elem & again()
   }
};

// Any

def Any [
   coll! pred {}
   coll1 coll2 pred {
      let iter1 = Iterate coll1;
      let iter2 = Iterate coll2;
      loop {
         iter1 {False} ?elem1 =>
         iter2 {False} ?elem2 =>
         pred elem1 elem2 | again()
      }
   }
   colls... pred {
      let iter = Iterate colls...;
      loop {
         iter {False} ?elems... =>
         pred elems... | again()
      }
   }
];

method Any list^CONS pred {
   pred (UnsafeFirst list) | loop (UnsafeRest list) [
      (elem\rest) {pred elem | again rest}
      _           {False}
   ]
};
method Any _^NIL _ {False};

method Any coll^COLLECTION pred {
   let iter = Iterate coll;
   loop {
      iter {False} ?elem =>
      pred elem | again()
   }
};

// Default definition of IsIn for sequences

method IsIn needle seq^SEQUENCE {
   Any seq (%Is needle)
};

// Count, CountIs, CountAtMost, CountAtLeast

def Count [
   coll pred {
      let iter = Iterate coll;
      loop 0 ?n {
         iter {n} ?elem =>
         if (pred elem)
            {again (n+1)}
            {again n}
      }
   }
   colls... pred {
      let iter = Iterate colls...;
      loop 0 ?n {
         iter {n} ?elems... =>
         if (pred elems...)
            {again (n+1)}
            {again n}
      }
   }
];

def CountIs [
   coll count pred {
      if (count < 0) {False} =>
      let iter = Iterate coll;
      loop count ?n {
         iter {n %Is 0} ?elem =>
         if (pred elem)
            {n ~%Is 0 & again (n-1)}
            {again n}
      }
   }
   colls... count pred {
      if (count < 0) {False} =>
      let iter = Iterate colls...;
      loop count ?n {
         iter {n %Is 0} ?elems... =>
         if (pred elems...)
            {n ~%Is 0 & again (n-1)}
            {again n}
      }
   }
];

def CountAtMost [
   coll count pred {
      if (count < 0) {False} =>
      let iter = Iterate coll;
      loop count ?n {
         iter {True} ?elem =>
         if (pred elem)
            {n ~%Is 0 & again (n-1)}
            {again n}
      }
   }
   colls... count pred {
      if (count < 0) {False} =>
      let iter = Iterate colls...;
      loop count ?n {
         iter {True} ?elems... =>
         if (pred elems...)
            {n ~%Is 0 & again (n-1)}
            {again n}
      }
   }
];

def CountAtLeast [
   coll count pred {
      if (count <= 0) {True} =>
      let iter = Iterate coll;
      loop count ?n {
         iter {False} ?elem =>
         if (pred elem)
            {n %Is 1 | again (n-1)}
            {again n}
      }
   }
   colls... count pred {
      if (count <= 0) {True} =>
      let iter = Iterate colls...;
      loop count ?n {
         iter {False} ?elems... =>
         if (pred elems...)
            {n %Is 1 | again (n-1)}
            {again n}
      }
   }
];
