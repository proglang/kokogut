// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2005-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Data.RegisteredLists =>

use Kokogut.Core;
use Kokogut.Collections.Core;
use Kokogut.Collections.Ends;
use Kokogut.Collections.Fold;
use Kokogut.Data.LazyLists;

c_define "
   KO_RECORD_FIELD(registered_list, 0, first);
";

c_type REGISTERED_LIST is:SEQUENCE (private RegisteredListCon) "
   KO_MOVE_RECORD_BEGIN(2);
   ko_mark(ko_first_ptr_of_registered_list(dest));
   KO_MOVE_RECORD_END(2);
";

c_define "
   /* ko_next_of_registered_node overlaps ko_first_of_registered_list */
   KO_RECORD_FIELD(registered_node, 0, next);
   KO_RECORD_FIELD(registered_node, 1, key);
   KO_RECORD_FIELD(registered_node, 2, value);
";

private c_type REGISTERED_NODE RegisteredNodeCon "
   KO_MOVE_RECORD_BEGIN(4);
   ko_mark_count(3);
   ko_mark(ko_next_ptr_of_registered_node(dest));
   ko_mark(ko_key_ptr_of_registered_node(dest));
   ko_mark(ko_value_ptr_of_registered_node(dest));
   KO_MOVE_RECORD_END(4);
";

c_define "
   KO_RECORD_FIELD(registered_key, 0, list);
";

c_type REGISTERED_KEY (private RegisteredKeyCon) "
   KO_MOVE_RECORD_BEGIN(2);
   ko_mark(ko_list_ptr_of_registered_key(dest));
   KO_MOVE_RECORD_END(2);
";

def RegisteredList() {
   c_eval [(c_con RegisteredListCon)] "
      KO_RESULT = ko_alloc(2);
      ko_init_descr(KO_RESULT, RegisteredListCon);
      ko_init_first_of_registered_list(KO_RESULT, KO_ABSENT);
   "
};

def Register list! value {};

method Register list^REGISTERED_LIST value {
   c_eval key [list value (c_con RegisteredKeyCon) (c_con RegisteredNodeCon)] "
      ko_value_t node;
      key = ko_alloc(6);
      ko_init_descr(key, RegisteredKeyCon);
      ko_init_list_of_registered_key(key, list);
      node = ko_offset(key, 2);
      ko_init_descr(node, RegisteredNodeCon);
      ko_init_next_of_registered_node(node,
         ko_first_of_registered_list(list));
      ko_init_key_of_registered_node(node, key);
      ko_init_value_of_registered_node(node, value);
      ko_set_first_of_registered_list(list, node);
   "
};

method Close key^REGISTERED_KEY {
   c_stat [key (c_con RegisteredNodeCon)] [prev old next] "
      prev = ko_list_of_registered_key(key);
      if (prev != KO_ABSENT) {
         ko_init_list_of_registered_key(key, KO_ABSENT);
         old = ko_first_of_registered_list(prev);
         for(;;) {
            if (ko_key_of_registered_node(old) == key) {
               ko_set_next_of_registered_node(prev,
                  ko_next_of_registered_node(old));
               break;
            }
            next = ko_alloc(4);
            ko_init_descr(next, RegisteredNodeCon);
            ko_init_next_of_registered_node(next, KO_ABSENT);
            ko_init_key_of_registered_node(next,
               ko_key_of_registered_node(old));
            ko_init_value_of_registered_node(next,
               ko_value_of_registered_node(old));
            ko_set_next_of_registered_node(prev, next);
            prev = next;
            old = ko_next_of_registered_node(old);
         }
      }
   "
};

private def IterateRegistered list {
   var current = c_expr [list] "ko_first_of_registered_list(list)";
   ?absent present {
      c_case [value next] [current] "
         if (current == KO_ABSENT) goto absent;
         value = ko_value_of_registered_node(current);
         next = ko_next_of_registered_node(current);
         goto present;
      " [
         absent:  {absent()}
         present: {current = next; present value}
      ]
   }
};

method Iterate list^REGISTERED_LIST {
   IterateRegistered list
};

method PureIterator list^REGISTERED_LIST {
   let iter = IterateRegistered list;
   Lazy (function {iter {} ?elem {elem, Lazy again}})
};

method DoAddFirst list^REGISTERED_LIST elem {
   Register list elem;
   Null
};

method TryCutFirst list^REGISTERED_LIST absent present {
   c_case [value] [list] "
      ko_value_t first;
      first = ko_first_of_registered_list(list);
      if (first == KO_ABSENT) goto absent;
      ko_init_list_of_registered_key(ko_key_of_registered_node(first),
         KO_ABSENT);
      value = ko_value_of_registered_node(first);
      ko_set_first_of_registered_list(list,
         ko_next_of_registered_node(first));
      goto present;
   " [
      absent:  {absent()}
      present: {present value}
   ]
};

method Fold list^REGISTERED_LIST init fun {
   loop init (c_expr [list] "ko_first_of_registered_list(list)") ?acc iter {
      c_case [value next] [iter] "
         if (iter == KO_ABSENT) goto absent;
         value = ko_value_of_registered_node(iter);
         next = ko_next_of_registered_node(iter);
         goto present;
      " [
         absent:  {acc}
         present: {again (fun acc value) next}
      ]
   }
};

method FoldBack list^REGISTERED_LIST init fun {
   loop (c_expr [list] "ko_first_of_registered_list(list)") ?iter {
      c_case [value next] [iter] "
         if (iter == KO_ABSENT) goto absent;
         value = ko_value_of_registered_node(iter);
         next = ko_next_of_registered_node(iter);
         goto present;
      " [
         absent:  {init}
         present: {fun value (again next)}
      ]
   }
};

method Each list^REGISTERED_LIST fun {
   loop (c_expr [list] "ko_first_of_registered_list(list)") ?iter {
      c_case [value next] [iter] "
         if (iter == KO_ABSENT) goto absent;
         value = ko_value_of_registered_node(iter);
         next = ko_next_of_registered_node(iter);
         goto present;
      " [
         absent:  {}
         present: {fun value; again next}
      ]
   }
};

method LazyList list^REGISTERED_LIST {
   let iter = IterateRegistered list;
   Lazy (function {iter {} ?elem {elem, Lazy again}})
};

method LazyList list^REGISTERED_LIST rest {
   let iter = IterateRegistered list;
   let commitRest = {CommitLazyAsync (LazyList rest)};
   Lazy (function {iter commitRest ?elem {elem, Lazy again}})
};
