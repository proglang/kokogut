// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Data.Arrays.Core =>

use Kokogut.Exceptions [];
use Kokogut.Collections.Core;
use Kokogut.Collections.Whole;
use Kokogut.Collections.Single;
use Kokogut.Collections.Part;

c_export "
   struct ko_array {
      KO_CHANGED_OBJECT;
      ko_value_t size, offset, limit;
      void *elems;
      ko_bool_t locked;
      ko_value_t unchanged_part;
   };

   struct ko_raw_array {
      KO_CHANGED_OBJECT;
      ko_value_t size, offset, limit;
      void *elems;
      ko_bool_t locked;
   };

   KO_OBJECT_FIELD_RAW(array, size, ko_value_t);
   KO_OBJECT_FIELD_RAW(array, offset, ko_value_t);
   KO_OBJECT_FIELD_RAW(array, limit, ko_value_t);
   KO_OBJECT_FIELD_RAW(array, elems, void *);
   KO_OBJECT_FIELD_RAW(array, locked, ko_bool_t);
   KO_OBJECT_FIELD_RAW(array, unchanged_part, ko_value_t);
";

// Representation of Arrays
// ------------------------
//
//                                limit
//                   ,--------------^--------------.
//    +--------------+--------------+--------------+
//    |     free     |   elements   |     free     |
//    +--------------+--------------+--------------+
//    `------.------'^`------.------'
//         offset    |     size
//                 elems
//
// In BOOL_ARRAY 'elems' points to the beginning of the allocated area,
// because individual elements cannot be addressed by pointers.
//
// 'locked' means that it should not be resized nor reallocated,
// perhaps because some foreign thread is accessing the array.
//
// 'unchanged_part' is present in OBJECT_ARRAY only. It's the size of
// the initial part of the array which is known to contain no pointers
// to young objects. Minor GC scans elements only from 'unchanged_part'
// to the end. If 'unchanged_part' is not at the end, the array must
// have been marked as a changed object.

c_export "
   #define KO_CHECK_ARRAY_IS_UNLOCKED(arr)               \\
      KO_STAT_BEGIN {                                    \\
         if (KO_UNLIKELY(ko_locked_of_array(arr)))       \\
            KO_FAIL_BY_JUMP1(KO_FailObjectLocked1, arr); \\
      } KO_STAT_END

   static inline ko_value_t
   ko_end_offset_of_array(ko_value_t arr) {
      return ko_sub(ko_limit_of_array(arr), ko_size_of_array(arr));
   }

   static inline void
   ko_add_size_of_array(ko_value_t arr, ko_value_t shift) {
      ko_set_size_of_array(arr, ko_add(ko_size_of_array(arr), shift));
   }

   static inline void
   ko_sub_size_of_array(ko_value_t arr, ko_value_t shift) {
      ko_set_size_of_array(arr, ko_sub(ko_size_of_array(arr), shift));
   }

   static inline void
   ko_add_offset_of_array(ko_value_t arr, ko_value_t shift) {
      ko_set_offset_of_array(arr, ko_add(ko_offset_of_array(arr), shift));
   }

   static inline void
   ko_sub_offset_of_array(ko_value_t arr, ko_value_t shift) {
      ko_set_offset_of_array(arr, ko_sub(ko_offset_of_array(arr), shift));
   }

   static inline void
   ko_add_limit_of_array(ko_value_t arr, ko_value_t shift) {
      ko_set_limit_of_array(arr, ko_add(ko_limit_of_array(arr), shift));
   }

   static inline void
   ko_sub_limit_of_array(ko_value_t arr, ko_value_t shift) {
      ko_set_limit_of_array(arr, ko_sub(ko_limit_of_array(arr), shift));
   }

   static inline void
   ko_add_unchanged_part_of_array(ko_value_t arr, ko_value_t shift) {
      ko_set_unchanged_part_of_array(arr,
         ko_add(ko_unchanged_part_of_array(arr), shift));
   }

   static inline void
   ko_sub_unchanged_part_of_array(ko_value_t arr, ko_value_t shift) {
      ko_set_unchanged_part_of_array(arr,
         ko_sub(ko_unchanged_part_of_array(arr), shift));
   }

   static inline ko_bool_t
   ko_in_first_half(ko_value_t x, ko_value_t y) {
      return (ko_nint_t)x <= (ko_nint_t)y >> 1; /* x < y/2 */
   }

   static inline ko_bool_t
   ko_array_has_space_for(ko_value_t arr, ko_value_t size) {
      return ko_ge(ko_add(ko_offset_of_array(arr), ko_limit_of_array(arr)),
         size);
   }

   #define KO_ALLOC_ARRAY(KIND, kind, type, result, size)           \\
      KO_STAT_BEGIN {                                               \\
         ko_value_t ko_arr_size, ko_arr_limit;                      \\
         ko_nint_t ko_arr_bytes;                                    \\
         ko_##type##_t *ko_arr_elems;                               \\
         ko_arr_size = size;                                        \\
         ko_arr_limit = ko_arr_size;                                \\
         if (ko_lt(ko_arr_limit, KO_MIN_##KIND##_ARRAY_ALLOC))      \\
            ko_arr_limit = KO_MIN_##KIND##_ARRAY_ALLOC;             \\
         ko_arr_bytes = ko_##type##s_sint_to_bytes(ko_arr_limit);   \\
         ko_calling_malloc(ko_arr_bytes);                           \\
         KO_SAFE_MALLOC(ko_arr_elems, ko_arr_bytes);                \\
         result = ko_make_##kind##_array(ko_arr_size, ko_arr_limit, \\
            ko_arr_elems);                                          \\
      } KO_STAT_END

   #define KO_ALLOC_ARRAY_CLEANUP(KIND, kind, type, result, size, cleanup) \\
      KO_STAT_BEGIN {                                                      \\
         ko_value_t ko_arr_size, ko_arr_limit;                             \\
         ko_nint_t ko_arr_bytes;                                           \\
         ko_##type##_t *ko_arr_elems;                                      \\
         ko_arr_size = size;                                               \\
         ko_arr_limit = ko_arr_size;                                       \\
         if (ko_lt(ko_arr_limit, KO_MIN_##KIND##_ARRAY_ALLOC))             \\
            ko_arr_limit = KO_MIN_##KIND##_ARRAY_ALLOC;                    \\
         ko_arr_bytes = ko_##type##s_sint_to_bytes(ko_arr_limit);          \\
         ko_calling_malloc(ko_arr_bytes);                                  \\
         KO_SAFE_MALLOC_CLEANUP(ko_arr_elems, ko_arr_bytes, cleanup);      \\
         result = ko_make_##kind##_array(ko_arr_size, ko_arr_limit,        \\
            ko_arr_elems);                                                 \\
      } KO_STAT_END

   #define KO_MAYBE_SHRINK_ARRAY(KIND, kind, type, arr)                \\
      KO_STAT_BEGIN {                                                  \\
         ko_value_t ko_arr_size = ko_size_of_array(arr);               \\
         ko_value_t ko_arr_alloc = ko_ge(ko_arr_size,                  \\
            KO_MIN_##KIND##_ARRAY_ALLOC) ?                             \\
            ko_arr_size :                                              \\
            KO_MIN_##KIND##_ARRAY_ALLOC;                               \\
         if (KO_UNLIKELY(                                              \\
            ko_gt(ko_offset_of_array(arr), ko_arr_alloc) ||            \\
            ko_gt(ko_end_offset_of_array(arr), ko_arr_alloc)           \\
         )) {                                                          \\
            ko_value_t ko_arr_shift;                                   \\
            ko_##type##_t *ko_arr_elems, *ko_arr_newElems;             \\
            ko_arr_shift = ko_offset_of_array(arr);                    \\
            ko_arr_elems = ko_elems_of_##kind##_array(arr);            \\
            if (ko_arr_shift != ko_sint(0)) {                          \\
               ko_arr_newElems = ko_shift_##type##s_back(ko_arr_elems, \\
                  ko_arr_shift);                                       \\
               ko_move_##type##s(ko_arr_newElems, ko_arr_elems,        \\
                  ko_arr_size);                                        \\
               ko_set_offset_of_array(arr, ko_sint(0));                \\
               ko_add_limit_of_array(arr, ko_arr_shift);               \\
               ko_set_elems_of_##kind##_array(arr, ko_arr_newElems);   \\
               ko_arr_elems = ko_arr_newElems;                         \\
            }                                                          \\
            ko_calling_realloc(                                        \\
               ko_##type##s_sint_to_bytes(ko_limit_of_array(arr)),     \\
               ko_##type##s_sint_to_bytes(ko_arr_alloc));              \\
            ko_arr_newElems = realloc(ko_arr_elems,                    \\
               ko_##type##s_sint_to_bytes(ko_arr_alloc));              \\
            if (KO_LIKELY(ko_arr_newElems != NULL)) {                  \\
               ko_set_limit_of_array(arr, ko_arr_alloc);               \\
               ko_set_elems_of_##kind##_array(arr, ko_arr_newElems);   \\
            }                                                          \\
         }                                                             \\
      } KO_STAT_END

   #define KO_SHIFT_ARRAY(KIND, kind, type, arr, offset)                   \\
      KO_STAT_BEGIN {                                                      \\
         ko_value_t ko_arr_newOffset, ko_arr_shift;                        \\
         ko_##type##_t *ko_arr_elems, *ko_arr_newElems;                    \\
         ko_arr_newOffset = offset;                                        \\
         ko_arr_shift = ko_sub(ko_arr_newOffset, ko_offset_of_array(arr)); \\
         ko_arr_elems = ko_elems_of_##kind##_array(arr);                   \\
         ko_arr_newElems = ko_shift_##type##s(ko_arr_elems, ko_arr_shift); \\
         ko_move_##type##s(ko_arr_newElems, ko_arr_elems,                  \\
            ko_size_of_array(arr));                                        \\
         ko_set_offset_of_array(arr, ko_arr_newOffset);                    \\
         ko_sub_limit_of_array(arr, ko_arr_shift);                         \\
         ko_set_elems_of_##kind##_array(arr, ko_arr_newElems);             \\
      } KO_STAT_END

   #define KO_REALLOC_RIGHT_ARRAY(KIND, kind, type, arr, limit)      \\
      KO_STAT_BEGIN {                                                \\
         ko_value_t ko_arr_offset, ko_arr_newLimit, ko_arr_newAlloc; \\
         ko_##type##_t *ko_arr_newMemory, *ko_arr_newElems;          \\
         ko_arr_offset = ko_offset_of_array(arr);                    \\
         ko_arr_newLimit = limit;                                    \\
         ko_arr_newAlloc = ko_add(ko_arr_offset, ko_arr_newLimit);   \\
         KO_CHECK_##KIND##_ARRAY_SIZE(ko_arr_newAlloc);              \\
         ko_set_locked_of_array(arr, ko_true);                       \\
         ko_calling_realloc(                                         \\
            ko_##type##s_sint_to_bytes(ko_add(ko_arr_offset,         \\
               ko_limit_of_array(arr))),                             \\
            ko_##type##s_sint_to_bytes(ko_arr_newAlloc));            \\
         ko_set_locked_of_array(arr, ko_false);                      \\
         KO_SAFE_REALLOC(ko_arr_newMemory,                           \\
            ko_shift_##type##s_back(ko_elems_of_##kind##_array(arr), \\
               ko_arr_offset),                                       \\
            ko_##type##s_sint_to_bytes(ko_arr_newAlloc));            \\
         ko_arr_newElems = ko_shift_##type##s(ko_arr_newMemory,      \\
            ko_arr_offset);                                          \\
         ko_set_limit_of_array(arr, ko_arr_newLimit);                \\
         ko_set_elems_of_##kind##_array(arr, ko_arr_newElems);       \\
      } KO_STAT_END

   #define KO_REALLOC_LEFT_ARRAY(KIND, kind, type, arr, offset)            \\
      KO_STAT_BEGIN {                                                      \\
         ko_value_t ko_arr_newOffset, ko_arr_limit, ko_arr_newAlloc;       \\
         ko_##type##_t *ko_arr_newMemory, *ko_arr_newElems;                \\
         ko_arr_newOffset = offset;                                        \\
         ko_arr_limit = ko_limit_of_array(arr);                            \\
         ko_arr_newAlloc = ko_add(ko_arr_newOffset, ko_arr_limit);         \\
         KO_CHECK_##KIND##_ARRAY_SIZE(ko_arr_newAlloc);                    \\
         ko_set_locked_of_array(arr, ko_true);                             \\
         ko_calling_realloc(                                               \\
            ko_##type##s_sint_to_bytes(ko_add(ko_offset_of_array(arr),     \\
               ko_arr_limit)),                                             \\
            ko_##type##s_sint_to_bytes(ko_arr_newAlloc));                  \\
         ko_set_locked_of_array(arr, ko_false);                            \\
         KO_SAFE_REALLOC(ko_arr_newMemory,                                 \\
            ko_shift_##type##s_back(ko_elems_of_##kind##_array(arr),       \\
               ko_offset_of_array(arr)),                                   \\
            ko_##type##s_sint_to_bytes(ko_arr_newAlloc));                  \\
         ko_arr_newElems = ko_shift_##type##s(ko_arr_newMemory,            \\
            ko_arr_newOffset);                                             \\
         ko_move_##type##s(ko_arr_newElems,                                \\
            ko_shift_##type##s(ko_arr_newMemory, ko_offset_of_array(arr)), \\
            ko_size_of_array(arr));                                        \\
         ko_set_offset_of_array(arr, ko_arr_newOffset);                    \\
         ko_set_elems_of_##kind##_array(arr, ko_arr_newElems);             \\
      } KO_STAT_END

   #define KO_ADD_ARRAY(KIND, kind, type, arr, index, elem)                  \\
      KO_STAT_BEGIN {                                                        \\
         if (ko_in_first_half(index, ko_size_of_array(arr))) {               \\
            ko_##type##_t *ko_arr_elems;                                     \\
            if (KO_UNLIKELY(ko_offset_of_array(arr) == ko_sint(0))) {        \\
               ko_value_t ko_arr_newSize = ko_add1(ko_size_of_array(arr));   \\
               KO_CHECK_##KIND##_ARRAY_SIZE(ko_arr_newSize);                 \\
               if (                                                          \\
                  ko_array_has_space_for(arr, ko_arr_newSize) &&             \\
                  !ko_in_first_half(ko_end_offset_of_array(arr),             \\
                     ko_arr_newSize)                                         \\
               )                                                             \\
                  KO_SHIFT_##KIND##_ARRAY(arr,                               \\
                     ko_add1(ko_shr_sn(ko_sub(ko_limit_of_array(arr),        \\
                        ko_arr_newSize), 1)));                               \\
               else                                                          \\
                  KO_REALLOC_LEFT_##KIND##_ARRAY(arr,                        \\
                     ko_add1(ko_shr_sn(ko_arr_newSize, 1)));                 \\
            }                                                                \\
            ko_arr_elems = ko_elems_of_##kind##_array(arr);                  \\
            ko_move_##type##s(ko_arr_elems - 1, ko_arr_elems, index);        \\
            ko_sub_offset_of_array(arr, ko_sint(1));                         \\
            ko_add_limit_of_array(arr, ko_sint(1));                          \\
            ko_set_elems_of_##kind##_array(arr, --ko_arr_elems);             \\
            *ko_shift_##type##s(ko_arr_elems, index) = elem;                 \\
         }                                                                   \\
         else {                                                              \\
            ko_##type##_t *ko_arr_dest;                                      \\
            if (KO_UNLIKELY(ko_size_of_array(arr)                            \\
               == ko_limit_of_array(arr))) {                                 \\
               ko_value_t ko_arr_newSize = ko_add1(ko_size_of_array(arr));   \\
               KO_CHECK_##KIND##_ARRAY_SIZE(ko_arr_newSize);                 \\
               if (                                                          \\
                  ko_array_has_space_for(arr, ko_arr_newSize) &&             \\
                  !ko_in_first_half(ko_offset_of_array(arr), ko_arr_newSize) \\
               )                                                             \\
                  KO_SHIFT_##KIND##_ARRAY(arr,                               \\
                     ko_shr_sn(ko_sub1(ko_offset_of_array(arr)), 1));        \\
               else                                                          \\
                  KO_REALLOC_RIGHT_##KIND##_ARRAY(arr,                       \\
                     ko_add(ko_arr_newSize, ko_shr_sn(ko_arr_newSize, 1)));  \\
            }                                                                \\
            ko_arr_dest = ko_shift_##type##s(                                \\
               ko_elems_of_##kind##_array(arr), index);                      \\
            ko_move_##type##s(                                               \\
               ko_arr_dest + 1,                                              \\
               ko_arr_dest,                                                  \\
               ko_sub(ko_size_of_array(arr), index));                        \\
            *ko_arr_dest = elem;                                             \\
         }                                                                   \\
         ko_add_size_of_array(arr, ko_sint(1));                              \\
      } KO_STAT_END

   #define KO_ADD_FIRST_ARRAY(KIND, kind, type, arr, elem)              \\
      KO_STAT_BEGIN {                                                   \\
         ko_##type##_t *ko_arr_elems;                                   \\
         if (KO_UNLIKELY(ko_offset_of_array(arr) == ko_sint(0))) {      \\
            ko_value_t ko_arr_newSize = ko_add1(ko_size_of_array(arr)); \\
            KO_CHECK_##KIND##_ARRAY_SIZE(ko_arr_newSize);               \\
            if (                                                        \\
               ko_array_has_space_for(arr, ko_arr_newSize) &&           \\
               !ko_in_first_half(ko_end_offset_of_array(arr),           \\
                  ko_arr_newSize)                                       \\
            )                                                           \\
               KO_SHIFT_##KIND##_ARRAY(arr,                             \\
                  ko_add1(ko_shr_sn(ko_sub(ko_limit_of_array(arr),      \\
                     ko_arr_newSize), 1)));                             \\
            else                                                        \\
               KO_REALLOC_LEFT_##KIND##_ARRAY(arr,                      \\
                  ko_add1(ko_shr_sn(ko_arr_newSize, 1)));               \\
         }                                                              \\
         ko_arr_elems = ko_elems_of_##kind##_array(arr);                \\
         ko_add_size_of_array(arr, ko_sint(1));                         \\
         ko_sub_offset_of_array(arr, ko_sint(1));                       \\
         ko_add_limit_of_array(arr, ko_sint(1));                        \\
         ko_set_elems_of_##kind##_array(arr, --ko_arr_elems);           \\
         *ko_arr_elems = elem;                                          \\
      } KO_STAT_END

   #define KO_ADD_LAST_ARRAY(KIND, kind, type, arr, elem)                    \\
      KO_STAT_BEGIN {                                                        \\
         ko_##type##_t *ko_arr_dest;                                         \\
         if (KO_UNLIKELY(ko_size_of_array(arr) == ko_limit_of_array(arr))) { \\
            ko_value_t ko_arr_newSize = ko_add1(ko_size_of_array(arr));      \\
            KO_CHECK_##KIND##_ARRAY_SIZE(ko_arr_newSize);                    \\
            if (                                                             \\
               ko_array_has_space_for(arr, ko_arr_newSize) &&                \\
               !ko_in_first_half(ko_offset_of_array(arr), ko_arr_newSize)    \\
            )                                                                \\
               KO_SHIFT_##KIND##_ARRAY(arr,                                  \\
                  ko_shr_sn(ko_sub1(ko_offset_of_array(arr)), 1));           \\
            else                                                             \\
               KO_REALLOC_RIGHT_##KIND##_ARRAY(arr,                          \\
                  ko_add(ko_arr_newSize, ko_shr_sn(ko_arr_newSize, 1)));     \\
         }                                                                   \\
         ko_arr_dest = ko_shift_##type##s(                                   \\
            ko_elems_of_##kind##_array(arr),                                 \\
            ko_size_of_array(arr));                                          \\
         ko_add_size_of_array(arr, ko_sint(1));                              \\
         *ko_arr_dest = elem;                                                \\
      } KO_STAT_END

   #define KO_REMOVE_ARRAY(KIND, kind, type, arr, index)              \\
      KO_STAT_BEGIN {                                                 \\
         ko_sub_size_of_array(arr, ko_sint(1));                       \\
         if (ko_in_first_half(index, ko_size_of_array(arr))) {        \\
            ko_##type##_t *ko_arr_elems;                              \\
            ko_arr_elems = ko_elems_of_##kind##_array(arr);           \\
            ko_move_##type##s(ko_arr_elems + 1, ko_arr_elems, index); \\
            ko_add_offset_of_array(arr, ko_sint(1));                  \\
            ko_sub_limit_of_array(arr, ko_sint(1));                   \\
            ko_set_elems_of_##kind##_array(arr, ko_arr_elems + 1);    \\
         }                                                            \\
         else {                                                       \\
            ko_##type##_t *ko_arr_dest;                               \\
            ko_arr_dest = ko_shift_##type##s(                         \\
               ko_elems_of_##kind##_array(arr), index);               \\
            ko_move_##type##s(                                        \\
               ko_arr_dest,                                           \\
               ko_arr_dest + 1,                                       \\
               ko_sub(ko_size_of_array(arr), index));                 \\
         }                                                            \\
      } KO_STAT_END

   #define KO_REMOVE_FIRST_ARRAY(KIND, kind, type, arr) \\
      KO_STAT_BEGIN {                                   \\
         ko_sub_size_of_array(arr, ko_sint(1));         \\
         ko_add_offset_of_array(arr, ko_sint(1));       \\
         ko_sub_limit_of_array(arr, ko_sint(1));        \\
         ko_set_elems_of_##kind##_array(arr,            \\
            ko_elems_of_##kind##_array(arr) + 1);       \\
      } KO_STAT_END

   #define KO_ADD_PART_ARRAY(KIND, kind, type, arr, index, partSize)         \\
      KO_STAT_BEGIN {                                                        \\
         if (ko_in_first_half(index, ko_size_of_array(arr))) {               \\
            ko_##type##_t *ko_arr_elems, *ko_arr_newElems;                   \\
            if (KO_UNLIKELY(ko_gt(partSize, ko_offset_of_array(arr)))) {     \\
               ko_value_t ko_arr_newSize = ko_add(ko_size_of_array(arr),     \\
                  partSize);                                                 \\
               KO_CHECK_##KIND##_ARRAY_SIZE(ko_arr_newSize);                 \\
               if (                                                          \\
                  ko_array_has_space_for(arr, ko_arr_newSize) &&             \\
                  !ko_in_first_half(ko_end_offset_of_array(arr),             \\
                     ko_arr_newSize)                                         \\
               )                                                             \\
                  KO_SHIFT_##KIND##_ARRAY(arr,                               \\
                     ko_add(ko_shr_sn(ko_sub(ko_add(ko_offset_of_array(arr), \\
                        ko_limit_of_array(arr)), ko_arr_newSize), 1),        \\
                        partSize));                                          \\
               else                                                          \\
                  KO_REALLOC_LEFT_##KIND##_ARRAY(arr,                        \\
                     ko_add(ko_shr_sn(ko_arr_newSize, 1), partSize));        \\
            }                                                                \\
            ko_arr_elems = ko_elems_of_##kind##_array(arr);                  \\
            ko_arr_newElems = ko_shift_##type##s_back(ko_arr_elems,          \\
               partSize);                                                    \\
            ko_move_##type##s(ko_arr_newElems, ko_arr_elems, index);         \\
            ko_sub_offset_of_array(arr, partSize);                           \\
            ko_add_limit_of_array(arr, partSize);                            \\
            ko_set_elems_of_##kind##_array(arr, ko_arr_newElems);            \\
         }                                                                   \\
         else {                                                              \\
            ko_value_t ko_arr_newSize = ko_add(ko_size_of_array(arr),        \\
               partSize);                                                    \\
            ko_##type##_t *ko_arr_dest;                                      \\
            KO_CHECK_##KIND##_ARRAY_SIZE(ko_arr_newSize);                    \\
            if (KO_UNLIKELY(ko_gt(ko_arr_newSize,                            \\
               ko_limit_of_array(arr)))) {                                   \\
               if (                                                          \\
                  ko_array_has_space_for(arr, ko_arr_newSize) &&             \\
                  !ko_in_first_half(ko_offset_of_array(arr), ko_arr_newSize) \\
               )                                                             \\
                  KO_SHIFT_##KIND##_ARRAY(arr,                               \\
                     ko_shr_sn(ko_sub(ko_add(ko_offset_of_array(arr),        \\
                        ko_limit_of_array(arr)), ko_arr_newSize), 1));       \\
               else                                                          \\
                  KO_REALLOC_RIGHT_##KIND##_ARRAY(arr,                       \\
                     ko_add(ko_arr_newSize, ko_shr_sn(ko_arr_newSize, 1)));  \\
            }                                                                \\
            ko_arr_dest = ko_shift_##type##s(                                \\
               ko_elems_of_##kind##_array(arr), index);                      \\
            ko_move_##type##s(                                               \\
               ko_shift_##type##s(ko_arr_dest, partSize),                    \\
               ko_arr_dest,                                                  \\
               ko_sub(ko_size_of_array(arr), index));                        \\
         }                                                                   \\
         ko_add_size_of_array(arr, partSize);                                \\
      } KO_STAT_END

   #define KO_ADD_LAST_PART_ARRAY(KIND, kind, type, arr, partSize)         \\
      KO_STAT_BEGIN {                                                      \\
         ko_value_t ko_arr_newSize = ko_add(ko_size_of_array(arr),         \\
            partSize);                                                     \\
         KO_CHECK_##KIND##_ARRAY_SIZE(ko_arr_newSize);                     \\
         if (KO_UNLIKELY(ko_gt(ko_arr_newSize, ko_limit_of_array(arr)))) { \\
            if (                                                           \\
               ko_array_has_space_for(arr, ko_arr_newSize) &&              \\
               !ko_in_first_half(ko_offset_of_array(arr), ko_arr_newSize)  \\
            )                                                              \\
               KO_SHIFT_##KIND##_ARRAY(arr,                                \\
                  ko_shr_sn(ko_sub(ko_add(ko_offset_of_array(arr),         \\
                     ko_limit_of_array(arr)), ko_arr_newSize), 1));        \\
            else                                                           \\
               KO_REALLOC_RIGHT_##KIND##_ARRAY(arr,                        \\
                  ko_add(ko_arr_newSize, ko_shr_sn(ko_arr_newSize, 1)));   \\
         }                                                                 \\
         ko_set_size_of_array(arr, ko_arr_newSize);                        \\
      } KO_STAT_END

   #define KO_REMOVE_PART_ARRAY(KIND, kind, type, arr, start, end)   \\
      KO_STAT_BEGIN {                                                \\
         ko_value_t ko_arr_partSize;                                 \\
         ko_arr_partSize = ko_sub(end, start);                       \\
         ko_sub_size_of_array(arr, ko_arr_partSize);                 \\
         if (ko_in_first_half(start, ko_size_of_array(arr))) {       \\
            ko_##type##_t *ko_arr_elems, *ko_arr_newElems;           \\
            ko_arr_elems = ko_elems_of_##kind##_array(arr);          \\
            ko_arr_newElems = ko_shift_##type##s(ko_arr_elems,       \\
               ko_arr_partSize);                                     \\
            ko_move_##type##s(ko_arr_newElems, ko_arr_elems, start); \\
            ko_add_offset_of_array(arr, ko_arr_partSize);            \\
            ko_sub_limit_of_array(arr, ko_arr_partSize);             \\
            ko_set_elems_of_##kind##_array(arr, ko_arr_newElems);    \\
         }                                                           \\
         else {                                                      \\
            ko_##type##_t *ko_arr_elems;                             \\
            ko_arr_elems = ko_elems_of_##kind##_array(arr);          \\
            ko_move_##type##s(                                       \\
               ko_shift_##type##s(ko_arr_elems, start),              \\
               ko_shift_##type##s(ko_arr_elems, end),                \\
               ko_sub(ko_size_of_array(arr), start));                \\
         }                                                           \\
      } KO_STAT_END

   #define KO_REMOVE_FIRST_PART_ARRAY(KIND, kind, type, arr, partSize) \\
      KO_STAT_BEGIN {                                                  \\
         ko_value_t ko_arr_partSize = partSize;                        \\
         ko_sub_size_of_array(arr, ko_arr_partSize);                   \\
         if (ko_size_of_array(arr) != ko_sint(0)) {                    \\
            ko_add_offset_of_array(arr, ko_arr_partSize);              \\
            ko_sub_limit_of_array(arr, ko_arr_partSize);               \\
            ko_set_elems_of_##kind##_array(arr,                        \\
               ko_shift_##type##s(ko_elems_of_##kind##_array(arr),     \\
                  ko_arr_partSize));                                   \\
         }                                                             \\
      } KO_STAT_END
";

type ARRAY is:FLAT_SEQUENCE;

method Add arr^ARRAY index elem {
   let arr' = Clone arr;
   DoAdd arr' index elem;
   arr'
};

method Remove arr^ARRAY index {
   let arr' = Clone arr;
   DoRemove arr' index;
   arr'
};

method AddPart arr^ARRAY index part^SEQUENCE {
   let arr' = Clone arr;
   DoAddPart arr' index part;
   arr'
};

method AddPart arr^ARRAY part^COLLECTION {
   let arr' = Clone arr;
   DoAddPart arr' part;
   arr'
};

method RemovePart arr^ARRAY begin end {
   let arr' = Clone arr;
   DoRemovePart arr' begin end;
   arr'
};

method RemovePart arr^ARRAY begin {
   let arr' = Clone arr;
   DoRemovePart arr' begin;
   arr'
};

def SwapObjectArray arr1 arr2 {
   c_stat [arr1 arr2] "
      ko_value_t size, offset, limit;
      void *elems;
      ko_value_t unchanged;
      KO_CHECK_ARRAY_IS_UNLOCKED(arr1);
      KO_CHECK_ARRAY_IS_UNLOCKED(arr2);
      size = ko_size_of_array(arr1);
      offset = ko_offset_of_array(arr1);
      limit = ko_limit_of_array(arr1);
      elems = ko_elems_of_array(arr1);
      unchanged = ko_unchanged_part_of_array(arr1);
      ko_set_size_of_array(arr1, ko_size_of_array(arr2));
      ko_set_offset_of_array(arr1, ko_offset_of_array(arr2));
      ko_set_limit_of_array(arr1, ko_limit_of_array(arr2));
      ko_set_elems_of_array(arr1, ko_elems_of_array(arr2));
      ko_set_unchanged_part_of_array(arr1, ko_unchanged_part_of_array(arr2));
      ko_set_size_of_array(arr2, size);
      ko_set_offset_of_array(arr2, offset);
      ko_set_limit_of_array(arr2, limit);
      ko_set_elems_of_array(arr2, elems);
      ko_set_unchanged_part_of_array(arr2, unchanged);
      ko_mark_changed_object(arr1);
      ko_mark_changed_object(arr2);
   "
};

def SwapRawArray arr1 arr2 {
   c_stat [arr1 arr2] "
      ko_value_t size, offset, limit;
      void *elems;
      KO_CHECK_ARRAY_IS_UNLOCKED(arr1);
      KO_CHECK_ARRAY_IS_UNLOCKED(arr2);
      size = ko_size_of_array(arr1);
      offset = ko_offset_of_array(arr1);
      limit = ko_limit_of_array(arr1);
      elems = ko_elems_of_array(arr1);
      ko_set_size_of_array(arr1, ko_size_of_array(arr2));
      ko_set_offset_of_array(arr1, ko_offset_of_array(arr2));
      ko_set_limit_of_array(arr1, ko_limit_of_array(arr2));
      ko_set_elems_of_array(arr1, ko_elems_of_array(arr2));
      ko_set_size_of_array(arr2, size);
      ko_set_offset_of_array(arr2, offset);
      ko_set_limit_of_array(arr2, limit);
      ko_set_elems_of_array(arr2, elems);
   "
};
