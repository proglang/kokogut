// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2009 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Data.WeakHashDicts =>

use Kokogut.Core;
use Kokogut.Unsafe;
use Kokogut.ExceptionTypes;
use Kokogut.Exceptions;
use Kokogut.WeakRefs;
use Kokogut.Lists;
use Kokogut.Collections.Core;
use Kokogut.Collections.Whole;
use Kokogut.Collections.Single;
use Kokogut.Collections.Ends;
use Kokogut.Collections.Set;
use Kokogut.Collections.Map;
use Kokogut.Collections.Fold;
use Kokogut.Collections.Test;
use Kokogut.Data.Hashing;

// ENTRY

c_define "
   KO_RECORD_FIELD(entry, 0, key);
   KO_RECORD_FIELD(entry, 1, value);
";

// ENTRIES

c_define "
   KO_RECORD_FIELD_RAW(entries, 0, mask);

   static inline ko_value_t *
   ko_elems_of_entries(ko_value_t entries) {
      return ko_field_ptr(entries, 1);
   }

   static inline ko_value_t *
   ko_hash_ptr_at_entries(ko_value_t entries, ko_value_t i) {
      return ko_shift_values(ko_elems_of_entries(entries), ko_mul_sn(i, 2));
   }

   static inline ko_value_t
   ko_hash_at_entries(ko_value_t entries, ko_value_t i) {
      return *ko_hash_ptr_at_entries(entries, i);
   }

   static inline void
   ko_set_hash_at_entries(ko_value_t entries, ko_value_t i, ko_value_t hash) {
      *ko_hash_ptr_at_entries(entries, i) = hash;
   }

   static inline ko_value_t *
   ko_weak_entry_ptr_at_entries(ko_value_t entries, ko_value_t i) {
      return ko_shift_values(ko_elems_of_entries(entries),
         ko_add_sn(ko_mul_sn(i, 2), 1));
   }

   static inline ko_value_t
   ko_weak_entry_at_entries(ko_value_t entries, ko_value_t i) {
      return *ko_weak_entry_ptr_at_entries(entries, i);
   }

   static inline void
   ko_init_weak_entry_at_entries(ko_value_t entries, ko_value_t i,
      ko_value_t weak_entry)
   {
      *ko_weak_entry_ptr_at_entries(entries, i) = weak_entry;
   }

   static inline void
   ko_set_weak_entry_at_entries(ko_value_t entries, ko_value_t i,
      ko_value_t weak_entry)
   {
      ko_set_field_by_ptr(entries, ko_weak_entry_ptr_at_entries(entries, i),
         weak_entry);
   }
";

private c_type ENTRIES EntriesCon "
   ko_nint_t size = ko_nint_of_sint(ko_mask_of_entries(src)) + 1;
   ko_value_t *ptr, **top;
   KO_MOVE_RECORD_BEGIN(2 + 2 * size);
   ko_mark_count(size);
   ptr = ko_elems_of_entries(dest);
   top = ko_marked_top;
   for (; size != 0; --size) {
      ++ptr;
      *top++ = ptr++;
   }
   ko_marked_top = top;
   return (ko_value_t)ptr;
";

c_defineName [KO_EntriesCon:(c_con EntriesCon)];

c_define "
   static ko_value_t
   ko_make_entries(ko_value_t size) {
      ko_value_t entries = ko_alloc(2 + 2 * ko_nint_of_sint(size));
      ko_value_t *ptr;
      ko_nint_t n;
      ko_init_descr(entries, KO_EntriesCon);
      ko_set_mask_of_entries(entries, ko_sub1(size));
      ptr = ko_elems_of_entries(entries);
      for (n = ko_nint_of_sint(size); n != 0; --n) {
         *ptr++ = ko_sint(-1);
         *ptr++ = KO_ABSENT;
      }
      return entries;
   }
";

c_define "
   KO_RECORD_FIELD_RAW(dict, 0, hash);
   KO_RECORD_FIELD_RAW(dict, 1, equal);
   KO_RECORD_FIELD(dict, 2, entries);
   KO_RECORD_FIELD_RAW(dict, 3, size);
   KO_RECORD_FIELD_RAW(dict, 4, removed);
   KO_RECORD_FIELD(dict, 5, dead);
";

c_type WEAK_HASH_DICT is:DICTIONARY (private WeakHashDictCon) "
   KO_MOVE_RECORD_BEGIN(7);
   ko_mark_count(2);
   ko_mark(ko_entries_ptr_of_dict(dest));
   ko_mark(ko_dead_ptr_of_dict(dest));
   KO_MOVE_RECORD_END(7);
";

c_define "#define KO_INITIAL_ALLOC 16";

private def EmptyWeakHashDict() {
   let entries = c_expr "ko_make_entries(ko_sint(KO_INITIAL_ALLOC))";
   let dead = DeadWeakRefs();
   c_eval [HashNew entries dead (c_con WeakHashDictCon)] "
      KO_RESULT = ko_alloc(7);
      ko_init_descr(KO_RESULT, WeakHashDictCon);
      ko_init_hash_of_dict(KO_RESULT, HashNew);
      ko_init_equal_of_dict(KO_RESULT, KO_DUMMY);
      ko_init_entries_of_dict(KO_RESULT, entries);
      ko_init_size_of_dict(KO_RESULT, ko_sint(0));
      ko_init_removed_of_dict(KO_RESULT, ko_sint(0));
      ko_init_dead_of_dict(KO_RESULT, dead);
   "
};

private def ChangedWeakHashDict src {
   let entries = c_expr "ko_make_entries(ko_sint(KO_INITIAL_ALLOC))";
   let dead = DeadWeakRefs();
   c_eval [src entries dead (c_con WeakHashDictCon)] "
      KO_RESULT = ko_alloc(7);
      ko_init_descr(KO_RESULT, WeakHashDictCon);
      ko_init_hash_of_dict(KO_RESULT, ko_hash_of_dict(src));
      ko_init_equal_of_dict(KO_RESULT, ko_equal_of_dict(src));
      ko_init_entries_of_dict(KO_RESULT, entries);
      ko_init_size_of_dict(KO_RESULT, ko_sint(0));
      ko_init_removed_of_dict(KO_RESULT, ko_sint(0));
      ko_init_dead_of_dict(KO_RESULT, dead);
   "
};

// The algorithm for resolving collisions is taken from Python.
private def LookupGet entries key hash equal {
   let mask = c_expr [entries] "ko_mask_of_entries(entries)";
   loop (hash %UnsafeAnd mask) hash ?index perturb {
      let foundHash = c_expr [entries index] "
         ko_hash_at_entries(entries, index)
      ";
      if (foundHash %UnsafeEq hash) {
         let weakEntry = c_expr [entries index] "
            ko_weak_entry_at_entries(entries, index)
         ";
         weakEntry {
            let nextIndex = c_expr [index perturb mask] "
               ko_and(ko_add1(ko_add(ko_mul_sn(index, 5), perturb)), mask)
            ";
            let nextPerturb = c_expr [perturb] "
               (ko_value_t)(((ko_unint_t)perturb >> 5) | 1)
            ";
            again nextIndex nextPerturb
         } ?entry =>
         let foundKey = c_expr [entry] "ko_key_of_entry(entry)";
         if (foundKey %equal key) {index} =>
         let nextIndex = c_expr [index perturb mask] "
            ko_and(ko_add1(ko_add(ko_mul_sn(index, 5), perturb)), mask)
         ";
         let nextPerturb = c_expr [perturb] "
            (ko_value_t)(((ko_unint_t)perturb >> 5) | 1)
         ";
         again nextIndex nextPerturb
      } =>
      if (foundHash %Is (-1)) {Null} =>
      let nextIndex = c_expr [index perturb mask] "
         ko_and(ko_add1(ko_add(ko_mul_sn(index, 5), perturb)), mask)
      ";
      let nextPerturb = c_expr [perturb] "
         (ko_value_t)(((ko_unint_t)perturb >> 5) | 1)
      ";
      again nextIndex nextPerturb
   }
};

private def LookupAdd entries key hash equal {
   let mask = c_expr [entries] "ko_mask_of_entries(entries)";
   loop (hash %UnsafeAnd mask) hash Null ?index perturb free {
      let foundHash = c_expr [entries index] "
         ko_hash_at_entries(entries, index)
      ";
      if (foundHash %UnsafeEq hash) {
         let weakEntry = c_expr [entries index] "
            ko_weak_entry_at_entries(entries, index)
         ";
         weakEntry {
            let nextIndex = c_expr [index perturb mask] "
               ko_and(ko_add1(ko_add(ko_mul_sn(index, 5), perturb)), mask)
            ";
            let nextPerturb = c_expr [perturb] "
               (ko_value_t)(((ko_unint_t)perturb >> 5) | 1)
            ";
            let nextFree = if (free %Is Null) {index} {free};
            again nextIndex nextPerturb nextFree
         } ?entry =>
         let foundKey = c_expr [entry] "ko_key_of_entry(entry)";
         if (foundKey %equal key) {index} =>
         let nextIndex = c_expr [index perturb mask] "
            ko_and(ko_add1(ko_add(ko_mul_sn(index, 5), perturb)), mask)
         ";
         let nextPerturb = c_expr [perturb] "
            (ko_value_t)(((ko_unint_t)perturb >> 5) | 1)
         ";
         again nextIndex nextPerturb free
      } =>
      if (foundHash %Is (-1)) {if (free %Is Null) {index} {free}} =>
      let nextIndex = c_expr [index perturb mask] "
         ko_and(ko_add1(ko_add(ko_mul_sn(index, 5), perturb)), mask)
      ";
      let nextPerturb = c_expr [perturb] "
         (ko_value_t)(((ko_unint_t)perturb >> 5) | 1)
      ";
      let nextFree = if (foundHash %Is (-2) & free %Is Null) {index} {free};
      again nextIndex nextPerturb nextFree
   }
};

private def LookupByEntry entries weakEntry hash {
   let mask = c_expr [entries] "ko_mask_of_entries(entries)";
   loop (hash %UnsafeAnd mask) hash ?index perturb {
      if (c_expr [entries index] "
         ko_weak_entry_at_entries(entries, index)
      " %IsSame weakEntry) {index} =>
      let foundHash = c_expr [entries index] "
         ko_hash_at_entries(entries, index)
      ";
      if (foundHash %Is (-1)) {Null} =>
      let nextIndex = c_expr [index perturb mask] "
         ko_and(ko_add1(ko_add(ko_mul_sn(index, 5), perturb)), mask)
      ";
      let nextPerturb = c_expr [perturb] "
         (ko_value_t)(((ko_unint_t)perturb >> 5) | 1)
      ";
      again nextIndex nextPerturb
   }
};

private def LookupNew entries hash {
   let mask = c_expr [entries] "ko_mask_of_entries(entries)";
   loop (hash %UnsafeAnd mask) hash ?index perturb {
      let foundHash = c_expr [entries index] "
         ko_hash_at_entries(entries, index)
      ";
      if (foundHash %Is (-1)) {index} =>
      let nextIndex = c_expr [index perturb mask] "
         ko_and(ko_add1(ko_add(ko_mul_sn(index, 5), perturb)), mask)
      ";
      let nextPerturb = c_expr [perturb] "
         (ko_value_t)(((ko_unint_t)perturb >> 5) | 1)
      ";
      again nextIndex nextPerturb
   }
};

private def RemoveDeadWeakRefs dict {
   let dead = c_expr [dict] "ko_dead_of_dict(dict)";
   EachDeadWeakRef dead ?weakEntry hash =>
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   let index = LookupByEntry entries weakEntry hash;
   if (index ~%Is Null) =>
   c_stat [entries index dict] "
      ko_set_hash_at_entries(entries, index, ko_sint(-2));
      ko_set_weak_entry_at_entries(entries, index, KO_ABSENT);
      ko_set_size_of_dict(dict, ko_sub1(ko_size_of_dict(dict)));
      ko_set_removed_of_dict(dict, ko_add1(ko_removed_of_dict(dict)));
   "
};

private def DoAddWeakHashDictInternal dict key value hash {
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   let dead = c_expr [dict] "ko_dead_of_dict(dict)";
   let weakEntry = WeakPair key (key, value) dead hash;
   let equal = c_expr [dict] "ko_equal_of_dict(dict)";
   let index = LookupAdd entries key hash equal;
   let foundHash = c_expr [entries index] "ko_hash_at_entries(entries, index)";
   if (c_cond [foundHash] "ko_lt0(foundHash)") {
      c_stat [entries index hash weakEntry] "
         ko_set_hash_at_entries(entries, index, hash);
         ko_set_weak_entry_at_entries(entries, index, weakEntry);
      ";
      let newSize = c_expr [dict] "ko_add1(ko_size_of_dict(dict))";
      c_stat [dict newSize] "ko_set_size_of_dict(dict, newSize);";
      let removed = c_expr [dict] "ko_removed_of_dict(dict)";
      let alloc = c_expr [entries] "ko_add1(ko_mask_of_entries(entries))";
      if (foundHash %Is (-1)) {
         if (c_cond [newSize removed alloc] "
            KO_UNLIKELY(ko_ge(ko_mul_sn(ko_add(newSize, removed), 2), alloc))
         ") {
            let newAlloc = c_expr [newSize] "
               ko_round_up_to_power_of_2_s(ko_max(ko_mul_sn(newSize, 4),
                  ko_sint(KO_INITIAL_ALLOC)))
            ";
            let newEntries = c_expr [newAlloc] "ko_make_entries(newAlloc)";
            loop 0 ?i {
               if (i ~%UnsafeEq alloc) =>
               let movedHash = c_expr [entries i] "
                  ko_hash_at_entries(entries, i)
               ";
               if (c_cond [movedHash] "ko_ge0(movedHash)") {
                  let newIndex = LookupNew newEntries movedHash;
                  let movedWeakEntry = c_expr [entries i] "
                     ko_weak_entry_at_entries(entries, i)
                  ";
                  c_stat [newEntries newIndex movedHash movedWeakEntry] "
                     ko_set_hash_at_entries(
                        newEntries, newIndex, movedHash);
                     ko_set_weak_entry_at_entries(
                        newEntries, newIndex, movedWeakEntry);
                  "
               };
               again (i %UnsafeAdd 1)
            };
            c_stat [dict newEntries] "
               ko_set_entries_of_dict(dict, newEntries);
               ko_set_removed_of_dict(dict, ko_sint(0));
            "
         }
      } {
         c_stat [dict removed] "
            ko_set_removed_of_dict(dict, ko_sub1(removed));
         "
      }
   } {
      c_stat [entries index hash weakEntry] "
         ko_set_hash_at_entries(entries, index, hash);
         ko_set_weak_entry_at_entries(entries, index, weakEntry);
      "
   }
};

private def DoAddWeakHashDict dict key value {
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   dict->DoAddWeakHashDictInternal key value hash
};

def WeakHashDict [
   () {EmptyWeakHashDict()}
   src! {}
   src srcs... {
      let dict = src->WeakHashDict;
      loop srcs [
         (part\parts) {dict->DoUnion part; again parts}
         _            {dict}
      ]
   }
];

method WeakHashDict src^WEAK_HASH_DICT {src->CloneWeakHashDict};

method WeakHashDict src {
   let dict = EmptyWeakHashDict();
   dict->DoUnion src;
   dict
};

method Like coll _^WEAK_HASH_DICT {
   WeakHashDict coll
};

method Iterate dict^WEAK_HASH_DICT {
   var index = 0;
   function ?absent present {
      let i = index;
      let entries = c_expr [dict] "ko_entries_of_dict(dict)";
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))")
         {absent()} =>
      index = i %UnsafeAdd 1;
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again absent present} =>
      weakEntry {again absent present} present
   }
};

method List dict^WEAK_HASH_DICT rest {
   let rest' = List rest;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   let index = c_expr [entries] "ko_add1(ko_mask_of_entries(entries))";
   loop index rest' ?i acc {
      if (i %Is 0) {acc} =>
      let i' = i %UnsafeSub 1;
      let weakEntry = c_expr [entries i:i'] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT") {again i' acc} =>
      weakEntry {again i' acc} ?entry =>
      again i' (entry\acc)
   }
};

method IsEmpty dict^WEAK_HASH_DICT {
   c_expr [dict] "ko_size_of_dict(dict)" %Is 0
};

method Size dict^WEAK_HASH_DICT {
   c_expr [dict] "ko_size_of_dict(dict)"
};

private def CloneWeakHashDict dict {
   let result = dict->ChangedWeakHashDict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))")
         {result} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      let hash = c_expr [entries i] "ko_hash_at_entries(entries, i)";
      weakEntry {again (i %UnsafeAdd 1)} ?entry =>
      let key = c_expr [entry] "ko_key_of_entry(entry)";
      let value = c_expr [entry] "ko_value_of_entry(entry)";
      result->DoAddWeakHashDictInternal key value hash;
      again (i %UnsafeAdd 1)
   }
};

method Clone dict^WEAK_HASH_DICT {dict->CloneWeakHashDict};

method Replace dict^WEAK_HASH_DICT src {
   let elems = List src;
   ClearWeakHashDict dict;
   loop elems [
      ((key, value) \ rest) {
         dict->DoAddWeakHashDict key value;
         again rest
      }
      [] {}
   ]
};

private def ClearWeakHashDict dict {
   let entries = c_expr "ko_make_entries(ko_sint(KO_INITIAL_ALLOC))";
   let dead = DeadWeakRefs();
   c_stat [dict entries dead] "
      ko_set_entries_of_dict(dict, entries);
      ko_set_size_of_dict(dict, ko_sint(0));
      ko_set_removed_of_dict(dict, ko_sint(0));
      ko_set_dead_of_dict(dict, dead);
   "
};

method Clear dict^WEAK_HASH_DICT {
   ClearWeakHashDict dict
};

method DoCutWhole dict^WEAK_HASH_DICT {
   let newDict = dict->CloneWeakHashDict;
   ClearWeakHashDict dict;
   newDict
};

method '@' dict^WEAK_HASH_DICT key {
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   let equal = c_expr [dict] "ko_equal_of_dict(dict)";
   let index = LookupGet entries key hash equal;
   if (index %Is Null) {Fail (KeyNotFound key)} =>
   let weakEntry = c_expr [entries index] "
      ko_weak_entry_at_entries(entries, index)
   ";
   weakEntry {Fail (KeyNotFound key)} ?entry =>
   c_expr [entry] "ko_value_of_entry(entry)"
};

method '@' dict^WEAK_HASH_DICT key value {
   RemoveDeadWeakRefs dict;
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   dict->DoAddWeakHashDictInternal key value hash
};

method Get dict^WEAK_HASH_DICT key absent present {
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   let equal = c_expr [dict] "ko_equal_of_dict(dict)";
   let index = LookupGet entries key hash equal;
   if (index %Is Null) {absent()} =>
   let weakEntry = c_expr [entries index] "
      ko_weak_entry_at_entries(entries, index)
   ";
   weakEntry absent ?entry =>
   c_expr [entry] "ko_value_of_entry(entry)"
   ->present
};

method GetOrSet dict^WEAK_HASH_DICT key absent {
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   let equal = c_expr [dict] "ko_equal_of_dict(dict)";
   let index = LookupGet entries key hash equal;
   if (index %Is Null) {
      RemoveDeadWeakRefs dict;
      let value' = absent();
      dict->DoAddWeakHashDict key value';
      value'
   } =>
   let weakEntry = c_expr [entries index] "
      ko_weak_entry_at_entries(entries, index)
   ";
   weakEntry {
      RemoveDeadWeakRefs dict;
      let value' = absent();
      dict->DoAddWeakHashDict key value';
      value'
   } ?entry =>
   c_expr [entry] "ko_value_of_entry(entry)"
};

method IsIn key dict^WEAK_HASH_DICT {
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   let equal = c_expr [dict] "ko_equal_of_dict(dict)";
   LookupGet entries key hash equal ~%Is Null
};

method DoAdd dict^WEAK_HASH_DICT key value {
   RemoveDeadWeakRefs dict;
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   dict->DoAddWeakHashDictInternal key value hash
};

private def DoRemoveWeakHashDictInternal dict key hash {
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   let equal = c_expr [dict] "ko_equal_of_dict(dict)";
   let index = LookupGet entries key hash equal;
   if (index ~%Is Null) =>
   c_stat [entries index dict] "
      ko_set_hash_at_entries(entries, index, ko_sint(-2));
      ko_set_weak_entry_at_entries(entries, index, KO_ABSENT);
      ko_set_size_of_dict(dict, ko_sub1(ko_size_of_dict(dict)));
      ko_set_removed_of_dict(dict, ko_add1(ko_removed_of_dict(dict)));
   "
};

private def DoRemoveWeakHashDict dict key {
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   dict->DoRemoveWeakHashDictInternal key hash
};

method DoRemove dict^WEAK_HASH_DICT key {
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   dict->DoRemoveWeakHashDictInternal key hash
};

method TryRemove dict^WEAK_HASH_DICT key absent present {
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   let equal = c_expr [dict] "ko_equal_of_dict(dict)";
   let index = LookupGet entries key hash equal;
   if (index %Is Null) {absent()} =>
   c_stat [entries index dict] "
      ko_set_hash_at_entries(entries, index, ko_sint(-2));
      ko_set_weak_entry_at_entries(entries, index, KO_ABSENT);
      ko_set_size_of_dict(dict, ko_sub1(ko_size_of_dict(dict)));
      ko_set_removed_of_dict(dict, ko_add1(ko_removed_of_dict(dict)));
   ";
   present()
};

method DoCut dict^WEAK_HASH_DICT key {
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   let equal = c_expr [dict] "ko_equal_of_dict(dict)";
   let index = LookupGet entries key hash equal;
   if (index %Is Null) {Fail (KeyNotFound key)} =>
   let weakEntry = c_expr [entries index] "
      ko_weak_entry_at_entries(entries, index)
   ";
   weakEntry {Fail (KeyNotFound key)} ?entry =>
   let value = c_expr [entry] "ko_value_of_entry(entry)";
   c_stat [entries index dict] "
      ko_set_hash_at_entries(entries, index, ko_sint(-2));
      ko_set_weak_entry_at_entries(entries, index, KO_ABSENT);
      ko_set_size_of_dict(dict, ko_sub1(ko_size_of_dict(dict)));
      ko_set_removed_of_dict(dict, ko_add1(ko_removed_of_dict(dict)));
   ";
   value
};

method TryCut dict^WEAK_HASH_DICT key absent present {
   let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   let equal = c_expr [dict] "ko_equal_of_dict(dict)";
   let index = LookupGet entries key hash equal;
   if (index %Is Null) {absent()} =>
   let weakEntry = c_expr [entries index] "
      ko_weak_entry_at_entries(entries, index)
   ";
   weakEntry absent ?entry =>
   let value = c_expr [entry] "ko_value_of_entry(entry)";
   c_stat [entries index dict] "
      ko_set_hash_at_entries(entries, index, ko_sint(-2));
      ko_set_weak_entry_at_entries(entries, index, KO_ABSENT);
      ko_set_size_of_dict(dict, ko_sub1(ko_size_of_dict(dict)));
      ko_set_removed_of_dict(dict, ko_add1(ko_removed_of_dict(dict)));
   ";
   present value
};

method First dict^WEAK_HASH_DICT {
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))")
         {Fail EmptyCollection} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      weakEntry {again (i %UnsafeAdd 1)}
   }
};

method GetFirst dict^WEAK_HASH_DICT absent present {
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))")
         {absent()} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      weakEntry {again (i %UnsafeAdd 1)} present
   }
};

method DoCutFirst dict^WEAK_HASH_DICT {
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))")
         {Fail EmptyCollection} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      weakEntry {again (i %UnsafeAdd 1)} ?entry =>
      c_stat [entries i dict] "
         ko_set_hash_at_entries(entries, i, ko_sint(-2));
         ko_set_weak_entry_at_entries(entries, i, KO_ABSENT);
         ko_set_size_of_dict(dict, ko_sub1(ko_size_of_dict(dict)));
         ko_set_removed_of_dict(dict, ko_add1(ko_removed_of_dict(dict)));
      ";
      entry
   }
};

method TryCutFirst dict^WEAK_HASH_DICT absent present {
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))")
         {absent()} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      weakEntry {again (i %UnsafeAdd 1)} ?entry =>
      c_stat [entries i dict] "
         ko_set_hash_at_entries(entries, i, ko_sint(-2));
         ko_set_weak_entry_at_entries(entries, i, KO_ABSENT);
         ko_set_size_of_dict(dict, ko_sub1(ko_size_of_dict(dict)));
         ko_set_removed_of_dict(dict, ko_add1(ko_removed_of_dict(dict)));
      ";
      present entry
   }
};

method Union dict^WEAK_HASH_DICT part^COLLECTION {
   let result = dict->CloneWeakHashDict;
   Each part ?(key, value) {result->DoAddWeakHashDict key value};
   result
};

method Difference dict^WEAK_HASH_DICT part^COLLECTION {
   let result = dict->CloneWeakHashDict;
   Each part (result->DoRemoveWeakHashDict _);
   result
};

method Intersection dict^WEAK_HASH_DICT part^COLLECTION {
   let result = dict->ChangedWeakHashDict;
   Each part ?key {
      let hash = (c_expr [dict] "ko_hash_of_dict(dict)") key dict;
      let entries = c_expr [dict] "ko_entries_of_dict(dict)";
      let equal = c_expr [dict] "ko_equal_of_dict(dict)";
      let index = LookupGet entries key hash equal;
      if (index ~%Is Null) =>
      let weakEntry = c_expr [entries index] "
         ko_weak_entry_at_entries(entries, index)
      ";
      weakEntry {} ?entry =>
      let value = c_expr [entry] "ko_value_of_entry(entry)";
      result->DoAddWeakHashDictInternal key value hash
   };
   result
};

method DoUnion dict^WEAK_HASH_DICT part^COLLECTION {
   RemoveDeadWeakRefs dict;
   Each part ?(key, value) {dict->DoAddWeakHashDict key value}
};

method DoDifference dict^WEAK_HASH_DICT part^COLLECTION {
   Each part (dict->DoRemoveWeakHashDict _)
};

method MapDict dict^WEAK_HASH_DICT fun {
   let result = dict->ChangedWeakHashDict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))")
         {result} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      let hash = c_expr [entries i] "ko_hash_at_entries(entries, i)";
      weakEntry {again (i %UnsafeAdd 1)} ?entry =>
      let key = c_expr [entry] "ko_key_of_entry(entry)";
      let value = c_expr [entry] "ko_value_of_entry(entry)";
      result->DoAddWeakHashDictInternal key (fun key value) hash;
      again (i %UnsafeAdd 1)
   }
};

method MapDictPartial dict^WEAK_HASH_DICT fun {
   let result = dict->ChangedWeakHashDict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))")
         {result} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      let hash = c_expr [entries i] "ko_hash_at_entries(entries, i)";
      weakEntry {again (i %UnsafeAdd 1)} ?entry =>
      let key = c_expr [entry] "ko_key_of_entry(entry)";
      let value = c_expr [entry] "ko_value_of_entry(entry)";
      try {fun key value}
         (%IsSame PassToken) {}
         ?value' {result->DoAddWeakHashDictInternal key value' hash};
      again (i %UnsafeAdd 1)
   }
};

method Fold dict^WEAK_HASH_DICT init fun {
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop init 0 ?acc i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))") {acc} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again acc (i %UnsafeAdd 1)} =>
      weakEntry {again acc (i %UnsafeAdd 1)} ?entry =>
      again (fun acc entry) (i %UnsafeAdd 1)
   }
};

method Each dict^WEAK_HASH_DICT fun {
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (~c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))") =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      weakEntry {again (i %UnsafeAdd 1)} ?entry =>
      fun entry;
      again (i %UnsafeAdd 1)
   }
};

method Select dict^WEAK_HASH_DICT pred {
   let result = dict->ChangedWeakHashDict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))")
         {result} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      let hash = c_expr [entries i] "ko_hash_at_entries(entries, i)";
      weakEntry {again (i %UnsafeAdd 1)} ?entry =>
      if (pred entry) {
         let key = c_expr [entry] "ko_key_of_entry(entry)";
         let value = c_expr [entry] "ko_value_of_entry(entry)";
         result->DoAddWeakHashDictInternal key value hash
      };
      again (i %UnsafeAdd 1)
   }
};

method Reject dict^WEAK_HASH_DICT pred {
   let result = dict->ChangedWeakHashDict;
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))")
         {result} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      let hash = c_expr [entries i] "ko_hash_at_entries(entries, i)";
      weakEntry {again (i %UnsafeAdd 1)} ?entry =>
      if (~pred entry) {
         let key = c_expr [entry] "ko_key_of_entry(entry)";
         let value = c_expr [entry] "ko_value_of_entry(entry)";
         result->DoAddWeakHashDictInternal key value hash
      };
      again (i %UnsafeAdd 1)
   }
};

method DoReject dict^WEAK_HASH_DICT pred {
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (~c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))") =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      weakEntry {again (i %UnsafeAdd 1)} ?entry =>
      if (pred entry) {
         c_stat [entries i dict] "
            ko_set_hash_at_entries(entries, i, ko_sint(-2));
            ko_set_weak_entry_at_entries(entries, i, KO_ABSENT);
            ko_set_size_of_dict(dict, ko_sub1(ko_size_of_dict(dict)));
            ko_set_removed_of_dict(dict, ko_add1(ko_removed_of_dict(dict)));
         "
      };
      again (i %UnsafeAdd 1)
   }
};

method Every dict^WEAK_HASH_DICT pred {
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))") {True} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      weakEntry {again (i %UnsafeAdd 1)} ?entry =>
      pred entry & again (i %UnsafeAdd 1)
   }
};

method Any dict^WEAK_HASH_DICT pred {
   let entries = c_expr [dict] "ko_entries_of_dict(dict)";
   loop 0 ?i {
      if (c_cond [i entries] "ko_gt(i, ko_mask_of_entries(entries))")
         {False} =>
      let weakEntry = c_expr [entries i] "
         ko_weak_entry_at_entries(entries, i)
      ";
      if (c_cond [weakEntry] "weakEntry == KO_ABSENT")
         {again (i %UnsafeAdd 1)} =>
      weakEntry {again (i %UnsafeAdd 1)} ?entry =>
      pred entry | again (i %UnsafeAdd 1)
   }
};
