// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Data.Wrappers;

use Kokogut.Records;
use Kokogut.Lists;
use Kokogut.Collections.Core;
use Kokogut.Collections.Whole;
use Kokogut.Collections.Single;
use Kokogut.Collections.Ends;
use Kokogut.Collections.Set;
use Kokogut.Collections.Map;
use Kokogut.Collections.Join;
use Kokogut.Collections.Fold;
use Kokogut.Collections.Test;
use Kokogut.Data.LazyLists;

method Iterate set^KEYS {
   let iter = Iterate set.dictionary;
   ?absent present {
      iter absent ?(key, _) {present key}
   }
};

method List set^KEYS rest {
   let iter = Iterate set.dictionary;
   let absent = {List rest};
   loop {iter absent ?(key, _) {key \ again()}}
};

method IsEmpty set^KEYS {IsEmpty set.dictionary};

method Size set^KEYS {Size set.dictionary};

method SizeIs set^KEYS size {SizeIs set.dictionary size};

method SizeAtMost set^KEYS maxSize {SizeAtMost set.dictionary maxSize};

method SizeAtLeast set^KEYS minSize {SizeAtLeast set.dictionary minSize};

method Difference set^KEYS part {Keys (Difference set.dictionary part)};

method Intersection set^KEYS part {Keys (Intersection set.dictionary part)};

method Clone set^KEYS {Keys (Clone set.dictionary)};

method Clear set^KEYS {Clear set.dictionary};

method DoCutWhole set^KEYS {Keys (DoCutWhole set.dictionary)};

method Difference set^KEYS part^COLLECTION {
   Keys (Difference set.dictionary part)
};

method Intersection set^KEYS part^COLLECTION {
   Keys (Intersection set.dictionary part)
};

method DoDifference set^KEYS part {DoDifference set.dictionary part};

method IsIn elem set^KEYS {elem->IsIn set.dictionary};

method Remove set^KEYS elem {Keys (Remove set.dictionary elem)};

method DoRemove set^KEYS elem {DoRemove set.dictionary elem};

method TryRemove set^KEYS elem absent present {
   TryRemove set.dictionary elem absent present
};

method First set^KEYS {let (key, _) = First set.dictionary; key};

method GetFirst set^KEYS absent present {
   GetFirst set.dictionary absent ?(key, _) {present key}
};

method DoCutFirst set^KEYS {let (key, _) = DoCutFirst set.dictionary; key};

method TryCutFirst set^KEYS absent present {
   TryCutFirst set.dictionary absent ?(key, _) {present key}
};

method MapGen set^KEYS fun {
   MapGen set.dictionary ?(key, _) {fun key}
};

method MapList set^KEYS fun {
   MapList set.dictionary ?(key, _) {fun key}
};

method MapPartialGen set^KEYS fun {
   MapPartialGen set.dictionary ?(key, _) {fun key}
};

method MapPartialList set^KEYS fun {
   MapPartialList set.dictionary ?(key, _) {fun key}
};

method MapJoinGen set^KEYS fun {
   MapJoinGen set.dictionary ?(key, _) {fun key}
};

method MapJoinList set^KEYS fun {
   MapJoinList set.dictionary ?(key, _) {fun key}
};

method Fold set^KEYS init fun {
   Fold set.dictionary init ?acc (key, _) {fun acc key}
};

method Fold1 set^KEYS initFun fun {
   Fold1 set.dictionary ?(key, _) {initFun key} ?acc (key, _) {fun acc key}
};

method Each set^KEYS fun {Each set.dictionary ?(key, _) {fun key}};

method SelectGen set^KEYS pred {
   SelectGen set.dictionary ?(key, _) {pred key}
};

method RejectGen set^KEYS pred {
   RejectGen set.dictionary ?(key, _) {pred key}
};

method SelectList set^KEYS pred {
   SelectList set.dictionary ?(key, _) {pred key}
};

method RejectList set^KEYS pred {
   RejectList set.dictionary ?(key, _) {pred key}
};

method DoReject set^KEYS pred {
   DoReject set.dictionary ?(key, _) {pred key}
};

method Every set^KEYS pred {Every set.dictionary ?(key, _) {pred key}};

method Any set^KEYS pred {Any set.dictionary ?(key, _) {pred key}};

// Common code for dictionary wrappers

type DICTIONARY_WRAPPER is:DICTIONARY;

method Like coll dict^DICTIONARY_WRAPPER {
   dict->Change dictionary:(coll->Like dict.dictionary)
};

method Iterate dict^DICTIONARY_WRAPPER {
   Iterate dict.dictionary
};

method PureIterator dict^DICTIONARY_WRAPPER {
   PureIterator dict.dictionary
};

method List dict^DICTIONARY_WRAPPER rest {
   List dict.dictionary rest
};

method LazyList dict^DICTIONARY_WRAPPER {
   LazyList dict.dictionary
};

method LazyList dict^DICTIONARY_WRAPPER rest {
   LazyList dict.dictionary rest
};

method IsEmpty dict^DICTIONARY_WRAPPER {
   IsEmpty dict.dictionary
};

method Size dict^DICTIONARY_WRAPPER {
   Size dict.dictionary
};

method SizeIs dict^DICTIONARY_WRAPPER size {
   SizeIs dict.dictionary size
};

method SizeAtMost dict^DICTIONARY_WRAPPER maxSize {
   SizeAtMost dict.dictionary maxSize
};

method SizeAtLeast dict^DICTIONARY_WRAPPER minSize {
   SizeAtLeast dict.dictionary minSize
};

method Clone dict^DICTIONARY_WRAPPER {
   dict->Change dictionary:(Clone dict.dictionary)
};

method Replace dict^DICTIONARY_WRAPPER other {
   Replace dict.dictionary other
};

method Clear dict^DICTIONARY_WRAPPER {
   Clear dict.dictionary
};

method DoCutWhole dict^DICTIONARY_WRAPPER {
   dict->Change dictionary:(DoCutWhole dict.dictionary)
};

method '@' dict^DICTIONARY_WRAPPER key {
   dict.dictionary@key
};

method '@' dict^DICTIONARY_WRAPPER key value {
   dict.dictionary@key = value
};

method Get dict^DICTIONARY_WRAPPER key absent present {
   Get dict.dictionary key absent present
};

method GetOrSet dict^DICTIONARY_WRAPPER key absent {
   GetOrSet dict.dictionary key absent
};

method IsIn key dict^DICTIONARY_WRAPPER {
   key->IsIn dict.dictionary
};

method Add dict^DICTIONARY_WRAPPER key value {
   dict->Change dictionary:(Add dict.dictionary key value)
};

method Remove dict^DICTIONARY_WRAPPER key {
   dict->Change dictionary:(Remove dict.dictionary key)
};

method DoAdd dict^DICTIONARY_WRAPPER key value {
   DoAdd dict.dictionary key value
};

method DoRemove dict^DICTIONARY_WRAPPER key {
   DoRemove dict.dictionary key
};

method DoCut dict^DICTIONARY_WRAPPER key {
   DoCut dict.dictionary key
};

method First dict^DICTIONARY_WRAPPER {
   First dict.dictionary
};

method GetFirst dict^DICTIONARY_WRAPPER absent present {
   GetFirst dict.dictionary absent present
};

method DoCutFirst dict^DICTIONARY_WRAPPER {
   DoCutFirst dict.dictionary
};

method TryCutFirst dict^DICTIONARY_WRAPPER absent present {
   TryCutFirst dict.dictionary absent present
};

method Union dict^DICTIONARY_WRAPPER part {
   dict->Change dictionary:(Union dict.dictionary part)
};

method Difference dict^DICTIONARY_WRAPPER part {
   dict->Change dictionary:(Difference dict.dictionary part)
};

method Intersection dict^DICTIONARY_WRAPPER part {
   dict->Change dictionary:(Intersection dict.dictionary part)
};

method DoUnion dict^DICTIONARY_WRAPPER part {
   DoUnion dict.dictionary part
};

method DoDifference dict^DICTIONARY_WRAPPER part {
   DoDifference dict.dictionary part
};

method MapGen dict^DICTIONARY_WRAPPER fun {
   MapGen dict.dictionary fun
};

method MapList dict^DICTIONARY_WRAPPER fun {
   MapList dict.dictionary fun
};

method Map dict^DICTIONARY_WRAPPER fun {
   dict->Change dictionary:(Map dict.dictionary fun)
};

method MapDict dict^DICTIONARY_WRAPPER fun {
   dict->Change dictionary:(MapDict dict.dictionary fun)
};

method MapPartialGen dict^DICTIONARY_WRAPPER fun {
   MapPartialGen dict.dictionary fun
};

method MapPartialList dict^DICTIONARY_WRAPPER fun {
   MapPartialList dict.dictionary fun
};

method MapPartial dict^DICTIONARY_WRAPPER fun {
   dict->Change dictionary:(MapPartial dict.dictionary fun)
};

method MapDictPartial dict^DICTIONARY_WRAPPER fun {
   dict->Change dictionary:(MapDictPartial dict.dictionary fun)
};

method MapJoinGen dict^DICTIONARY_WRAPPER fun {
   MapJoinGen dict.dictionary fun
};

method MapJoinList dict^DICTIONARY_WRAPPER fun {
   MapJoinList dict.dictionary fun
};

method MapJoin dict^DICTIONARY_WRAPPER fun {
   dict->Change dictionary:(MapJoin dict.dictionary fun)
};

method Fold dict^DICTIONARY_WRAPPER init fun {
   Fold dict.dictionary init fun
};

method Fold1 dict^DICTIONARY_WRAPPER initFun fun {
   Fold1 dict.dictionary initFun fun
};

method Each dict^DICTIONARY_WRAPPER fun {
   Each dict.dictionary fun
};

method SelectGen dict^DICTIONARY_WRAPPER pred {
   SelectGen dict.dictionary pred
};

method RejectGen dict^DICTIONARY_WRAPPER pred {
   RejectGen dict.dictionary pred
};

method SelectList dict^DICTIONARY_WRAPPER pred {
   SelectList dict.dictionary pred
};

method RejectList dict^DICTIONARY_WRAPPER pred {
   RejectList dict.dictionary pred
};

method Select dict^DICTIONARY_WRAPPER pred {
   dict->Change dictionary:(Select dict.dictionary pred)
};

method Reject dict^DICTIONARY_WRAPPER pred {
   dict->Change dictionary:(Reject dict.dictionary pred)
};

method DoReject dict^DICTIONARY_WRAPPER pred {
   DoReject dict.dictionary pred
};

method Every dict^DICTIONARY_WRAPPER pred {
   Every dict.dictionary pred
};

method Any dict^DICTIONARY_WRAPPER pred {
   Any dict.dictionary pred
};

// Returning a default value for absent keys

type WITH_DEFAULT is:DICTIONARY_WRAPPER WithDefault dictionary default;

method '@' dict^WITH_DEFAULT key {
   Get dict.dictionary key {dict.default}
};

method DoCut dict^WITH_DEFAULT key {
   TryCut dict.dictionary key {dict.default}
};

method First dict^WITH_DEFAULT {
   GetFirst dict.dictionary {dict.default}
};

method DoCutFirst dict^WITH_DEFAULT {
   TryCutFirst dict.dictionary {dict.default}
};

// Perform an action for absent keys

type WHEN_ABSENT is:DICTIONARY_WRAPPER WhenAbsent dictionary absent;

method '@' dict^WHEN_ABSENT key {
   Get dict.dictionary key {dict.absent key}
};

method DoCut dict^WHEN_ABSENT key {
   TryCut dict.dictionary key {dict.absent key}
};

// Set a new value for absent keys

type SET_DEFAULT is:DICTIONARY_WRAPPER SetDefault dictionary absent;

method '@' dict^SET_DEFAULT key {
   GetOrSet dict.dictionary key dict.absent
};

method DoCut dict^SET_DEFAULT key {
   let dict' = dict.dictionary;
   TryCut dict' key {let value = dict.absent(); dict'@key = value; value}
};
