// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Kokogut.Data.LazyLists =>

use Kokogut.Core;
use Kokogut.Unsafe;
use Kokogut.ExceptionTypes;
use Kokogut.Exceptions;
use Kokogut.Numbers.Ints;
use Kokogut.Lists;
use Kokogut.Threads.Signals;
use Kokogut.Lazy [];
use Kokogut.Collections.Core;
use Kokogut.Collections.Whole;
use Kokogut.Collections.Ends;
use Kokogut.Collections.Part;
use Kokogut.Collections.Map;
use Kokogut.Collections.Join;
use Kokogut.Collections.Test;
use Kokogut.Collections.Find;

type LAZY_COMMIT is:SPECIAL_RESULT LazyCommit prepare complete signals;

c_define "
   KO_RECORD_FIELD(lazy_commit, 0, prepare);
   KO_RECORD_FIELD(lazy_commit, 1, complete);
   KO_RECORD_FIELD(lazy_commit, 2, signals);
";

def CommitLazySync [
   prepare complete {Fail (LazyCommit prepare complete UnblockSyncSignals)}
   prepare          {again prepare Identity}
];

def CommitLazyAsync [
   prepare complete {Fail (LazyCommit prepare complete UnblockSignals)}
   prepare          {again prepare Identity}
];

/*
// This portable implementation is 4 times slower than the one below:

private def ComputeValue compute
   mutex (ref computed) computedCond (ref state)
{
   BlockSignals =>
   try {
      Unlock mutex compute
   } (commit->HasType LAZY_COMMIT) handled:{
      PrepareLazy commit mutex (ref computed) computedCond (ref state)
   } exn {
      state = {Fail exn};
      computed = True;
      Notify computedCond;
      Fail exn
   } ?value {
      state = {value};
      computed = True;
      Notify computedCond;
      value
   }
};

private def PrepareLazy commit
   mutex (ref computed) computedCond (ref state)
{
   try {
      Unlock mutex {commit.signals commit.prepare}
   } exn {
      state = {
         PrepareLazy commit
            mutex (ref computed) computedCond (ref state)
      };
      computed = True;
      Notify computedCond;
      Fail exn
   } ?prepared {
      ComputeValue {commit.complete prepared}
         mutex (ref computed) computedCond (ref state)
   }
};

type LAZY_LIST is:SEQUENCE (private LazyCon) [
   (private compute) {
      private let mutex = Mutex();
      private var computed = False;
      private let computedCond = Condition mutex (ref computed);
      private var state = {
         state = {Wait computedCond; state()};
         ComputeValue compute mutex (ref computed) computedCond (ref state)
      };
      extend {Lock mutex => state()};
   }
   (private prepare) (private complete) (private signals) {
      private let mutex = Mutex();
      private var computed = False;
      private let computedCond = Condition mutex (ref computed);
      private var state = {
         state = {Wait computedCond; state()};
         PrepareLazy (LazyCommit prepare complete signals)
            mutex (ref computed) computedCond (ref state)
      };
      extend {Lock mutex => state()};
   }
];

def Lazy compute {LazyCon compute};

def LazySync [
   prepare          {again prepare Identity}
   prepare complete {LazyCon prepare complete UnblockSyncSignals}
];

def LazyAsync [
   prepare          {again prepare Identity}
   prepare complete {LazyCon prepare complete UnblockSignals}
];
*/

c_define "
   /* A lazy list has the same reprezentation as a lazy variable,
    * except that 'dynamic' is not used, and there is one more state,
    * LazyListPrepareCon, with three fields: */
   KO_RECORD_FIELD(lazy, 0, prepare);
   KO_RECORD_FIELD(lazy, 1, complete);
   KO_RECORD_FIELD(lazy, 2, signals);
";

private def PrepareLazy this prepare complete signals {
   try {
      signals prepare
   } exn {
      c_stat [
         this (c_entry PrepareLazy 4) prepare complete signals
         (c_con LazyListPrepareCon)
      ] [unblocked] "
         unblocked = ko_first_of_lazy(this);
         if (KO_UNLIKELY(unblocked != KO_ABSENT)) {
            ko_set_blocked_all_of_thread(unblocked,
               ko_add1(ko_blocked_all_of_thread(unblocked)));
            ko_unblock_first_thread(unblocked);
            ko_set_code_of_thread(unblocked, PrepareLazy4);
            ko_registers_of_thread(unblocked)[1] = this;
            ko_registers_of_thread(unblocked)[2] = prepare;
            ko_registers_of_thread(unblocked)[3] = complete;
            ko_registers_of_thread(unblocked)[4] = signals;
            ko_mark_changed_object(unblocked);
            KO_UNLINK_FIRST_FROM_LAZY(this, unblocked);
            ko_set_thread_of_lazy(this, unblocked);
         }
         else {
            ko_init_descr(this, LazyListPrepareCon);
            ko_set_prepare_of_lazy(this, prepare);
            ko_set_complete_of_lazy(this, complete);
            ko_set_signals_of_lazy(this, signals);
         }
      ";
      Fail exn
   } ?prepared =>
   try {complete prepared} (commit->if (c_cond [commit (c_con LazyCommit)] "
      ko_descr_is(commit, LazyCommit)
   ")) handled:{
      PrepareLazy this
         (c_expr [commit] "ko_prepare_of_lazy_commit(commit)")
         (c_expr [commit] "ko_complete_of_lazy_commit(commit)")
         (c_expr [commit] "ko_signals_of_lazy_commit(commit)")
   } exn {
      c_stat [this (c_con LazyListExceptionCon) exn] "
         ko_value_t thread = ko_first_of_lazy(this);
         if (KO_UNLIKELY(thread != KO_ABSENT)) {
            do {
               ko_value_t next = ko_next_related_of_thread(thread);
               ko_unblock_thread(thread);
               ko_set_code_of_thread(thread, ko_fail);
               ko_registers_of_thread(thread)[1] = exn;
               ko_mark_changed_object(thread);
               thread = next;
            } while (thread != KO_ABSENT);
            ko_init_first_of_lazy(this, KO_ABSENT);
            ko_init_last_of_lazy(this, KO_ABSENT);
         }
         ko_init_descr(this, LazyListExceptionCon);
         ko_set_exception_of_lazy(this, exn);
      ";
      Fail exn
   } ?value {
      c_stat [this (c_con LazyListValueCon) value] "
         ko_value_t thread = ko_first_of_lazy(this);
         if (KO_UNLIKELY(thread != KO_ABSENT)) {
            do {
               ko_value_t next = ko_next_related_of_thread(thread);
               ko_unblock_thread(thread);
               ko_registers_of_thread(thread)[1] = value;
               ko_mark_changed_object(thread);
               thread = next;
            } while (thread != KO_ABSENT);
            ko_init_first_of_lazy(this, KO_ABSENT);
            ko_init_last_of_lazy(this, KO_ABSENT);
         }
         ko_init_descr(this, LazyListValueCon);
         ko_set_value_of_lazy(this, value);
      ";
      if (c_cond "
         KO_UNLIKELY(ko_signal_pending()) &&
         ko_blocked_all == ko_sint(1) && ko_blocked_async == ko_sint(0)
      ") {HandleSignals()};
      c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
      value
   }
};

c_type LAZY_LIST is:SEQUENCE {

   c_con (private LazyListComputeCon) "
      KO_MOVE_RECORD_BEGIN(2);
      ko_mark(ko_compute_ptr_of_lazy(dest));
      KO_MOVE_RECORD_END(4); /* 4 for switching the constructor */
   " [{
      c_case [compute] [
         this (c_con LazyListComputeCon) (c_con LazyListComputingCon)
      ] "
         if (KO_UNLIKELY(ko_descr_of_ptr(this) != LazyListComputeCon))
            goto retry;
         ko_blocked_all = ko_add1(ko_blocked_all);
         compute = ko_compute_of_lazy(this);
         ko_init_descr(this, LazyListComputingCon);
         ko_set_thread_of_lazy(this, ko_current_thread);
         ko_init_first_of_lazy(this, KO_ABSENT);
         ko_init_last_of_lazy(this, KO_ABSENT);
         goto computing;
      " [
         retry: {this()}
         computing: {
            try compute (commit->if (c_cond [commit (c_con LazyCommit)] "
               ko_descr_is(commit, LazyCommit)
            ")) handled:{
               PrepareLazy this
                  (c_expr [commit] "ko_prepare_of_lazy_commit(commit)")
                  (c_expr [commit] "ko_complete_of_lazy_commit(commit)")
                  (c_expr [commit] "ko_signals_of_lazy_commit(commit)")
            } exn {
               c_stat [this (c_con LazyListExceptionCon) exn] "
                  ko_value_t thread = ko_first_of_lazy(this);
                  if (KO_UNLIKELY(thread != KO_ABSENT)) {
                     do {
                        ko_value_t next = ko_next_related_of_thread(thread);
                        ko_unblock_thread(thread);
                        ko_set_code_of_thread(thread, ko_fail);
                        ko_registers_of_thread(thread)[1] = exn;
                        ko_mark_changed_object(thread);
                        thread = next;
                     } while (thread != KO_ABSENT);
                     ko_init_first_of_lazy(this, KO_ABSENT);
                     ko_init_last_of_lazy(this, KO_ABSENT);
                  }
                  ko_init_descr(this, LazyListExceptionCon);
                  ko_set_exception_of_lazy(this, exn);
               ";
               Fail exn
            } ?value {
               c_stat [this (c_con LazyListValueCon) value] "
                  ko_value_t thread = ko_first_of_lazy(this);
                  if (KO_UNLIKELY(thread != KO_ABSENT)) {
                     do {
                        ko_value_t next = ko_next_related_of_thread(thread);
                        ko_unblock_thread(thread);
                        ko_registers_of_thread(thread)[1] = value;
                        ko_mark_changed_object(thread);
                        thread = next;
                     } while (thread != KO_ABSENT);
                     ko_init_first_of_lazy(this, KO_ABSENT);
                     ko_init_last_of_lazy(this, KO_ABSENT);
                  }
                  ko_init_descr(this, LazyListValueCon);
                  ko_set_value_of_lazy(this, value);
               ";
               if (c_cond "
                  KO_UNLIKELY(ko_signal_pending()) &&
                  ko_blocked_all == ko_sint(1) &&
                  ko_blocked_async == ko_sint(0)
               ") {HandleSignals()};
               c_stat "ko_blocked_all = ko_sub1(ko_blocked_all);";
               value
            }
         }
      ]
   }];

   c_con (private LazyListPrepareCon) "
      KO_MOVE_RECORD_BEGIN(4);
      ko_mark_count(3);
      ko_mark(ko_prepare_ptr_of_lazy(dest));
      ko_mark(ko_complete_ptr_of_lazy(dest));
      ko_mark(ko_signals_ptr_of_lazy(dest));
      KO_MOVE_RECORD_END(4);
   " [{
      c_case [prepare complete signals] [
         this (c_con LazyListPrepareCon) (c_con LazyListComputingCon)
      ] "
         if (KO_UNLIKELY(ko_descr_of_ptr(this) != LazyListPrepareCon))
            goto retry;
         ko_blocked_all = ko_add1(ko_blocked_all);
         prepare = ko_prepare_of_lazy(this);
         complete = ko_complete_of_lazy(this);
         signals = ko_signals_of_lazy(this);
         ko_init_descr(this, LazyListComputingCon);
         ko_set_thread_of_lazy(this, ko_current_thread);
         ko_init_first_of_lazy(this, KO_ABSENT);
         ko_init_last_of_lazy(this, KO_ABSENT);
         goto computing;
      " [
         retry:     {this()}
         computing: {PrepareLazy this prepare complete signals}
      ]
   }];

   c_con (private LazyListComputingCon) "
      KO_MOVE_RECORD_BEGIN(4);
      ko_mark_count(3);
      ko_mark(ko_thread_ptr_of_lazy(dest));
      ko_mark(ko_first_ptr_of_lazy(dest));
      ko_mark(ko_last_ptr_of_lazy(dest));
      KO_MOVE_RECORD_END(4);
   " [{
      c_case [] [
         this LockAlreadyLocked (c_entry SignalWhileWaitingForLazyList 1)
      ] "
         if (KO_UNLIKELY(ko_thread_of_lazy(this) == ko_current_thread))
            KO_FAIL(LockAlreadyLocked);
         /* A difference between LAZY and LAZY_LIST is that in
          * synchronous mode signals are processed while waiting for
          * a lazy list, but not while waiting for a lazy variable. */
         KO_PROCESS_SYNC_SIGNAL(this, ;, SignalWhileWaitingForLazyList1);
         ko_set_state_of_thread(ko_current_thread,
            &ko_thread_waiting_for_lazy_list);
         ko_set_value_of_thread(ko_current_thread, this);
         KO_LINK_TO_LAZY(this);
         KO_CONTEXT_SWITCH();
      " []
   }];

   c_con (private LazyListValueCon) "
      KO_MOVE_RECORD_BEGIN(2);
      ko_mark(ko_value_ptr_of_lazy(dest));
      KO_MOVE_RECORD_END(2);
   " [{c_expr [this] "ko_value_of_lazy(this)"}];

   c_con (private LazyListExceptionCon) "
      KO_MOVE_RECORD_BEGIN(2);
      ko_mark(ko_exception_ptr_of_lazy(dest));
      KO_MOVE_RECORD_END(2);
   " [{Fail (c_expr [this] "ko_exception_of_lazy(this)")}];
};

private def SignalWhileWaitingForLazyList state {
   HandleSignalsOrFirstSync state;
   c_case [] [state] "
      ko_value_t list = ko_value_of_signal_state(state);
      KO_SIGNALS_HANDLED(state, KO_R[0] = list;,
         ko_descr(KO_R[0])->ko_fast_entries[0]);
   " []
};

c_define [(c_entry SignalWhileWaitingForLazyList 1)] "
   static void
   ko_kick_waiting_for_lazy_list(ko_value_t thread, ko_value_t signalState) {
      ko_value_t llist;
      (void)signalState;
      llist = ko_value_of_thread(thread);
      KO_UNLINK_FROM_LAZY(llist, thread);
      ko_set_restart(thread, SignalWhileWaitingForLazyList1);
      ko_link_thread(thread);
   }

   static struct ko_thread_state ko_thread_waiting_for_lazy_list = {
      ko_send_signal_by_queue,
      ko_handles_signals_wait,
      ko_kick_waiting_for_lazy_list,
      ko_unlink_waiting_for_lazy,
      ko_true /* ko_signal_deadlock */
   };
";

def Lazy compute {
   c_eval [(c_con LazyListComputeCon) compute] "
      KO_RESULT = ko_alloc(4);
      ko_init_descr(KO_RESULT, LazyListComputeCon);
      ko_init_compute_of_lazy(KO_RESULT, compute);
   "
};

def LazySync [
   prepare {again prepare Identity}
   prepare complete {
      c_eval [(c_con LazyListPrepareCon) prepare complete UnblockSyncSignals] "
         KO_RESULT = ko_alloc(4);
         ko_init_descr(KO_RESULT, LazyListPrepareCon);
         ko_init_prepare_of_lazy(KO_RESULT, prepare);
         ko_init_complete_of_lazy(KO_RESULT, complete);
         ko_init_signals_of_lazy(KO_RESULT, UnblockSyncSignals);
      "
   }
];

def LazyAsync [
   prepare {again prepare Identity}
   prepare complete {
      c_eval [(c_con LazyListPrepareCon) prepare complete UnblockSignals] "
         KO_RESULT = ko_alloc(4);
         ko_init_descr(KO_RESULT, LazyListPrepareCon);
         ko_init_prepare_of_lazy(KO_RESULT, prepare);
         ko_init_complete_of_lazy(KO_RESULT, complete);
         ko_init_signals_of_lazy(KO_RESULT, UnblockSignals);
      "
   }
];

let LazyNil = Lazy {};

def LazyList [
   ()           {LazyNil}
   coll!        {}
   coll! rest   {}
   coll rest... {LazyList coll (LazyList rest...)}
];

method LazyList list^CONS {
   Lazy => UnsafeFirst list,
   loop (UnsafeRest list) [
      (elem\rest) {Lazy {elem, again rest}}
      _           {LazyNil}
   ]
};
method LazyList _^NIL {LazyNil};

method LazyList list^CONS rest {
   Lazy => UnsafeFirst list,
   loop (UnsafeRest list) [
      (elem\rest) {Lazy {elem, again rest}}
      _           {LazyList rest}
   ]
};
method LazyList _^NIL rest {LazyList rest};

method LazyList llist^LAZY_LIST {llist};
method LazyList llist^LAZY_LIST rest {
   LazyAsync llist (function [
      (elem, rest) {elem, LazyAsync rest again}
      _            {CommitLazyAsync (LazyList rest)}
   ])
};

method LazyList coll^COLLECTION {
   let iter = Iterate coll;
   Lazy (function {iter {} ?elem {elem, Lazy again}})
};
method LazyList coll^COLLECTION rest {
   let iter = Iterate coll;
   let absent = {CommitLazyAsync (LazyList rest)};
   Lazy (function {iter absent ?elem {elem, Lazy again}})
};

method Like coll _^LAZY_LIST {LazyList coll};

method Is llist1^LAZY_LIST llist2 {
   case (llist1()) [
      (elem1, rest1) {
         case (llist2()) [
            (elem2, rest2) {
               elem1 %Is elem2 & again rest1 rest2
            }
            _ {False}
         ]
      }
      _ {
         case (llist2()) [
            (_, _) {False}
            _      {True}
         ]
      }
   ]
};

method Hash llist^LAZY_LIST {
   case (llist()) [
      (first, rest) {
         loop (Hash first) rest ?acc iter {
            case (iter()) [
               (elem, rest) {again (acc->CombineHash (Hash elem)) rest}
               _            {acc}
            ]
         }
      }
      _ {0}
   ]
};

method '==' llist1^LAZY_LIST llist2^LAZY_LIST {
   case (llist1()) [
      (elem1, rest1) {
         case (llist2()) [
            (elem2, rest2) {
               elem1 == elem2 & again rest1 rest2
            }
            _ {False}
         ]
      }
      _ {
         case (llist2()) [
            (_, _) {False}
            _      {True}
         ]
      }
   ]
};

method '<' llist1^LAZY_LIST llist2^LAZY_LIST {
   case (llist2()) [
      (elem2, rest2) {
         case (llist1()) [
            (elem1, rest1) {
               case (elem1 %Compare elem2) [
                  #'<' {True}
                  #'=' {again rest1 rest2}
                  #'>' {False}
               ]
            }
            _ {True}
         ]
      }
      _ {False}
   ]
};

method '<=' llist1^LAZY_LIST llist2^LAZY_LIST {
   case (llist1()) [
      (elem1, rest1) {
         case (llist2()) [
            (elem2, rest2) {
               case (elem1 %Compare elem2) [
                  #'<' {True}
                  #'=' {again rest1 rest2}
                  #'>' {False}
               ]
            }
            _ {False}
         ]
      }
      _ {True}
   ]
};

method Compare llist1^LAZY_LIST llist2^LAZY_LIST {
   case (llist1()) [
      (elem1, rest1) {
         case (llist2()) [
            (elem2, rest2) {
               case (elem1 %Compare elem2) [
                  #'=' {again rest1 rest2}
                  cmp  {cmp}
               ]
            }
            _ {#'>'}
         ]
      }
      _ {
         case (llist2()) [
            (_, _) {#'<'}
            _      {#'='}
         ]
      }
   ]
};

method Iterate (var llist)^LAZY_LIST {
   ?absent present {
      case (llist()) [
         (elem, rest) {llist = rest; present elem}
         _            {absent()}
      ]
   }
};

method PureIterator gen^GENERATOR {
   let iter = gen();
   Lazy (function {iter {} ?elem {elem, Lazy again}})
};

method PureIterator llist^LAZY_LIST {llist};

method PureIterator coll^COLLECTION {LazyList coll};

method Next llist^LAZY_LIST {llist()};

method IsEmpty llist^LAZY_LIST {
   llist() %Is Null
};

method First llist^LAZY_LIST {
   case (llist()) [
      (x, _) {x}
      _      {Fail EmptyCollection}
   ]
};

method GetFirst llist^LAZY_LIST absent present {
   case (llist()) [
      (x, _) {present x}
      _      {absent()}
   ]
};

method RemoveFirst llist^LAZY_LIST {
   case (llist()) [
      (_, xs) {xs}
      _       {llist}
   ]
};

// TODO: Part, RemovePart

method AddPart llist^LAZY_LIST part {
   LazyAsync llist (function [
      (x, xs) {x, LazyAsync xs again}
      _       {CommitLazyAsync (LazyList part)}
   ])
};

method Front llist^LAZY_LIST pred {
   LazyAsync llist (function [
      (elem->pred, rest) {elem, LazyAsync rest again}
      _                  {Null}
   ])
};

method RemoveFront llist^LAZY_LIST pred {
   loop llist [
      (->where Apply = (->pred, rest)) {again rest}
      iter                             {iter}
   ]
};

method CutFront llist^LAZY_LIST pred {
   var result;
   loop llist [
      (->where Apply = (elem->pred, rest)) {elem \ again rest}
      iter                                 {result = iter; []}
   ]->LazyList, result
};

method Map llist^LAZY_LIST fun {
   LazyAsync llist (function [
      (elem, rest) {fun elem, LazyAsync rest again}
      _            {}
   ])
};

method MapJoin llist^LAZY_LIST fun {
   LazyAsync llist (function [
      (coll, colls) {
         CommitLazyAsync (LazyList (fun coll) (LazyAsync colls again))
      }
      _ {}
   ])
};

method Select llist^LAZY_LIST pred {
   LazyAsync llist (function [
      (elem, rest) {
         if (pred elem)
            {elem, LazyAsync rest again}
            {CommitLazyAsync rest again}
      }
      _ {}
   ])
};

method Reject llist^LAZY_LIST pred {
   LazyAsync llist (function [
      (elem, rest) {
         if (pred elem)
            {CommitLazyAsync rest again}
            {elem, LazyAsync rest again}
      }
      _ {}
   ])
};
