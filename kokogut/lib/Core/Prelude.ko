// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2009 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Prelude =>

reexport Kokogut.Objects [];
reexport Kokogut.Core except:[LT EQ GT];
reexport Kokogut.ExceptionTypes;
reexport Kokogut.Exceptions [Fail EnsureAll];
reexport Kokogut.Numbers.Ints except:[SIntCon BIntCon FailArithmeticError];
reexport Kokogut.Numbers.BitOps;
reexport Kokogut.Numbers.Ratios except:[RatioCon];
reexport Kokogut.Numbers.Infinity;
reexport Kokogut.Numbers.Floats except:[FloatCon];
reexport Kokogut.Strings [STRING String Char CharCode];
reexport Kokogut.Symbols [SYMBOL Symbol PrivateSymbol];
reexport Kokogut.Lists [LIST CONS NIL '\\' List];
reexport Kokogut.Pairs [PAIR ','];
reexport Kokogut.Types [
   TYPE Type AllSupertypes IsSubtype HasType DeclareSupertype NewType
];
reexport Kokogut.Functions [
   FUNCTION NAMED_FUNCTION UNNAMED_FUNCTION FunctionName
   GENERIC_CASE GenericCase ApplyGenericCase NamedFunction UnnamedFunction
   DefineMethod DefineMethodSuper
];
reexport Kokogut.Records [SINGLETON RECORD Change];
reexport Kokogut.Modules;
reexport Kokogut.Keywords;
reexport Kokogut.Time [TIME TicksPerSecond Time AbsoluteTime];
reexport Kokogut.Threads.Core [];
reexport Kokogut.Threads.Signals [
   Signal
   BlockSignals UnblockSignals BlockAsyncSignals UnblockAsyncSignals
   BlockSyncSignals UnblockSyncSignals SignalBlockingState
   Using Ensure EXIT_TOKEN ExitToken WithExit
];
reexport Kokogut.Threads.SystemSignals [];
reexport Kokogut.Threads.Scheduler [];
reexport Kokogut.Threads.ReceiveSignal [];
reexport Kokogut.Threads.DefaultSignalHandler;
reexport Kokogut.Threads.NewThread [];
reexport Kokogut.Threads.Mutexes [
   MUTEX Mutex Lock Unlock IsLocked
   LockRead UnlockRead IsLockedRead
   LockUpdate UnlockUpdate IsLockedUpdate
   LockWriteUpdating UnlockWriteUpdating
];
reexport Kokogut.Threads.Sleeping;
reexport Kokogut.Variables [VAR FORWARD NewForward];
reexport Kokogut.Dynamic [DYNAMIC NewDynamic AttachDynamic];
reexport Kokogut.Lazy [LAZY];
reexport Kokogut.ObjectIds;
reexport Kokogut.WeakRefs [
   DEAD_WEAK_REFS DeadWeakRefs EachDeadWeakRef
   WEAK_REF WeakRef WeakPair
];
reexport Kokogut.Finalizers [];
reexport Kokogut.Debug [];
reexport Kokogut.GC [];
reexport Kokogut.Init;
reexport Kokogut.Collections.Core [
   COLLECTION SEQUENCE FLAT_SEQUENCE SET DICTIONARY
   Iterate EmptyIterator Traverse
   GENERATOR Generate EmptyGenerator Generator Collect
   KEYS Keys Like PureIterator Next '@' Size
];
reexport Kokogut.Collections.Reverse except:[ReverseCons];
reexport Kokogut.Collections.Whole;
reexport Kokogut.Collections.Single;
reexport Kokogut.Collections.Ends;
reexport Kokogut.Collections.Part;
reexport Kokogut.Collections.Set;
reexport Kokogut.Collections.Map;
reexport Kokogut.Collections.Join;
reexport Kokogut.Collections.Fold;
reexport Kokogut.Collections.Test;
reexport Kokogut.Collections.Find;
reexport Kokogut.Collections.Compare;
reexport Kokogut.Collections.Split;
reexport Kokogut.Collections.SepBy;
reexport Kokogut.Collections.Unique;
reexport Kokogut.Collections.Sort;
reexport Kokogut.Collections.Permutations;
reexport Kokogut.Data.Ranges;
reexport Kokogut.Data.LazyLists;
reexport Kokogut.Data.Strings [Right Left Center];
reexport Kokogut.Data.Vectors [VECTOR OBJECT_VECTOR Vector];
reexport Kokogut.Data.Arrays.Core [ARRAY];
reexport Kokogut.Data.Arrays.ObjectArrays;
reexport Kokogut.Data.Arrays.BoolArrays;
reexport Kokogut.Data.Arrays.ByteArrays;
reexport Kokogut.Data.Arrays.FloatArrays;
reexport Kokogut.Data.Arrays.CharArrays;
reexport Kokogut.Data.HashSets;
reexport Kokogut.Data.HashSetsBy;
reexport Kokogut.Data.HashDicts;
reexport Kokogut.Data.HashDictsBy;
reexport Kokogut.Data.WeakHashDicts;
reexport Kokogut.Data.WeakHashDictsBy;
reexport Kokogut.Data.TreeSets;
reexport Kokogut.Data.TreeSetsBy;
reexport Kokogut.Data.TreeDicts;
reexport Kokogut.Data.TreeDictsBy;
reexport Kokogut.Data.RegisteredLists;
reexport Kokogut.Data.Indexed;
reexport Kokogut.Data.Wrappers;
reexport Kokogut.Characters.Tables [];
reexport Kokogut.Characters.Properties;
reexport Kokogut.Characters.Predicates;
reexport Kokogut.Characters.Normalization;
reexport Kokogut.Characters.Boundaries;
reexport Kokogut.Characters.Case;
reexport Kokogut.Show;
reexport Kokogut.ShowSymbols;
reexport Kokogut.ShowNumbers [];
reexport Kokogut.ShowExceptions;
reexport Kokogut.IO.Streams;
reexport Kokogut.IO.ArrayStreams [];
reexport Kokogut.IO.Buffers;
reexport Kokogut.IO.Filters;
reexport Kokogut.IO.Encodings except:[GetDefaultEncoding];
reexport Kokogut.IO.UnicodeEncodings;
reexport Kokogut.IO.CStrings [];
reexport Kokogut.Serialization;
reexport Kokogut.IO.Environment;
reexport Kokogut.IO.DefaultEncoding [];
reexport Kokogut.IO.LineSep;
reexport Kokogut.IO.RawFiles [
   RAW_FILE OpenRawFile CreateRawFile
   RawStdIn RawStdOut RawStdErr
];
reexport Kokogut.IO.TextFiles;

c_export();
