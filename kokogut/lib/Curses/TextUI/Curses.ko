// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2005,2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module TextUI.Curses =>

// Not supported (yet):
// baudrate, delay_output, napms,
// dupwin, putwin, getwin, isendwin, mvwin,
// erasechar, killchar,
// longname,
// mvcur,
// mvderwin,
// newpad, subpad, prefresh, pnoutrefresh, pechochar,
// newterm, delscreen, set_term,
// printw, wprintw, mvprintw, mvwprintw, vw_printw, vwprintw,
// scanw, wscanw, mvscanw, mvwscanw, vw_scanw, vwscanw,
// timeout, wtimeout, notimeout,
// putp, tputs,
// savetty, resetty,
// ripoffline,
// scr_dump, scr_init, scr_restore, scr_set,
// scrl, wscrl, scroll,
// setupterm,
// slk_*,
// standout, wstandout, standend, wstandend,
// syncok, wcursyncup, wsyncdown, wsyncup,
// termattrs,
// termname,
// tigetflag, tigetnum, tigetstr, tparm,
// typeahead,
// unctrl,
// ungetch,
// use_env,
// vidattr, vid_attr, vidputs, vid_puts,

reexport TextUI.Curses.Core;
reexport TextUI.Curses.Windows except:[StdScrValue];
reexport TextUI.Curses.Init;
reexport TextUI.Curses.Parameters;
reexport TextUI.Curses.Refresh;
reexport TextUI.Curses.Move;
reexport TextUI.Curses.Attrs except:[SerializeAttr DeserializeAttr];
reexport TextUI.Curses.Colors;
reexport TextUI.Curses.Chars;
reexport TextUI.Curses.Keys;
