// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module TextUI.Curses.Windows =>

use TextUI.Curses.Core [];

c_export "
   struct ko_window {
      KO_FOREIGN_OBJECT;
      WINDOW *value;
   };

   KO_OBJECT_FIELD_RAW(window, value, WINDOW *);
";

c_type WINDOW (private WindowCon) "KO_MOVE_OBJECT(struct ko_window);" "
   WINDOW *win = ko_value_of_window(obj);
   if (win != NULL) {
      delwin(win);
      ko_set_value_of_window(obj, NULL);
   }
";

c_exportName [KO_WINDOW:WINDOW KO_WindowCon:(c_con WindowCon)];
c_export "
   KO_DESCR_CHECKER(window, KO_WindowCon);

   #define KO_CHECK_IS_WINDOW(obj) \\
      KO_CHECK_TYPE(obj, ko_is_window(obj), KO_WINDOW)

   #define KO_WINDOW_VALUE(winC, win)    \\
      KO_STAT_BEGIN {                    \\
         KO_CHECK_IS_WINDOW(win);        \\
         winC = ko_value_of_window(win); \\
      } KO_STAT_END
";

TypeSerialization "TextUI.Curses.WINDOW" WINDOW;

var StdScrValue = Null;
ref StdScr = function [
   () {StdScrValue}
   win {
      if (win %Is Null) {c_stat "stdscr = NULL;"} {
         c_stat [win] "
            KO_CHECK_IS_WINDOW(win);
            stdscr = ko_value_of_window(win);
         "
      };
      StdScrValue = win
   }
   win body {
      Using {let old = StdScrValue; StdScr = win; old} (ref StdScr) ?_ =>
      body()
   }
];

c_export "ko_value_t ko_make_window(WINDOW *win);";
c_define [(c_con WindowCon)] "
   ko_value_t
   ko_make_window(WINDOW *win) {
      ko_value_t result;
      result = ko_alloc_bytes(sizeof(struct ko_window));
      ko_init_descr(result, WindowCon);
      ko_set_value_of_window(result, win);
      ko_register_foreign_object(result);
      return result;
   }
";

def DerWin orig lines cols y x {
   c_eval [orig lines cols y x] "
      WINDOW *origC;
      int linesC, colsC, yC, xC;
      WINDOW *result;
      KO_WINDOW_VALUE(origC, orig);
      KO_INT_VALUE_GE0(linesC, lines, \"number of lines\");
      KO_INT_VALUE_GE0(colsC, cols, \"number of colums\");
      KO_INT_VALUE_GE0(yC, y, \"y position\");
      KO_INT_VALUE_GE0(xC, x, \"x position\");
      result = derwin(origC, linesC, colsC, yC, xC);
      KO_FAIL_CURSES_IF_NULL(result, \"derwin\");
      KO_RESULT = ko_make_window(result);
   "
};

def NewWin lines cols y x {
   c_eval [lines cols y x] "
      int linesC, colsC, yC, xC;
      WINDOW *result;
      KO_INT_VALUE_GE0(linesC, lines, \"number of lines\");
      KO_INT_VALUE_GE0(colsC, cols, \"number of colums\");
      KO_INT_VALUE_GE0(yC, y, \"y position\");
      KO_INT_VALUE_GE0(xC, x, \"x position\");
      result = newwin(linesC, colsC, yC, xC);
      KO_FAIL_CURSES_IF_NULL(result, \"newwin\");
      KO_RESULT = ko_make_window(result);
   "
};

def SubWin orig lines cols y x {
   c_eval [orig lines cols y x] "
      WINDOW *origC;
      int linesC, colsC, yC, xC;
      WINDOW *result;
      KO_WINDOW_VALUE(origC, orig);
      KO_INT_VALUE_GE0(linesC, lines, \"number of lines\");
      KO_INT_VALUE_GE0(colsC, cols, \"number of colums\");
      KO_INT_VALUE_GE0(yC, y, \"y position\");
      KO_INT_VALUE_GE0(xC, x, \"x position\");
      result = subwin(origC, linesC, colsC, yC, xC);
      KO_FAIL_CURSES_IF_NULL(result, \"subwin\");
      KO_RESULT = ko_make_window(result);
   "
};

def DelWin win {
   c_stat [win (ref StdScrValue)] "
      WINDOW *winC;
      KO_WINDOW_VALUE(winC, win);
      if (winC != NULL) {
         int status;
         status = delwin(winC);
         KO_FAIL_CURSES_IF_ERR(status, \"delwin\");
         ko_set_value_of_window(win, NULL);
         if (ko_value_of_var(StdScrValue) == win)
            ko_init_value_of_var(StdScrValue, KO_Null);
      }
   ";
};
// TODO: Some operations will probably crash when given a NULL window.

method Hash x^WINDOW {
   c_expr [x] "ko_sint((ko_nint_t)ko_value_of_window(x) & KO_MAX_HASH)"
};

def GetBegYX win {
   c_let [y x] [win] "
      WINDOW *winC;
      int yC, xC;
      KO_WINDOW_VALUE(winC, win);
      getbegyx(winC, yC, xC);
      y = ko_int(yC);
      x = ko_int(xC);
   ";
   y, x
};

def GetMaxYX win {
   c_let [y x] [win] "
      WINDOW *winC;
      int yC, xC;
      KO_WINDOW_VALUE(winC, win);
      getmaxyx(winC, yC, xC);
      y = ko_int(yC);
      x = ko_int(xC);
   ";
   y, x
};

def GetParYX win {
   c_let [y x] [win] "
      WINDOW *winC;
      int yC, xC;
      KO_WINDOW_VALUE(winC, win);
      getparyx(winC, yC, xC);
      y = ko_int(yC);
      x = ko_int(xC);
   ";
   y, x
};

def GetYX win {
   c_let [y x] [win] "
      WINDOW *winC;
      int yC, xC;
      KO_WINDOW_VALUE(winC, win);
      getyx(winC, yC, xC);
      y = ko_int(yC);
      x = ko_int(xC);
   ";
   y, x
};

def CopyWin srcWin destWin sMinRow sMinCol dMinRow dMinCol dMaxRow dMaxCol
   overlay
{
   c_stat [
      srcWin destWin sMinRow sMinCol dMinRow dMinCol dMaxRow dMaxCol overlay
   ] "
      WINDOW *srcWinC, *destWinC;
      int sMinRowC, sMinColC, dMinRowC, dMinColC, dMaxRowC, dMaxColC, status;
      KO_WINDOW_VALUE(srcWinC, srcWin);
      KO_WINDOW_VALUE(destWinC, destWin);
      KO_INT_VALUE_GE0(sMinRowC, sMinRow, \"line number\");
      KO_INT_VALUE_GE0(sMinColC, sMinCol, \"column number\");
      KO_INT_VALUE_GE0(dMinRowC, dMinRow, \"line number\");
      KO_INT_VALUE_GE0(dMinColC, dMinCol, \"column number\");
      KO_INT_VALUE_GE0(dMaxRowC, dMaxRow, \"line number\");
      KO_INT_VALUE_GE0(dMaxColC, dMaxCol, \"column number\");
      KO_CHECK_IS_BOOL(overlay);
      status = copywin(srcWinC, destWinC,
         sMinRowC, sMinColC, dMinRowC, dMinColC, dMaxRowC, dMaxColC,
         overlay == KO_True);
      KO_FAIL_CURSES_IF_ERR(status, \"copywin\");
   "
};

def Overlay srcWin destWin {
   c_stat [srcWin destWin] "
      WINDOW *srcWinC, *destWinC;
      int status;
      KO_WINDOW_VALUE(srcWinC, srcWin);
      KO_WINDOW_VALUE(destWinC, destWin);
      status = overlay(srcWinC, destWinC);
      KO_FAIL_CURSES_IF_ERR(status, \"overlay\");
   "
};

def Overwrite srcWin destWin {
   c_stat [srcWin destWin] "
      WINDOW *srcWinC, *destWinC;
      int status;
      KO_WINDOW_VALUE(srcWinC, srcWin);
      KO_WINDOW_VALUE(destWinC, destWin);
      status = overwrite(srcWinC, destWinC);
      KO_FAIL_CURSES_IF_ERR(status, \"overwrite\");
   "
};
