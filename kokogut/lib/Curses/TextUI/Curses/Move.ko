// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module TextUI.Curses.Move =>

use TextUI.Curses.Core [];
use TextUI.Curses.Windows [];

def Move y x {
   c_stat [y x] "
      int yC, xC, status;
      KO_INT_VALUE_GE0(yC, y, \"y position\");
      KO_INT_VALUE_GE0(xC, x, \"x position\");
      status = move(yC, xC);
      KO_FAIL_CURSES_IF_ERR(status, \"move\");
   "
};

def WMove win y x {
   c_stat [win y x] "
      WINDOW *winC;
      int yC, xC, status;
      KO_WINDOW_VALUE(winC, win);
      KO_INT_VALUE_GE0(yC, y, \"y position\");
      KO_INT_VALUE_GE0(xC, x, \"x position\");
      status = wmove(winC, yC, xC);
      KO_FAIL_CURSES_IF_ERR(status, \"wmove\");
   "
};

def DelCh() {
   c_stat "
      int status = delch();
      KO_FAIL_CURSES_IF_ERR(status, \"delch\");
   "
};

def WDelCh win {
   c_stat [win] "
      WINDOW *winC;
      int status;
      KO_WINDOW_VALUE(winC, win);
      status = wdelch(winC);
      KO_FAIL_CURSES_IF_ERR(status, \"wdelch\");
   "
};

def MvDelCh y x {
   c_stat [y x] "
      int yC, xC, status;
      KO_INT_VALUE_GE0(yC, y, \"y position\");
      KO_INT_VALUE_GE0(xC, x, \"x position\");
      status = mvdelch(yC, xC);
      KO_FAIL_CURSES_IF_ERR(status, \"mvdelch\");
   "
};

def MvWDelCh win y x {
   c_stat [win y x] "
      WINDOW *winC;
      int yC, xC, status;
      KO_WINDOW_VALUE(winC, win);
      KO_INT_VALUE_GE0(yC, y, \"y position\");
      KO_INT_VALUE_GE0(xC, x, \"x position\");
      status = mvwdelch(winC, yC, xC);
      KO_FAIL_CURSES_IF_ERR(status, \"mvwdelch\");
   "
};

def InsertLn() {
   c_stat "
      int status = insertln();
      KO_FAIL_CURSES_IF_ERR(status, \"insertln\");
   "
};

def WInsertLn win {
   c_stat [win] "
      WINDOW *winC;
      int status;
      KO_WINDOW_VALUE(winC, win);
      status = winsertln(winC);
      KO_FAIL_CURSES_IF_ERR(status, \"winsertln\");
   "
};

def DeleteLn() {
   c_stat "
      int status = deleteln();
      KO_FAIL_CURSES_IF_ERR(status, \"deleteln\");
   "
};

def WDeleteLn win {
   c_stat [win] "
      WINDOW *winC;
      int status;
      KO_WINDOW_VALUE(winC, win);
      status = wdeleteln(winC);
      KO_FAIL_CURSES_IF_ERR(status, \"wdeleteln\");
   "
};

def InsDelLn n {
   c_stat [n] "
      int nC, status;
      KO_INT_VALUE(nC, n, \"line count\");
      status = insdelln(nC);
      KO_FAIL_CURSES_IF_ERR(status, \"insdelln\");
   "
};

def WInsDelLn win n {
   c_stat [win n] "
      WINDOW *winC;
      int nC, status;
      KO_WINDOW_VALUE(winC, win);
      KO_INT_VALUE(nC, n, \"line count\");
      status = winsdelln(winC, nC);
      KO_FAIL_CURSES_IF_ERR(status, \"winsdelln\");
   "
};

def SetScrReg top bot {
   c_stat [top bot] "
      int topC, botC, status;
      KO_INT_VALUE_GE0(topC, top, \"line number\");
      KO_INT_VALUE_GE0(botC, bot, \"line number\");
      status = setscrreg(topC, botC);
      KO_FAIL_CURSES_IF_ERR(status, \"setscrreg\");
   "
};

def WSetScrReg win top bot {
   c_stat [win top bot] "
      WINDOW *winC;
      int topC, botC, status;
      KO_WINDOW_VALUE(winC, win);
      KO_INT_VALUE_GE0(topC, top, \"line number\");
      KO_INT_VALUE_GE0(botC, bot, \"line number\");
      status = wsetscrreg(winC, topC, botC);
      KO_FAIL_CURSES_IF_ERR(status, \"wsetscrreg\");
   "
};

def Erase() {
   c_stat "
      int status = erase();
      KO_FAIL_CURSES_IF_ERR(status, \"erase\");
   "
};

def WErase win {
   c_stat [win] "
      WINDOW *winC;
      int status;
      KO_WINDOW_VALUE(winC, win);
      status = werase(winC);
      KO_FAIL_CURSES_IF_ERR(status, \"werase\");
   "
};

def ClearScr() {
   c_stat "
      int status = clear();
      KO_FAIL_CURSES_IF_ERR(status, \"clear\");
   "
};

def WClearScr win {
   c_stat [win] "
      WINDOW *winC;
      int status;
      KO_WINDOW_VALUE(winC, win);
      status = wclear(winC);
      KO_FAIL_CURSES_IF_ERR(status, \"wclear\");
   "
};

def ClrToBot() {
   c_stat "
      int status = clrtobot();
      KO_FAIL_CURSES_IF_ERR(status, \"clrtobot\");
   "
};

def WClrToBot win {
   c_stat [win] "
      WINDOW *winC;
      int status;
      KO_WINDOW_VALUE(winC, win);
      status = wclrtobot(winC);
      KO_FAIL_CURSES_IF_ERR(status, \"wclrtobot\");
   "
};

def ClrToEOL() {
   c_stat "
      int status = clrtoeol();
      KO_FAIL_CURSES_IF_ERR(status, \"clrtoeol\");
   "
};

def WClrToEOL win {
   c_stat [win] "
      WINDOW *winC;
      int status;
      KO_WINDOW_VALUE(winC, win);
      status = wclrtoeol(winC);
      KO_FAIL_CURSES_IF_ERR(status, \"wclrtoeol\");
   "
};
