// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2005-2007,2009 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module TextUI.ReadLine.Variables =>

use TextUI.ReadLine.Core;
use TextUI.ReadLine.Display;
use TextUI.ReadLine.Terminal;

//// ReadLine library version

lazy ReadLineVersionString = c_eval "
   KO_MAKE_CSTRING(KO_RESULT, rl_library_version);
"->DecodeCString;

let ReadLineVersionCode = c_expr "ko_sint(rl_readline_version)";

let IsGnuReadLine = c_cond "rl_gnu_readline_p";

//// ReadLine state

def ReadLineState [
   name {
      case name [
         #none         {c_cond "RL_ISSTATE(RL_STATE_NONE)"}
         #initializing {c_cond "RL_ISSTATE(RL_STATE_INITIALIZING)"}
         #initialized  {c_cond "RL_ISSTATE(RL_STATE_INITIALIZED)"}
         #termPrepped  {c_cond "RL_ISSTATE(RL_STATE_TERMPREPPED)"}
         #readCmd      {c_cond "RL_ISSTATE(RL_STATE_READCMD)"}
         #metaNext     {c_cond "RL_ISSTATE(RL_STATE_METANEXT)"}
         #dispatching  {c_cond "RL_ISSTATE(RL_STATE_DISPATCHING)"}
         #moreInput    {c_cond "RL_ISSTATE(RL_STATE_MOREINPUT)"}
         #iSearch      {c_cond "RL_ISSTATE(RL_STATE_ISEARCH)"}
         #nSearch      {c_cond "RL_ISSTATE(RL_STATE_NSEARCH)"}
         #search       {c_cond "RL_ISSTATE(RL_STATE_SEARCH)"}
         #numericArg   {c_cond "RL_ISSTATE(RL_STATE_NUMERICARG)"}
         #macroInput   {c_cond "RL_ISSTATE(RL_STATE_MACROINPUT)"}
         #macroDef     {c_cond "RL_ISSTATE(RL_STATE_MACRODEF)"}
         #overwrite    {c_cond "RL_ISSTATE(RL_STATE_OVERWRITE)"}
         #completing   {c_cond "RL_ISSTATE(RL_STATE_COMPLETING)"}
         #sigHandler   {c_cond "RL_ISSTATE(RL_STATE_SIGHANDLER)"}
         #undoing      {c_cond "RL_ISSTATE(RL_STATE_UNDOING)"}
         #done         {c_cond "RL_ISSTATE(RL_STATE_DONE)"}
      ]
   }
   name value {
      case name [
         #none {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_NONE);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_NONE);"}
         }
         #initializing {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_INITIALIZING);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_INITIALIZING);"}
         }
         #initialized {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_INITIALIZED);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_INITIALIZED);"}
         }
         #termPrepped {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_TERMPREPPED);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_TERMPREPPED);"}
         }
         #readCmd {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_READCMD);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_READCMD);"}
         }
         #metaNext {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_METANEXT);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_METANEXT);"}
         }
         #dispatching {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_DISPATCHING);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_DISPATCHING);"}
         }
         #moreInput {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_MOREINPUT);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_MOREINPUT);"}
         }
         #iSearch {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_ISEARCH);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_ISEARCH);"}
         }
         #nSearch {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_NSEARCH);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_NSEARCH);"}
         }
         #search {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_SEARCH);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_SEARCH);"}
         }
         #numericArg {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_NUMERICARG);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_NUMERICARG);"}
         }
         #macroInput {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_MACROINPUT);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_MACROINPUT);"}
         }
         #macroDef {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_MACRODEF);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_MACRODEF);"}
         }
         #overwrite {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_OVERWRITE);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_OVERWRITE);"}
         }
         #completing {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_COMPLETING);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_COMPLETING);"}
         }
         #sigHandler {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_SIGHANDLER);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_SIGHANDLER);"}
         }
         #undoing {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_UNDOING);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_UNDOING);"}
         }
         #done {
            if value
               {c_stat "RL_SETSTATE(RL_STATE_DONE);"}
               {c_stat "RL_UNSETSTATE(RL_STATE_DONE);"}
         }
      ]
   }
];

ref EditingMode = function [
   () {
      if (c_cond "rl_editing_mode") {#emacs} {#vi}
   }
   mode {
      case mode [
         #emacs {c_stat "rl_editing_mode = 1;"}
         #vi    {c_stat "rl_editing_mode = 0;"}
         _      {Fail (BadArgument mode "mode" "#emacs | #vi")}
      ]
   }
];

// rl_insert_mode is undocumented.
ifDefined HaveInsertMode {
ref InsertMode = function [
   ()   {c_cond "rl_insert_mode"}
   mode {
      if mode
         {c_stat "rl_insert_mode = 1;"}
         {c_stat "rl_editing_mode = 0;"}
   }
];
};

//// ReadLineName

c_define "static char *ko_readline_name = NULL;";

ref ReadLineName = function [
   () {
      c_eval "KO_MAKE_CSTRING(KO_RESULT, rl_readline_name);"
      ->DecodeCString
   }
   name {
      c_stat [nameC:(EncodeCString name)] "
         ko_nint_t size;
         char *buf;
         size = ko_nsize_of_cstring(nameC) + 1;
         KO_SAFE_MALLOC(buf, size);
         memcpy(buf, ko_chars_of_cstring(nameC), size);
         free(ko_readline_name);
         ko_readline_name = buf;
         rl_readline_name = buf;
      "
   }
];

//// Prompt

ref Prompt = {
   c_eval "KO_MAKE_CSTRING(KO_RESULT, rl_prompt);"
   ->DecodeCString
};

//// Done

ref Done = function [
   ()    {c_cond "rl_done"}
   value {
      if value
         {c_stat "rl_done = 1;"}
         {c_stat "rl_done = 0;"}
   }
];

//// PendingInput

ref PendingInput = function [
   ()  {c_expr "rl_pending_input < 0 ? KO_Null : ko_sint(rl_pending_input)"}
   key {
      c_stat [key] "
         if (key == KO_Null)
            rl_pending_input = EOF;
         else {
            KO_CHECK_IS_BYTE(key);
            rl_pending_input = ko_nint_of_sint(key);
         }
      "
   }
];

//// Dispatching

ref Dispatching = {c_cond "rl_dispatching"};

//// ExplicitArg

ref ExplicitArg = function [
   ()  {c_cond "rl_explicit_arg"}
   arg {
      if arg
         {c_stat "rl_explicit_arg = 1;"}
         {c_stat "rl_explicit_arg = 0;"}
   }
];

//// NumericArg

ref NumericArg = function [
   ()  {c_expr "ko_int(rl_numeric_arg)"}
   arg {
      c_stat [arg] "
         int argC;
         KO_INT_VALUE(argC, arg, \"numeric argument\");
         rl_numeric_arg = argC;
      "
   }
];

//// TerminalName

c_define "static char *ko_terminal_name = NULL;";

ref TerminalName = function [
   () {
      c_case [nameC] "
         if (rl_terminal_name == NULL) goto absent;
         KO_MAKE_CSTRING(nameC, rl_terminal_name);
         goto present;
      " [
         absent:  {Null}
         present: {DecodeCString nameC}
      ]
   }
   name {
      if (name %Is Null) {
         c_stat "
            free(ko_terminal_name);
            ko_terminal_name = NULL;
            rl_terminal_name = NULL;
         "
      } =>
      c_stat [nameC:(EncodeCString name)] "
         ko_nint_t size;
         char *buf;
         size = ko_nsize_of_cstring(nameC) + 1;
         KO_SAFE_MALLOC(buf, size);
         memcpy(buf, ko_chars_of_cstring(nameC), size);
         free(ko_terminal_name);
         ko_terminal_name = buf;
         rl_terminal_name = buf;
      "
   }
];

//// InStream

c_define "static FILE *ko_instream = NULL;";

private var InStreamVar = RawStdIn;

ref InStream = function [
   ()     {InStreamVar}
   stream {
      c_stat [(ref InStreamVar) stream RawStdIn] "
         FILE *streamC;
         if (stream == RawStdIn)
            streamC = stdin;
         else {
            int fd;
            KO_RAW_FILE_FD(fd, stream);
            streamC = fdopen(fd, \"r\");
            if (KO_UNLIKELY(streamC == NULL)) KO_FAIL_IO();
         }
         if (ko_instream != NULL && ko_instream != stdin) {
            int fd = fileno(ko_instream);
            int fd2 = dup(fd);
            int status;
            if (KO_UNLIKELY(fd2 < 0)) KO_FAIL_IO();
            fclose(ko_instream);
            status = dup2(fd2, fd);
            if (KO_UNLIKELY(status < 0)) KO_FAIL_IO();
            close(fd2);
         }
         ko_instream = streamC;
         rl_instream = streamC;
         ko_set_value_of_var(InStreamVar, stream);
      "
   }
];

//// OutStream

c_define "static FILE *ko_outstream = NULL;";

private var OutStreamVar = RawStdOut;

ref OutStream = function [
   ()     {OutStreamVar}
   stream {
      c_stat [(ref OutStreamVar) stream RawStdOut] "
         FILE *streamC;
         if (stream == RawStdOut)
            streamC = stdout;
         else {
            int fd;
            KO_RAW_FILE_FD(fd, stream);
            streamC = fdopen(fd, \"w\");
            if (KO_UNLIKELY(streamC == NULL)) KO_FAIL_IO();
         }
         if (ko_outstream != NULL && ko_outstream != stdout) {
            int fd = fileno(ko_outstream);
            int fd2 = dup(fd);
            int status;
            if (KO_UNLIKELY(fd2 < 0)) KO_FAIL_IO();
            fclose(ko_outstream);
            status = dup2(fd2, fd);
            if (KO_UNLIKELY(status < 0)) KO_FAIL_IO();
            close(fd2);
         }
         ko_outstream = streamC;
         rl_outstream = streamC;
         ko_set_value_of_var(OutStreamVar, stream);
      "
   }
];

//// PreferEnvWinSize

ifDefined HavePreferEnvWinSize {
ref PreferEnvWinSize = function [
   ()    {c_cond "rl_prefer_env_winsize"}
   value {
      if value
         {c_stat "rl_prefer_env_winsize = 1;"}
         {c_stat "rl_prefer_env_winsize = 0;"}
   }
];
};

//// StartupHook

private var StartupHookVar = Null;

c_define [(ref StartupHookVar)] "
   static int
   ko_startup_hook(void) {
      ko_leave_foreign();
      KO_CHECK_READLINE_EXCEPTION {
         KO_R[0] = ko_value_of_var(StartupHookVar);
         KO_CALL_INDIRECT(0);
         KO_CHECK_READLINE_EXCEPTION {}
      }
      ko_enter_foreign();
      return 0;
   }
";

ref StartupHook = function [
   ()  {StartupHookVar}
   fun {
      c_stat [(ref StartupHookVar) fun] "
         rl_startup_hook = fun == KO_Null ? NULL : ko_startup_hook;
         ko_set_value_of_var(StartupHookVar, fun);
      "
   }
];

//// PreInputHook

private var PreInputHookVar = Null;

c_define [(ref PreInputHookVar)] "
   static int
   ko_pre_input_hook(void) {
      ko_leave_foreign();
      KO_CHECK_READLINE_EXCEPTION {
         KO_R[0] = ko_value_of_var(PreInputHookVar);
         KO_CALL_INDIRECT(0);
         KO_CHECK_READLINE_EXCEPTION {}
      }
      ko_enter_foreign();
      return 0;
   }
";

ref PreInputHook = function [
   ()  {PreInputHookVar}
   fun {
      PreInputHookVar = fun;
      c_stat [fun] "
         rl_pre_input_hook = fun == KO_Null ? NULL : ko_pre_input_hook;
      "
   }
];

//// EventHook

private var EventHookVar = Null;

c_define [(ref EventHookVar)] "
   static int
   ko_event_hook(void) {
      ko_leave_foreign();
      KO_CHECK_READLINE_EXCEPTION {
         KO_R[0] = ko_value_of_var(EventHookVar);
         KO_CALL_INDIRECT(0);
         KO_CHECK_READLINE_EXCEPTION {}
      }
      ko_enter_foreign();
      return 0;
   }
";

ref EventHook = function [
   ()  {EventHookVar}
   fun {
      c_stat [(ref EventHookVar) fun] "
         rl_event_hook = fun == KO_Null ? NULL : ko_event_hook;
         ko_set_value_of_var(EventHookVar, fun);
      "
   }
];

//// GetCFunction

private var GetCFunctionVar = ReadByteFrom;

private def CallGetCFunction stream {
   let value = GetCFunctionVar stream;
   if (value ~%Is Null) {c_stat [value] "KO_CHECK_IS_BYTE(value);"};
   value
};

c_define [(ref InStreamVar) (c_entry CallGetCFunction 1)] "
   static int
   ko_getc_function(FILE *stream) {
      int key = EOF;
      ko_leave_foreign();
      KO_CHECK_READLINE_EXCEPTION {
         KO_R[1] = stream == rl_instream ? ko_value_of_var(InStreamVar) :
            ko_make_raw_file_no_close(fileno(stream));
         KO_CALL(CallGetCFunction1);
         KO_CHECK_READLINE_EXCEPTION {
            if (KO_R[1] != KO_Null) key = ko_nint_of_sint(KO_R[1]);
         }
      }
      ko_enter_foreign();
      return key;
   }
";

ref GetCFunction = function [
   ()  {GetCFunctionVar}
   fun {
      c_stat [(ref GetCFunctionVar) fun ReadByteFrom] "
         rl_getc_function = fun == ReadByteFrom ? rl_getc : ko_getc_function;
         ko_set_value_of_var(GetCFunctionVar, fun);
      "
   }
];

//// RedisplayFunction

private var RedisplayFunctionVar = Redisplay;

c_define [(ref RedisplayFunctionVar)] "
   static void
   ko_redisplay_function(void) {
      ko_leave_foreign();
      KO_CHECK_READLINE_EXCEPTION {
         KO_R[0] = ko_value_of_var(RedisplayFunctionVar);
         KO_CALL_INDIRECT(0);
         KO_CHECK_READLINE_EXCEPTION {}
      }
      ko_enter_foreign();
   }
";

ref RedisplayFunction = function [
   ()  {RedisplayFunctionVar}
   fun {
      c_stat [(ref RedisplayFunctionVar) fun Redisplay] "
         rl_redisplay_function =
            fun == Redisplay ? rl_redisplay :
            ko_redisplay_function;
         ko_set_value_of_var(RedisplayFunctionVar, fun);
      "
   }
];

//// PrepTermFunction

private var PrepTermFunctionVar = PrepTerminal;

c_define [(ref PrepTermFunctionVar)] "
   #if RL_READLINE_VERSION >= 0x0501
      #define ko_null_prep_term_function NULL
   #else
      static void
      ko_null_prep_term_function(int meta) {
         (void)meta;
         /* Work around a bug fixed in readline-5.1:
          * rl_prep_term_function was called even if NULL. */
      }
   #endif

   static void
   ko_prep_term_function(int meta) {
      ko_leave_foreign();
      KO_CHECK_READLINE_EXCEPTION {
         KO_R[0] = ko_value_of_var(PrepTermFunctionVar);
         KO_R[1] = meta ? KO_True : KO_False;
         KO_CALL_INDIRECT(1);
         KO_CHECK_READLINE_EXCEPTION {}
      }
      ko_enter_foreign();
   }
";

ref PrepTermFunction = function [
   ()  {PrepTermFunctionVar}
   fun {
      c_stat [(ref PrepTermFunctionVar) fun PrepTerminal] "
         rl_prep_term_function =
            fun == KO_Null ? ko_null_prep_term_function :
            fun == PrepTerminal ? rl_prep_terminal :
            ko_prep_term_function;
         ko_set_value_of_var(PrepTermFunctionVar, fun);
      "
   }
];

//// DeprepTermFunction

private var DeprepTermFunctionVar = DeprepTerminal;

c_define [(ref DeprepTermFunctionVar)] "
   #if RL_READLINE_VERSION >= 0x0501
      #define ko_null_deprep_term_function NULL
   #else
      static void
      ko_null_deprep_term_function(void) {
         /* Work around a bug fixed in readline-5.1:
          * rl_deprep_term_function was called even if NULL. */
      }
   #endif

   static void
   ko_deprep_term_function(void) {
      ko_leave_foreign();
      KO_CHECK_READLINE_EXCEPTION {
         KO_R[0] = ko_value_of_var(DeprepTermFunctionVar);
         KO_CALL_INDIRECT(0);
         KO_CHECK_READLINE_EXCEPTION {}
      }
      ko_enter_foreign();
   }
";

ref DeprepTermFunction = function [
   ()  {DeprepTermFunctionVar}
   fun {
      c_stat [(ref DeprepTermFunctionVar) fun DeprepTerminal] "
         rl_deprep_term_function =
            fun == KO_Null ? ko_null_deprep_term_function :
            fun == DeprepTerminal ? rl_deprep_terminal :
            ko_deprep_term_function;
         ko_set_value_of_var(DeprepTermFunctionVar, fun);
      "
   }
];

//// EraseEmptyLine

ref EraseEmptyLine = function [
   ()    {c_cond "rl_erase_empty_line"}
   value {
      if value
         {c_stat "rl_erase_empty_line = 1;"}
         {c_stat "rl_erase_empty_line = 0;"}
   }
];

//// AlreadyPrompted

ref AlreadyPrompted = function [
   ()    {c_cond "rl_already_prompted"}
   value {
      if value
         {c_stat "rl_already_prompted = 1;"}
         {c_stat "rl_already_prompted = 0;"}
   }
];

//// NumBytesToRead

ref NumBytesToRead = function [
   ()    {c_expr "ko_int(rl_num_chars_to_read)"}
   count {
      c_stat [count] "
         int countC;
         KO_INT_VALUE_GE0(countC, count, \"count\");
         rl_num_chars_to_read = countC;
      "
   }
];

//// ExecutingMacro

ref ExecutingMacro = {
   c_case [macroC] "
      if (rl_executing_macro == NULL) goto absent;
      KO_MAKE_CSTRING(macroC, rl_executing_macro);
      goto present;
   " [
      absent:  {Null}
      present: {DecodeCString macroC}
   ]
};
