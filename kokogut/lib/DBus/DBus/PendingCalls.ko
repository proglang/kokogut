// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module System.DBus.PendingCalls =>

use Threads;
use System.DBus.Core [];
use System.DBus.Messages [];

c_define "
   static inline DBusPendingCall *
   ko_value_of_dbus_pending_call(ko_value_t cptr) {
      return (DBusPendingCall *)ko_value_of_cptr(cptr);
   }
";

c_define [(c_entry Notify 1)] "
   static void
   ko_dbus_pending_call_completed(DBusPendingCall *pending, void *data) {
      struct ko_external *completedPtr;
      (void)pending;
      completedPtr = (struct ko_external *)data;
      KO_R[1] = completedPtr->ko_value;
      KO_CALL(Notify1);
      if (KO_UNLIKELY(ko_failed))
         ko_impossible_error(\"ko_dbus_pending_call_completed: \"
            \"Notify failed\");
   }
";

type PENDING_CALL PendingCallCon _rawPending _mutex {
   var isClosed = False;
   let _finalizer = Finalizer this {
      Lock _mutex =>
      isClosed = True;
      Notify _completed;
      c_stat [rawPending:_rawPending] "
         DBusPendingCall *rawPendingC;
         rawPendingC = ko_value_of_dbus_pending_call(rawPending);
         dbus_pending_call_set_notify(rawPendingC, NULL, NULL, NULL);
         dbus_pending_call_unref(rawPendingC);
      "
   };
   let _completed = Condition _mutex {
      if isClosed {Fail (ObjectClosed PENDING_CALL)};
      c_cond [rawPending:_rawPending] "
         dbus_pending_call_get_completed(
            ko_value_of_dbus_pending_call(rawPending)) != 0
      "
   };
   try {
      c_stat [rawPending:_rawPending completed:_completed] "
         struct ko_external *completedPtr;
         dbus_bool_t status;
         KO_SAFE_MALLOC(completedPtr, sizeof(struct ko_external));
         ko_register_external(completedPtr, completed);
         status = dbus_pending_call_set_notify(
            ko_value_of_dbus_pending_call(rawPending),
            ko_dbus_pending_call_completed, completedPtr,
            ko_unregister_external_and_free);
         if (KO_UNLIKELY(!status)) {
            ko_unregister_external_and_free(completedPtr);
            KO_FAIL_OUT_OF_MEMORY();
         }
      "
   } exn {Close _finalizer; Fail exn};
};

c_defineName [
   KO_DBUS_PENDING_CALL:PENDING_CALL
   KO_DBusPendingCallCon:(c_con PendingCallCon)
];
c_define "
   KO_DESCR_CHECKER(dbus_pending_call, KO_DBusPendingCallCon);

   #define KO_CHECK_IS_DBUS_PENDING_CALL(obj) \\
      KO_CHECK_TYPE(obj, ko_is_dbus_pending_call(obj), KO_DBUS_PENDING_CALL)
";

TypeSerialization "System.DBus.PENDING_CALL" PENDING_CALL;

method Close pending^PENDING_CALL {
   Lock pending._mutex {
      if ~pending.isClosed =>
      c_stat [rawPending:pending._rawPending] "
         dbus_pending_call_cancel(ko_value_of_dbus_pending_call(rawPending));
      "
   };
   Close pending._finalizer
};

def WaitForReply pending {
   c_stat [pending] "KO_CHECK_IS_DBUS_PENDING_CALL(pending);";
   Lock pending._mutex =>
   Wait pending._completed;
   c_eval [rawPending:pending._rawPending] "
      DBusMessage *msgC;
      msgC = dbus_pending_call_steal_reply(
         ko_value_of_dbus_pending_call(rawPending));
      if (KO_UNLIKELY(msgC == NULL))
         KO_RESULT = KO_Null;
      else {
         KO_RESULT = ko_wrap_dbus_message(msgC);
         if (dbus_message_get_type(msgC) == DBUS_MESSAGE_TYPE_ERROR)
            KO_FAIL(KO_RESULT);
      }
   "
};

def CanWaitForReply pending {
   c_stat [pending] "KO_CHECK_IS_DBUS_PENDING_CALL(pending);";
   Lock pending._mutex =>
   Wait pending._completed
};
