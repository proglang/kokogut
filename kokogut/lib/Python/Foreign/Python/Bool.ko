// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004,2006-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Foreign.Python.Bool =>

use Foreign.Python.Core;

c_define "static KO_STATIC_PY_TYPE_OBJECT(ko_PyBOOL, &PyBool_Type);";
let PyBOOL = c_expr "KO_STATIC_PTR(ko_PyBOOL)";

c_define "
   static KO_STATIC_PY_OBJECT(ko_PyFalse, Py_False);
   static KO_STATIC_PY_OBJECT(ko_PyTrue,  Py_True);
";
let PyFalse = c_expr "KO_STATIC_PTR(ko_PyFalse)";
let PyTrue  = c_expr "KO_STATIC_PTR(ko_PyTrue)";

method PyObject _^TRUE  {PyTrue};
method PyObject _^FALSE {PyFalse};

def PyIsTrue obj! {};
method PyIsTrue   _^NULL       {False};
method PyIsTrue obj^BOOL       {obj};
method PyIsTrue obj^INT        {obj ~%Is 0};
method PyIsTrue obj^NUMBER     {obj ~= 0};
method PyIsTrue obj^COLLECTION {~IsEmpty obj};
method PyIsTrue obj^PY_OBJECT  {
   c_eval #callback [obj:(ToPython obj)] "
      PyObject *objPy;
      int result;
      objPy = ko_to_python(obj);
      KO_ENTER_FOREIGN_PYTHON();
      result = PyObject_IsTrue(objPy);
      KO_FAIL_PY_IN_FOREIGN_IF(result == -1);
      KO_LEAVE_FOREIGN_PYTHON();
      KO_RESULT = ko_bool(result);
   "
};
method PyIsTrue _ {True};

MethodFromPython PyBOOL ?obj {
   c_eval [obj] "
      PyObject *objPy;
      KO_CHECK_IS_PY_OBJECT(obj);
      KO_ENTER_PYTHON();
      objPy = ko_to_python(obj);
      if (objPy == Py_True)
         KO_RESULT = KO_True;
      else if (KO_LIKELY(objPy == Py_False))
         KO_RESULT = KO_False;
      else {
         PyErr_SetString(PyExc_TypeError, \"expected a boolean\");
         KO_FAIL_PY();
      }
      KO_LEAVE_PYTHON();
   "
};
