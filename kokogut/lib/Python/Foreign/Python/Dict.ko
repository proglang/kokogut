// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004,2006-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Foreign.Python.Dict =>

use Foreign.Python.Core [];

c_define "static KO_STATIC_PY_TYPE_OBJECT(ko_PyDICT, &PyDict_Type);";
let PyDICT = c_expr "KO_STATIC_PTR(ko_PyDICT)";

def PyDict [
   () {
      c_eval result "
         PyObject *dictPy;
         KO_ENTER_PYTHON();
         dictPy = PyDict_New();
         KO_FAIL_PY_IF(dictPy == NULL);
         result = ko_wrap_new_from_python(dictPy);
         KO_LEAVE_PYTHON();
      "
   }
   coll {
      let dict = PyDict();
      DoUnion dict coll;
      dict
   }
   colls... {
      let dict = PyDict();
      loop colls [
         (part\parts) {DoUnion dict part; again parts}
         _            {dict}
      ]
   }
];
