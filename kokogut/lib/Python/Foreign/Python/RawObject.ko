// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Foreign.Python.RawObject =>

use Foreign.Python.Core;

def RawPyCall [
   fun {
      c_stat [fun] "KO_CHECK_IS_PY_OBJECT(fun);";
      c_eval #callback result [fun] "
         PyObject *funPy, *argsPy, *resultPy;
         funPy = ko_to_python(fun);
         KO_ENTER_FOREIGN_PYTHON();
         argsPy = PyTuple_New(0);
         KO_FAIL_PY_IN_FOREIGN_IF(argsPy == NULL);
         resultPy = PyObject_Call(funPy, argsPy, NULL);
         Py_DECREF(argsPy);
         KO_LEAVE_FOREIGN_PYTHON_WRAP_NEW_FROM_PYTHON(result, resultPy);
      "
   }
   fun (arg1 & ~(_, _)) {
      c_stat [fun] "KO_CHECK_IS_PY_OBJECT(fun);";
      c_eval #callback result [fun arg1:(ToPython arg1)] "
         PyObject *funPy, *arg1Py, *argsPy, *resultPy;
         funPy = ko_to_python(fun);
         arg1Py = ko_to_python(arg1);
         KO_ENTER_FOREIGN_PYTHON();
         argsPy = PyTuple_New(1);
         KO_FAIL_PY_IN_FOREIGN_IF(argsPy == NULL);
         Py_INCREF(arg1Py);
         PyTuple_SET_ITEM(argsPy, 0, arg1Py);
         resultPy = PyObject_Call(funPy, argsPy, NULL);
         Py_DECREF(argsPy);
         KO_LEAVE_FOREIGN_PYTHON_WRAP_NEW_FROM_PYTHON(result, resultPy);
      "
   }
   fun (arg1 & ~(_, _)) (arg2 & ~(_, _)) {
      c_stat [fun] "KO_CHECK_IS_PY_OBJECT(fun);";
      c_eval #callback result [
         fun arg1:(ToPython arg1) arg2:(ToPython arg2)
      ] "
         PyObject *funPy, *arg1Py, *arg2Py, *argsPy, *resultPy;
         funPy = ko_to_python(fun);
         arg1Py = ko_to_python(arg1);
         arg2Py = ko_to_python(arg2);
         KO_ENTER_PYTHON();
         argsPy = PyTuple_New(2);
         KO_FAIL_PY_IN_FOREIGN_IF(argsPy == NULL);
         Py_INCREF(arg1Py);
         PyTuple_SET_ITEM(argsPy, 0, arg1Py);
         Py_INCREF(arg2Py);
         PyTuple_SET_ITEM(argsPy, 1, arg2Py);
         resultPy = PyObject_Call(funPy, argsPy, NULL);
         Py_DECREF(argsPy);
         KO_LEAVE_FOREIGN_PYTHON_WRAP_NEW_FROM_PYTHON(result, resultPy);
      "
   }
   fun (arg1 & ~(_, _)) (arg2 & ~(_, _)) (arg3 & ~(_, _)) {
      c_stat [fun] "KO_CHECK_IS_PY_OBJECT(fun);";
      c_eval #callback result [
         fun arg1:(ToPython arg1) arg2:(ToPython arg2) arg3:(ToPython arg3)
      ] "
         PyObject *funPy, *arg1Py, *arg2Py, *arg3Py, *argsPy, *resultPy;
         funPy = ko_to_python(fun);
         arg1Py = ko_to_python(arg1);
         arg2Py = ko_to_python(arg2);
         arg3Py = ko_to_python(arg3);
         KO_ENTER_PYTHON();
         argsPy = PyTuple_New(3);
         KO_FAIL_PY_IN_FOREIGN_IF(argsPy == NULL);
         Py_INCREF(arg1Py);
         PyTuple_SET_ITEM(argsPy, 0, arg1Py);
         Py_INCREF(arg2Py);
         PyTuple_SET_ITEM(argsPy, 1, arg2Py);
         Py_INCREF(arg3Py);
         PyTuple_SET_ITEM(argsPy, 2, arg3Py);
         resultPy = PyObject_Call(funPy, argsPy, NULL);
         Py_DECREF(argsPy);
         KO_LEAVE_FOREIGN_PYTHON_WRAP_NEW_FROM_PYTHON(result, resultPy);
      "
   }
   fun args... {
      c_stat [fun] "KO_CHECK_IS_PY_OBJECT(fun);";
      loop args [] Null [
         (x\xs) acc kwArgs {
            case x [
               (name, value) {
                  c_let [newKwArgs] [
                     kwArgs name:(PySymbol name) value:(ToPython value)
                  ] "
                     PyObject *kwArgsPy, *namePy, *valuePy;
                     int status;
                     KO_ENTER_PYTHON();
                     if (kwArgs == KO_Null) {
                        kwArgsPy = PyDict_New();
                        KO_FAIL_PY_IF(kwArgsPy == NULL);
                        newKwArgs = ko_wrap_new_from_python(kwArgsPy);
                     }
                     else
                        kwArgsPy = ko_to_python(newKwArgs = kwArgs);
                     namePy = ko_to_python(name);
                     valuePy = ko_to_python(value);
                     status = PyDict_SetItem(kwArgsPy, namePy, valuePy);
                     KO_FAIL_PY_IF(status == -1);
                     KO_LEAVE_PYTHON();
                  ";
                  again xs acc newKwArgs
               }
               _ {again xs (ToPython x \ acc) kwArgs}
            ]
         }
         _ args kwArgs {
            c_eval #callback result [fun args kwArgs] "
               PyObject *funPy, *argsPy, *kwArgsPy;
               ko_nint_t n;
               ko_value_t src;
               PyObject *resultPy;
               funPy = ko_to_python(fun);
               KO_ENTER_PYTHON();
               n = ko_nsize_of_list(args);
               argsPy = PyTuple_New(n);
               KO_FAIL_PY_IF(argsPy == NULL);
               src = args;
               while (n != 0) {
                  PyObject *argPy = ko_to_python(ko_first(src));
                  --n;
                  Py_INCREF(argPy);
                  PyTuple_SET_ITEM(argsPy, n, argPy);
                  src = ko_rest(src);
               }
               kwArgsPy = kwArgs == KO_Null ? NULL : ko_to_python(kwArgs);
               KO_ENTER_FOREIGN();
               resultPy = PyObject_Call(funPy, argsPy, kwArgsPy);
               Py_DECREF(argsPy);
               KO_LEAVE_FOREIGN_PYTHON_WRAP_NEW_FROM_PYTHON(result, resultPy);
            "
         }
      ]
   }
];

def RawPyAttr obj field {
   c_stat [obj] "KO_CHECK_IS_PY_OBJECT(obj);";
   c_eval #callback result [obj field:(PySymbol field)] "
      PyObject *objPy, *fieldPy, *valuePy;
      objPy = ko_to_python(obj);
      fieldPy = ko_to_python(field);
      KO_ENTER_FOREIGN_PYTHON();
      valuePy = PyObject_GetAttr(objPy, fieldPy);
      KO_LEAVE_FOREIGN_PYTHON_WRAP_NEW_FROM_PYTHON(result, valuePy);
   "
};

def RawPySpecialAttr obj field {
   c_stat [obj] "KO_CHECK_IS_PY_OBJECT(obj);";
   c_eval #callback result [obj field:(PySpecialSymbol field)] "
      PyObject *objPy, *fieldPy, *valuePy;
      objPy = ko_to_python(obj);
      fieldPy = ko_to_python(field);
      KO_ENTER_FOREIGN_PYTHON();
      valuePy = PyObject_GetAttr(objPy, fieldPy);
      KO_LEAVE_FOREIGN_PYTHON_WRAP_NEW_FROM_PYTHON(result, valuePy);
   "
};

def RawPyItem coll key {
   c_stat [coll] "KO_CHECK_IS_PY_OBJECT(coll);";
   c_eval #callback result [coll key:(ToPython key)] "
      PyObject *collPy, *keyPy, *valuePy;
      collPy = ko_to_python(coll);
      keyPy = ko_to_python(key);
      KO_ENTER_FOREIGN_PYTHON();
      valuePy = PyObject_GetItem(collPy, keyPy);
      KO_LEAVE_FOREIGN_PYTHON_WRAP_NEW_FROM_PYTHON(result, valuePy);
   "
};
