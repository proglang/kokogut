// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2006-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Foreign.Python.Slice =>

use Foreign.Python.Core;

c_define "static KO_STATIC_PY_TYPE_OBJECT(ko_PySLICE, &PySlice_Type);";
let PySLICE = c_expr "KO_STATIC_PTR(ko_PySLICE)";

def PySlice [
   end {
      c_eval result [end:(ToPython end)] "
         PyObject *endPy, *slicePy;
         KO_ENTER_PYTHON();
         endPy = ko_to_python(end);
         slicePy = PySlice_New(NULL, endPy, NULL);
         KO_FAIL_PY_IF(slicePy == NULL);
         result = ko_wrap_new_from_python(slicePy);
         KO_LEAVE_PYTHON();
      "
   }
   begin end {
      c_eval result [begin:(ToPython begin) end:(ToPython end)] "
         PyObject *beginPy, *endPy, *slicePy;
         KO_ENTER_PYTHON();
         beginPy = ko_to_python(begin);
         endPy = ko_to_python(end);
         slicePy = PySlice_New(beginPy, endPy, NULL);
         KO_FAIL_PY_IF(slicePy == NULL);
         result = ko_wrap_new_from_python(slicePy);
         KO_LEAVE_PYTHON();
      "
   }
   begin end step {
      c_eval result [
         begin:(ToPython begin) end:(ToPython end) step:(ToPython step)
      ] "
         PyObject *beginPy, *endPy, *stepPy, *slicePy;
         KO_ENTER_PYTHON();
         beginPy = ko_to_python(begin);
         endPy = ko_to_python(end);
         stepPy = ko_to_python(step);
         slicePy = PySlice_New(beginPy, endPy, stepPy);
         KO_FAIL_PY_IF(slicePy == NULL);
         result = ko_wrap_new_from_python(slicePy);
         KO_LEAVE_PYTHON();
      "
   }
];

def IsPySlice obj {
   c_eval [obj] "
      if (!ko_is_py_object(obj))
         KO_RESULT = KO_False;
      else {
         KO_ENTER_PYTHON();
         KO_RESULT = ko_bool(PySlice_Check(ko_to_python(obj)));
         KO_LEAVE_PYTHON();
      }
   "
};

def PySliceIndices slice size {
   c_let #callback [begin end step_:step sliceSize] [slice size] "
      ko_pysize_t sizeC;
      PyObject *slicePy;
      ko_pysize_t beginC, endC, stepC, sliceSizeC;
      int status;
      KO_CHECK_IS_PY_OBJECT(slice);
      slicePy = ko_to_python(slice);
      KO_INT_VALUE_GE0(sizeC, size, \"size\");
      KO_ENTER_FOREIGN_PYTHON();
      KO_CHECK_PY_TYPE_IN_FOREIGN(PySlice_Check(slicePy),
         \"expected a slice\");
      status = PySlice_GetIndicesEx((PySliceObject *)slicePy, sizeC,
         &beginC, &endC, &stepC, &sliceSizeC);
      KO_LEAVE_FOREIGN_PYTHON_FAIL_PY_IF(status == -1);
      begin = ko_int(beginC);
      end = ko_int(endC);
      step_ = ((PySliceObject *)slicePy)->step == Py_None ? KO_Null :
         ko_int(stepC);
      sliceSize = ko_int(sliceSizeC);
   ";
   begin, end, step, sliceSize
};
