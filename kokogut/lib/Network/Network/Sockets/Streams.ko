// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2005-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Network.Sockets.Streams =>

type SOCKET_INPUT is:BYTE_INPUT
   (private SocketInput) socket mutex (ref reading) (ref writing);

type SOCKET_OUTPUT is:BYTE_OUTPUT
   (private SocketOutput) socket mutex (ref reading) (ref writing);

TypeSerialization "Network.Sockets.SOCKET_INPUT" SOCKET_INPUT;
TypeSerialization "Network.Sockets.SOCKET_OUTPUT" SOCKET_OUTPUT;

def SocketStreams socket {
   let mutex = Mutex();
   var reading = True;
   var writing = True;
   SocketInput socket mutex (ref reading) (ref writing),
   SocketOutput socket mutex (ref reading) (ref writing)
};

method Close stream^SOCKET_INPUT {
   Lock stream.mutex =>
   if stream.reading {
      stream.reading = False;
      if ~stream.writing {Close stream.socket}
   }
};

method Close stream^SOCKET_OUTPUT {
   Lock stream.mutex =>
   if stream.writing {
      stream.writing = False;
      if ~stream.reading {Close stream.socket}
   }
};

method ReadSomeFrom stream^SOCKET_INPUT output maxSize {
   ReadSomeFrom stream.socket output maxSize
};

method CanReadFrom stream^SOCKET_INPUT {
   CanReadFrom stream.socket
};

method WriteSomeTo stream^SOCKET_OUTPUT input maxSize flush {
   WriteSomeTo stream.socket input maxSize flush
};

method WriteTo stream^SOCKET_OUTPUT obj {
   WriteTo stream.socket obj
};

method CanWriteTo stream^SOCKET_OUTPUT {
   CanWriteTo stream.socket
};