// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2005-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Network.Sockets.Families =>

c_define "#include <sys/socket.h>";

type FAMILY Family code;

c_export "
   KO_RECORD_FIELD(family, 0, code);
";

c_exportName [KO_FAMILY:FAMILY KO_FamilyCon:(c_con Family)];
c_export "
   KO_DESCR_CHECKER(family, KO_FamilyCon);

   #define KO_CHECK_IS_FAMILY(obj) \\
      KO_CHECK_TYPE(obj, ko_is_family(obj), KO_FAMILY)
";

RecordSerialization "Network.Sockets.FAMILY" FAMILY Family;

c_export "
   #ifdef AF_MAX
      #define KO_MAX_FAMILY AF_MAX
   #else
      #if HAVE_STRUCT_SOCKADDR_SA_LEN
         #define KO_MAX_FAMILY 0xFF
      #else
         #define KO_MAX_FAMILY 0xFFFF
      #endif
   #endif
";

let InetFamily = Family (c_expr "ko_sint(AF_INET)");
ifDefined HaveInet6Family {
   let Inet6Family = Family (c_expr "ko_sint(AF_INET6)");
};
let UnixFamily = Family (c_expr "ko_sint(AF_UNIX)");
