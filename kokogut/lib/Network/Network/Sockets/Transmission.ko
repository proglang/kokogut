// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2005-2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module Network.Sockets.Transmission =>

use Foreign.C;
use Network.Sockets.Addresses;
use Network.Sockets.Core [];

c_define "#include <sys/socket.h>";

def Receive socket output maxSize (->where Keyword
   peek:False
   oob:False
   waitAll:False
   = [peek oob waitAll])...
{
   loop =>
   c_case [] [socket output maxSize peek oob waitAll] "
      int fd;
      ko_value_t size;
      int flags;
      ko_value_t pos;
      ssize_t result;
      KO_CHECK_IS_SOCKET(socket);
      KO_SOCKET_FD(fd, socket);
      KO_CHECK_IS_BYTE_ARRAY(output);
      KO_CHECK_ARRAY_IS_UNLOCKED(output);
      KO_NORMALIZE_SIZE(size, maxSize);
      flags = 0;
      KO_IF(peek,    flags |= MSG_PEEK;,    ;);
      KO_IF(oob,     flags |= MSG_OOB;,     ;);
      KO_IF(waitAll, flags |= MSG_WAITALL;, ;);
      pos = ko_size_of_array(output);
      KO_ADD_LAST_PART_BYTE_ARRAY(output, size);
      result = recv(fd, ko_ptr_at_byte_array(output, pos),
         ko_nint_of_sint(size), flags);
      if (KO_UNLIKELY(result == -1)) {
         switch (errno) {
            case EINTR:
               ko_set_size_of_array(output, pos);
               goto interrupted;
            case KO_EAGAIN:
               ko_set_size_of_array(output, pos);
               goto wait;
            default:
               ko_set_size_of_array(output, pos);
               KO_FAIL_IO();
         }
      }
      if (KO_UNLIKELY(result == 0)) {
         ko_set_size_of_array(output, pos);
         goto last;
      }
      ko_set_size_of_array(output, ko_add_sn(pos, result));
      goto done;
   " [
      done:        {False}
      last:        {True}
      interrupted: {ProcessSystemSignalsHandleSync(); again()}
      wait:        {WaitForReading socket; again()}
   ]
};

def ReceiveFrom socket output maxSize (->where Keyword
   peek:False
   oob:False
   waitAll:False
   = [peek oob waitAll])...
{
   let address = AllocateAddress();
   loop =>
   c_case [addressOrNull] [socket output maxSize peek oob waitAll address] "
      int fd;
      ko_value_t size;
      int flags;
      ko_value_t pos;
      ssize_t result;
      KO_CHECK_IS_SOCKET(socket);
      KO_SOCKET_FD(fd, socket);
      KO_CHECK_IS_BYTE_ARRAY(output);
      KO_CHECK_ARRAY_IS_UNLOCKED(output);
      KO_NORMALIZE_SIZE(size, maxSize);
      flags = 0;
      KO_IF(peek,    flags |= MSG_PEEK;,    ;);
      KO_IF(oob,     flags |= MSG_OOB;,     ;);
      KO_IF(waitAll, flags |= MSG_WAITALL;, ;);
      pos = ko_size_of_array(output);
      KO_ADD_LAST_PART_BYTE_ARRAY(output, size);
      result = recvfrom(fd, ko_ptr_at_byte_array(output, pos),
         ko_nint_of_sint(size), flags,
         ko_addr_of_address(address), ko_len_ptr_of_address(address));
      if (KO_UNLIKELY(result == -1)) {
         switch (errno) {
            case EINTR:
               ko_set_size_of_array(output, pos);
               goto interrupted;
            case KO_EAGAIN:
               ko_set_size_of_array(output, pos);
               goto wait;
            default:
               ko_set_size_of_array(output, pos);
               KO_FAIL_IO();
         }
      }
      ko_set_size_of_array(output, ko_add_sn(pos, result));
      addressOrNull = ko_realloc_address(address);
      goto done;
   " [
      done:        {addressOrNull}
      interrupted: {ProcessSystemSignalsHandleSync(); again()}
      wait:        {WaitForReading socket; again()}
   ]
};

let CanReceive = CanReadFrom;

def Send socket input maxSize (->where Keyword
   eor:False
   oob:False
   = [eor oob])...
{
   loop =>
   c_case [] [socket input maxSize eor oob] "
      int fd;
      ko_value_t size;
      int flags;
      ssize_t result;
      KO_CHECK_IS_SOCKET(socket);
      KO_SOCKET_FD(fd, socket);
      KO_CHECK_IS_BYTE_ARRAY(input);
      KO_CHECK_ARRAY_IS_UNLOCKED(input);
      KO_NORMALIZE_BUFFER_SIZE(size, maxSize, ko_size_of_array(input));
      flags = 0;
      KO_IF(eor, flags |= MSG_EOR;, ;);
      KO_IF(oob, flags |= MSG_OOB;, ;);
      result = send(fd, ko_elems_of_byte_array(input),
         ko_nint_of_sint(size), flags);
      if (KO_UNLIKELY(result == -1)) {
         switch (errno) {
            case EINTR:
               goto interrupted;
            case KO_EAGAIN:
               goto wait;
            default:
               KO_FAIL_IO();
         }
      }
      KO_REMOVE_FIRST_PART_BYTE_ARRAY(input, ko_sint(result));
      goto done;
   " [
      done:        {}
      interrupted: {ProcessSystemSignalsHandleSync(); again()}
      wait:        {WaitForWriting socket; again()}
   ]
};

def SendTo socket address input maxSize (->where Keyword
   eor:False
   oob:False
   = [eor oob])...
{
   loop =>
   c_case [] [socket address input maxSize eor oob] "
      int fd;
      ko_value_t size;
      int flags;
      ssize_t result;
      KO_CHECK_IS_SOCKET(socket);
      KO_SOCKET_FD(fd, socket);
      KO_CHECK_IS_ADDRESS(address);
      KO_CHECK_IS_BYTE_ARRAY(input);
      KO_CHECK_ARRAY_IS_UNLOCKED(input);
      KO_NORMALIZE_BUFFER_SIZE(size, maxSize, ko_size_of_array(input));
      flags = 0;
      KO_IF(eor, flags |= MSG_EOR;, ;);
      KO_IF(oob, flags |= MSG_OOB;, ;);
      result = sendto(fd, ko_elems_of_byte_array(input),
         ko_nint_of_sint(size), flags,
         ko_addr_of_address(address), ko_len_of_address(address));
      if (KO_UNLIKELY(result == -1)) {
         switch (errno) {
            case EINTR:
               goto interrupted;
            case KO_EAGAIN:
               goto wait;
            default:
               KO_FAIL_IO();
         }
      }
      KO_REMOVE_FIRST_PART_BYTE_ARRAY(input, ko_sint(result));
      goto done;
   " [
      done:        {}
      interrupted: {ProcessSystemSignalsHandleSync(); again()}
      wait:        {WaitForWriting socket; again()}
   ]
};

let CanSend = CanWriteTo;
