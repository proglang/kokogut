// This file is a part of the Kokogut library.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004,2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// The Kokogut library is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version;
// with a "linking exception".
//
// The linking exception allows you to link a "work that uses the
// Library" with a publicly distributed version of the Library to
// produce an executable file containing portions of the Library,
// and distribute that executable file under terms of your choice,
// without any of the additional requirements listed in section 6
// of LGPL version 2 or section 4 of LGPL version 3.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module System.Errors =>

let E2Big           = IOError (c_expr "ko_sint(E2BIG)");
let EAcces          = IOError (c_expr "ko_sint(EACCES)");
let EAddrInUse      = IOError (c_expr "ko_sint(EADDRINUSE)");
let EAddrNotAvail   = IOError (c_expr "ko_sint(EADDRNOTAVAIL)");
let EAFNoSupport    = IOError (c_expr "ko_sint(EAFNOSUPPORT)");
let EAgain          = IOError (c_expr "ko_sint(EAGAIN)");
let EAlready        = IOError (c_expr "ko_sint(EALREADY)");
let EBadF           = IOError (c_expr "ko_sint(EBADF)");
ifDefined HaveEBadMsg {let EBadMsg = IOError (c_expr "ko_sint(EBADMSG)")};
let EBusy           = IOError (c_expr "ko_sint(EBUSY)");
ifDefined HaveECanceled {let ECanceled = IOError (c_expr "ko_sint(ECANCELED)")};
let EChild          = IOError (c_expr "ko_sint(ECHILD)");
let EConnAborted    = IOError (c_expr "ko_sint(ECONNABORTED)");
let EConnRefused    = IOError (c_expr "ko_sint(ECONNREFUSED)");
let EConnReset      = IOError (c_expr "ko_sint(ECONNRESET)");
let EDeadlk         = IOError (c_expr "ko_sint(EDEADLK)");
let EDestAddrReq    = IOError (c_expr "ko_sint(EDESTADDRREQ)");
let EDom            = IOError (c_expr "ko_sint(EDOM)");
let EDQuot          = IOError (c_expr "ko_sint(EDQUOT)");
let EExist          = IOError (c_expr "ko_sint(EEXIST)");
let EFault          = IOError (c_expr "ko_sint(EFAULT)");
let EFBig           = IOError (c_expr "ko_sint(EFBIG)");
let EHostUnreach    = IOError (c_expr "ko_sint(EHOSTUNREACH)");
ifDefined HaveEIdRm {let EIdRm = IOError (c_expr "ko_sint(EIDRM)")};
ifDefined HaveEIlSeq {let EIlSeq = IOError (c_expr "ko_sint(EILSEQ)")};
let EInProgress     = IOError (c_expr "ko_sint(EINPROGRESS)");
let EIntr           = IOError (c_expr "ko_sint(EINTR)");
let EInval          = IOError (c_expr "ko_sint(EINVAL)");
let EIO             = IOError (c_expr "ko_sint(EIO)");
let EIsConn         = IOError (c_expr "ko_sint(EISCONN)");
let EIsDir          = IOError (c_expr "ko_sint(EISDIR)");
let ELoop           = IOError (c_expr "ko_sint(ELOOP)");
let EMFile          = IOError (c_expr "ko_sint(EMFILE)");
let EMLink          = IOError (c_expr "ko_sint(EMLINK)");
let EMsgSize        = IOError (c_expr "ko_sint(EMSGSIZE)");
ifDefined HaveEMultiHop {let EMultiHop = IOError (c_expr "ko_sint(EMULTIHOP)")};
let ENameTooLong    = IOError (c_expr "ko_sint(ENAMETOOLONG)");
let ENetDown        = IOError (c_expr "ko_sint(ENETDOWN)");
ifDefined HaveENetReset {let ENetReset = IOError (c_expr "ko_sint(ENETRESET)")};
let ENetUnreach     = IOError (c_expr "ko_sint(ENETUNREACH)");
let ENFile          = IOError (c_expr "ko_sint(ENFILE)");
let ENoBufS         = IOError (c_expr "ko_sint(ENOBUFS)");
ifDefined HaveENoData {let ENoData = IOError (c_expr "ko_sint(ENODATA)")};
let ENoDev          = IOError (c_expr "ko_sint(ENODEV)");
let ENoEnt          = IOError (c_expr "ko_sint(ENOENT)");
let ENoExec         = IOError (c_expr "ko_sint(ENOEXEC)");
let ENoLck          = IOError (c_expr "ko_sint(ENOLCK)");
ifDefined HaveENoLink {let ENoLink = IOError (c_expr "ko_sint(ENOLINK)")};
let ENoMem          = IOError (c_expr "ko_sint(ENOMEM)");
ifDefined HaveENoMsg {let ENoMsg = IOError (c_expr "ko_sint(ENOMSG)")};
let ENoProtoOpt     = IOError (c_expr "ko_sint(ENOPROTOOPT)");
let ENoSpc          = IOError (c_expr "ko_sint(ENOSPC)");
ifDefined HaveENoSR {let ENoSR = IOError (c_expr "ko_sint(ENOSR)")};
ifDefined HaveENoStr {let ENoStr = IOError (c_expr "ko_sint(ENOSTR)")};
let ENoSys          = IOError (c_expr "ko_sint(ENOSYS)");
let ENotConn        = IOError (c_expr "ko_sint(ENOTCONN)");
let ENotDir         = IOError (c_expr "ko_sint(ENOTDIR)");
let ENotEmpty       = IOError (c_expr "ko_sint(ENOTEMPTY)");
let ENotSock        = IOError (c_expr "ko_sint(ENOTSOCK)");
ifDefined HaveENotSup {let ENotSup = IOError (c_expr "ko_sint(ENOTSUP)")};
let ENoTTY          = IOError (c_expr "ko_sint(ENOTTY)");
let ENXIO           = IOError (c_expr "ko_sint(ENXIO)");
let EOpNotSupp      = IOError (c_expr "ko_sint(EOPNOTSUPP)");
ifDefined HaveEOverflow {let EOverflow = IOError (c_expr "ko_sint(EOVERFLOW)")};
let EPerm           = IOError (c_expr "ko_sint(EPERM)");
let EPipe           = IOError (c_expr "ko_sint(EPIPE)");
ifDefined HaveEProto {let EProto = IOError (c_expr "ko_sint(EPROTO)")};
let EProtoNoSupport = IOError (c_expr "ko_sint(EPROTONOSUPPORT)");
let EProtoType      = IOError (c_expr "ko_sint(EPROTOTYPE)");
let ERange          = IOError (c_expr "ko_sint(ERANGE)");
let EROFS           = IOError (c_expr "ko_sint(EROFS)");
let ESPipe          = IOError (c_expr "ko_sint(ESPIPE)");
let ESrch           = IOError (c_expr "ko_sint(ESRCH)");
let EStale          = IOError (c_expr "ko_sint(ESTALE)");
ifDefined HaveETime {let ETime = IOError (c_expr "ko_sint(ETIME)")};
let ETimedOut       = IOError (c_expr "ko_sint(ETIMEDOUT)");
let ETxtBsy         = IOError (c_expr "ko_sint(ETXTBSY)");
let EWouldBlock     = IOError (c_expr "ko_sint(EWOULDBLOCK)");
let EXDev           = IOError (c_expr "ko_sint(EXDEV)");
