/*
 * This file is a part of the Kokogut library.
 *
 * Kokogut is a compiler of the Kogut programming language.
 * Copyright (C) 2004-2005,2007 by Marcin 'Qrczak' Kowalczyk
 * (QrczakMK@gmail.com)
 *
 * The Kokogut library is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version;
 * with a "linking exception".
 *
 * The linking exception allows you to link a "work that uses the
 * Library" with a publicly distributed version of the Library to
 * produce an executable file containing portions of the Library,
 * and distribute that executable file under terms of your choice,
 * without any of the additional requirements listed in section 6
 * of LGPL version 2 or section 4 of LGPL version 3.
 *
 * Kokogut is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef KOKOGUT_FEATURES_H
#define KOKOGUT_FEATURES_H

#if !defined(_POSIX_C_SOURCE) || _POSIX_C_SOURCE < 200112L
   #undef _POSIX_C_SOURCE
   #define _POSIX_C_SOURCE 200112L
#endif
/* Needed on some Linuxes for struct timespec in time.h. */

#if !defined(_XOPEN_SOURCE) || _XOPEN_SOURCE < 600
   #undef _XOPEN_SOURCE
   #define _XOPEN_SOURCE 600
#endif
/* 500 needed for blksize_t, 600 needed for Python. */

#define __EXTENSIONS__ 1
/* Needed on Solaris for struct timeval in sys/time.h
 * when _XOPEN_SOURCE is also defined. */

#endif /* KOKOGUT_FEATURES_H */
