/*
 * This file is a part of the Kokogut library.
 *
 * Kokogut is a compiler of the Kogut programming language.
 * Copyright (C) 2004-2009 by Marcin 'Qrczak' Kowalczyk
 * (QrczakMK@gmail.com)
 *
 * The Kokogut library is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version;
 * with a "linking exception".
 *
 * The linking exception allows you to link a "work that uses the
 * Library" with a publicly distributed version of the Library to
 * produce an executable file containing portions of the Library,
 * and distribute that executable file under terms of your choice,
 * without any of the additional requirements listed in section 6
 * of LGPL version 2 or section 4 of LGPL version 3.
 *
 * Kokogut is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef KOKOGUT_RUNTIME_H
#define KOKOGUT_RUNTIME_H

#include <kokogut/features.h>
#include <kokogut/config.h>
#include <limits.h>
#if HAVE_INTTYPES_H
   #include <inttypes.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#if KO_INCLUDE_STDIO_H
   #include <stdio.h>
   /* <stdio.h> is needed before <gmp.h> for mpz_out_str prototype. */
#endif
#include <gmp.h>

#ifdef __cplusplus
extern "C" {
#endif

#if CHAR_BIT != 8
   #error CHAR_BIT must be 8
#endif

#if HAVE_INTTYPES_H

   typedef int8_t ko_int8_t;
   typedef uint8_t ko_uint8_t;
   #ifdef INT8_MAX
      #define KO_INT8_MIN INT8_MIN
      #define KO_INT8_MAX INT8_MAX
      #define KO_UINT8_MAX UINT8_MAX
   #else
      #define KO_INT8_MIN (-0x80)
      #define KO_INT8_MAX 0x7F
      #define KO_UINT8_MAX 0xFFu
   #endif

   typedef int16_t ko_int16_t;
   typedef uint16_t ko_uint16_t;
   #ifdef INT16_MAX
      #define KO_INT16_MIN INT16_MIN
      #define KO_INT16_MAX INT16_MAX
      #define KO_UINT16_MAX UINT16_MAX
   #else
      #define KO_INT16_MIN (-0x7FFF-1)
      #define KO_INT16_MAX 0x7FFF
      #define KO_UINT16_MAX 0xFFFFu
   #endif

   typedef int32_t ko_int32_t;
   typedef uint32_t ko_uint32_t;
   #ifdef INT32_MAX
      #define KO_INT32_MIN INT32_MIN
      #define KO_INT32_MAX INT32_MAX
      #define KO_UINT32_MAX UINT32_MAX
   #else
      #define KO_INT32_MIN (-0x7FFFFFFF-1)
      #define KO_INT32_MAX 0x7FFFFFFF
      #define KO_UINT32_MAX 0xFFFFFFFFu
   #endif

   #if HAVE_INT64_T
      #define KO_HAVE_INT64 1
      typedef int64_t ko_int64_t;
      typedef uint64_t ko_uint64_t;
      #ifdef INT64_MAX
         #define KO_INT64_MIN INT64_MIN
         #define KO_INT64_MAX INT64_MAX
         #define KO_UINT64_MAX UINT64_MAX
      #else
         #define KO_INT64_MIN (-0x7FFFFFFFFFFFFFFF-1)
         #define KO_INT64_MAX 0x7FFFFFFFFFFFFFFF
         #define KO_UINT64_MAX 0xFFFFFFFFFFFFFFFFu
      #endif
   #endif

   #if HAVE_INT128_T
      #define KO_HAVE_INT128 1
      typedef int128_t ko_int128_t;
      typedef uint128_t ko_uint128_t;
      #ifdef INT128_MAX
         #define KO_INT128_MIN INT128_MIN
         #define KO_INT128_MAX INT128_MAX
         #define KO_UINT128_MAX UINT128_MAX
      #else
         #define KO_INT128_MIN (-0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF-1)
         #define KO_INT128_MAX 0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
         #define KO_UINT128_MAX 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFu
      #endif
   #endif

#else

   typedef signed char ko_int8_t;
   typedef unsigned char ko_uint8_t;
   #define KO_INT8_MIN CHAR_MIN
   #define KO_INT8_MAX CHAR_MAX
   #define KO_UINT8_MAX UCHAR_MAX

   #if SIZEOF_SHORT == 2
      typedef short ko_int16_t;
      typedef unsigned short ko_uint16_t;
      #define KO_INT16_MIN SHRT_MIN
      #define KO_INT16_MAX SHRT_MAX
      #define KO_UINT16_MAX USHRT_MAX
   #else
      #error A 16 bit integer type is required
   #endif

   #if SIZEOF_INT == 4
      typedef int ko_int32_t;
      typedef unsigned ko_uint32_t;
      #define KO_INT32_MIN INT_MIN
      #define KO_INT32_MAX INT_MAX
      #define KO_UINT32_MAX UINT_MAX
   #elif SIZEOF_LONG == 4
      typedef long ko_int32_t;
      typedef unsigned long ko_uint32_t;
      #define KO_INT32_MIN LONG_MIN
      #define KO_INT32_MAX LONG_MAX
      #define KO_UINT32_MAX ULONG_MAX
   #else
      #error A 32 bit integer type is required
   #endif

   #if SIZEOF_LONG == 8
      #define KO_HAVE_INT64
      typedef long ko_int64_t;
      typedef unsigned long ko_uint64_t;
      #define KO_INT64_MIN LONG_MIN
      #define KO_INT64_MAX LONG_MAX
      #define KO_UINT64_MAX ULONG_MAX
   #elif HAVE_LONG_LONG && SIZEOF_LONG_LONG == 8
      #define KO_HAVE_INT64
      typedef long long ko_int64_t;
      typedef unsigned long long ko_uint64_t;
      #ifdef LLONG_MAX
         #define KO_INT64_MIN LLONG_MIN
         #define KO_INT64_MAX LLONG_MAX
         #define KO_UINT64_MAX ULLONG_MAX
      #else
         #define KO_INT64_MIN (-0x7FFFFFFFFFFFFFFFLL-1)
         #define KO_INT64_MAX 0x7FFFFFFFFFFFFFFFLL
         #define KO_UINT64_MAX 0xFFFFFFFFFFFFFFFFuLL
      #endif
   #endif

#endif

#if !KO_HAVE_INT64 && HAVE___INT64_T
   #define KO_HAVE_INT64 1
   typedef __int64_t ko_int64_t;
   typedef __uint64_t ko_uint64_t;
   #define KO_INT64_MIN ((ko_int64_t)-1<<63)
   #define KO_INT64_MAX (-(((ko_int64_t)-1<<63)+1))
   #define KO_UINT64_MAX ((ko_uint64_t)-1)
#endif

#if !KO_HAVE_INT128 && HAVE___INT128_T
   #define KO_HAVE_INT128 1
   typedef __int128_t ko_int128_t;
   typedef __uint128_t ko_uint128_t;
   #define KO_INT128_MIN ((ko_int128_t)-1<<127)
   #define KO_INT128_MAX (-(((ko_int128_t)-1<<127)+1))
   #define KO_UINT128_MAX ((ko_uint128_t)-1)
#endif

#define KO_SIZEOF_PTR SIZEOF_VOID_P

#if KO_SIZEOF_PTR < SIZEOF_INT
   #error A pointer must not be smaller than an int
#elif KO_SIZEOF_PTR < SIZEOF_LONG
   #if SIZEOF_INT < 4
      #error int type must have at least 32 bits
   #endif
   typedef int ko_nint_t;
   typedef unsigned ko_unint_t;
   #define KO_SIZEOF_NINT SIZEOF_INT
   #define KO_NINT_MIN INT_MIN
   #define KO_NINT_MAX INT_MAX
   #define KO_UNINT_MAX UINT_MAX
#else
   typedef long ko_nint_t;
   typedef unsigned long ko_unint_t;
   #define KO_SIZEOF_NINT SIZEOF_LONG
   #define KO_NINT_MIN LONG_MIN
   #define KO_NINT_MAX LONG_MAX
   #define KO_UNINT_MAX ULONG_MAX
#endif

#define KO_BITS_IN_NINT (KO_SIZEOF_NINT * CHAR_BIT)

#if 2 * KO_SIZEOF_NINT == 8 && KO_HAVE_INT64
   #define KO_HAVE_DINT 1
   typedef ko_int64_t ko_dint_t;
   typedef ko_uint64_t ko_udint_t;
   #define KO_SIZEOF_DINT 8
   #define KO_DINT_MIN KO_INT64_MIN
   #define KO_DINT_MAX KO_INT64_MAX
   #define KO_UDINT_MAX KO_UINT64_MAX
#elif 2 * KO_SIZEOF_NINT == 16 && KO_HAVE_INT128
   #define KO_HAVE_DINT 1
   typedef ko_int128_t ko_dint_t;
   typedef ko_uint128_t ko_udint_t;
   #define KO_SIZEOF_DINT 16
   #define KO_DINT_MIN KO_INT128_MIN
   #define KO_DINT_MAX KO_INT128_MAX
   #define KO_UDINT_MAX KO_UINT128_MAX
#endif

/* Don't use bool from <stdbool.h> because gcc generates poor code for it.
 * Don't use int directly because of readability. */
typedef int ko_bool_t;
#define ko_false 0
#define ko_true 1

typedef ko_uint8_t ko_byte_t;
typedef ko_uint8_t ko_nchar_t;
#define KO_NCHARS 256
typedef ko_uint32_t ko_wchar_t;

#define KO_FILL_UNINT_BYTES(x) ((x) * ((ko_unint_t)-1 / 0xFF))

#if KO_HAVE_BUILTIN_EXPECT
   #define KO_LIKELY(cond) __builtin_expect(cond, ko_true)
   #define KO_UNLIKELY(cond) __builtin_expect(cond, ko_false)
#else
   #define KO_LIKELY(cond) (cond)
   #define KO_UNLIKELY(cond) (cond)
#endif

#if KO_HAVE_ATTR_REGPARM
   #define KO_REGPARM(n) __attribute__((regparm(n)))
#else
   #define KO_REGPARM(n)
#endif

#if KO_HAVE_ATTR_NORETURN
   #define KO_NORETURN __attribute__((noreturn))
#else
   #define KO_NORETURN
#endif

#if KO_HAVE_ATTR_PACKED
   #define KO_PACKED __attribute__((packed))
#else
   #define KO_PACKED
#endif

#define KO_MAX_FAST_ARITY 8
#define KO_NUM_REGISTERS 24

#define KO_STAT_BEGIN do
#define KO_STAT_END while (0)
#define KO_DEF_END void ko_dummy_def(void)

/*
 * Executable Code
 * ---------------
 */

typedef enum {
   KO_CALLBACKS_SAFE, KO_CALLBACKS_UNSAFE, KO_CALLBACKS_UNSAFE_IN_FINALIZER
} ko_callback_safety_t;
extern ko_callback_safety_t ko_callback_safety;

#define KO_RETURN return
#define KO_RETURN_FROM_CALLBACK(value)          \
   KO_STAT_BEGIN {                              \
      ko_callback_safety = KO_CALLBACKS_UNSAFE; \
      return value;                             \
   } KO_STAT_END
/* KO_RETURN is temporarily redefined to KO_RETURN_FROM_CALLBACK
 * in C fragments marked as #callback. */

typedef void (*ko_fun_ptr)(void);
typedef ko_fun_ptr (*ko_code_t)(void);
#define KO_CODE(name) ko_fun_ptr name(void)

#if KO_USE_MANGLER
   #define KO_JUMP(code)                \
      KO_STAT_BEGIN {                   \
         asm("/* KO_JUMP */");          \
         KO_RETURN((ko_fun_ptr)(code)); \
      } KO_STAT_END
#else
   #define KO_JUMP(code) KO_RETURN((ko_fun_ptr)(code))
#endif

#define KO_STRING_LIT_AUX(x) #x
#define KO_STRING_LIT(x) KO_STRING_LIT_AUX(x)
#define KO_LOCATION __FILE__ ":" KO_STRING_LIT(__LINE__)

#if __STDC_VERSION__ >= 199901L
   #define KO_DEFAULT_FUNCTION_NAME __func__
#elif __GNUC__ >= 2
   #define KO_DEFAULT_FUNCTION_NAME __FUNCTION__
#else
   #define KO_DEFAULT_FUNCTION_NAME NULL
#endif

#define KO_FUNCTION_NAME KO_DEFAULT_FUNCTION_NAME
/* KO_FUNCTION_NAME is temporarily redefined in C fragments. */

KO_CODE(ko_finished);
#if KO_GOOD_MANGLER
   static inline void ko_enter(ko_code_t code) {code();}
#else
   void ko_enter(ko_code_t code);
#endif
void ko_call(ko_code_t code, const char *loc, const char *fun);
#define KO_CALL(code) ko_call(code, KO_LOCATION, KO_FUNCTION_NAME)
extern ko_bool_t ko_failed;

extern KO_NORETURN void ko_c_case_end(const char *loc, const char *fun);

/*
 * Object Descriptors
 * ------------------
 */

typedef struct ko_descr *ko_descr_t;
typedef ko_descr_t *ko_value_t;

#define KO_OBJECT ko_descr_t ko_obj_descr

struct ko_descr {
   KO_OBJECT;
   ko_value_t (*ko_move)(ko_value_t src, ko_value_t dest) KO_REGPARM(2);
   void (*ko_finalize)(ko_value_t obj) KO_REGPARM(1);
   void (*ko_save_minor)(ko_value_t obj) KO_REGPARM(1);
   void (*ko_save_major)(ko_value_t obj) KO_REGPARM(1);
   ko_value_t ko_type;
   ko_code_t ko_fast_entries[KO_MAX_FAST_ARITY + 1];
   ko_code_t ko_list_entry;
};

#define KO_STATIC_PTR(obj) (&(obj).ko_obj_descr)

static inline ko_descr_t
ko_descr_of_ptr(ko_value_t obj) {
   return *obj;
}

static inline void
ko_init_descr(ko_value_t obj, ko_descr_t descr) {
   *obj = descr;
}

extern struct ko_descr ko_descr_descr;

/*
 * Immediate Values
 * ----------------
 */

#define KO_DUMMY ((ko_value_t)0)
#define KO_ABSENT ((ko_value_t)0)
#define ko_sint(i) ((ko_value_t)(((ko_nint_t)(i) << 1) + 1))

static inline ko_bool_t
ko_is_sint(ko_value_t obj) {
   return ((ko_nint_t)obj & 1) != 0;
}

static inline ko_bool_t
ko_is_pointer(ko_value_t obj) {
   return ((ko_nint_t)obj & 1) == 0;
}

static inline ko_nint_t
ko_nint_of_sint(ko_value_t x) {
   return (ko_nint_t)x >> 1;
}

/*
 * Objects with Fields
 * -------------------
 */

#define KO_SHIFT_PTR(type, ptr, offset) \
   ((type)((unsigned char *)(ptr) + (offset)))

#define KO_DIFF_PTR(ptr1, ptr2) \
   ((unsigned char *)(ptr1) - (unsigned char *)(ptr2))

static inline ko_nint_t
ko_values_sint_to_bytes(ko_value_t n) {
   return ((ko_nint_t)n - 1) * (KO_SIZEOF_PTR / 2);
}

static inline ko_value_t *
ko_shift_values(ko_value_t *ptr, ko_value_t i) {
   return KO_SHIFT_PTR(ko_value_t *, ptr, ko_values_sint_to_bytes(i));
}

static inline ko_value_t *
ko_shift_values_back(ko_value_t *ptr, ko_value_t i) {
   return KO_SHIFT_PTR(ko_value_t *, ptr, -ko_values_sint_to_bytes(i));
}

#if KO_HAVE_FLEXIBLE_ARRAY_MEMBERS
   typedef struct {
      KO_OBJECT;
      ko_value_t ko_fields[];
   } ko_object_t;
   #define KO_OBJECT_T(size) ko_object_t
   typedef ko_object_t ko_object_t_0;
#else
   #define KO_OBJECT_T(size)         \
      struct {                       \
         KO_OBJECT;                  \
         ko_value_t ko_fields[size]; \
      }
   typedef KO_OBJECT_T(256) ko_object_t;
   typedef struct {KO_OBJECT;} ko_object_t_0;
#endif

typedef KO_OBJECT_T(1) ko_object_t_1;
typedef KO_OBJECT_T(2) ko_object_t_2;
typedef KO_OBJECT_T(3) ko_object_t_3;
typedef KO_OBJECT_T(4) ko_object_t_4;
typedef KO_OBJECT_T(5) ko_object_t_5;
typedef KO_OBJECT_T(6) ko_object_t_6;
typedef KO_OBJECT_T(7) ko_object_t_7;
typedef KO_OBJECT_T(8) ko_object_t_8;

static inline ko_value_t *
ko_field_ptr(ko_value_t obj, ko_nint_t i) {
   return &((ko_object_t *)obj)->ko_fields[i];
}

static inline ko_value_t
ko_field(ko_value_t obj, ko_nint_t i) {
   return *ko_field_ptr(obj, i);
}

static inline void
ko_init_field(ko_value_t obj, ko_nint_t i, ko_value_t value) {
   *ko_field_ptr(obj, i) = value;
}

static inline ko_value_t
ko_offset(ko_value_t obj, ko_nint_t offset) {
   return KO_SHIFT_PTR(ko_value_t, obj, offset * KO_SIZEOF_PTR);
}

/*
 * Global Names
 * ------------
 */

#define KO_GLOBAL(name, module) Ko_##name##_ptr_##module
#define KO_STATIC(name, module) (&Ko_##name##_obj_##module.ko_obj_descr)
#define KO_DESCR(name, module) (&Ko_##name##_descr_##module)
#define KO_ENTRY(name, module, arity) Ko_##name##_entry##arity##_##module
#define KO_LIST_ENTRY(name, module) Ko_##name##_entry_##module

#define KO_DECLARE_GLOBAL(name, module) \
   extern ko_value_t Ko_##name##_ptr_##module
#define KO_DECLARE_STATIC(name, module, size) \
   extern ko_object_t_##size Ko_##name##_obj_##module
#define KO_DECLARE_DESCR(name, module) \
   extern struct ko_descr Ko_##name##_descr_##module
#define KO_DECLARE_ENTRY(name, module, arity) \
   KO_CODE(Ko_##name##_entry##arity##_##module)
#define KO_DECLARE_LIST_ENTRY(name, module) \
   KO_CODE(Ko_##name##_entry##_##module)

#define KO_DECLARE_TYPE(name, module) KO_DECLARE_STATIC(name, module, 4)

/*
 * Getting the Object Descriptor
 * -----------------------------
 */

KO_DECLARE_DESCR(SIntCon, Ints_Numbers_Kokogut);

static inline ko_descr_t
ko_descr(ko_value_t obj) {
   return KO_UNLIKELY(ko_is_sint(obj)) ?
      KO_DESCR(SIntCon, Ints_Numbers_Kokogut) :
      ko_descr_of_ptr(obj);
}

static inline ko_bool_t
ko_descr_is(ko_value_t obj, ko_descr_t descr) {
   return ko_is_pointer(obj) && ko_descr_of_ptr(obj) == descr;
}

/*
 * Making field accessors
 * ----------------------
 */

#define KO_RECORD_FIELD(type, offset, field)                                \
   static inline ko_value_t                                                 \
   ko_##field##_of_##type(ko_value_t obj_) {                                \
      return ko_field(obj_, offset);                                        \
   }                                                                        \
   static inline ko_value_t *                                               \
   ko_##field##_ptr_of_##type(ko_value_t obj_) {                            \
      return ko_field_ptr(obj_, offset);                                    \
   }                                                                        \
   static inline void                                                       \
   ko_init_##field##_of_##type(ko_value_t obj_, ko_value_t value_) {        \
      ko_init_field(obj_, offset, value_);                                  \
   }                                                                        \
   static inline void                                                       \
   ko_set_##field##_of_##type(ko_value_t obj_, ko_value_t value_) {         \
      ko_set_field(obj_, offset, value_);                                   \
   }                                                                        \
   static inline void                                                       \
   ko_set_##field##_of_##type##_nogc(ko_value_t obj_, ko_value_t value_) {  \
      ko_set_field_nogc(obj_, offset, value_);                              \
   } KO_DEF_END

#define KO_RECORD_FIELD_RAW(type, offset, field)                           \
   static inline ko_value_t                                                \
   ko_##field##_of_##type(ko_value_t obj_) {                               \
      return ko_field(obj_, offset);                                       \
   }                                                                       \
   static inline ko_value_t *                                              \
   ko_##field##_ptr_of_##type(ko_value_t obj_) {                           \
      return ko_field_ptr(obj_, offset);                                   \
   }                                                                       \
   static inline void                                                      \
   ko_init_##field##_of_##type(ko_value_t obj_, ko_value_t value_) {       \
      ko_init_field(obj_, offset, value_);                                 \
   }                                                                       \
   static inline void                                                      \
   ko_set_##field##_of_##type(ko_value_t obj_, ko_value_t value_) {        \
      ko_init_field(obj_, offset, value_);                                 \
   }                                                                       \
   static inline void                                                      \
   ko_set_##field##_of_##type##_nogc(ko_value_t obj_, ko_value_t value_) { \
      ko_init_field(obj_, offset, value_);                                 \
   } KO_DEF_END

#define KO_OBJECT_FIELD(type, field)                                         \
   static inline ko_value_t                                                  \
   ko_##field##_of_##type(ko_value_t obj_) {                                 \
      return ((struct ko_##type *)obj_)->field;                              \
   }                                                                         \
   static inline ko_value_t *                                                \
   ko_##field##_ptr_of_##type(ko_value_t obj_) {                             \
      return &((struct ko_##type *)obj_)->field;                             \
   }                                                                         \
   static inline void                                                        \
   ko_init_##field##_of_##type(ko_value_t obj_, ko_value_t value_) {         \
      ((struct ko_##type *)obj_)->field = value_;                            \
   }                                                                         \
   static inline void                                                        \
   ko_set_##field##_of_##type(ko_value_t obj_, ko_value_t value_) {          \
      ko_set_field_by_ptr(obj_, &((struct ko_##type *)obj_)->field, value_); \
   }                                                                         \
   static inline void                                                        \
   ko_set_##field##_of_##type##_nogc(ko_value_t obj_, ko_value_t value_) {   \
      ko_set_field_by_ptr_nogc(obj_, &((struct ko_##type *)obj_)->field,     \
         value_);                                                            \
   } KO_DEF_END

#define KO_OBJECT_FIELD_RAW(type, field, field_t)                       \
   static inline field_t                                                \
   ko_##field##_of_##type(ko_value_t obj_) {                            \
      return ((struct ko_##type *)obj_)->field;                         \
   }                                                                    \
   static inline field_t *                                              \
   ko_##field##_ptr_of_##type(ko_value_t obj_) {                        \
      return &((struct ko_##type *)obj_)->field;                        \
   }                                                                    \
   static inline void                                                   \
   ko_init_##field##_of_##type(ko_value_t obj_, field_t value_) {       \
      ((struct ko_##type *)obj_)->field = value_;                       \
   }                                                                    \
   static inline void                                                   \
   ko_set_##field##_of_##type(ko_value_t obj_, field_t value_) {        \
      ((struct ko_##type *)obj_)->field = value_;                       \
   }                                                                    \
   static inline void                                                   \
   ko_set_##field##_of_##type##_nogc(ko_value_t obj_, field_t value_) { \
      ((struct ko_##type *)obj_)->field = value_;                       \
   } KO_DEF_END

#define KO_OBJECT_FIELD_RAW_INDIRECT(type, field, path, field_t)        \
   static inline field_t                                                \
   ko_##field##_of_##type(ko_value_t obj_) {                            \
      return ((struct ko_##type *)obj_)->path;                          \
   }                                                                    \
   static inline field_t *                                              \
   ko_##field##_ptr_of_##type(ko_value_t obj_) {                        \
      return &((struct ko_##type *)obj_)->path;                         \
   }                                                                    \
   static inline void                                                   \
   ko_init_##field##_of_##type(ko_value_t obj_, field_t value_) {       \
      ((struct ko_##type *)obj_)->path = value_;                        \
   }                                                                    \
   static inline void                                                   \
   ko_set_##field##_of_##type(ko_value_t obj_, field_t value_) {        \
      ((struct ko_##type *)obj_)->path = value_;                        \
   }                                                                    \
   static inline void                                                   \
   ko_set_##field##_of_##type##_nogc(ko_value_t obj_, field_t value_) { \
      ((struct ko_##type *)obj_)->path = value_;                        \
   } KO_DEF_END

#define KO_OBJECT_FIELD_BITS(type, field, field_t)                      \
   static inline field_t                                                \
   ko_##field##_of_##type(ko_value_t obj_) {                            \
      return ((struct ko_##type *)obj_)->field;                         \
   }                                                                    \
   static inline void                                                   \
   ko_init_##field##_of_##type(ko_value_t obj_, field_t value_) {       \
      ((struct ko_##type *)obj_)->field = value_;                       \
   }                                                                    \
   static inline void                                                   \
   ko_set_##field##_of_##type(ko_value_t obj_, field_t value_) {        \
      ((struct ko_##type *)obj_)->field = value_;                       \
   }                                                                    \
   static inline void                                                   \
   ko_set_##field##_of_##type##_nogc(ko_value_t obj_, field_t value_) { \
      ((struct ko_##type *)obj_)->field = value_;                       \
   } KO_DEF_END

#define KO_OBJECT_FIELD_PTR(type, field, field_t) \
   static inline field_t *                        \
   ko_##field##_of_##type(ko_value_t obj_) {      \
      return &((struct ko_##type *)obj_)->field;  \
   } KO_DEF_END

#define KO_OBJECT_FIELD_ARRAY(type, field, field_t) \
   static inline field_t *                          \
   ko_##field##_of_##type(ko_value_t obj_) {        \
      return ((struct ko_##type *)obj_)->field;     \
   } KO_DEF_END

/*
 * Making type checkers
 * --------------------
 */

#define KO_DESCR_CHECKER(type, descr)  \
   static inline ko_bool_t             \
   ko_is_##type(ko_value_t obj_) {     \
      return ko_descr_is(obj_, descr); \
   } KO_DEF_END

#define KO_TYPE_CHECKER(type_name, type_obj)        \
   static inline ko_bool_t                          \
   ko_is_##type_name(ko_value_t obj_) {             \
      return ko_descr(obj_)->ko_type == (type_obj); \
   } KO_DEF_END

/*
 * Stack Elements
 * --------------
 */

struct ko_source_loc {
   ko_code_t ko_code;
   ko_value_t ko_loc;
};

#if KO_HAVE_FLEXIBLE_ARRAY_MEMBERS
   typedef struct {
      ko_nint_t ko_size;
      ko_nint_t ko_num_source_locs;
      struct ko_source_loc ko_source_locs[];
   } ko_frame_descr_t;
   #define KO_FRAME_DESCR_T(size) ko_frame_descr_t
   typedef ko_frame_descr_t ko_frame_descr_t_0;
#else
   #define KO_FRAME_DESCR_T(size)                   \
      struct {                                      \
         ko_nint_t ko_size;                         \
         ko_nint_t ko_num_source_locs;              \
         struct ko_source_loc ko_source_locs[size]; \
      }
   typedef KO_FRAME_DESCR_T(256) ko_frame_descr_t;
   typedef struct {
      ko_nint_t ko_size;
      ko_nint_t ko_num_source_locs;
   } ko_frame_descr_t_0;
#endif

typedef union {
   ko_value_t ko_value;
   ko_code_t ko_code;
   ko_frame_descr_t *ko_frame_descr;
} ko_stack_elem_t;

/*
 * Registers
 * ---------
 */

extern ko_value_t KO_R[KO_NUM_REGISTERS];
extern ko_code_t KO_RET;
extern ko_stack_elem_t *KO_S;
extern ko_value_t ko_try_offset;
extern ko_value_t ko_dynamic;
extern ko_value_t ko_blocked_all, ko_blocked_async;

/*
 * The Stack
 * ---------
 *
 * Stack consists of frames and grows up. Each frame has a frame
 * descriptor as its last word. The descriptor tells the frame size
 * in words, including the descriptor pointer itself, and the mapping
 * between return pointers and source code locations.
 *
 * ko_stack_begin points to the bottom of the stack. KO_S points
 * to the top, i.e. to the beginning of the free space. ko_stack_end
 * points to the top of the memory allocated for the stack.
 *
 * If ko_true_stack_end is not NULL, it means that ko_stack_end
 * has been temporarily adjusted to force a context switch or handle
 * a signal and ko_true_stack_end is the true end of the stack.
 *
 * A context switch is signalled through ko_stack_end only when
 * ko_stack_end_lock is false.
 */

extern ko_stack_elem_t *volatile ko_stack_begin, *volatile ko_stack_end,
   *volatile ko_true_stack_end;

/*
 * KO_PUSH_FRAME(descr, size, restart, regs) pushes a new frame with the
 * given descriptor and size in words. The descriptor and the return address
 * are written at the end of the frame, the rest is uninitialized.
 *
 * It also marks a point where the current thread may be preempted.
 * restart should be a code pointer immediately before the KO_PUSH_FRAME call.
 * The absolute value of regs is the number of registers to preserve;
 * regs is positive when they start with KO_R[1] and negative when they
 * start with KO_R[0].
 *
 * ko_pop_frame(size) removes the last frame.
 */

extern ko_code_t ko_restart;
extern int ko_live_regs;
extern ko_nint_t ko_frame_size;
KO_CODE(ko_stack_overflow);

#define KO_PUSH_FRAME(descr, size, restart, regs)                    \
   KO_STAT_BEGIN {                                                   \
      KO_S += size;                                                  \
      if (KO_UNLIKELY(KO_DIFF_PTR(ko_stack_end, KO_S) < 0)) {        \
         ko_restart = restart;                                       \
         ko_live_regs = regs;                                        \
         ko_frame_size = size;                                       \
         KO_JUMP(ko_stack_overflow);                                 \
      }                                                              \
      KO_S[-1].ko_frame_descr = (ko_frame_descr_t *)(void *)(descr); \
      KO_S[-2].ko_code = KO_RET;                                     \
   } KO_STAT_END

static inline void
ko_pop_frame(ko_nint_t size) {
   KO_RET = KO_S[-2].ko_code;
   KO_S -= size;
}

#define KO_POP_FRAME_AND_RETURN(size)    \
   KO_STAT_BEGIN {                       \
      ko_code_t ret_ = KO_S[-2].ko_code; \
      KO_S -= (size);                    \
      KO_JUMP(ret_);                     \
   } KO_STAT_END

/*
 * The Heap
 * --------
 */

extern void *ko_young_begin, *ko_young_top, *ko_young_limit;
extern ko_nint_t ko_young_size;
extern void *ko_old_begin, *ko_old_top, *ko_old_limit;
extern ko_nint_t ko_old_size;

static inline ko_bool_t
ko_is_young(ko_value_t obj) {
   return (ko_unint_t)KO_DIFF_PTR(obj, ko_young_begin)
      < (ko_unint_t)ko_young_size;
}

static inline ko_bool_t
ko_is_old(ko_value_t obj) {
   return (ko_unint_t)KO_DIFF_PTR(obj, ko_old_begin)
      < (ko_unint_t)ko_old_size;
}

/*
 * Allocation
 * ----------
 *
 * ko_alloc allocates a new object of the given size in words.
 */

void ko_collect_auto(ko_nint_t estimatedSize, ko_nint_t allocSize);
ko_value_t ko_garbage_collect_and_alloc(ko_nint_t size);
void ko_collect_some_garbage(void);

static inline ko_value_t
ko_alloc(ko_nint_t size) {
   ko_value_t result = (ko_value_t)ko_young_top;
   ko_young_top = KO_SHIFT_PTR(void *, ko_young_top, size * KO_SIZEOF_PTR);
   return KO_LIKELY(KO_DIFF_PTR(ko_young_limit, ko_young_top) >= 0) ?
      result :
      ko_garbage_collect_and_alloc(size);
}

/*
 * Finding Live Objects
 * --------------------
 */

extern ko_value_t **ko_marked_begin, **ko_marked_end, **ko_marked_top,
   **ko_marked_limit;

void ko_resize_marked(ko_nint_t count);

static inline void
ko_mark_count(ko_nint_t count) {
   if (KO_UNLIKELY(
      KO_DIFF_PTR(ko_marked_end, ko_marked_top) < count * KO_SIZEOF_PTR
   ))
      ko_resize_marked(count);
}

static inline void
ko_mark(ko_value_t *ptr) {
   *ko_marked_top++ = ptr;
}

static inline ko_value_t *
ko_mark_array(ko_value_t *ptr, ko_nint_t count) {
   ko_value_t **top = ko_marked_top;
   for (; count != 0; --count)
      *top++ = ptr++;
   ko_marked_top = top;
   return ptr;
}

static inline void
ko_mark_changed(ko_value_t *ptr) {
   ko_mark(ptr);
   if (KO_UNLIKELY(KO_DIFF_PTR(ko_marked_limit, ko_marked_top) < 0))
      ko_collect_some_garbage();
}

static inline void
ko_mark_changed_nogc(ko_value_t *ptr) {
   ko_mark_count(1);
   ko_mark(ptr);
}

static inline void
ko_mark_changed_array(ko_value_t *ptr, ko_nint_t count) {
   ko_mark_count(count);
   ko_mark_array(ptr, count);
   if (KO_UNLIKELY(KO_DIFF_PTR(ko_marked_limit, ko_marked_top) < 0))
      ko_collect_some_garbage();
}

/*
 * Write Barrier
 * -------------
 *
 * A global variable must be changed using ko_set_global. If it's a field
 * of a static object, ko_set_global_field might be more convenient.
 * A field of a dynamic object is better changed using ko_set_field,
 * although ko_set_global / ko_set_global_field work too.
 *
 * If you are sure that the object being changed is young or that the
 * new value is not young, plain assignment is enough.
 */

static inline void
ko_set_global(ko_value_t *ptr, ko_value_t value) {
   *ptr = value;
   ko_mark_changed(ptr);
}

#if KO_HAVE_FLEXIBLE_ARRAY_MEMBERS
   static inline void
   ko_set_global_field(ko_value_t obj, ko_nint_t i, ko_value_t value) {
      ko_set_global(ko_field_ptr(obj, i), value);
   }
#else
   /* When flexible array members are not used, accessing global objects
    * relies on illegal type punning. In this case gcc-4.2.0 generates
    * wrong code for some rare cases of inlined ko_set_global_field, so
    * we don't inline it. */
   void ko_set_global_field(ko_value_t obj, ko_nint_t i, ko_value_t value);
#endif

static inline void
ko_set_descr(ko_value_t obj, ko_descr_t descr) {
   ko_init_descr(obj, descr);
   if (!ko_is_young(obj))
      ko_mark_changed((ko_value_t *)obj);
}

static inline void
ko_set_field_by_ptr(ko_value_t obj, ko_value_t *ptr, ko_value_t value) {
   *ptr = value;
   if (!ko_is_young(obj))
      ko_mark_changed(ptr);
}

static inline void
ko_set_field_by_ptr_nogc(ko_value_t obj, ko_value_t *ptr, ko_value_t value) {
   *ptr = value;
   if (!ko_is_young(obj))
      ko_mark_changed_nogc(ptr);
}

static inline void
ko_set_field(ko_value_t obj, ko_nint_t i, ko_value_t value) {
   ko_set_field_by_ptr(obj, ko_field_ptr(obj, i), value);
}

static inline void
ko_set_field_nogc(ko_value_t obj, ko_nint_t i, ko_value_t value) {
   ko_set_field_by_ptr_nogc(obj, ko_field_ptr(obj, i), value);
}

/*
 * Protected Variables
 * -------------------
 *
 * Pointers to heap-allocated objects which are stored in variables
 * outside of locations known to the garbage collector must be
 * registered in order for the garbage collector to find them.
 *
 * A value of type KO_PROTECTED_T(size) describes such variables.
 * Its first two fields are initialized by ko_protect and can be
 * initially 0, the next field contains a count of variables, and the
 * final field is a variable length array containing pointers to
 * ko_value_t variables. The variables can be unregistered with
 * ko_unprotect.
 *
 * The variable pointers will be updated as the garbage collector
 * moves the objects during garbage collection. The variables must
 * not be changed manually to point to different objects.
 */

#if KO_HAVE_FLEXIBLE_ARRAY_MEMBERS
   typedef struct ko_protected {
      struct ko_protected *ko_prev, *ko_next;
      ko_nint_t ko_count;
      ko_value_t *ko_vars[];
   } ko_protected_t;
   #define KO_PROTECTED_T(size) ko_protected_t
   typedef ko_protected_t ko_protected_t_0;
#else
   #define KO_PROTECTED_T(size)     \
      struct {                      \
         void *ko_prev, *ko_next;   \
         ko_nint_t ko_count;        \
         ko_value_t *ko_vars[size]; \
      }
   typedef KO_PROTECTED_T(256) ko_protected_t;
   typedef struct {
      void *ko_prev, *ko_next;
      ko_nint_t ko_count;
   } ko_protected_t_0;
#endif

extern ko_protected_t_0 ko_young_protected, ko_old_protected;

#if KO_HAVE_FLEXIBLE_ARRAY_MEMBERS
   static inline void
   ko_protect(ko_protected_t *prot) {
      prot->ko_next = ko_young_protected.ko_next;
      ko_young_protected.ko_next->ko_prev = prot;
      ko_young_protected.ko_next = prot;
      prot->ko_prev = &ko_young_protected;
   }

   static inline void
   ko_unprotect(ko_protected_t *prot) {
      prot->ko_prev->ko_next = prot->ko_next;
      prot->ko_next->ko_prev = prot->ko_prev;
   }
#else
   static inline void
   ko_protect(void *prot) {
      ((ko_protected_t *)prot)->ko_next = ko_young_protected.ko_next;
      ((ko_protected_t *)ko_young_protected.ko_next)->ko_prev = prot;
      ko_young_protected.ko_next = prot;
      ((ko_protected_t *)prot)->ko_prev = &ko_young_protected;
   }

   static inline void
   ko_unprotect(void *prot) {
      ((ko_protected_t *)((ko_protected_t *)prot)->ko_prev)->ko_next =
         ((ko_protected_t *)prot)->ko_next;
      ((ko_protected_t *)((ko_protected_t *)prot)->ko_next)->ko_prev =
         ((ko_protected_t *)prot)->ko_prev;
   }
#endif

/*
 * Foreign objects
 * ---------------
 */

#define KO_FOREIGN_OBJECT KO_OBJECT; ko_value_t ko_next_foreign

#define KO_CHANGED_OBJECT KO_FOREIGN_OBJECT; ko_value_t ko_next_changed

/*
 * Mapping a function descriptor to arity and location information
 * ---------------------------------------------------------------
 */

struct ko_function_map_entry {
   ko_descr_t ko_descr;
   ko_value_t ko_arities, ko_loc;
};

#define KO_FUNCTION_INFO(fun, arities, loc, descr, iter)    \
   KO_STAT_BEGIN {                                          \
      ko_descr_t descr = ko_descr_of_ptr(fun);              \
      struct ko_function_map_entry *iter = ko_function_map; \
      while (iter->ko_descr != descr) ++iter;               \
      arities = iter->ko_arities;                           \
      loc = iter->ko_loc;                                   \
   } KO_STAT_END

/*
 * Calling from C
 * --------------
 */

/* Passing arguments */

#if KO_MAX_FAST_ARITY != 8
   #error Fix here if KO_MAX_FAST_ARITY is changed
#endif

static inline void
ko_arg1(ko_value_t arg1) {
   KO_R[1] = arg1;
}

static inline void
ko_arg2(ko_value_t arg1, ko_value_t arg2) {
   KO_R[1] = arg1; KO_R[2] = arg2;
}

static inline void
ko_arg3(ko_value_t arg1, ko_value_t arg2, ko_value_t arg3) {
   KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3;
}

static inline void
ko_arg4(ko_value_t arg1, ko_value_t arg2, ko_value_t arg3, ko_value_t arg4) {
   KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3; KO_R[4] = arg4;
}

static inline void
ko_arg5(ko_value_t arg1, ko_value_t arg2, ko_value_t arg3, ko_value_t arg4,
   ko_value_t arg5)
{
   KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3; KO_R[4] = arg4;
   KO_R[5] = arg5;
}

static inline void
ko_arg6(ko_value_t arg1, ko_value_t arg2, ko_value_t arg3, ko_value_t arg4,
   ko_value_t arg5, ko_value_t arg6)
{
   KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3; KO_R[4] = arg4;
   KO_R[5] = arg5; KO_R[6] = arg6;
}

static inline void
ko_arg7(ko_value_t arg1, ko_value_t arg2, ko_value_t arg3, ko_value_t arg4,
   ko_value_t arg5, ko_value_t arg6, ko_value_t arg7)
{
   KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3; KO_R[4] = arg4;
   KO_R[5] = arg5; KO_R[6] = arg6; KO_R[7] = arg7;
}

static inline void
ko_arg8(ko_value_t arg1, ko_value_t arg2, ko_value_t arg3, ko_value_t arg4,
   ko_value_t arg5, ko_value_t arg6, ko_value_t arg7, ko_value_t arg8)
{
   KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3; KO_R[4] = arg4;
   KO_R[5] = arg5; KO_R[6] = arg6; KO_R[7] = arg7; KO_R[8] = arg8;
}

static inline void
ko_fun_arg0(ko_value_t self) {
   KO_R[0] = self;
}

static inline void
ko_fun_arg1(ko_value_t self, ko_value_t arg1) {
   KO_R[0] = self; KO_R[1] = arg1;
}

static inline void
ko_fun_arg2(ko_value_t self, ko_value_t arg1, ko_value_t arg2) {
   KO_R[0] = self; KO_R[1] = arg1; KO_R[2] = arg2;
}

static inline void
ko_fun_arg3(ko_value_t self, ko_value_t arg1, ko_value_t arg2, ko_value_t arg3)
{
   KO_R[0] = self; KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3;
}

static inline void
ko_fun_arg4(ko_value_t self, ko_value_t arg1, ko_value_t arg2, ko_value_t arg3,
   ko_value_t arg4)
{
   KO_R[0] = self; KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3;
   KO_R[4] = arg4;
}

static inline void
ko_fun_arg5(ko_value_t self, ko_value_t arg1, ko_value_t arg2, ko_value_t arg3,
   ko_value_t arg4, ko_value_t arg5)
{
   KO_R[0] = self; KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3;
   KO_R[4] = arg4; KO_R[5] = arg5;
}

static inline void
ko_fun_arg6(ko_value_t self, ko_value_t arg1, ko_value_t arg2, ko_value_t arg3,
   ko_value_t arg4, ko_value_t arg5, ko_value_t arg6)
{
   KO_R[0] = self; KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3;
   KO_R[4] = arg4; KO_R[5] = arg5; KO_R[6] = arg6;
}

static inline void
ko_fun_arg7(ko_value_t self, ko_value_t arg1, ko_value_t arg2, ko_value_t arg3,
   ko_value_t arg4, ko_value_t arg5, ko_value_t arg6, ko_value_t arg7)
{
   KO_R[0] = self; KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3;
   KO_R[4] = arg4; KO_R[5] = arg5; KO_R[6] = arg6; KO_R[7] = arg7;
}

static inline void
ko_fun_arg8(ko_value_t self, ko_value_t arg1, ko_value_t arg2, ko_value_t arg3,
   ko_value_t arg4, ko_value_t arg5, ko_value_t arg6, ko_value_t arg7,
   ko_value_t arg8)
{
   KO_R[0] = self; KO_R[1] = arg1; KO_R[2] = arg2; KO_R[3] = arg3;
   KO_R[4] = arg4; KO_R[5] = arg5; KO_R[6] = arg6; KO_R[7] = arg7;
   KO_R[8] = arg8;
}

/* NULL */

KO_DECLARE_STATIC(Null, Core_Kokogut, 0);

#define KO_Null KO_STATIC(Null, Core_Kokogut)

/* BOOL */

KO_DECLARE_TYPE(BOOL, Core_Kokogut);
KO_DECLARE_STATIC(True, Core_Kokogut, 0);
KO_DECLARE_STATIC(False, Core_Kokogut, 0);

#define KO_True  KO_STATIC(True,  Core_Kokogut)
#define KO_False KO_STATIC(False, Core_Kokogut)

static inline ko_value_t
ko_bool(ko_bool_t x) {
   return x ? KO_True : KO_False;
}

KO_DECLARE_ENTRY(Is, Core_Kokogut, 2);
KO_DECLARE_ENTRY(261261, Core_Kokogut, 2);
KO_DECLARE_LIST_ENTRY(261261, Core_Kokogut);
KO_DECLARE_ENTRY(3126261, Core_Kokogut, 2);
KO_DECLARE_LIST_ENTRY(3126261, Core_Kokogut);
KO_DECLARE_ENTRY(260, Core_Kokogut, 2);
KO_DECLARE_LIST_ENTRY(260, Core_Kokogut);
KO_DECLARE_ENTRY(262, Core_Kokogut, 2);
KO_DECLARE_LIST_ENTRY(262, Core_Kokogut);
KO_DECLARE_ENTRY(260261, Core_Kokogut, 2);
KO_DECLARE_LIST_ENTRY(260261, Core_Kokogut);
KO_DECLARE_ENTRY(262261, Core_Kokogut, 2);
KO_DECLARE_LIST_ENTRY(262261, Core_Kokogut);
KO_DECLARE_ENTRY(Compare, Core_Kokogut, 2);

KO_DECLARE_GLOBAL(LT, Core_Kokogut);
#define KO_LT KO_GLOBAL(LT, Core_Kokogut)
KO_DECLARE_GLOBAL(EQ, Core_Kokogut);
#define KO_EQ KO_GLOBAL(EQ, Core_Kokogut)
KO_DECLARE_GLOBAL(GT, Core_Kokogut);
#define KO_GT KO_GLOBAL(GT, Core_Kokogut)

/* STRUCT */

KO_DECLARE_TYPE(STRUCT, Core_Kokogut);

/* Exceptions */

KO_DECLARE_DESCR(SourceLoc, ExceptionTypes_Kokogut);
KO_DECLARE_STATIC(AllConditionsFalse, ExceptionTypes_Kokogut, 0);

KO_CODE(ko_fail_at);
KO_CODE(ko_fail);
KO_DECLARE_ENTRY(Try, Exceptions_Kokogut, 2);
KO_DECLARE_ENTRY(Try, Exceptions_Kokogut, 3);
KO_DECLARE_ENTRY(Fail, Exceptions_Kokogut, 1);
KO_DECLARE_LIST_ENTRY(NotAFunctionEntry, Exceptions_Kokogut);
KO_DECLARE_LIST_ENTRY(IncompleteObjectEntry, Exceptions_Kokogut);
KO_DECLARE_ENTRY(FailBadArguments, Functions_Kokogut, 4);
KO_DECLARE_ENTRY(FailBadVarArity, Exceptions_Kokogut, 1);
KO_DECLARE_ENTRY(FailBadType, Exceptions_Kokogut, 3);
KO_DECLARE_ENTRY(FailNotAFunction, Exceptions_Kokogut, 2);
KO_DECLARE_ENTRY(FailNoField, Records_Kokogut, 3);
KO_DECLARE_ENTRY(FailNotSettable, Records_Kokogut, 3);
KO_DECLARE_ENTRY(FailNoMatch01, Exceptions_Kokogut, 2);
KO_DECLARE_ENTRY(FailNoMatch, Exceptions_Kokogut, 2);
KO_DECLARE_ENTRY(FailNotDefined, Exceptions_Kokogut, 2);
KO_DECLARE_ENTRY(FailAlreadyDefined, Exceptions_Kokogut, 2);
KO_DECLARE_ENTRY(FailTypeIsFinal, Exceptions_Kokogut, 2);
KO_DECLARE_ENTRY(FailArithmeticError, Ints_Numbers_Kokogut, 3);
KO_DECLARE_ENTRY(FailArithmeticError, Ints_Numbers_Kokogut, 4);

KO_DECLARE_ENTRY(ToListEntry, Exceptions_Kokogut, 0);
KO_DECLARE_ENTRY(ToListEntry, Exceptions_Kokogut, 1);
KO_DECLARE_ENTRY(ToListEntry, Exceptions_Kokogut, 2);
KO_DECLARE_ENTRY(ToListEntry, Exceptions_Kokogut, 3);
KO_DECLARE_ENTRY(ToListEntry, Exceptions_Kokogut, 4);
KO_DECLARE_ENTRY(ToListEntry, Exceptions_Kokogut, 5);
KO_DECLARE_ENTRY(ToListEntry, Exceptions_Kokogut, 6);
KO_DECLARE_ENTRY(ToListEntry, Exceptions_Kokogut, 7);
KO_DECLARE_ENTRY(ToListEntry, Exceptions_Kokogut, 8);

#define KO_FAIL_AT(exn, loc) \
   KO_STAT_BEGIN {           \
      ko_arg2(exn, loc);     \
      KO_POP_FRAME();        \
      KO_JUMP(ko_fail_at);   \
   } KO_STAT_END

#define KO_FAIL(exn)    \
   KO_STAT_BEGIN {      \
      ko_arg1(exn);     \
      KO_POP_FRAME();   \
      KO_JUMP(ko_fail); \
   } KO_STAT_END

#if KO_MAX_FAST_ARITY != 8
   #error Fix here if KO_MAX_FAST_ARITY is changed
#endif

#define KO_FAIL_BY_JUMP0(entry) \
   KO_STAT_BEGIN {              \
      KO_POP_FRAME();           \
      KO_JUMP(entry);           \
   } KO_STAT_END

#define KO_FAIL_BY_JUMP1(entry, arg1) \
   KO_STAT_BEGIN {                    \
      ko_arg1(arg1);                  \
      KO_POP_FRAME();                 \
      KO_JUMP(entry);                 \
   } KO_STAT_END

#define KO_FAIL_BY_JUMP2(entry, arg1, arg2) \
   KO_STAT_BEGIN {                          \
      ko_arg2(arg1, arg2);                  \
      KO_POP_FRAME();                       \
      KO_JUMP(entry);                       \
   } KO_STAT_END

#define KO_FAIL_BY_JUMP3(entry, arg1, arg2, arg3) \
   KO_STAT_BEGIN {                                \
      ko_arg3(arg1, arg2, arg3);                  \
      KO_POP_FRAME();                             \
      KO_JUMP(entry);                             \
   } KO_STAT_END

#define KO_FAIL_BY_JUMP4(entry, arg1, arg2, arg3, arg4) \
   KO_STAT_BEGIN {                                      \
      ko_arg4(arg1, arg2, arg3, arg4);                  \
      KO_POP_FRAME();                                   \
      KO_JUMP(entry);                                   \
   } KO_STAT_END

#define KO_FAIL_BY_JUMP5(entry, arg1, arg2, arg3, arg4, arg5) \
   KO_STAT_BEGIN {                                            \
      ko_arg5(arg1, arg2, arg3, arg4, arg5);                  \
      KO_POP_FRAME();                                         \
      KO_JUMP(entry);                                         \
   } KO_STAT_END

#define KO_FAIL_BY_JUMP6(entry, arg1, arg2, arg3, arg4, arg5, arg6) \
   KO_STAT_BEGIN {                                                  \
      ko_arg6(arg1, arg2, arg3, arg4, arg5, arg6);                  \
      KO_POP_FRAME();                                               \
      KO_JUMP(entry);                                               \
   } KO_STAT_END

#define KO_FAIL_BY_JUMP7(entry, arg1, arg2, arg3, arg4, arg5, arg6, arg7) \
   KO_STAT_BEGIN {                                                        \
      ko_arg7(arg1, arg2, arg3, arg4, arg5, arg6, arg7);                  \
      KO_POP_FRAME();                                                     \
      KO_JUMP(entry);                                                     \
   } KO_STAT_END

#define KO_FAIL_BY_JUMP8(entry, arg1, arg2, arg3, arg4, arg5, arg6, arg7, \
   arg8)                                                                  \
   KO_STAT_BEGIN {                                                        \
      ko_arg8(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);            \
      KO_POP_FRAME();                                                     \
      KO_JUMP(entry);                                                     \
   } KO_STAT_END

#define KO_FAIL_BY_JUMP(entry, args) \
   KO_STAT_BEGIN {                   \
      ko_arg1(args);                 \
      KO_POP_FRAME();                \
      KO_JUMP(entry);                \
   } KO_STAT_END

/* NUMBER */

KO_DECLARE_ENTRY(243, Ints_Numbers_Kokogut, 2);
KO_DECLARE_LIST_ENTRY(243, Ints_Numbers_Kokogut);
KO_DECLARE_ENTRY(245, Ints_Numbers_Kokogut, 2);
KO_DECLARE_LIST_ENTRY(245, Ints_Numbers_Kokogut);
KO_DECLARE_ENTRY(245, Ints_Numbers_Kokogut, 1);
KO_DECLARE_LIST_ENTRY(245, Ints_Numbers_Kokogut);
KO_DECLARE_ENTRY(242, Ints_Numbers_Kokogut, 2);
KO_DECLARE_LIST_ENTRY(242, Ints_Numbers_Kokogut);
KO_DECLARE_ENTRY(247, Ratios_Numbers_Kokogut, 2);
KO_DECLARE_LIST_ENTRY(247, Ratios_Numbers_Kokogut);
KO_DECLARE_ENTRY(247, Ratios_Numbers_Kokogut, 1);
KO_DECLARE_LIST_ENTRY(247, Ratios_Numbers_Kokogut);
KO_DECLARE_ENTRY(Abs, Ints_Numbers_Kokogut, 1);
KO_DECLARE_ENTRY(Sqr, Ints_Numbers_Kokogut, 1);
KO_DECLARE_ENTRY(Quot, Ints_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(Rem, Ints_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(QuotRem, Ints_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(Div, Ints_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(Mod, Ints_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(DivMod, Ints_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(IsDivisible, Ints_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(IsEven, Ints_Numbers_Kokogut, 1);
KO_DECLARE_ENTRY(GCD, Ints_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(BitNot, BitOps_Numbers_Kokogut, 1);
KO_DECLARE_ENTRY(BitAnd, BitOps_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(BitAndNot, BitOps_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(BitOr, BitOps_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(BitXor, BitOps_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(BitShift, BitOps_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(TestBit, BitOps_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(SetBit, BitOps_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(ClearBit, BitOps_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(SizeInBits, BitOps_Numbers_Kokogut, 1);

/* INT */

KO_DECLARE_DESCR(BIntCon, Ints_Numbers_Kokogut);

struct ko_bint {
   KO_FOREIGN_OBJECT;
   mpz_t ko_value;
};

#define KO_DUMMY_MPZ {{0, 0, 0}}

static inline ko_bool_t
ko_is_sint2(ko_value_t x, ko_value_t y) {
   return ((ko_nint_t)x & (ko_nint_t)y & 1) != 0;
}

#define ko_mpz_of_bint(obj) ((struct ko_bint *)(obj))->ko_value
/* This is a macro, not a function, because mpz_t is an array type
 * which can't be returned from a function. */

static inline ko_bool_t
ko_lt(ko_value_t x, ko_value_t y) {
   return (ko_nint_t)x < (ko_nint_t)y;
}
static inline ko_bool_t
ko_gt(ko_value_t x, ko_value_t y) {
   return (ko_nint_t)x > (ko_nint_t)y;
}
static inline ko_bool_t
ko_le(ko_value_t x, ko_value_t y) {
   return (ko_nint_t)x <= (ko_nint_t)y;
}
static inline ko_bool_t
ko_ge(ko_value_t x, ko_value_t y) {
   return (ko_nint_t)x >= (ko_nint_t)y;
}
static inline ko_bool_t
ko_lt0(ko_value_t x) {
   return (ko_nint_t)x < 0;
}
static inline ko_bool_t
ko_gt0(ko_value_t x) {
   return (ko_nint_t)x > 1;
}
static inline ko_bool_t
ko_le0(ko_value_t x) {
   return (ko_nint_t)x <= 1;
}
static inline ko_bool_t
ko_ge0(ko_value_t x) {
   return (ko_nint_t)x >= 0;
}
static inline ko_bool_t
ko_in_range(ko_value_t x, ko_value_t limit) {
   return (ko_unint_t)x < (ko_unint_t)limit;
}
static inline ko_bool_t
ko_in_range_incl(ko_value_t x, ko_value_t limit) {
   return (ko_unint_t)x <= (ko_unint_t)limit;
}

static inline ko_value_t
ko_compare(ko_value_t x, ko_value_t y) {
   return ko_lt(x, y) ? KO_LT : x == y ? KO_EQ : KO_GT;
}
static inline ko_value_t
ko_min(ko_value_t x, ko_value_t y) {
   return ko_lt(y, x) ? y : x;
}
static inline ko_value_t
ko_max(ko_value_t x, ko_value_t y) {
   return ko_lt(y, x) ? x : y;
}
static inline ko_value_t
ko_add(ko_value_t x, ko_value_t y) {
   return (ko_value_t)((ko_nint_t)x + (ko_nint_t)y - 1);
}
static inline ko_value_t
ko_add_sn(ko_value_t x, ko_nint_t y) {
   return (ko_value_t)((ko_nint_t)x + (y << 1));
}
static inline ko_value_t
ko_add1(ko_value_t x) {
   return (ko_value_t)((ko_nint_t)x + 2);
}
static inline ko_value_t
ko_sub(ko_value_t x, ko_value_t y) {
   return (ko_value_t)((ko_nint_t)x - (ko_nint_t)y + 1);
}
static inline ko_value_t
ko_sub_sn(ko_value_t x, ko_nint_t y) {
   return (ko_value_t)((ko_nint_t)x - (y << 1));
}
static inline ko_value_t
ko_sub1(ko_value_t x) {
   return (ko_value_t)((ko_nint_t)x - 2);
}
static inline ko_value_t
ko_neg(ko_value_t x) {
   return (ko_value_t)(2 - (ko_nint_t)x);
}
static inline ko_value_t
ko_abs(ko_value_t x) {
   return ko_ge0(x) ? x : ko_neg(x);
}
static inline ko_value_t
ko_mul(ko_value_t x, ko_value_t y) {
   return ko_sint(ko_nint_of_sint(x) * ko_nint_of_sint(y));
}
static inline ko_value_t
ko_mul_sn(ko_value_t x, ko_nint_t y) {
   return (ko_value_t)((ko_nint_t)x * y - y + 1);
}
static inline ko_value_t
ko_quot(ko_value_t x, ko_value_t y) {
   return ko_sint(ko_nint_of_sint(x) / ko_nint_of_sint(y));
}
static inline ko_value_t
ko_rem(ko_value_t x, ko_value_t y) {
   return ko_sint(ko_nint_of_sint(x) % ko_nint_of_sint(y));
}
static inline ko_value_t
ko_div(ko_value_t x, ko_value_t y) {
   ko_nint_t xv = ko_nint_of_sint(x);
   ko_nint_t yv = ko_nint_of_sint(y);
   return ko_sint(yv >= 0 ?
      xv >= 0 ? xv / yv : (xv + 1) / yv - 1 :
      xv > 0 ? (xv - 1) / yv - 1 : xv / yv);
}
static inline ko_value_t
ko_div_ssgt0(ko_value_t x, ko_value_t y) {
   ko_nint_t xv = ko_nint_of_sint(x);
   ko_nint_t yv = ko_nint_of_sint(y);
   return ko_sint(xv >= 0 ? xv / yv : (xv + 1) / yv - 1);
}
static inline ko_value_t
ko_div_sslt0(ko_value_t x, ko_value_t y) {
   ko_nint_t xv = ko_nint_of_sint(x);
   ko_nint_t yv = ko_nint_of_sint(y);
   return ko_sint(xv > 0 ? (xv - 1) / yv - 1 : xv / yv);
}
static inline ko_value_t
ko_mod(ko_value_t x, ko_value_t y) {
   ko_nint_t xv = ko_nint_of_sint(x);
   ko_nint_t yv = ko_nint_of_sint(y);
   ko_nint_t r = xv % yv;
   return ko_sint((xv ^ yv) >= 0 || r == 0 ? r : r + yv);
}
static inline ko_value_t
ko_mod_ssgt0(ko_value_t x, ko_value_t y) {
   ko_nint_t xv = ko_nint_of_sint(x);
   ko_nint_t yv = ko_nint_of_sint(y);
   ko_nint_t r = xv % yv;
   return ko_sint(xv >= 0 || r == 0 ? r : r + yv);
}
static inline ko_value_t
ko_mod_sslt0(ko_value_t x, ko_value_t y) {
   ko_nint_t xv = ko_nint_of_sint(x);
   ko_nint_t yv = ko_nint_of_sint(y);
   ko_nint_t r = xv % yv;
   return ko_sint(xv < 0 || r == 0 ? r : r + yv);
}
static inline ko_bool_t
ko_is_divisible(ko_value_t x, ko_value_t y) {
   return ko_nint_of_sint(x) % ko_nint_of_sint(y) == 0;
}
static inline ko_bool_t
ko_is_even(ko_value_t x) {
   return ((ko_nint_t)x & 2) == 0;
}
static inline ko_value_t
ko_not(ko_value_t x) {
   return (ko_value_t)((ko_nint_t)x ^ (-2));
}
static inline ko_value_t
ko_and(ko_value_t x, ko_value_t y) {
   return (ko_value_t)((ko_nint_t)x & (ko_nint_t)y);
}
static inline ko_value_t
ko_and_not(ko_value_t x, ko_value_t y) {
   return (ko_value_t)((ko_nint_t)x & ((ko_nint_t)y ^ (-2)));
}
static inline ko_bool_t
ko_test(ko_value_t x, ko_nint_t y) {
   return ((ko_nint_t)x & (y << 1)) != 0;
}
static inline ko_value_t
ko_or(ko_value_t x, ko_value_t y) {
   return (ko_value_t)((ko_nint_t)x | (ko_nint_t)y);
}
static inline ko_value_t
ko_xor(ko_value_t x, ko_value_t y) {
   return (ko_value_t)((ko_nint_t)x ^ (ko_nint_t)y ^ 1);
}
static inline ko_value_t
ko_shl(ko_value_t x, ko_value_t y) {
   return (ko_value_t)((((ko_nint_t)x - 1) << ko_nint_of_sint(y)) + 1);
}
static inline ko_value_t
ko_shl_sn(ko_value_t x, ko_nint_t y) {
   return (ko_value_t)((((ko_nint_t)x - 1) << y) + 1);
}
static inline ko_value_t
ko_shr(ko_value_t x, ko_value_t y) {
   return (ko_value_t)(((ko_nint_t)x >> ko_nint_of_sint(y)) | 1);
}
static inline ko_value_t
ko_shr_sn(ko_value_t x, ko_nint_t y) {
   return (ko_value_t)(((ko_nint_t)x >> y) | 1);
}

#define KO_SINT_MIN (KO_NINT_MIN / 2)
#define KO_SINT_MAX (KO_NINT_MAX / 2)

extern struct ko_bint ko_neg_min_sint;

ko_value_t ko_bint_of_mpz(mpz_t value);
ko_value_t ko_int_of_mpz(mpz_t value);

static inline int
ko_sgn_of_bint(ko_value_t x) {
   return mpz_sgn(ko_mpz_of_bint(x));
}

static inline ko_bool_t
ko_eq_ii(ko_value_t x, ko_value_t y) {
   return ko_is_sint(x) ? x == y :
      !ko_is_sint(y) &&
      mpz_cmp(ko_mpz_of_bint(x), ko_mpz_of_bint(y)) == 0;
}

static inline ko_bool_t
ko_ne_ii(ko_value_t x, ko_value_t y) {
   return ko_is_sint(x) ? x != y :
      ko_is_sint(y) ||
      mpz_cmp(ko_mpz_of_bint(x), ko_mpz_of_bint(y)) != 0;
}

static inline ko_bool_t
ko_lt_ii(ko_value_t x, ko_value_t y) {
   return ko_is_sint(x) ?
      ko_is_sint(y) ?
         ko_lt(x, y) :
         ko_sgn_of_bint(y) >= 0 :
      ko_is_sint(y) ?
         ko_sgn_of_bint(x) < 0 :
         mpz_cmp(ko_mpz_of_bint(x), ko_mpz_of_bint(y)) < 0;
}

static inline ko_bool_t
ko_gt_ii(ko_value_t x, ko_value_t y) {
   return ko_is_sint(x) ?
      ko_is_sint(y) ?
         ko_gt(x, y) :
         ko_sgn_of_bint(y) < 0 :
      ko_is_sint(y) ?
         ko_sgn_of_bint(x) >= 0 :
         mpz_cmp(ko_mpz_of_bint(y), ko_mpz_of_bint(x)) < 0;
}

static inline ko_bool_t
ko_le_ii(ko_value_t x, ko_value_t y) {
   return ko_is_sint(x) ?
      ko_is_sint(y) ?
         ko_le(x, y) :
         ko_sgn_of_bint(y) >= 0 :
      ko_is_sint(y) ?
         ko_sgn_of_bint(x) < 0 :
         mpz_cmp(ko_mpz_of_bint(y), ko_mpz_of_bint(x)) >= 0;
}

static inline ko_bool_t
ko_ge_ii(ko_value_t x, ko_value_t y) {
   return ko_is_sint(x) ?
      ko_is_sint(y) ?
         ko_ge(x, y) :
         ko_sgn_of_bint(y) < 0 :
      ko_is_sint(y) ?
         ko_sgn_of_bint(x) >= 0 :
         mpz_cmp(ko_mpz_of_bint(x), ko_mpz_of_bint(y)) >= 0;
}

static inline ko_bool_t
ko_eq_is(ko_value_t x, ko_value_t y) {
   return x == y;
}
static inline ko_bool_t
ko_ne_is(ko_value_t x, ko_value_t y) {
   return x != y;
}
static inline ko_bool_t
ko_lt_is(ko_value_t x, ko_value_t y) {
   return ko_is_sint(x) ? ko_lt(x, y) : ko_sgn_of_bint(x) < 0;
}
static inline ko_bool_t
ko_gt_is(ko_value_t x, ko_value_t y) {
   return ko_is_sint(x) ? ko_gt(x, y) : ko_sgn_of_bint(x) >= 0;
}
static inline ko_bool_t
ko_le_is(ko_value_t x, ko_value_t y) {
   return ko_is_sint(x) ? ko_le(x, y) : ko_sgn_of_bint(x) < 0;
}
static inline ko_bool_t
ko_ge_is(ko_value_t x, ko_value_t y) {
   return ko_is_sint(x) ? ko_ge(x, y) : ko_sgn_of_bint(x) >= 0;
}

static inline ko_bool_t
ko_lt0_i(ko_value_t x) {
   return ko_is_sint(x) ? ko_lt0(x) : ko_sgn_of_bint(x) < 0;
}
static inline ko_bool_t
ko_gt0_i(ko_value_t x) {
   return ko_is_sint(x) ? ko_gt0(x) : ko_sgn_of_bint(x) >= 0;
}
static inline ko_bool_t
ko_le0_i(ko_value_t x) {
   return ko_is_sint(x) ? ko_le0(x) : ko_sgn_of_bint(x) < 0;
}
static inline ko_bool_t
ko_ge0_i(ko_value_t x) {
   return ko_is_sint(x) ? ko_ge0(x) : ko_sgn_of_bint(x) >= 0;
}

static inline ko_value_t
ko_compare_ii(ko_value_t x, ko_value_t y) {
   if (ko_is_sint(x)) {
      if (ko_is_sint(y))
         return ko_lt(x, y) ? KO_LT : x == y ? KO_EQ : KO_GT;
      else
         return ko_sgn_of_bint(y) < 0 ? KO_GT : KO_LT;
   } else {
      if (ko_is_sint(y))
         return ko_sgn_of_bint(x) < 0 ? KO_LT : KO_GT;
      else {
         int cmp = mpz_cmp(ko_mpz_of_bint(x), ko_mpz_of_bint(y));
         return cmp < 0 ? KO_LT : cmp == 0 ? KO_EQ : KO_GT;
      }
   }
}

static inline ko_value_t
ko_min_ii(ko_value_t x, ko_value_t y) {
   return ko_lt_ii(y, x) ? y : x;
}
static inline ko_value_t
ko_max_ii(ko_value_t x, ko_value_t y) {
   return ko_lt_ii(y, x) ? x : y;
}

static inline ko_value_t
ko_min_is(ko_value_t x, ko_value_t y) {
   return ko_gt_is(x, y) ? y : x;
}
static inline ko_value_t
ko_max_is(ko_value_t x, ko_value_t y) {
   return ko_gt_is(x, y) ? x : y;
}

ko_value_t ko_add_ss_b(ko_value_t x, ko_value_t y);

static inline ko_value_t
ko_add_ss_i(ko_value_t x, ko_value_t y) {
   ko_value_t small = ko_add(x, y);
   return KO_LIKELY(
      (((ko_nint_t)small ^ (ko_nint_t)x) &
      ((ko_nint_t)small ^ (ko_nint_t)y)) >= 0
      /* small has the same sign as x or y */
   ) ?
      small :
      ko_add_ss_b(x, y);
}

static inline ko_value_t
ko_add_ssgt0_i(ko_value_t x, ko_value_t y) {
   ko_value_t small = ko_add(x, y);
   return KO_LIKELY(ko_ge(small, x)) ?
      small :
      ko_add_ss_b(x, y);
}

ko_value_t ko_sub_ss_b(ko_value_t x, ko_value_t y);

static inline ko_value_t
ko_sub_ss_i(ko_value_t x, ko_value_t y) {
   ko_value_t small = ko_sub(x, y);
   return KO_LIKELY(
      (((ko_nint_t)x ^ (ko_nint_t)y) &
      ((ko_nint_t)small ^ (ko_nint_t)x)) >= 0
      /* x and y have the same sign, or small and x have the same sign */
   ) ?
      small :
      ko_sub_ss_b(x, y);
}

static inline ko_value_t
ko_sub_ssgt0_i(ko_value_t x, ko_value_t y) {
   ko_value_t small = ko_sub(x, y);
   return KO_LIKELY(ko_le(small, x)) ?
      small :
      ko_sub_ss_b(x, y);
}

static inline ko_value_t
ko_sub_sgt0s_i(ko_value_t x, ko_value_t y) {
   ko_value_t small = ko_sub(x, y);
   return KO_LIKELY(((ko_nint_t)y & (ko_nint_t)small) >= 0) ?
      /* y is non-negative or small is non-negative */
      small :
      ko_sub_ss_b(x, y);
}

static inline ko_value_t
ko_sub_slt0s_i(ko_value_t x, ko_value_t y) {
   ko_value_t small = ko_sub(x, y);
   return KO_LIKELY(((ko_nint_t)y | (ko_nint_t)small) < 0) ?
      /* y is negative or small is negative */
      small :
      ko_sub_ss_b(x, y);
}

static inline ko_value_t
ko_neg_s_i(ko_value_t x) {
   ko_value_t small = ko_neg(x);
   return KO_LIKELY(((ko_nint_t)x & (ko_nint_t)small) >= 0) ?
      small :
      KO_STATIC_PTR(ko_neg_min_sint);
}

static inline ko_value_t
ko_neg_i_i(ko_value_t x) {
   if (ko_is_sint(x))
      return ko_neg_s_i(x);
   else {
      mpz_t result;
      mpz_init_set(result, ko_mpz_of_bint(x));
      mpz_neg(result, result);
      return ko_int_of_mpz(result);
   }
}

static inline ko_value_t
ko_abs_s_i(ko_value_t x) {
   if (ko_ge0(x))
      return x;
   else {
      ko_value_t small = ko_neg(x);
      return KO_LIKELY(ko_ge0(small)) ?
         small :
         KO_STATIC_PTR(ko_neg_min_sint);
   }
}

ko_value_t ko_mul_ss_b(ko_value_t x, ko_value_t y);

static inline ko_value_t
ko_mul_ss_i(ko_value_t x, ko_value_t y) {
   #if KO_HAVE_DINT
      ko_dint_t result = (ko_dint_t)ko_nint_of_sint(x)
         * (ko_dint_t)ko_nint_of_sint(y);
      return KO_LIKELY(result >= KO_SINT_MIN && result <= KO_SINT_MAX) ?
         ko_sint(result) :
         ko_mul_ss_b(x, y);
   #else
      #define KO_MAX_SAFE_MUL (((ko_nint_t)1 << (KO_BITS_IN_NINT / 2 - 1)) - 1)
      return KO_LIKELY(
         ko_ge(x, ko_sint(-KO_MAX_SAFE_MUL)) &&
         ko_le(x, ko_sint( KO_MAX_SAFE_MUL)) &&
         ko_ge(y, ko_sint(-KO_MAX_SAFE_MUL)) &&
         ko_le(y, ko_sint( KO_MAX_SAFE_MUL))
      ) ?
         ko_mul(x, y) :
         ko_mul_ss_b(x, y);
      #undef KO_MAX_SAFE_MUL
   #endif
}

static inline ko_value_t
ko_mul_sngt0_i(ko_value_t x, ko_nint_t y) {
   return KO_LIKELY(
      ko_ge(x, ko_sint(KO_SINT_MIN / y)) &&
      ko_le(x, ko_sint(KO_SINT_MAX / y))
   ) ?
      ko_mul_sn(x, y) :
      ko_mul_ss_b(x, ko_sint(y));
}

static inline ko_value_t
ko_mul_snlt0_i(ko_value_t x, ko_nint_t y) {
   return KO_LIKELY(
      ko_ge(x, ko_sint(KO_SINT_MAX / y)) &&
      ko_le(x, ko_sint(KO_SINT_MIN / y))
   ) ?
      ko_mul_sn(x, y) :
      ko_mul_ss_b(x, ko_sint(y));
}

ko_value_t ko_sqr_s_b(ko_value_t x);

static inline ko_value_t
ko_sqr_s_i(ko_value_t x) {
   #define KO_MAX_SAFE_SQR (((ko_nint_t)1 << (KO_BITS_IN_NINT / 2 - 1)) - 1)
   return KO_LIKELY(
      ko_ge(x, ko_sint(-KO_MAX_SAFE_SQR)) &&
      ko_le(x, ko_sint( KO_MAX_SAFE_SQR))
   ) ?
      ko_mul(x, x) :
      ko_sqr_s_b(x);
   #undef KO_MAX_SAFE_SQR
}

static inline ko_value_t
ko_quot_ss_i(ko_value_t x, ko_value_t y) {
   return KO_UNLIKELY(x == ko_sint(KO_SINT_MIN) && y == ko_sint(-1)) ?
      KO_STATIC_PTR(ko_neg_min_sint) :
      ko_quot(x, y);
}

ko_value_t ko_quot_rem_ss_ii(ko_value_t x, ko_value_t y);

static inline ko_value_t
ko_div_ss_i(ko_value_t x, ko_value_t y) {
   ko_nint_t xv = ko_nint_of_sint(x);
   ko_nint_t yv = ko_nint_of_sint(y);
   return
      yv >= 0 ? ko_sint(xv >= 0 ? xv / yv : (xv + 1) / yv - 1) :
      xv > 0 ? ko_sint((xv - 1) / yv - 1) :
      KO_UNLIKELY(xv == KO_SINT_MIN && yv == -1) ?
         KO_STATIC_PTR(ko_neg_min_sint) :
      ko_sint(xv / yv);
}

ko_value_t ko_div_mod_ss(ko_value_t x, ko_value_t y);

static inline ko_bool_t
ko_is_even_i(ko_value_t x) {
   return
      ko_is_sint(x) ? ko_is_even(x) :
      #ifdef mpz_even_p
         mpz_even_p(ko_mpz_of_bint(x));
      #else
         (mpz_get_ui(ko_mpz_of_bint(x)) & 1) == 0;
      #endif
}

/* Arguments of ko_gcd must not be 0. */
ko_nint_t ko_gcd(ko_nint_t x, ko_nint_t y);

static inline ko_value_t
ko_gcd_ss_i(ko_value_t x, ko_value_t y) {
   if (y == ko_sint(0))
      return x;
   else if (x == ko_sint(0))
      return y;
   else {
      ko_nint_t result = ko_gcd(ko_nint_of_sint(x), ko_nint_of_sint(y));
      return KO_UNLIKELY(result == -KO_SINT_MIN) ?
         KO_STATIC_PTR(ko_neg_min_sint) :
         ko_sint(result);
   }
}

ko_value_t ko_bit_shift_ss_b(ko_value_t x, ko_value_t y);

static inline ko_value_t
ko_bit_shift_ssgt0_i(ko_value_t x, ko_value_t y) {
   if (KO_LIKELY(ko_lt(y, ko_sint(KO_BITS_IN_NINT - 2)))) {
      ko_nint_t shift = ko_nint_of_sint(y);
      ko_value_t limit = (ko_value_t)((-KO_SINT_MIN >> (shift - 1)) | 1);
      if (KO_LIKELY(ko_ge(x, ko_neg(limit)) && ko_lt(x, limit)))
         return ko_shl_sn(x, shift);
   }
   return ko_bit_shift_ss_b(x, y);
}

static inline ko_value_t
ko_bit_shift_sslt0_i(ko_value_t x, ko_value_t y) {
   return KO_LIKELY(ko_ge(y, ko_sint(-(KO_BITS_IN_NINT - 2)))) ?
      ko_shr_sn(x, -ko_nint_of_sint(y)) :
      ko_ge0(x) ? ko_sint(0) : ko_sint(-1);
}

static inline ko_value_t
ko_bit_shift_ss_i(ko_value_t x, ko_value_t y) {
   return ko_gt0(y) ? ko_bit_shift_ssgt0_i(x, y) : ko_bit_shift_sslt0_i(x, y);
}

static inline ko_value_t
ko_bit_shift_ns_i(ko_nint_t x, ko_value_t y, ko_nint_t bits) {
   return ko_ge0(y) ?
      KO_LIKELY(ko_le(y, ko_sint(KO_BITS_IN_NINT - 2 - bits))) ?
         ko_sint(x << ko_nint_of_sint(y)) :
         ko_bit_shift_ss_b(ko_sint(x), y) :
      KO_LIKELY(ko_gt(y, ko_sint(-bits))) ?
         ko_sint(x >> -ko_nint_of_sint(y)) :
         x >= 0 ? ko_sint(0) : ko_sint(-1);
}

static inline ko_bool_t
ko_test_bit_ssgt0(ko_value_t x, ko_value_t i) {
   return KO_LIKELY(ko_le(i, ko_sint(KO_BITS_IN_NINT - 2))) ?
      (((ko_nint_t)x >> ko_nint_of_sint(i)) & 2) != 0 :
      ko_lt0(x);
};

static inline ko_bool_t
ko_test_bit_ss(ko_value_t x, ko_value_t i) {
   return KO_LIKELY(ko_ge0(i)) && ko_test_bit_ssgt0(x, i);
}

ko_value_t ko_set_bit_ssgt0_b(ko_value_t x, ko_value_t i);

static inline ko_value_t
ko_set_bit_ssgt0_i(ko_value_t x, ko_value_t i) {
   return KO_LIKELY(ko_le(i, ko_sint(KO_BITS_IN_NINT - 2))) ?
      (ko_value_t)((ko_nint_t)x | ((ko_nint_t)2 << ko_nint_of_sint(i))) :
      ko_lt0(x) ? x : ko_set_bit_ssgt0_b(x, i);
};

static inline ko_value_t
ko_set_bit_ss_i(ko_value_t x, ko_value_t i) {
   return KO_LIKELY(ko_ge0(i)) ? ko_set_bit_ssgt0_i(x, i) : x;
}

ko_value_t ko_clear_bit_ssgt0_b(ko_value_t x, ko_value_t i);

static inline ko_value_t
ko_clear_bit_ssgt0_i(ko_value_t x, ko_value_t i) {
   return KO_LIKELY(ko_le(i, ko_sint(KO_BITS_IN_NINT - 2))) ?
      (ko_value_t)((ko_nint_t)x & ~((ko_nint_t)2 << ko_nint_of_sint(i))) :
      ko_lt0(x) ? ko_clear_bit_ssgt0_b(x, i) : x;
};

static inline ko_value_t
ko_clear_bit_ss_i(ko_value_t x, ko_value_t i) {
   return KO_LIKELY(ko_ge0(i)) ? ko_clear_bit_ssgt0_i(x, i) : x;
}

/* ko_size_in_bits(0) is undefined. */
#if KO_HAVE_BUILTIN_CLZL
   static inline ko_nint_t
   ko_size_in_bits(ko_unint_t x) {
      return SIZEOF_LONG * CHAR_BIT - __builtin_clzl(x);
   }
#else
   ko_nint_t ko_size_in_bits(ko_unint_t x);
#endif

static inline ko_value_t
ko_size_in_bits_s(ko_value_t x) {
   return ko_sint(ko_size_in_bits((ko_unint_t)
      (KO_LIKELY(ko_ge0(x)) ? x : ko_not(x))) - 1);
}

static inline ko_nint_t
ko_round_up_to_power_of_2(ko_nint_t x) {
   return (ko_nint_t)1 << ko_size_in_bits((ko_unint_t)(x - 1));
}

static inline ko_value_t
ko_round_up_to_power_of_2_s(ko_value_t x) {
   return (ko_value_t)(((ko_nint_t)1 <<
      ko_size_in_bits((ko_unint_t)ko_sub1(x))) + 1);
}

ko_value_t ko_ratio(ko_nint_t x, ko_nint_t y, ko_nint_t d);

static inline ko_value_t
ko_divide_ss(ko_value_t x, ko_value_t y) {
   ko_nint_t xv = ko_nint_of_sint(x);
   ko_nint_t yv = ko_nint_of_sint(y);
   ko_nint_t r = xv % yv;
   return r == 0 ?
      KO_UNLIKELY(xv == KO_SINT_MIN && yv == -1) ?
         KO_STATIC_PTR(ko_neg_min_sint) :
         ko_sint(xv / yv) :
      ko_ratio(xv, yv, r);
}

ko_value_t ko_recip_s(ko_value_t x);

/* RATIO */

KO_DECLARE_DESCR(RatioCon, Ratios_Numbers_Kokogut);

KO_DECLARE_STATIC(Ratio, Ratios_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(DivideShortInt, Ratios_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(DivideShortInt, Ratios_Numbers_Kokogut, 3);

/* INF, NEG_INF */

KO_DECLARE_STATIC(Inf,    Infinity_Numbers_Kokogut, 0);
KO_DECLARE_STATIC(NegInf, Infinity_Numbers_Kokogut, 0);

KO_DECLARE_ENTRY(Min, Infinity_Numbers_Kokogut, 2);
KO_DECLARE_ENTRY(Max, Infinity_Numbers_Kokogut, 2);

/* FLOAT */

typedef double ko_float_t;
#define KO_SIZEOF_FLOAT SIZEOF_DOUBLE

KO_DECLARE_DESCR(FloatCon, Floats_Numbers_Kokogut);

struct ko_float {
   KO_OBJECT;
   ko_float_t ko_value KO_PACKED;
};

KO_DECLARE_STATIC(Float, Floats_Numbers_Kokogut, 2);
KO_DECLARE_STATIC(DefaultReal, Floats_Numbers_Kokogut, 1);

/* Time */

#if HAVE_STRUCT_TIMESPEC_TV_SEC
   typedef struct timespec ko_timespec_t;
#else
   typedef struct {time_t tv_sec; long tv_nsec;} ko_timespec_t;
#endif

/* Threads */

extern ko_value_t ko_young_threads, ko_old_threads, ko_current_thread,
   ko_main_thread, ko_receives_signals, ko_sleeping_threads, ko_watched_files;

struct ko_thread_state;

#define KO_REGISTERS_IN_THREAD (KO_MAX_FAST_ARITY + 1)

#if KO_USE_PTHREADS
   struct ko_os_thread;
#endif

struct ko_thread {
   KO_CHANGED_OBJECT;
   struct ko_thread_state *state;
   ko_value_t value;
   ko_value_t prev_of_all, next_of_all;
   ko_value_t prev, next;
   ko_timespec_t wakeup;
   ko_value_t prev_related, next_related;
   ko_value_t first_joining, last_joining;
   ko_value_t first_signal, last_signal;
   ko_value_t read_locks;
   ko_nint_t penalty;
   ko_bool_t current;
   ko_code_t code;
   ko_value_t registers[KO_REGISTERS_IN_THREAD];
   ko_code_t ret;
   ko_stack_elem_t *stack_begin, *stack_end, *stack_ptr;
   ko_value_t try_offset;
   ko_value_t dynamic;
   ko_value_t blocked_all, blocked_async;
   #if KO_USE_PTHREADS
      struct ko_os_thread *bound;
   #endif
};

/*
 * Threads which are not finished or failed are on lists which start
 * in 'ko_young_threads' and 'ko_old_threads', linked using fields
 * 'prev_of_all' and 'next_of_all'. These links behave like weak
 * references.
 *
 * Threads which are running, reading and writing are all linked in a
 * single ring using fields 'prev' and 'next'. One of these threads is
 * pointed by 'ko_current_thread'.
 *
 * Threads which are blocked on the same synchronization object are on
 * a list which starts in the synchronization object, linked using
 * fields 'prev_related' and 'next_related'.
 *
 * Threads which are sleeping and timed-receiving are on a list which
 * starts in 'ko_sleeping_threads', linked using fields 'prev_related'
 * and 'next_related'. They are sorted according to the time of
 * wake-up.
 *
 * 'read_locks' is a stack of mutexes locked by this thread for
 * reading. It is used only for explicit checking whether the thread
 * has locked the given mutex, and for detecting errors when
 * unlocking.
 *
 * 'current' is true when the thread doesn't have fields 'code',
 * 'registers', 'ret', 'stack_begin', 'stack_end', 'stack_ptr',
 * 'try_offset', 'dynamic', 'blocked_all', and 'blocked_async' filled.
 * Their logical values are stored in the global state of the runtime.
 *
 * 'bound' points to an OS thread when the thread is bound or foreign
 * (or both).
 */

KO_OBJECT_FIELD_RAW(thread, state, struct ko_thread_state *);
KO_OBJECT_FIELD_RAW(thread, value, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, prev_of_all, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, next_of_all, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, prev, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, next, ko_value_t);
KO_OBJECT_FIELD_PTR(thread, wakeup, ko_timespec_t);
KO_OBJECT_FIELD_RAW(thread, prev_related, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, next_related, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, first_joining, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, last_joining, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, first_signal, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, last_signal, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, read_locks, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, penalty, ko_nint_t);
KO_OBJECT_FIELD_RAW(thread, current, ko_bool_t);
KO_OBJECT_FIELD_RAW(thread, code, ko_code_t);
KO_OBJECT_FIELD_ARRAY(thread, registers, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, ret, ko_code_t);
KO_OBJECT_FIELD_RAW(thread, stack_begin, ko_stack_elem_t *);
KO_OBJECT_FIELD_RAW(thread, stack_end, ko_stack_elem_t *);
KO_OBJECT_FIELD_RAW(thread, stack_ptr, ko_stack_elem_t *);
KO_OBJECT_FIELD_RAW(thread, try_offset, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, dynamic, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, blocked_all, ko_value_t);
KO_OBJECT_FIELD_RAW(thread, blocked_async, ko_value_t);
#if KO_USE_PTHREADS
   KO_OBJECT_FIELD_RAW(thread, bound, struct ko_os_thread *);
#endif

KO_DECLARE_ENTRY(AbsoluteTicksInFuture, Time_Kokogut, 1);
KO_DECLARE_ENTRY(Handle, Signals_Threads_Kokogut, 2);
KO_DECLARE_ENTRY(Handle, Signals_Threads_Kokogut, 1);
KO_DECLARE_STATIC(Signal, Signals_Threads_Kokogut, 1);
KO_DECLARE_ENTRY(Receive, ReceiveSignal_Threads_Kokogut, 0);
KO_DECLARE_ENTRY(Receive, ReceiveSignal_Threads_Kokogut, 1);

/* Moving the stack frame when an exception is considered handled */

static inline ko_bool_t
ko_signal_pending(void) {
   return ko_first_signal_of_thread(ko_current_thread) != KO_ABSENT;
}

KO_DECLARE_ENTRY(HandleSignals, Signals_Threads_Kokogut, 0);
#define KO_HandleSignals0 KO_ENTRY(HandleSignals, Signals_Threads_Kokogut, 0)

#define KO_EXCEPTION_HANDLED(size)                                       \
   KO_STAT_BEGIN {                                                       \
      ko_stack_elem_t *ko_exn_stack;                                     \
      if (                                                               \
         KO_UNLIKELY(ko_signal_pending()) &&                             \
         ko_blocked_all == ko_sint(1) && ko_blocked_async == ko_sint(0)  \
      ) {                                                                \
         ko_callback_safety = KO_CALLBACKS_SAFE;                         \
         KO_CALL(KO_HandleSignals0);                                     \
         ko_callback_safety = KO_CALLBACKS_UNSAFE;                       \
         if (ko_failed) KO_JUMP(ko_fail);                                \
      }                                                                  \
      ko_exn_stack = ko_stack_begin                                      \
         + ko_nint_of_sint(KO_S[-(size) - 5].ko_value);                  \
      ko_blocked_all = ko_sub1(ko_blocked_all);                          \
      KO_S[-2].ko_code = ko_exn_stack[-2].ko_code;                       \
      memmove(ko_exn_stack - 7, KO_S - (size), (size) * KO_SIZEOF_NINT); \
      KO_S = ko_exn_stack - 7 + (size);                                  \
   } KO_STAT_END

/* MUTEX */

KO_DECLARE_DESCR(MutexCon, Mutexes_Threads_Kokogut);

/* Variables */

KO_DECLARE_DESCR(VarCon, Variables_Kokogut);
KO_DECLARE_DESCR(AnonForwardCon, Variables_Kokogut);
KO_DECLARE_DESCR(NamedForwardCon, Variables_Kokogut);

/* DYNAMIC */

KO_DECLARE_DESCR(DynamicCon, Dynamic_Kokogut);

KO_RECORD_FIELD(dynamic, 0, value);

KO_RECORD_FIELD(dynamic_cell, 0, variable);
KO_RECORD_FIELD(dynamic_cell, 1, value);
KO_RECORD_FIELD(dynamic_cell, 2, next);

static inline ko_value_t
ko_get_dynamic(ko_value_t variable) {
   ko_value_t cell = ko_dynamic;
   while (cell != KO_ABSENT) {
      if (ko_variable_of_dynamic_cell(cell) == variable)
         return ko_value_of_dynamic_cell(cell);
      cell = ko_next_of_dynamic_cell(cell);
   }
   return ko_value_of_dynamic(variable);
}

static inline void
ko_set_dynamic(ko_value_t variable, ko_value_t value) {
   ko_value_t cell = ko_dynamic;
   while (cell != KO_ABSENT) {
      if (ko_variable_of_dynamic_cell(cell) == variable) {
         ko_set_value_of_dynamic_cell(cell, value);
         return;
      }
      cell = ko_next_of_dynamic_cell(cell);
   }
   ko_set_value_of_dynamic(variable, value);
}

/* Dynamic variables which are globally undefined */

KO_DECLARE_DESCR(DynamicMayFailCon, Dynamic_Kokogut);

KO_RECORD_FIELD(dynamic, 0, name);

#define KO_FailNotDefined2 KO_ENTRY(FailNotDefined, Exceptions_Kokogut, 2)

#define KO_GET_DYNAMIC_MAY_FAIL(result, variable, name, loc)                  \
   KO_STAT_BEGIN {                                                            \
      ko_value_t cell_ = ko_dynamic;                                          \
      for(;;) {                                                               \
         if (KO_UNLIKELY(cell_ == KO_ABSENT))                                 \
            KO_FAIL_BY_JUMP2(KO_FailNotDefined2, name, loc);                  \
         if (ko_variable_of_dynamic_cell(cell_) == (variable)) {              \
            result = ko_value_of_dynamic_cell(cell_);                         \
            break;                                                            \
         }                                                                    \
         cell_ = ko_next_of_dynamic_cell(cell_);                              \
      }                                                                       \
   } KO_STAT_END

#define KO_SET_DYNAMIC_MAY_FAIL(variable, value, name, loc)                   \
   KO_STAT_BEGIN {                                                            \
      ko_value_t cell_ = ko_dynamic;                                          \
      for(;;) {                                                               \
         if (KO_UNLIKELY(cell_ == KO_ABSENT))                                 \
            KO_FAIL_BY_JUMP2(KO_FailNotDefined2, name, loc);                  \
         if (ko_variable_of_dynamic_cell(cell_) == (variable)) {              \
            ko_set_value_of_dynamic_cell(cell_, value);                       \
            break;                                                            \
         }                                                                    \
         cell_ = ko_next_of_dynamic_cell(cell_);                              \
      }                                                                       \
   } KO_STAT_END

/* LAZY */

KO_DECLARE_DESCR(LazyComputeCon, Lazy_Kokogut);

/* PAIR */

KO_DECLARE_DESCR(Pair, Pairs_Kokogut);

KO_RECORD_FIELD(pair, 0, left);
KO_RECORD_FIELD(pair, 1, right);

#define KO_PairCon KO_DESCR(Pair, Pairs_Kokogut)

/* SYMBOL */

KO_DECLARE_DESCR(SymbolCon, Symbols_Kokogut);

struct ko_symbol {
   ko_value_t ko_ptr;
   struct {
      KO_OBJECT;
      ko_value_t ko_fields[3];
   } ko_obj;
};

struct ko_symbol_ptr {
   ko_value_t *ko_ptr;
   ko_nint_t ko_symbol;
};

void ko_intern_symbols(struct ko_symbol *symbols, ko_nint_t count,
   struct ko_symbol_ptr *ptrs, ko_nint_t ptrCount);

/* TYPE */

KO_DECLARE_TYPE(TYPE, Types_Kokogut);
KO_DECLARE_DESCR(TypeCon, Types_Kokogut);

KO_RECORD_FIELD(type, 0, name);
KO_RECORD_FIELD_RAW(type, 1, hash);
KO_RECORD_FIELD(type, 2, supertypes);
KO_RECORD_FIELD_RAW(type, 3, final);

KO_DECLARE_ENTRY(TypeDictEntries, Types_Kokogut, 0);
KO_DECLARE_DESCR(TypeDictCon, Types_Kokogut);
KO_DECLARE_ENTRY(HasType, Types_Kokogut, 1);
KO_DECLARE_ENTRY(HasType, Types_Kokogut, 2);
KO_DECLARE_ENTRY(HasType, Types_Kokogut, 3);
KO_DECLARE_LIST_ENTRY(HasType, Types_Kokogut);
KO_DECLARE_ENTRY(UnsafeHasType, Types_Kokogut, 1);
KO_DECLARE_ENTRY(UnsafeHasType, Types_Kokogut, 2);
KO_DECLARE_ENTRY(UnsafeHasType, Types_Kokogut, 3);
KO_DECLARE_LIST_ENTRY(UnsafeHasType, Types_Kokogut);
KO_DECLARE_ENTRY(CheckType, Types_Kokogut, 3);
KO_DECLARE_ENTRY(CheckType, Types_Kokogut, 4);
KO_DECLARE_LIST_ENTRY(CheckType, Types_Kokogut);
KO_DECLARE_ENTRY(UnsafeCheckType, Types_Kokogut, 3);
KO_DECLARE_ENTRY(UnsafeCheckType, Types_Kokogut, 4);
KO_DECLARE_LIST_ENTRY(UnsafeCheckType, Types_Kokogut);

static inline ko_value_t
ko_type_of(ko_value_t obj) {
   return ko_descr(obj)->ko_type;
}

/* TYPE_DICT_ENTRIES */

KO_RECORD_FIELD_RAW(type_dict_entries, 0, next);
KO_RECORD_FIELD_RAW(type_dict_entries, 1, mask);
KO_RECORD_FIELD_RAW(type_dict_entries, 2, removed);

static inline ko_value_t *
ko_elems_of_type_dict_entries(ko_value_t entries) {
   return ko_field_ptr(entries, 3);
}

static inline ko_value_t *
ko_key_ptr_at_type_dict_entries(ko_value_t entries, ko_value_t i) {
   return ko_shift_values(ko_elems_of_type_dict_entries(entries),
      ko_mul_sn(i, 2));
}

static inline ko_value_t
ko_key_at_type_dict_entries(ko_value_t entries, ko_value_t i) {
   return *ko_key_ptr_at_type_dict_entries(entries, i);
}

static inline ko_value_t *
ko_value_ptr_at_type_dict_entries(ko_value_t entries, ko_value_t i) {
   return ko_shift_values(ko_elems_of_type_dict_entries(entries),
      ko_add_sn(ko_mul_sn(i, 2), 1));
}

static inline ko_value_t
ko_value_at_type_dict_entries(ko_value_t entries, ko_value_t i) {
   return *ko_value_ptr_at_type_dict_entries(entries, i);
}

/* TYPE_DICT */

KO_RECORD_FIELD(type_dict, 0, entries);
KO_RECORD_FIELD_RAW(type_dict, 1, size); /* Includes removed entries. */

/* The algorithm for resolving collisions is taken from Python. */
static inline ko_bool_t
ko_at_type_dict(ko_value_t dict, ko_value_t key, ko_value_t *value) {
   ko_value_t entries = ko_entries_of_type_dict(dict);
   ko_value_t hash = ko_hash_of_type(key);
   ko_value_t mask = ko_mask_of_type_dict_entries(entries);
   ko_value_t i = ko_and(hash, mask);
   ko_value_t found = ko_key_at_type_dict_entries(entries, i);
   ko_value_t perturb;
   if (found == key) {
      *value = ko_value_at_type_dict_entries(entries, i);
      return ko_true;
   }
   if (KO_UNLIKELY(found == KO_ABSENT)) return ko_false;
   perturb = hash;
   for(;;) {
      i = ko_and(ko_add1(ko_add(ko_mul_sn(i, 5), perturb)), mask);
      found = ko_key_at_type_dict_entries(entries, i);
      if (found == key) {
         *value = ko_value_at_type_dict_entries(entries, i);
         return ko_true;
      }
      if (KO_UNLIKELY(found == KO_ABSENT)) return ko_false;
      perturb = (ko_value_t)(((ko_unint_t)perturb >> 5) | 1);
   }
}

/* FUNCTION */

KO_DECLARE_TYPE(UNNAMED0uFUNCTION, Functions_Kokogut);
KO_DECLARE_TYPE(NAMED0uFUNCTION, Functions_Kokogut);
KO_DECLARE_DESCR(FunctionDescrCon, Functions_Kokogut);
KO_DECLARE_DESCR(GenericCaseCon, Functions_Kokogut);
KO_DECLARE_ENTRY(DefineMethod, Functions_Kokogut, 5);
KO_DECLARE_ENTRY(DefineMethodSuper, Functions_Kokogut, 5);
KO_DECLARE_ENTRY(Dispatch, Functions_Kokogut, 4);

/* Singletons and records */

KO_DECLARE_TYPE(SINGLETON, Records_Kokogut);
KO_DECLARE_TYPE(RECORD, Records_Kokogut);
KO_DECLARE_STATIC(SingletonName, Core_Kokogut, 2);
KO_DECLARE_STATIC(RecordConstructor, Core_Kokogut, 2);
KO_DECLARE_STATIC(RecordFields, Core_Kokogut, 2);

/* Modules */

KO_DECLARE_DESCR(ModuleCon, Modules_Kokogut);

/* COLLECTION */

KO_DECLARE_STATIC(264, Core_Collections_Kokogut, 3);
KO_DECLARE_ENTRY(264, Core_Collections_Kokogut, 2);
KO_DECLARE_ENTRY(264, Core_Collections_Kokogut, 3);
KO_DECLARE_LIST_ENTRY(264, Core_Collections_Kokogut);

KO_DECLARE_STATIC(Absent, Core_Collections_Kokogut, 1);
KO_DECLARE_STATIC(Present, Core_Collections_Kokogut, 1);

/* LIST */

KO_DECLARE_TYPE(LIST, Lists_Kokogut);
KO_DECLARE_DESCR(UnsafeCons, Lists_Kokogut);
KO_DECLARE_STATIC(Nil, Lists_Kokogut, 0);

#define KO_Nil KO_STATIC(Nil, Lists_Kokogut)
#define KO_ConsCon KO_DESCR(UnsafeCons, Lists_Kokogut)

static inline ko_value_t
ko_first(ko_value_t list) {
   return ko_field(list, 0);
}

static inline void
ko_init_first(ko_value_t cons, ko_value_t first) {
   ko_init_field(cons, 0, first);
}

static inline ko_value_t
ko_rest(ko_value_t list) {
   return ko_field(list, 1);
}

static inline void
ko_init_rest(ko_value_t cons, ko_value_t rest) {
   ko_init_field(cons, 1, rest);
}

static inline ko_nint_t
ko_nsize_of_list(ko_value_t list) {
   ko_nint_t size = 0;
   while (list != KO_Nil) {
      ++size;
      list = ko_rest(list);
   }
   return size;
}

static inline ko_value_t
ko_size_of_cons(ko_value_t list) {
   ko_value_t size = ko_sint(0);
   do {
      size = ko_add1(size);
      list = ko_rest(list);
   } while (list != KO_Nil);
   return size;
}

static inline ko_value_t
ko_size_of_list(ko_value_t list) {
   ko_value_t size = ko_sint(0);
   while (list != KO_Nil) {
      size = ko_add1(size);
      list = ko_rest(list);
   }
   return size;
}

static inline ko_nint_t
ko_nsize_of_cons(ko_value_t list) {
   ko_nint_t size = 0;
   do {
      ++size;
      list = ko_rest(list);
   } while (list != KO_Nil);
   return size;
}

#define KO_APPEND_CONS(result, list1, list2)          \
   KO_STAT_BEGIN {                                    \
      if ((list1) == KO_Nil)                          \
         result = list2;                              \
      else {                                          \
         ko_nint_t size_ = ko_nsize_of_cons(list1);   \
         ko_value_t src_, dest_;                      \
         result = ko_alloc(size_ * 3);                \
         src_ = list1;                                \
         dest_ = result;                              \
         for(;;) {                                    \
            ko_init_descr(dest_, KO_ConsCon);         \
            ko_init_first(dest_, ko_first(src_));     \
            src_ = ko_rest(src_);                     \
            if (src_ == KO_Nil) break;                \
            ko_init_rest(dest_, ko_offset(dest_, 3)); \
            dest_ = ko_offset(dest_, 3);              \
         }                                            \
         ko_init_rest(dest_, list2);                  \
      }                                               \
   } KO_STAT_END

#define KO_APPEND_LIST(result, list1, list2)   \
   KO_STAT_BEGIN {                             \
      if ((list2) == KO_Nil)                   \
         result = list1;                       \
      else                                     \
         KO_APPEND_CONS(result, list1, list2); \
   } KO_STAT_END

static inline ko_value_t
ko_list_size_plus(ko_value_t list, ko_value_t size) {
   while (list != KO_Nil) {
      size = ko_add1(size);
      list = ko_rest(list);
   }
   return size;
}

#define KO_SPLIT_LIST(begin, end, list, size)         \
   KO_STAT_BEGIN {                                    \
      ko_nint_t n_ = ko_nint_of_sint(size);           \
      if (n_ == 0) {                                  \
         begin = KO_Nil;                              \
         end = list;                                  \
      }                                               \
      else {                                          \
         ko_value_t src_, dest_;                      \
         begin = ko_alloc(n_ * 3);                    \
         src_ = list;                                 \
         dest_ = begin;                               \
         for(;;) {                                    \
            ko_init_descr(dest_, KO_ConsCon);         \
            ko_init_first(dest_, ko_first(src_));     \
            src_ = ko_rest(src_);                     \
            if (--n_ == 0) break;                     \
            ko_init_rest(dest_, ko_offset(dest_, 3)); \
            dest_ = ko_offset(dest_, 3);              \
         }                                            \
         ko_init_rest(dest_, KO_Nil);                 \
         end = src_;                                  \
      }                                               \
   } KO_STAT_END

static inline ko_value_t
ko_list_end(ko_value_t list, ko_value_t size) {
   ko_nint_t n = ko_nint_of_sint(size);
   while (n != 0) {
      list = ko_rest(list);
      --n;
   }
   return list;
}

/* STRING */

KO_DECLARE_TYPE(STRING, Strings_Kokogut);

KO_DECLARE_DESCR(NStringCon, Strings_Kokogut);
KO_DECLARE_DESCR(WStringCon, Strings_Kokogut);

#if KO_HAVE_FLEXIBLE_ARRAY_MEMBERS
   typedef struct {
      KO_OBJECT;
      ko_value_t ko_size;
      ko_nchar_t ko_chars[];
   } ko_nstring_t;
   typedef struct {
      KO_OBJECT;
      ko_value_t ko_size;
      ko_wchar_t ko_chars[];
   } ko_wstring_t;
   #define KO_NSTRING_T(size) ko_nstring_t
   #define KO_WSTRING_T(size) ko_wstring_t
   typedef ko_nstring_t ko_nstring_t_0;
   typedef struct {
      KO_OBJECT;
      ko_value_t ko_size;
      ko_nchar_t ko_chars[KO_SIZEOF_NINT];
   } ko_nstring_t_1;
#else
   #define KO_NSTRING_T(size)                          \
      struct {                                         \
         KO_OBJECT;                                    \
         ko_value_t ko_size;                           \
         ko_nchar_t ko_chars[((size) + KO_SIZEOF_NINT) \
            & ~(KO_SIZEOF_NINT - 1)];                  \
      }
   #define KO_WSTRING_T(size)       \
      struct {                      \
         KO_OBJECT;                 \
         ko_value_t ko_size;        \
         ko_wchar_t ko_chars[size]; \
      }
   typedef KO_NSTRING_T(256) ko_nstring_t;
   typedef KO_WSTRING_T(256) ko_wstring_t;
   typedef KO_NSTRING_T(0) ko_nstring_t_0;
   typedef KO_NSTRING_T(1) ko_nstring_t_1;
#endif

#if KO_SIZEOF_NINT == 4
   #define KO_NSTRING_END_0 '\0', '\0', '\0', '\0'
   #define KO_NSTRING_END_1 '\0', '\0', '\0'
   #define KO_NSTRING_END_2 '\0', '\0'
   #define KO_NSTRING_END_3 '\0'
#elif KO_SIZEOF_NINT == 8
   #define KO_NSTRING_END_0 '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'
   #define KO_NSTRING_END_1 '\0', '\0', '\0', '\0', '\0', '\0', '\0'
   #define KO_NSTRING_END_2 '\0', '\0', '\0', '\0', '\0', '\0'
   #define KO_NSTRING_END_3 '\0', '\0', '\0', '\0', '\0'
#else
   #error Strange ko_nint_t size
#endif
#define KO_NSTRING_END_4 '\0', '\0', '\0', '\0'
#define KO_NSTRING_END_5 '\0', '\0', '\0'
#define KO_NSTRING_END_6 '\0', '\0'
#define KO_NSTRING_END_7 '\0'

extern ko_nstring_t_0 ko_empty_string;
extern ko_nstring_t_1 ko_nchars[KO_NCHARS];
#define KO_NCHAR(ch) KO_STATIC_PTR(ko_nchars[ch])

ko_value_t ko_make_wchar(ko_wchar_t ch);

static inline ko_value_t
ko_make_char(ko_wchar_t ch) {
   return KO_LIKELY(ch < KO_NCHARS) ? KO_NCHAR(ch) : ko_make_wchar(ch);
}

static inline ko_value_t
ko_char(ko_value_t code) {
   return ko_make_char(ko_nint_of_sint(code));
}

static inline ko_bool_t
ko_string_is_narrow(ko_value_t obj) {
   return ko_descr_of_ptr(obj) == KO_DESCR(NStringCon, Strings_Kokogut);
}

static inline ko_nchar_t
ko_first_of_nstring(ko_value_t str) {
   return ((ko_nstring_t *)str)->ko_chars[0];
}

static inline ko_wchar_t
ko_first_of_wstring(ko_value_t str) {
   return ((ko_wstring_t *)str)->ko_chars[0];
}

static inline ko_wchar_t
ko_first_of_string(ko_value_t str) {
   return KO_LIKELY(ko_string_is_narrow(str)) ?
      ko_first_of_nstring(str) :
      ko_first_of_wstring(str);
}

static inline ko_value_t
ko_char_code(ko_value_t str) {
   return ko_sint(ko_first_of_string(str));
}

ko_bool_t ko_eq_nnstring(ko_value_t str1, ko_value_t str2);
ko_bool_t ko_eq_wwstring(ko_value_t str1, ko_value_t str2);

static inline ko_bool_t
ko_ne_nnstring(ko_value_t str1, ko_value_t str2) {
   return !ko_eq_nnstring(str1, str2);
}
static inline ko_bool_t
ko_ne_wwstring(ko_value_t str1, ko_value_t str2) {
   return !ko_eq_wwstring(str1, str2);
}

ko_bool_t ko_lt_nxstring(ko_value_t str1, ko_value_t str2);
ko_bool_t ko_lt_wxstring(ko_value_t str1, ko_value_t str2);
ko_bool_t ko_lt_xnstring(ko_value_t str1, ko_value_t str2);
ko_bool_t ko_lt_xwstring(ko_value_t str1, ko_value_t str2);
ko_bool_t ko_le_nxstring(ko_value_t str1, ko_value_t str2);
ko_bool_t ko_le_wxstring(ko_value_t str1, ko_value_t str2);
ko_bool_t ko_le_xnstring(ko_value_t str1, ko_value_t str2);
ko_bool_t ko_le_xwstring(ko_value_t str1, ko_value_t str2);

KO_DECLARE_ENTRY(Char, Strings_Kokogut, 1);
KO_DECLARE_ENTRY(CharCode, Strings_Kokogut, 1);

/* VECTOR */

KO_DECLARE_DESCR(ObjectVectorCon, Vectors_Data_Kokogut);

KO_RECORD_FIELD_RAW(vector, 0, size);

static inline ko_value_t *
ko_elems_of_object_vector(ko_value_t vec) {
   return ko_field_ptr(vec, 1);
}

static inline ko_value_t
ko_alloc_object_vector(ko_value_t size) {
   ko_value_t vec = ko_alloc(ko_nint_of_sint(ko_add(size, ko_sint(2))));
   ko_init_descr(vec, KO_DESCR(ObjectVectorCon, Vectors_Data_Kokogut));
   ko_init_size_of_vector(vec, size);
   return vec;
}

ko_value_t ko_unsafe_alloc_object_vector(ko_value_t size);

static inline ko_value_t *
ko_ptr_at_object_vector(ko_value_t vec, ko_value_t i) {
   return ko_shift_values(ko_elems_of_object_vector(vec), i);
}

static inline ko_value_t
ko_at_object_vector(ko_value_t vec, ko_value_t i) {
   return *ko_ptr_at_object_vector(vec, i);
}

static inline void
ko_init_at_object_vector(ko_value_t vec, ko_value_t i, ko_value_t elem) {
   *ko_ptr_at_object_vector(vec, i) = elem;
}

static inline void
ko_set_at_object_vector(ko_value_t vec, ko_value_t i, ko_value_t elem) {
   ko_set_field_by_ptr(vec, ko_ptr_at_object_vector(vec, i), elem);
}

/* Tracing */

void ko_trace_call_global(const char *fun, ko_nint_t arity);
void ko_trace_call(const char *fun, ko_nint_t arity);
void ko_trace_return(const char *fun);
void ko_trace_return_with_frame(const char *fun);
void ko_trace_tail_call(const char *fun);
void ko_trace_tail_recursion(const char *fun);
void ko_trace_module(const char *module);
void ko_trace_move(ko_value_t obj);
void ko_trace_finalize(ko_value_t obj);
void ko_trace_save(ko_value_t obj);
void ko_trace_message(const char *msg, ...);
void ko_trace_message_loc(const char *loc, const char *fun, const char *msg,
   ...);

#ifdef __cplusplus
}
#endif

#endif /* KOKOGUT_RUNTIME_H */
