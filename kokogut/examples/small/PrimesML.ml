(* Lazy lists *)
type 'a llist = Cons of 'a * 'a llist Lazy.t

let rec from start step = Cons (start, lazy (from (start + step) step))

let rec filter pred (Cons (x, xs)) =
   if pred x then Cons (x, lazy (filter pred (Lazy.force xs)))
   else filter pred (Lazy.force xs)

(* Primes *)
let rec check_prime (Cons (p, ps)) n =
   p * p > n || n mod p <> 0 && check_prime (Lazy.force ps) n

let rec primes = Cons (2, lazy (filter (check_prime primes) (from 3 2)))

let rec print_part (Cons (x, xs)) = function
  | 0 -> print_newline ()
  | n -> print_int x; print_char ' '; print_part (Lazy.force xs) (n - 1)
let _ = print_part primes 1000
