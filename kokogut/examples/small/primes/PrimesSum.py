#!/usr/bin/python

def main():
    D = 1000
    N = 2*D*D
    IsPrime = [True] * N
    S = 2

    for i in xrange(D):
        if IsPrime[i]:
            p = 2*i+3
            S += p % 10
            for j in xrange(i+p, N, p):
                IsPrime[j] = False

    for i in xrange(D, N):
        if IsPrime[i]:
            S += (2*i+3) % 10

    print S

main()
