let d = 1000;;
let n = 2 * d * d;;
let isPrime = Array.make n true;;
let s = ref 2;;

for i = 0 to d - 1 do
   if isPrime.(i) then begin
      let p = 2 * i + 3 in
      s := !s + p mod 10;
      let j = ref (i + p) in
      while !j < n do
         isPrime.(!j) <- false;
         j := !j + p
      done
   end
done;;

for i = d to n - 1 do
   if isPrime.(i) then
      s := !s + (2 * i + 3) mod 10
done;;

print_int !s;
print_newline ();;
