#!/usr/bin/python

def main():
    D = 1000
    N = 2*D*D
    IsPrime = [True] * N
    print 2,

    for i in xrange(D):
        if IsPrime[i]:
            p = 2*i+3
            print p,
            for j in xrange(i+p, N, p):
                IsPrime[j] = False

    for i in xrange(D, N):
        if IsPrime[i]:
            print 2*i+3,

    print

main()
