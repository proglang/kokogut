#include <stdbool.h>
#include <stdio.h>

int main(void) {
   int D = 1000;
   int N = 2*D*D;
   static bool IsPrime[2*1000*1000];
   int i;
   for (i = 0; i < N; ++i)
      IsPrime[i] = true;
   printf("2");

   for (i = 0; i < D; ++i) {
      if (IsPrime[i]) {
         int j;
         int p = 2*i+3;
         printf(" %d", p);
         for (j = i+p; j < N; j += p)
            IsPrime[j] = false;
      }
   }

   for (i = D; i < N; ++i)
      if (IsPrime[i])
         printf(" %d", 2*i+3);

   printf("\n");
   return 0;
}
