using System.Console;

class Primes {
  static Main () : void
  {
    def d = 1000;
    def n = 2 * d * d;
    def isNotPrime = array (n);
    Write ("2");

    for (mutable i <- 0; i < d; i <- i + 1) {
      unless (isNotPrime[i]) {
        def p = 2 * i + 3;
        Write (" {0}", p);
        for (mutable j <- i + p; j < n; j <- j + p)
          isNotPrime[j] <- true
      }
    };

    for (mutable i <- d; i < n; i <- i + 1) {
      unless (isNotPrime[i])
        Write (" {0}", 2 * i + 3)
    };

    WriteLine ()
  }
}
