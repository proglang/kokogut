#!/usr/bin/perl

$D = 1000;
$N = 2*$D*$D;
@IsPrime = (1) x $N;
$S = 2;

foreach my $i (0..$D-1) {
   if ($IsPrime[$i]) {
      my $p = 2*$i+3;
      $S += $p % 10;
      for (my $j = $i+$p; $j < $N; $j += $p) {
         $IsPrime[$j] = 0;
      }
   }
}

foreach my $i ($D..$N-1) {
   if ($IsPrime[$i]) {
      $S += (2*$i+3) % 10;
   }
}

print "$S\n";
