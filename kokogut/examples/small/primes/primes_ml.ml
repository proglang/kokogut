let d = 1000;;
let n = 2 * d * d;;
let isPrime = Array.make n true;;
print_int 2;;

for i = 0 to d - 1 do
   if isPrime.(i) then begin
      let p = 2 * i + 3 in
      print_char ' ';
      print_int p;
      let j = ref (i + p) in
      while !j < n do
         isPrime.(!j) <- false;
         j := !j + p
      done
   end
done;;

for i = d to n - 1 do
   if isPrime.(i) then begin
      print_char ' ';
      print_int (2 * i + 3);
   end
done;;

print_newline ();;
