#include <stdbool.h>
#include <stdio.h>

int main(void) {
   int D = 1000;
   int N = 2*D*D;
   static bool IsPrime[2*1000*1000];
   int S = 2;
   int i;
   for (i = 0; i < N; ++i)
      IsPrime[i] = true;

   for (i = 0; i < D; ++i) {
      if (IsPrime[i]) {
         int j;
         int p = 2*i+3;
         S += p % 10;;
         for (j = i+p; j < N; j += p)
            IsPrime[j] = false;
      }
   }

   for (i = D; i < N; ++i)
      if (IsPrime[i])
         S += (2*i+3) % 10;

   printf("%d\n", S);
   return 0;
}
