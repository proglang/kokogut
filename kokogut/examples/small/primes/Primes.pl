#!/usr/bin/perl

$D = 1000;
$N = 2*$D*$D;
@IsPrime = (1) x $N;
print 2;

foreach my $i (0..$D-1) {
   if ($IsPrime[$i]) {
      my $p = 2*$i+3;
      print " $p";
      for (my $j = $i+$p; $j < $N; $j += $p) {
         $IsPrime[$j] = 0;
      }
   }
}

foreach my $i ($D..$N-1) {
   if ($IsPrime[$i]) {
      print " ", 2*$i+3;
   }
}

print "\n";
