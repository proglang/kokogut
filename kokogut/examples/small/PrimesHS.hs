checkPrime :: Int -> [Int] -> Bool
checkPrime p (q:qs) = q*q > p || p `mod` q /= 0 && checkPrime p qs

primes :: [Int]
primes = 2 : [p | p <- [3, 5..], checkPrime p primes]

main = print $ take 1000 primes
