// This file is a part of an example distributed with Kokogut.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004,2006-2007,2009 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// Kokogut is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module KoScheme.Printer [KSWriteTo WriteResults] =>

use KoScheme.Values;

def KSWriteTo str val! r {};

method KSWriteTo str _^TRUE  _ {WriteTo str "#t"};
method KSWriteTo str _^FALSE _ {WriteTo str "#f"};

def KSWriteListTo str val! r {};

method KSWriteListTo str val^KS_PAIR r {
   WriteTo str " ";
   KSWriteTo str val.car r;
   KSWriteListTo str val.cdr r
};

method KSWriteListTo str _^KS_NIL _ {WriteTo str ")"};

method KSWriteListTo str val r {
   WriteTo str " . ";
   KSWriteTo str val r;
   WriteTo str ")"
};

method KSWriteTo str val^KS_PAIR r {
   WriteTo str "(";
   KSWriteTo str val.car r;
   KSWriteListTo str val.cdr r
};

method KSWriteTo str _^KS_NIL _ {WriteTo str "()"};

method KSWriteTo str val^SYMBOL _ {WriteTo str val};

method KSWriteTo str val^NUMBER _ {WriteTo str (ShowNumber val)};

method KSWriteTo str val^STRING r {
   if r {
      case val [
         (<= " ") {WriteTo str "#\\" ControlCharacterNames@(CharCode val)}
         "\127;"  {WriteTo str "#\\del"}
         _        {WriteTo str "#\\" val}
      ]
   } {WriteTo str val}
};

method KSWriteTo str val^KS_STRING r {
   if r {
      WriteTo str "\"";
      Each val ?ch {
         WriteTo str (case ch [
            "\"" {"\\\""}
            "\\" {"\\\\"}
            _    {ch}
         ])
      };
      WriteTo str "\""
   } {WriteTo str val}
};

method KSWriteTo str val^KS_VECTOR r {
   WriteTo str "#(";
   let size = Size val;
   if (size > 0) {
      KSWriteTo str val@0 r;
      Each (Range 1 size) ?i {
         WriteTo str " ";
         KSWriteTo str val@i r
      }
   };
   WriteTo str ")"
};

method KSWriteTo str _^CHAR_INPUT _ {WriteTo str "#<input-port>"};

method KSWriteTo str _^CHAR_OUTPUT _ {WriteTo str "#<output-port>"};

method KSWriteTo str _^KS_EOF_OBJECT _ {WriteTo str "#<eof-object>"};

method KSWriteTo str val^KS_NAMED_PROCEDURE _ {
   WriteTo str "#<procedure:" val.name ">"
};

method KSWriteTo str _^FUNCTION _ {WriteTo str "#<procedure>"};

method KSWriteTo str _^PROMISE _ {WriteTo str "#<promise>"};

method KSWriteTo str _^ENVIRONMENT _ {WriteTo str "#<environment>"};

method KSWriteTo str _^NULL _ {WriteTo str "#<unspecified>"};

def WriteResults [
   x  {if (x ~%Is Null) {KSWriteTo StdOut x True; WriteLine()}}
   () {WriteLine()}
   x ys... {
      KSWriteTo StdOut x True;
      Each ys ?y {Write " "; KSWriteTo StdOut y True};
      WriteLine()
   }
];
