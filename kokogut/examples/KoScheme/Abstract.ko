// This file is a part of an example distributed with Kokogut.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2004,2006 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// Kokogut is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module KoScheme.Abstract =>

type EXPR {
   type GLOBAL          Global name;
   type LOCAL           Local name;
   type LITERAL         Literal value;
   type CALL            Call proc args;
   type CALL_GLOBAL     CallGlobal name args;
   type CALL_LOCAL      CallLocal name args;
   type LAMBDA_FIXED    LambdaFixed params body;
   type LAMBDA_VARIABLE LambdaVariable params rest body;
   type NAMED_PROC      NamedProc name proc;
   type IF              If test cons alt;
   type DEFINE_GLOBAL   DefineGlobal name value;
   type SET_GLOBAL      SetGlobal name value;
   type SET_LOCAL       SetLocal name value;
   type AND             And expr1 expr2;
   type OR              Or expr1 expr2;
   type LET             Let names exprs body;
   type LET1            Let1 name expr body;
   type LET_REC         LetRec names exprs body;
   type LET_REC1        LetRec1 name expr body;
   type SEQ             Seq stat expr;
   type DELAY           Delay expr;
};
