KoScheme is a Scheme interpreter written in Kogut. It's licensed on GPL.

It conforms to R5RS except:
- It does not implement macros.
- eqv? gives non-standard but I believe more usesul results when
  comparing a NaN with a NaN (#t), 0.0 with -0.0 (#f), or complex
  numbers with mixed exactness of real and imaginary part (each part
  separately must agree on exactness). The standard literally says
  that eqv? on numbers is #t when they are = and their exactness
  agree, while KoScheme returns #t when they are indistinguishable
  except by object identity.
- real? returns #f when the imaginary part is inexact 0, as in SRFI-77

Principles:
- The language accurately implements the Scheme standard, modulo
  limitations mentioned above. In particular tail call optimization
  and continuations are fully supported.
- As many errors as possible are detected. For example returning a bad
  number of values, modifying literals, or accessing a letrec-bound
  variable too early is detected.
- The interpreter is simple (2300 lines excluding comments). It almost
  doesn't provide non-R5RS features, and doesn't try hard to make
  programs run fast. It's more a demonstration of Kogut programming
  than a practical Scheme implementation.

Command line arguments of the interpreter:
  -f FILE   evaluate commands from the file
     FILE   evaluate commands from the file
  -e EXPRS  evaluate commands from the argument
  -r        enter read-eval-print loop after evaluating files and
            arguments.
The read-eval-print loop is also entered when no arguments are given.

Kinds of numbers supported are arbitrary precision integers (exact),
rationals (exact), and floating point numbers corresponding to Kogut
FLOAT which corresponds to C double (inexact).

Characters are Unicode code points (U+0000..U+10FFFF). I/O automatically
converts between the default locale encoding and Unicode used internally.
Character predicates (char-alphabetic? etc.) are extended to Unicode.
The default case is lowercase. Due to the limitation of Scheme standard,
case mapping is done on each character independently.

Types:
- fundamental R5RS types: pairs, the empty list, symbols, numbers,
  characters, strings, vectors, ports, procedures
- the end of file object
- promises
- environments
- the unspecified value

Non-standard procedures (not included in scheme-report-environment):
- call/cc - alias for call-with-current-continuation
- error - aborts evaluation with an error message (string)
- exit, quit - exit the interpreter with the optional exit code (integer)

Extended behavior of standard procedures:
- quotient, remainder and modulo accept all real numbers
- log accepts 1 or 2 arguments, the optional second one is the base
- number->string and string->number accept any radix between 2 and 36,
  not just 2, 8, 10 and 16

Other extensions:
- Inexact infinities and NaN (+inf.0, -inf.0, +nan.0) and exact
  infinities (+inf, -inf) are supported. Inexact infinities and NaN
  may be components of complex numbers.

Eval accepts definitions as well as expressions. Definitions may add
bindings to any environment, but (scheme-report-environment version)
and (null-environment version) always produce fresh environments with
appropriate contents. (load filename) adds bindings to the shared
interaction environment even if it's executed from eval in a different
environment.

-- 
   __("<         Marcin Kowalczyk
   \__/       QrczakMK@gmail.com
    ^^
