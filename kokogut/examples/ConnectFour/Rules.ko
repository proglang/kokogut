// This file is a part of an example distributed with Kokogut.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2006-2007 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// Kokogut is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

module ConnectFour.Rules =>

use Unsafe;

def EmptyBoard (size & (width, _)) {FillNew width Array->Vector, size};
def CopyBoard (columns, size) {Map columns Array, size};

def MoveStatus (columns, _, height) x0 player {
   let column0 = columns@x0;
   let y0 = Size column0;
   if (y0 %UnsafeEq height) {#illegal} =>
   // horizontal
   if (
      loop 1 (x0 %UnsafeSub 1) ?count x {
         Get columns x {count} ?column =>
         Get column y0 {count} ?piece =>
         if (piece ~%IsSame player) {count} =>
         again (count %UnsafeAdd 1) (x %UnsafeSub 1)
      }->loop (x0 %UnsafeAdd 1) ?count x {
         Get columns x {count} ?column =>
         Get column y0 {count} ?piece =>
         if (piece ~%IsSame player) {count} =>
         again (count %UnsafeAdd 1) (x %UnsafeAdd 1)
      } %UnsafeGE 4 |
      // vertical
      y0 %UnsafeGE 3 & loop 1 (y0 %UnsafeSub 1) ?count y {
         Get column0 y {count} ?piece =>
         if (piece ~%IsSame player) {count} =>
         again (count %UnsafeAdd 1) (y %UnsafeSub 1)
      } %UnsafeGE 4 |
      // diagonal/
      loop 1 (x0 %UnsafeSub 1) (y0 %UnsafeSub 1) ?count x y {
         if (y < 0) {count} =>
         Get columns x {count} ?column =>
         Get column y {count} ?piece =>
         if (piece ~%IsSame player) {count} =>
         again (count %UnsafeAdd 1) (x %UnsafeSub 1) (y %UnsafeSub 1)
      }->loop (x0 %UnsafeAdd 1) (y0 %UnsafeAdd 1) ?count x y {
         Get columns x {count} ?column =>
         Get column y {count} ?piece =>
         if (piece ~%IsSame player) {count} =>
         again (count %UnsafeAdd 1) (x %UnsafeAdd 1) (y %UnsafeAdd 1)
      } %UnsafeGE 4 |
      // diagonal\
      loop 1 (x0 %UnsafeAdd 1) (y0 %UnsafeSub 1) ?count x y {
         if (y < 0) {count} =>
         Get columns x {count} ?column =>
         Get column y {count} ?piece =>
         if (piece ~%IsSame player) {count} =>
         again (count %UnsafeAdd 1) (x %UnsafeAdd 1) (y %UnsafeSub 1)
      }->loop (x0 %UnsafeSub 1) (y0 %UnsafeAdd 1) ?count x y {
         Get columns x {count} ?column =>
         Get column y {count} ?piece =>
         if (piece ~%IsSame player) {count} =>
         again (count %UnsafeAdd 1) (x %UnsafeSub 1) (y %UnsafeAdd 1)
      } %UnsafeGE 4
   ) {#win} {#regular}
};

def CanMove (columns, _, height) x {
   Size columns@x < height
};

def CanMoveAnywhere (columns, _, height) {
   Any columns ?column {Size column < height}
};

def WinningPieces (columns, _) x0 {
   let column0 = columns@x0;
   let y0 = Size column0 - 1;
   let player = Last column0;
   [
      // horizontal
      (loop [] (x0-1) ?squares x {
         Get columns x {squares} ?column =>
         Get column y0 {squares} ?piece =>
         if (piece ~%IsSame player) {squares} =>
         again ((x, y0) \ squares) (x-1)
      }->loop (x0+1) ?squares x {
         Get columns x {squares} ?column =>
         Get column y0 {squares} ?piece =>
         if (piece ~%IsSame player) {squares} =>
         again ((x, y0) \ squares) (x+1)
      })
      // vertical
      (loop [] (y0-1) ?squares y {
         Get column0 y {squares} ?piece =>
         if (piece ~%IsSame player) {squares} =>
         again ((x0, y) \ squares) (y-1)
      })
      // diagonal/
      (loop [] (x0-1) (y0-1) ?squares x y {
         Get columns x {squares} ?column =>
         Get column y {squares} ?piece =>
         if (piece ~%IsSame player) {squares} =>
         again ((x, y) \ squares) (x-1) (y-1)
      }->loop (x0+1) (y0+1) ?squares x y {
         Get columns x {squares} ?column =>
         Get column y {squares} ?piece =>
         if (piece ~%IsSame player) {squares} =>
         again ((x, y) \ squares) (x+1) (y+1)
      })
      // diagonal\
      (loop [] (x0+1) (y0-1) ?squares x y {
         Get columns x {squares} ?column =>
         Get column y {squares} ?piece =>
         if (piece ~%IsSame player) {squares} =>
         again ((x, y) \ squares) (x+1) (y-1)
      }->loop (x0-1) (y0+1) ?squares x y {
         Get columns x {squares} ?column =>
         Get column y {squares} ?piece =>
         if (piece ~%IsSame player) {squares} =>
         again ((x, y) \ squares) (x-1) (y+1)
      })
   ]->Map ?line {
      if (SizeAtLeast line 3)
         {(x0, y0) \ line}
         {[]}
   }
};
