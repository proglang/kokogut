// This file is a part of an example distributed with Kokogut.
//
// Kokogut is a compiler of the Kogut programming language.
// Copyright (C) 2006,2008 by Marcin 'Qrczak' Kowalczyk
// (QrczakMK@gmail.com)
//
// Kokogut is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// Kokogut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

use Threads;
use Random;
use TextUI.Curses;
use ConnectFour.Rules;
use ConnectFour.Thinking;
use ConnectFour.Display;

let MaxSize = 20;

def WiderSize    w h {w+1, h};
def NarrowerSize w h {w-1, h};
def TallerSize   w h {w, h+1};
def ShorterSize  w h {w, h-1};

def LargerSize w h {
   if [
      (w < h) {WiderSize  w h}
      (w > h) {TallerSize w h}
      _       {w+1, h+1}
   ]
};

def SmallerSize w h {
   if [
      (w > h) {NarrowerSize w h}
      (w < h) {ShorterSize  w h}
      _       {w-1, h-1}
   ]
};

def FixSize (size & (width, height)) how {
   let (newSize & (newWidth, newHeight)) = how width height;
   if [
      (newWidth->IsBetween 4 MaxSize) {
         if [
            (newHeight->IsBetween 4 MaxSize &
            SizeFitsInScreen newSize %Is Null) {newSize}
            (SizeFitsInScreen (newWidth, height) %Is Null) {newWidth, height}
            _ {size}
         ]
      }
      (newHeight->IsBetween 4 MaxSize &
      SizeFitsInScreen (width, newHeight) %Is Null) {width, newHeight}
      _ {size}
   ]
};

def DoMove (board & (columns, size)) history x player {
   DoAddLast columns@x player;
   DoAddLast history x;
   AddLastPiece board x;
   AddLastHistory size history player
};

def UndoMove (board & (columns, size)) history {
   RemoveLastHistory size history;
   let x = DoCutLast history;
   RemoveLastPiece board x;
   DoRemoveLast columns@x;
   x
};

def AskEnd size restore {
   Using {
      LockDisplay =>
      let oldCursorIsHidden = CursorIsHidden;
      ShowCursor();
      let text = "End game? ";
      DisplayMessage size text;
      let oldRedisplayHandler = RedisplayHandlerVar;
      RedisplayHandlerVar = {
         oldRedisplayHandler();
         DisplayMessage size text
      };
      CanDisplayInfo = False;
      oldCursorIsHidden, oldRedisplayHandler
   } ?(oldCursorIsHidden, oldRedisplayHandler) {
      LockDisplay =>
      if oldCursorIsHidden {HideCursor()};
      CanDisplayInfo = True;
      RedisplayHandlerVar = oldRedisplayHandler;
      restore()
   } ?_ =>
   loop =>
   GetCh()->case [
      ("y" | "Y") {True}
      ("n" | "N") {False}
      "\cL"       {RedisplayScreen(); again()}
      _           {again()}
   ]
};

def Help1Human width isFirst {
   let lastMove = EncodeX (width-1);
   let delete = if isFirst {"computer first"} {"undo"};
   [
      (String "left,right - choose square; Enter - move; \
         1-" lastMove " - move; \
         Delete - " (if isFirst {"computer moves first"} {"undo"}))
      (String "left,right - choose square; Enter - move; \
         1-" lastMove " - move; Delete - " delete)
      (String "left,right - choose; Enter - move; \
         1-" lastMove " - move; Delete - " delete)
      (String "left,right-choose; Enter-move; \
         1-" lastMove "-move; Delete-" delete)
      (String "left,right,Enter,1-" lastMove ",Delete")
      (String "<-,->,Enter,1-" lastMove ",Delete")
   ]
};

let Help1Computer = [
   "Insert - force move now; Delete - undo"
   "Insert - force move; Delete - undo"
   "Insert - force; Delete - undo"
   "Insert-force; Delete-undo"
   "Insert,Delete"
];

let Help1End = ["Delete - undo"];

let Help2 = [
   "up,down - change computer time; + - > < . , - change size; \
   N - new game; Q - quit"
   "up,down - computer time; + - > < . , - size; N - new game; Q - quit"
   "up,down-computer time; + - > < . ,-size; N-new game; Q-quit"
   "up,down-computer time; + - > < . ,-size; N-new; Q-quit"
   "up,down-time; + - > < . ,-size; N-new; Q-quit"
   "up,down,+,-,>,<,.,,,N,Q"
];

def NewGame (size & (width, _)) maxTime (->where Mod width = x0) {
   let board = EmptyBoard size;
   let history = Array();
   LockDisplay {
      Erase();
      DisplayEmptyBoard size;
      DisplayHelp size 1 (Help1Human width True);
      DisplayHelp size 2 Help2;
      DisplayInfo size "   " (ShowMaxTime maxTime);
      CursorOnFreeSquare board x0;
      NoRedisplayHandler()
   };
   HumanTurn board history maxTime x0
};

def FirstComputerTurn (size & (width, _)) maxTime {
   let board = EmptyBoard size;
   let history = Array();
   let x = Random width;
   LockDisplay {
      DoMove board history x #we;
      CursorOnLastPiece board x;
      NoRedisplayHandler()
   };
   HumanTurn board history maxTime x
};

def HumanTurn (board & (_, size & (width, _))) history (var maxTime) x0 {
   if (~CanMoveAnywhere board) {EndOfGame board history maxTime False} =>
   let isFirst = IsEmpty history;
   var x = (x0 %Mod width)->Until (CanMove board _) ?x {(x+1) %Mod width};
   let lastX = GetLast history Ignore;
   var smiley = "   ";
   let scores = Fill width #unknown->Vector;
   def displayInfo() {DisplayInfo size smiley (ShowMaxTime maxTime)};
   def displayScores() {Each (From 0) scores (DisplayScore size _ _)};
   def placeCursor() {CursorOnFreeSquare board x};
   def display() {
      if (lastX ~%Is Null) {MarkLastPiece board lastX};
      DisplayHelp size 1 (Help1Human width isFirst);
      displayInfo(); displayScores(); placeCursor()
   };
   LockDisplay {
      ShowCursor();
      display();
      RedisplayHandler {
         DisplayBoard board history #we;
         DisplayHelp size 2 Help2;
         display()
      }
   };
   def select how {
      LockDisplay =>
      x = how x->Until (CanMove board _) how;
      placeCursor()
   };
   def enter plan {
      let status = MoveStatus board x #they;
      LockDisplay {
         HideCursor();
         if (lastX ~%Is Null) {UnmarkLastPiece board lastX};
         RemoveScores size;
         DoMove board history x #they;
         CursorOnLastPiece board x;
         NoRedisplayHandler()
      };
      case status [
         #regular {ComputerTurn board history maxTime plan}
         #win     {EndOfGame board history maxTime True}
      ]
   };
   def changeMaxTime how {
      LockDisplay =>
      maxTime = how maxTime;
      displayInfo(); placeCursor()
   };
   def endGame() {
      IsEmpty history | AskEnd size {displayInfo(); placeCursor()}
   };
   def changeSize how nextKey {
      if (endGame()) {
         {NewGame (FixSize size how) maxTime
            (if (IsEmpty history) {x} {0})}
      } =>
      nextKey()
   };
   ThinkReply board ?newSmiley {
      LockDisplayNoMove =>
      smiley = newSmiley;
      if (CanDisplay & CanDisplayInfo) {displayInfo()}
   } ?x newScore {
      LockDisplayNoMove =>
      scores@x = newScore;
      if CanDisplay {DisplayScore size x newScore}
   } ?getPlan {
      loop =>
      GetCh()->case [
         (%Is KeyLeft)  {select ?x {(x-1) %Mod width}; again()}
         (%Is KeyRight) {select ?x {(x+1) %Mod width}; again()}
         (%Is KeyEnter | "\r" | "\n" | " ") {
            let plan = getPlan x; {enter plan}
         }
         (->HasType STRING->where DecodeX width = newX ~%Is Null) {
            if (MoveStatus board newX #they %Is #illegal) {again()} =>
            x = newX;
            let plan = getPlan x; {enter plan}
         }
         (%Is KeyBackspace | %Is KeyDC | "\b") {
            if (IsEmpty history) {{FirstComputerTurn size maxTime}} =>
            LockDisplay =>
            HideCursor();
            UnmarkLastPiece board lastX;
            RemoveScores size;
            UndoMove board history;
            let oldX = if (IsEmpty history) {lastX} {UndoMove board history};
            CursorOnFreeSquare board oldX;
            NoRedisplayHandler();
            {HumanTurn board history maxTime oldX}
         }
         (%Is KeyUp)   {changeMaxTime LongerTime; again()}
         (%Is KeyDown) {changeMaxTime ShorterTime; again()}
         ("+" | "=")   {changeSize LargerSize  again}
         "-"           {changeSize SmallerSize again}
         ">"           {changeSize WiderSize again}
         "<"           {changeSize NarrowerSize again}
         "."           {changeSize TallerSize again}
         ","           {changeSize ShorterSize again}
         ("n" | "N")   {if (endGame()) {{NewGame size maxTime 0}} {again()}}
         ("q" | "Q")   {if (endGame()) {{}} {again()}}
         "\cL"         {RedisplayScreen(); again()}
         _             {again()}
      ]
   }->Apply
};

def ComputerTurn (board & (_, size & (width, _))) history (var maxTime) plan {
   if (~CanMoveAnywhere board) {EndOfGame board history maxTime False} =>
   var startTime = Time();
   var time = 0;
   let lastX = Last history;
   var smiley = "   ";
   var scores = Fill width #unknown->Vector;
   def displayInfo() {
      DisplayInfo size smiley
         (time->IfNull {ShowMaxTime maxTime} (->ShowTime maxTime))
   };
   def displayScores() {Each (From 0) scores (DisplayScore size _ _)};
   def placeCursor() {CursorOnLastPiece board lastX};
   def display() {
      MarkLastPiece board lastX;
      DisplayHelp size 1 Help1Computer;
      displayInfo(); displayScores(); placeCursor()
   };
   LockDisplay {
      HideCursor();
      display();
      RedisplayHandler {
         DisplayBoard board history #they;
         DisplayHelp size 2 Help2;
         display()
      }
   };
   def enter x {
      let status = MoveStatus board x #we;
      LockDisplay {
         UnmarkLastPiece board lastX;
         RemoveScores size;
         DoMove board history x #we;
         displayInfo();
         CursorOnLastPiece board x;
         NoRedisplayHandler()
      };
      case status [
         #regular {HumanTurn board history maxTime x}
         #win     {EndOfGame board history maxTime True}
      ]
   };
   def changeMaxTime how queue {
      LockDisplay =>
      maxTime = how maxTime;
      displayInfo(); placeCursor();
      if (time ~%Is Null & time >= maxTime) {RegisterEvent queue {KeyIC}}
   };
   def endGame() {AskEnd size {displayInfo(); placeCursor()}};
   def changeSize how nextKey {
      if (endGame()) {{NewGame (FixSize size how) maxTime 0}} =>
      nextKey()
   };
   Think board plan ?newSmiley {
      LockDisplayNoMove =>
      smiley = newSmiley;
      if (CanDisplay & CanDisplayInfo) {displayInfo()}
   } ?newScores {
      LockDisplayNoMove =>
      scores = newScores;
      if CanDisplay {displayScores()}
   } ?waitForResult getMove {
      Using EventQueue ?queue =>
      RegisterEvent queue {waitForResult(); KeyIC};
      RegisterEvent queue {
         loop 0 ?oldTime =>
         Sleep (startTime+oldTime+1);
         LockDisplay {
            if (time %Is Null) {{KeyIC}} =>
            let newTime = Int (Time() - startTime);
            if (newTime >= maxTime) {{KeyIC}} =>
            time = newTime;
            if (CanDisplay & CanDisplayInfo) {displayInfo(); placeCursor()};
            {again newTime}
         }->Apply
      };
      loop =>
      RegisterEvent queue {CanReadFrom RawStdIn; EventReady GetCh};
      TakeEvent queue->case [
         (%Is KeyIC) {
            Lock DisplayMutex {time = Null};
            let x = getMove(); {enter x}
         }
         (%Is KeyBackspace | %Is KeyDC | "\b") {
            LockDisplay =>
            time = Null;
            UnmarkLastPiece board lastX;
            RemoveScores size;
            UndoMove board history;
            CursorOnFreeSquare board lastX;
            NoRedisplayHandler();
            {HumanTurn board history maxTime lastX}
         }
         (%Is KeyUp)   {changeMaxTime LongerTime  queue; again()}
         (%Is KeyDown) {changeMaxTime ShorterTime queue; again()}
         ("+" | "=")   {changeSize LargerSize  again}
         "-"           {changeSize SmallerSize again}
         ">"           {changeSize WiderSize again}
         "<"           {changeSize NarrowerSize again}
         "."           {changeSize TallerSize again}
         ","           {changeSize ShorterSize again}
         ("n" | "N")   {if (endGame()) {{NewGame size maxTime 0}} {again()}}
         ("q" | "Q")   {if (endGame()) {{}} {again()}}
         "\cL"         {RedisplayScreen(); again()}
         _             {again()}
      ]
   }->Apply
};

def EndOfGame (board & (columns, size)) history (var maxTime) win {
   let x = Last history;
   let player = Last columns@x;
   let winning = if win {WinningPieces board x};
   var smiley = if win {case player [#we {":-D"} #they {":-C"}]} {":-|"};
   def displayInfo() {DisplayInfo size smiley (ShowMaxTime maxTime)};
   def placeCursor() {CursorOnLastPiece board x};
   def display() {
      if win
         {MarkWinningPieces size winning}
         {MarkLastPiece board x};
      DisplayHelp size 1 Help1End;
      displayInfo(); placeCursor()
   };
   LockDisplay {
      HideCursor();
      display();
      RedisplayHandler {
         DisplayBoard board history player;
         DisplayHelp size 2 Help2;
         display()
      }
   };
   def changeMaxTime how {
      LockDisplay =>
      maxTime = how maxTime;
      displayInfo(); placeCursor()
   };
   def changeSize how {
      NewGame (FixSize size how) maxTime 0
   };
   loop =>
   GetCh()->case [
      (%Is KeyBackspace | %Is KeyDC | "\b") {
         let oldX = LockDisplay {
            if win
               {DisplayBoardLines size}
               {UnmarkLastPiece board x};
            UndoMove board history;
            let oldX = case player [
               #we   {UndoMove board history}
               #they {x}
            ];
            CursorOnFreeSquare board oldX;
            NoRedisplayHandler();
            oldX
         };
         HumanTurn board history maxTime oldX
      }
      (%Is KeyUp)   {changeMaxTime LongerTime; again()}
      (%Is KeyDown) {changeMaxTime ShorterTime; again()}
      ("+" | "=")   {changeSize LargerSize}
      "-"           {changeSize SmallerSize}
      ">"           {changeSize WiderSize}
      "<"           {changeSize NarrowerSize}
      "."           {changeSize TallerSize}
      ","           {changeSize ShorterSize}
      ("n" | "N")   {NewGame size maxTime 0}
      ("q" | "Q")   {}
      "\cL"         {RedisplayScreen(); again()}
      _             {again()}
   ]
};

InitCurses();
InitColors();
let initialSize = (8, 8);
case (SizeFitsInScreen initialSize) [
   (%Is Null) {
      MonitorMemoryUsage =>
      WithRedisplayHandler {NewGame initialSize 5 0};
      EndWin()
   }
   exn {
      EndWin();
      WriteLine (ShowException exn) "."
   }
];
