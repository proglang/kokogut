;;; kogut-mode.el --- major mode for editing Kogut code

;; This file is a part of Kokogut.
;;
;; Kokogut is a compiler of the Kogut programming language.
;; Copyright (C) 2004-2008,2012 by Marcin 'Qrczak' Kowalczyk
;; (QrczakMK@gmail.com)
;;
;; Kokogut is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; Kokogut is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

;; You can add the following to Emacs initialization script:
;;
;; (autoload 'kogut-mode "/path/to/kogut-mode"
;;   "Major mode for editing Kogut code" t)
;; (add-to-list 'auto-mode-alist '("\\.ko\\'" . kogut-mode))

(defgroup kogut nil
  "Major mode for editing Kogut code."
  :group 'languages)

(defcustom kogut-indent 3
  "*Indentation level width in Kogut code."
  :type 'integer
  :group 'kogut)

(defcustom kogut-fill-column 76
  "*Column for line wrapping of comments in Kogut code."
  :type 'integer
  :group 'kogut)

(defvar kogut-font-lock-name-face 'kogut-font-lock-name-face)
(defface kogut-font-lock-name-face
  '((t ())) ; Using default-face directly doesn't work for some reason.
  "Name."
  :group 'kogut)

(defvar kogut-font-lock-module-face 'kogut-font-lock-module-face)
(defface kogut-font-lock-module-face
  '((((background light)) (:foreground "lime green"))
    (((background dark)) (:foreground "green")))
  "Module name."
  :group 'kogut)

(defvar kogut-font-lock-symbol-face 'kogut-font-lock-symbol-face)
(defface kogut-font-lock-symbol-face
  '((((background light)) (:foreground "orchid"))
    (((background dark)) (:foreground "orange")))
  "Symbol literal with a hash sign."
  :group 'kogut)

(defvar kogut-font-lock-label-face 'kogut-font-lock-label-face)
(defface kogut-font-lock-label-face
  '((((background light)) (:foreground "goldenrod"))
    (((background dark)) (:foreground "LightSteelBlue")))
  "Label with a colon."
  :group 'kogut)

(defvar kogut-font-lock-field-face 'kogut-font-lock-field-face)
(defface kogut-font-lock-field-face
  '((((background light)) (:foreground "brown"))
    (((background dark)) (:foreground "khaki")))
  "Field selector with a dot."
  :group 'kogut)

(defvar kogut-font-lock-operator-face 'kogut-font-lock-operator-face)
(defface kogut-font-lock-operator-face
  '((((background light)) (:foreground "forest green"))
    (((background dark)) (:foreground "yellow")))
  "Operator, including named operators with a percent sign."
  :group 'kogut)

(defvar kogut-font-lock-punctuation-face 'kogut-font-lock-punctuation-face)
(defface kogut-font-lock-punctuation-face
  '((((background light)) (:foreground "dim gray"))
    (((background dark)) (:foreground "gray")))
  "Punctuation."
  :group 'kogut)

;; Syntax highlighting

(defvar kogut-mode-syntax-table nil
  "Syntax table used while in Kogut mode.")

(unless kogut-mode-syntax-table
  (let ((table (make-syntax-table)))
    (modify-syntax-entry ?\n "> b" table)
    (modify-syntax-entry ?\r "> b" table)
    (modify-syntax-entry ?\  " " table)
    (modify-syntax-entry ?!  "." table)
    (modify-syntax-entry ?\" "\"" table)
    (modify-syntax-entry ?#  "_" table)
    (modify-syntax-entry ?$  "." table)
    (modify-syntax-entry ?%  "_" table)
    (modify-syntax-entry ?&  "." table)
    (modify-syntax-entry ?'  "w" table)
    (modify-syntax-entry ?\( "()" table)
    (modify-syntax-entry ?\) ")(" table)
    (modify-syntax-entry ?*
      (if (and (boundp 'running-xemacs) running-xemacs) ". 23" ". 23n")
      table)
    (modify-syntax-entry ?+  "." table)
    (modify-syntax-entry ?,  "." table)
    (modify-syntax-entry ?-  "." table)
    (modify-syntax-entry ?.  "_" table)
    (modify-syntax-entry ?/
      (if (and (boundp 'running-xemacs) running-xemacs) ". 1456" ". 124b")
      table)
    (modify-syntax-entry ?:  "_" table)
    (modify-syntax-entry ?\; "." table)
    (modify-syntax-entry ?<  "." table)
    (modify-syntax-entry ?=  "." table)
    (modify-syntax-entry ?>  "." table)
    (modify-syntax-entry ??  "." table)
    (modify-syntax-entry ?@  "." table)
    (modify-syntax-entry ?\[ "(]" table)
    (modify-syntax-entry ?\\ "\\" table)
    (modify-syntax-entry ?\] ")[" table)
    (modify-syntax-entry ?^  "." table)
    (modify-syntax-entry ?_  "w" table)
    (modify-syntax-entry ?`  "." table)
    (modify-syntax-entry ?\{ "(}" table)
    (modify-syntax-entry ?|  "." table)
    (modify-syntax-entry ?\} "){" table)
    (modify-syntax-entry ?~  "." table)
    (setq kogut-mode-syntax-table table)))

(defconst kogut-standalone-keywords
  '("_" "again" "base" "super" "this"))

(defconst kogut-standard-defs
  '("c_con" "c_let" "c_type" "def" "define" "defined" "dynamic" "feature"
    "forward" "ifDefined" "lazy" "let" "ref" "subtypes" "type" "var"))

(defconst kogut-standard-macros
  '("case" "c_case" "c_con" "c_cond" "c_define" "c_defineName" "c_entry"
    "c_eval" "c_export" "c_exportName" "c_expr" "c_stat" "define" "dynamic"
    "export" "extend" "function" "handle" "if" "include" "local" "loop" "match"
    "maybe" "method" "module" "ofType" "private" "public" "receive" "reexport"
    "ref" "set" "struct" "try" "use" "var" "where" "with"))

(defconst kogut-standard-defs-with-names
  '("c_type" "def" "dynamic" "feature" "forward" "lazy" "let" "type" "var"))

(defconst kogut-font-lock-keywords
  (eval-when-compile
    (let* ((uname "\\<[[:alpha:]_][[:alnum:]_']*\\>")
           (qname "\\<'\\(?:[^\n'\\\\]\\|\\\\.\\)*\\(?:'\\>\\|$\\)")
           (name (concat "\\<\\(?:[[:alpha:]_][[:alnum:]_']*\\|"
                         "'\\(?:[^'\\\\\n]\\|\\\\.\\)*'\\)\\>"))
           (qualified-name (concat name "\\(?:\\." name "\\)*"))
           (def-keyword (regexp-opt kogut-standard-defs-with-names 'words))
           (module-keyword "\\(?:module\\|use\\|reexport\\)")
           (def-uname (concat "\\(?:(p\\(?:ublic\\|rivate\\)[ \t]+\\)?"
                              "\\(_?[[:upper:]][[:alnum:]_']*\\)"))
           (def-uname-1 (concat "\\(_?[[:upper:]][[:alnum:]_']*\\)"))
           (def-uname-2 (concat "(p\\(?:ublic\\|rivate\\)[ \t]+"
                                "\\(_?[[:upper:]][[:alnum:]_']*\\))"))
           (skip-def-name (concat "\\(?:" name "\\|"
                                  "(p\\(?:ublic\\|rivate\\)[ \t]+"
                                  name ")\\)"))
           (not-opt "\\(?:~[ \t]*\\)*")
           (begin-apply (concat "\\(?:^\\|[%&(*+,/;<=>\\{|-]\\|%" name "\\)"
                                "[ \t]*" not-opt "\\<"))
           (begin-def "\\(?:^\\|[;({]\\|=>\\)[ \t]*\\<")
           (before-op (concat "[ \t]*\\(?:$\\|[!%&)*+,./;<>@^\\|}-]\\|"
                              "=\\(?:$\\|[^>]\\)\\|"
                              "[ \t]" name "%\\)"))
           (visibility "\\(?:p\\(?:ublic\\|rivate\\)[ \t]+\\)?"))
      (list
       ;; Module names
       (cons (concat begin-apply module-keyword "[ \t]+"
                     "\\(?:" name ":[ \t]*\\)?"
                     "\\(" qualified-name "\\)")
             '(1 kogut-font-lock-module-face))
       ;; Tokens which include quoted names as their part
       (cons (concat "#" qname) 'kogut-font-lock-symbol-face)
       (cons (concat "\\." qname) 'kogut-font-lock-field-face)
       (cons (concat qname ":") 'kogut-font-lock-label-face)
       (cons (concat not-opt "\\(?:%" qname "\\|" qname "%\\)")
             'kogut-font-lock-operator-face)
       ;; Quoted names
       (cons qname 'kogut-font-lock-name-face)
       ;; Tokens which include unquoted names as their part
       (cons (concat "#" uname) 'kogut-font-lock-symbol-face)
       (cons (concat "\\." uname) 'kogut-font-lock-field-face)
       (cons (concat uname ":") 'kogut-font-lock-label-face)
       (cons (concat not-opt "\\(?:%" uname "\\|" uname "%\\)")
             'kogut-font-lock-operator-face)
       ;; Global unquoted names
       (cons (concat begin-def visibility def-keyword "[ \t]+" def-uname)
             '(2 font-lock-function-name-face))
       (cons (concat begin-def visibility "\\(?:c_\\)?type[ \t]+"
                     skip-def-name "[ \t]+\\(?:is:" qualified-name "[ \t]+\\)*"
                     def-uname)
             '(1 font-lock-function-name-face))
       (cons (concat begin-def visibility "ref[ \t]+" def-uname-1
                     "[ \t]*[={]")
             '(1 font-lock-function-name-face))
       (cons (concat begin-def visibility "ref[ \t]+" def-uname-2
                     "[ \t]*[={]")
             '(1 font-lock-function-name-face))
       (cons (concat begin-def visibility "c_con[ \t]+" def-uname-1
                     "[ \t]*\\(\"\\|$\\)")
             '(1 font-lock-function-name-face))
       (cons (concat begin-def visibility "c_con[ \t]+" def-uname-2
                     "[ \t]*\\(\"\\|$\\)")
             '(1 font-lock-function-name-face))
       ;; Standalone keywords
       (cons (regexp-opt kogut-standalone-keywords 'words)
             'font-lock-keyword-face)
       ;; Standard macros after an arrow
       (cons (concat "->[ \t]*" not-opt
                     (regexp-opt (append kogut-standard-defs
                                         kogut-standard-macros) t) "\\>")
             '(1 font-lock-keyword-face))
       ;; Names before operators may look like standard macros but aren't
       (cons (concat "\\(" uname "\\)" before-op)
             '(1 kogut-font-lock-name-face))
       ;; Standard macros
       (cons (concat begin-apply (regexp-opt kogut-standard-macros t) "\\>")
             '(1 font-lock-keyword-face))
       ;; Standard definitions
       (cons (concat begin-def visibility
                     (regexp-opt kogut-standard-defs t) "\\>")
             '(1 font-lock-keyword-face))
       ;; Remaining unquoted names
       (cons uname 'kogut-font-lock-name-face)
       ;; Punctuation and operators
       (cons "[=-]>\\|\\.\\.\\." 'kogut-font-lock-punctuation-face)
       (cons "[*+/<>@-]\\|[<=>~]=" 'kogut-font-lock-operator-face)
       (cons "[]!$&,();=?[\\\\^{|}~]" 'kogut-font-lock-punctuation-face)))))

;; Indentation

(defun kogut-finished-indenting ()
  (<= (- (point-max) (point)) indenting-end))

(defun kogut-indent-skip-chars (chars)
  (skip-chars-forward chars (- (point-max) indenting-end)))

(defun kogut-at-new-line ()
  (save-excursion (skip-chars-backward " \t") (bolp)))

(defconst kogut-outer-indent-regexp
  (concat "[%*+/<>-]\\|"                ; includes /* and //
          "~=\\|=\\(?:$\\|[^>]\\)\\|"
          "[([{][ \t]*\\(?:$\\|//\\)\\|"
          "[\"'][ \t]*\\(?:\\\\[ \t]*\\)?$")
  "A line matching this regexp is indented less than others.")

(defun kogut-choose-indent ()
  "Choose the indent of the text after the point, assuming we are at
the first non-whitespace character in a line:
- bracket-indent, when this is a closing bracket,
- outer-indent, when this is either an operator, or an opening bracket
  at the end of a line,
- inner-indent, otherwise."
  (cond ((looking-at "[])}]") bracket-indent)
        ((looking-at kogut-outer-indent-regexp) (max outer-indent min-indent))
        (t (max inner-indent min-indent))))

(defun kogut-maybe-indent-line ()
  "Automatically indent the current line if indenting a region."
  (if (and indenting-region
           (>= (point) indenting-region)
           (kogut-at-new-line))
      (kogut-indent-line-to (kogut-choose-indent))))

(defun kogut-to-last-safe-pos ()
  "Find the nearest place above where it's probably safe to start parsing.
We look for a line at the left margin preceded by an empty line."
  (while (and (zerop (forward-line -1))
              (not (and (not (eolp))
                        (looking-at "[^ \t]")
                        (save-excursion (forward-line -1) (eolp)))))))

(defun kogut-indent-here ()
  "The indent if we are at the first non-whitespace character in a line,
otherwise nil."
  (and (kogut-at-new-line) (current-column)))

(defun kogut-indent-scan-tokens
  (bracket-indent def-indent inner-indent base-min-indent)
  "Scan a sequence of tokens up to the closing bracket. Return the
computed indent of the target if the target was found within the
sequence, otherwise :arg.

Except when the brackets are the square brackets, the tokens are
interpreted as definitions separated by semicolons, consisting of
applications separated by operators, consisting of the function
and arguments.

Arguments:
- indent of the closing bracket
- indent of toplevel definitions (except for square brackets)
- indent of elements (in the case of square brackets)
- minimal indent of all the contents, applied after other rules"
  (let ((outer-indent def-indent)
        (min-indent base-min-indent)
        (state (if (eq closing-bracket ?\]) :arg :def))
        (result nil))
    ; The state can be:
    ; - :def before a definition (after a semicolon),
    ; - :fun before the first element of an application (after an operator),
    ; - :arg otherwise.
    (while (null result)
      (kogut-indent-skip-chars " \t\n")
      (if (kogut-finished-indenting)
          (setq result (kogut-choose-indent))
        (kogut-maybe-indent-line)
        (if (eq (char-after) closing-bracket)
            (progn (forward-char) (setq result :arg))
          (let ((indent-here (kogut-indent-here)))
            (when indent-here
              ; The current token was at the beginning of the line
              ; and might influence the indent of the following lines.
              (cond ((looking-at "/[*/]")
                     ; An opening comment is indented like an operator,
                     ; but it does not influence the indent of the
                     ; following lines.
                     )
                    ((eq closing-bracket ?\])
                     ; Any token inside square brackets sets the indent
                     ; for all the following lines.
                     (setq outer-indent indent-here
                           inner-indent indent-here))
                    ((eq state :def)
                     ; The first token of a definition sets the indent
                     ; of subsequent definitions.
                     (setq def-indent indent-here
                           outer-indent indent-here
                           inner-indent indent-here))
                    ((eq state :fun)
                     ; The first token of an application sets the indent
                     ; of subsequent applications of the same definition.
                     (setq outer-indent indent-here
                           inner-indent indent-here))
                    ((looking-at kogut-outer-indent-regexp)
                     ; An operator sets the indent for subsequent
                     ; operators at the beginning of a line.
                     (setq outer-indent indent-here))
                    (t
                     ; An argument sets the indent for subsequent
                     ; arguments of the same application.
                     (setq inner-indent indent-here)))))
          (when (and (not (eq closing-bracket ?\]))
                     (looking-at "=\\(?:$\\|[^=>]\\)"))
            ; The '=' operator is special: after it, all the following
            ; lines of the current definition are indented more than
            ; the beginning of the definition, even if they start with
            ; operators.
            (setq min-indent (+ def-indent kogut-indent)))
          (let ((new-state (kogut-indent-scan outer-indent inner-indent)))
            (cond ((integerp new-state)
                   ; Finished indentation inside this argument.
                   (setq result new-state))
                  ((eq closing-bracket ?\]))
                  ((eq new-state :skip))
                  (t (cond ((eq new-state :def)
                            ; A semicolon has been passed. The next
                            ; token will be the first token of a
                            ; definition.
                            (setq outer-indent def-indent
                                  inner-indent def-indent
                                  min-indent base-min-indent))
                           ((eq new-state :fun)
                            ; An operator has been passed. The next
                            ; token will be the first token of an
                            ; application.
                            (setq inner-indent outer-indent))
                           ((memq state '(:def :fun))
                            ; The function of an application has been
                            ; passed. Arguments of the application
                            ; will be indented more than the function.
                            (setq inner-indent (+ outer-indent kogut-indent))))
                     (setq state new-state)))))))
    result))

(defun kogut-indent-scan-bracketed (closing-bracket)
  (forward-char)
  (cond ((looking-at "[ \t]*\\(?:$\\|//\\)")
         ; The opening bracket is at the end of a line. Everything
         ; will be indented more than the current outer indent, until
         ; the closing bracket.
         (let ((indent (+ outer-indent kogut-indent)))
           (kogut-indent-scan-tokens outer-indent indent indent
                                     (max indent min-indent))))
        (new-line
         ; The opening bracket is at the beginning of a line, but not
         ; at the end. Arguments of the first application will be
         ; indented more than the bracket, and the first token of a
         ; subsequent application or definition will be shifted right
         ; using min-indent.
         (let ((indent (+ inner-indent kogut-indent)))
           (kogut-indent-scan-tokens inner-indent inner-indent indent
                                     (max indent min-indent))))
        (t
         ; The opening bracket is in the middle of a line. The outer
         ; indent nominally does not change, so multiple opening
         ; brackets on the same line do not increase the indent, but
         ; contents of the brackets will be shifted right using
         ; min-indent.
         (let ((indent (+ outer-indent kogut-indent)))
           (kogut-indent-scan-tokens outer-indent outer-indent indent
                                     (max indent min-indent))))))

(defun kogut-indent-scan-function ()
  (let* ((closing-bracket nil)
         (outer-indent (if new-line inner-indent outer-indent))
         (inner-indent (+ outer-indent kogut-indent))
         (result nil))
    (forward-char)
    (while (null result)
      (kogut-indent-skip-chars " \t\n")
      (if (kogut-finished-indenting)
          (setq result (kogut-choose-indent))
        (kogut-maybe-indent-line)
        (cond ((eq (char-after) ?\{)
               (let ((new-line (kogut-at-new-line)))
                 (setq result (kogut-indent-scan-bracketed ?\}))))
              ((looking-at "=>")
               (forward-char 2)
               (setq result :def))
              (t (let ((indent-here (kogut-indent-here)))
                   (when (and indent-here (not (looking-at "/[*/]")))
                     (setq inner-indent indent-here)))
                 (let ((new-state
                        (kogut-indent-scan outer-indent inner-indent)))
                   (when (integerp new-state)
                     (setq result new-state)))))))
    result))

(defun kogut-indent-scan-quoted-contents (bracket-indent indent)
  (let ((result nil))
    (while (null result)
      (kogut-indent-skip-chars " \t\n")
      (if (kogut-finished-indenting)
          (setq result (if (eq (char-after) closing-bracket)
                           bracket-indent
                         indent))
        (if (eq (char-after) closing-bracket)
            (progn (forward-char) (setq result :arg))
          (let ((indent-here (kogut-indent-here)))
            (when indent-here
              (setq indent indent-here)))
          (if (eq (char-after) ?\\)
              (forward-char 2)
            (kogut-indent-skip-chars word-chars))
          (when (kogut-finished-indenting)
            (setq result indent)))))
    result))

(defun kogut-indent-scan-quoted (closing-bracket word-chars)
  (let ((new-line (kogut-at-new-line)))
    (forward-char)
    (if (or (looking-at "[ \t]*\\(?:\\\\[ \t]*\\)?$") (not new-line))
        (kogut-indent-scan-quoted-contents outer-indent
                                    (+ outer-indent kogut-indent))
      (kogut-indent-scan-quoted-contents inner-indent inner-indent))))

(defun kogut-indent-scan-delimited-comment ()
  (forward-char 2)
  (let ((indent (+ outer-indent kogut-indent))
        (result nil)
        (nested 1))
    (while (null result)
      (kogut-indent-skip-chars " \t\n")
      (if (kogut-finished-indenting)
          (setq result (if (looking-at "\\*/") outer-indent indent))
        (if (looking-at "\\*/")
            (progn (forward-char 2)
                   (when (zerop (setq nested (1- nested)))
                     (setq result :skip)))
          (let ((indent-here (kogut-indent-here)))
            (when indent-here
              (setq indent indent-here)))
          (if (looking-at "/\\*")
              (progn (forward-char 2) (setq nested (1+ nested)))
            (forward-char)
            (kogut-indent-skip-chars "^/*\n"))
          (when (kogut-finished-indenting)
            (setq result indent)))))
    result))

(defun kogut-indent-scan-arg-after-label ()
  (forward-char)
  (kogut-indent-skip-chars " \t")
  (cond ((kogut-finished-indenting) (kogut-choose-indent))
        ((looking-at "$\\|/[/*]") :arg)
        (t (kogut-indent-scan-arg))))

(defun kogut-indent-scan-arg ()
  (let ((ch (char-after)))
    (cond ((null ch) inner-indent)
          ((eq ch ?\() (kogut-indent-scan-bracketed ?\)))
          ((eq ch ?\[) (kogut-indent-scan-bracketed ?\]))
          ((eq ch ?\{) (kogut-indent-scan-bracketed ?\}))
          ((eq ch ??) (kogut-indent-scan-function))
          ((eq ch ?\") (kogut-indent-scan-quoted ?\" "^\"\\\\\n"))
          ((eq ch ?')
           (let ((result (kogut-indent-scan-quoted ?' "^'\\\\\n")))
             (if (and (eq result :arg) (eq (char-after) ?:))
                 (kogut-indent-scan-arg-after-label)
               result)))
          ((looking-at "/\\*") (kogut-indent-scan-delimited-comment))
          ((looking-at "//") (forward-line 1) :skip)
          ((or (and (>= ch ?a) (<= ch ?z))
               (and (>= ch ?A) (<= ch ?Z))
               (and (>= ch ?0) (<= ch ?9))
               (eq ch ?_))
           (forward-char)
           (kogut-indent-skip-chars "[:alnum:]_'")
           (if (eq (char-after) ?:)
               (kogut-indent-scan-arg-after-label)
               :arg))
          ((looking-at "=>") (forward-char 2) :def)
          ((looking-at ";") (forward-char) :def)
          ((looking-at "[<=>~]=\\|->") (forward-char 2) :fun)
          ((looking-at "[!%&*+,/;<=>\\|~-]") (forward-char) :fun)
          (t (forward-char) :arg))))

(defun kogut-indent-scan (outer-indent inner-indent)
  "Scan a single token. Return the computed indent of the target
if the target was found within the sequence, :def if this is a semicolon,
:fun if this is an operator and thus the next token will be a function,
otherwise :arg."
  (let ((new-line (kogut-at-new-line)))
    (kogut-indent-scan-arg)))

(defun kogut-indent-scan-loop ()
  "Scan all tokens from the safe position above point to indenting-end."
  (kogut-to-last-safe-pos)
  (let ((closing-bracket nil))
    (kogut-indent-scan-tokens 0 0 0 0)))

(defun kogut-calculate-indent ()
  "Calculate indent appropriate for the current line."
  (save-excursion
    (beginning-of-line)
    (skip-chars-forward " \t")
    (let ((indenting-region nil)
          (indenting-end (- (point-max) (point))))
      (kogut-indent-scan-loop))))

(defun kogut-indent-line-to (indent)
  "Indent current line to the given amount."
  (let ((old-pos (- (point-max) (point))))
    (beginning-of-line)
    (let ((beg (point)))
      (skip-chars-forward " \t")
      (when (/= indent (current-column))
        (delete-region beg (point))
        (indent-to indent)))
    (let ((new-pos (- (point-max) old-pos)))
      (when (< (point) new-pos)
        (goto-char new-pos)))))

(defun kogut-indent-line ()
  "Indent current line as Kogut code."
  (interactive "*")
  (kogut-indent-line-to (kogut-calculate-indent))
  (when (and (not overwrite-mode) (kogut-at-new-line))
    ; Keep reindenting while regular characters are inserted, because
    ; they may influence the indent.
    (let ((done nil))
      (while (not done)
        (let* ((keyseq (read-key-sequence nil))
               (cmd (key-binding keyseq)))
          (cond ((and (memq cmd '(self-insert-command kogut-electric-closing))
                      (stringp keyseq))
                 (insert keyseq)
                 (if (and (string= keyseq " ")
                          (save-excursion
                            (skip-chars-backward " \t") (bolp)))
                     (setq done t)
                 (kogut-indent-line-to (kogut-calculate-indent))))
                (t (setq done t)
                   (when cmd
                     (call-interactively cmd)))))))))

(defun kogut-indent-region (start end)
  "Indent current region as Kogut code."
  (save-excursion
    (goto-char end)
    (end-of-line)
    (let ((indenting-end (- (point-max) (point))))
      (goto-char start)
      (beginning-of-line)
      (let ((indenting-region (point)))
        (kogut-indent-scan-loop)))))

(defun kogut-electric-closing ()
  "Insert a closing bracket, indenting it as Kogut code if it's at the
beginning of a line, preceded only by whitespace."
  (interactive "*")
  (let ((new-line (and (not overwrite-mode) (kogut-at-new-line))))
    (self-insert-command (prefix-numeric-value current-prefix-arg))
    (when new-line (kogut-indent-line))))

;; Filling

(defun kogut-fill-scan-quoted (closing-bracket word-chars)
  (forward-char)
  (let ((string-begin (point)))
    (let ((done nil))
      (while (not done)
        (skip-chars-forward word-chars)
        (cond ((eq (char-after) ?\\) (forward-char 2))
              (t (setq done t)))))
    (if (< (point) fill-point)
        (progn (unless (eobp) (forward-char)) nil)
      (when (eq closing-bracket ?\")
        (when (memq (char-before) '(?\  ?\t)) (backward-char))
        (save-excursion
          (save-restriction
            (narrow-to-region string-begin (point))
            (goto-char fill-point)
            (fill-paragraph justify)))
        (when (and (eolp) (not (eobp))) (delete-char 1)))
      t)))

(defun kogut-fill-scan-delimited-comment ()
  (forward-char 2)
  (when (and (not (zerop (skip-chars-forward "*")))
             (eq (char-after) ?/))
    (backward-char))
  (let ((comment-begin (point))
        (shift (current-column)))
    (let ((done nil)
          (nested 1))
      (while (not done)
        (skip-chars-forward "^/*")
        (cond ((looking-at "\\*/") (if (zerop (setq nested (1- nested)))
                                       (setq done t)
                                     (forward-char 2)))
              ((looking-at "/\\*") (forward-char 2) (setq nested (1+ nested)))
              ((eobp) (setq done t))
              (t (forward-char)))))
    (if (< (point) fill-point)
        (progn (unless (eobp) (forward-char 2)) nil)
      (save-excursion
        (goto-char comment-begin)
        (insert-char ?\  shift))
      (save-excursion
        (save-restriction
          (narrow-to-region comment-begin (point))
          (goto-char (+ fill-point shift))
          (fill-paragraph justify)))
      (delete-region comment-begin (+ comment-begin shift))
      (when (and (eolp) (not (eobp))) (delete-char 1) (insert-char ?\  1))
      t)))

(defun kogut-fill-scan-line-comment ()
  (let ((comment-begin (point))
        (shift (current-column)))
    (let ((prefix (concat (make-string shift ?\ ) "//")) 
          (done nil))
      (while (not done)
        (forward-line 1)
        (unless (looking-at prefix) (setq done t))))
    (if (< (point) fill-point)
        nil
      (save-excursion
        (goto-char comment-begin)
        (insert-char ?\  shift))
      (save-excursion
        (save-restriction
          (narrow-to-region comment-begin (point))
          (goto-char (+ fill-point shift))
          (fill-paragraph justify)))
      (delete-region comment-begin (+ comment-begin shift))
      t)))

(defun kogut-fill-paragraph (&optional justify)
  "If the point is inside a comment or string, fill a paragraph there.
Otherwise do nothing."
  (save-excursion
    (let ((fill-point (point))
          (done nil))
      (kogut-to-last-safe-pos)
      (while (and (not done) (< (point) fill-point))
        (let ((ch (char-after)))
          (when (cond ((eq ch ?\") (kogut-fill-scan-quoted ?\" "^\"\\\\"))
                      ((eq ch ?') (kogut-fill-scan-quoted ?' "^'\\\\"))
                      ((looking-at "/\\*") (kogut-fill-scan-delimited-comment))
                      ((looking-at "//") (kogut-fill-scan-line-comment))
                      ((or (and (>= ch ?a) (<= ch ?z))
                           (and (>= ch ?A) (<= ch ?Z))
                           (and (>= ch ?0) (<= ch ?9))
                           (eq ch ?_))
                       (forward-char) (skip-chars-forward "[:alnum:]_'") nil)
                      (t (forward-char) nil))
            (setq done t))))))
  t)

;; Keymap

(defvar kogut-mode-keymap nil
  "Keymap used while in Kogut mode.")

(unless kogut-mode-keymap
  (let ((keymap (make-sparse-keymap)))
    (define-key keymap ")" 'kogut-electric-closing)
    (define-key keymap "]" 'kogut-electric-closing)
    (define-key keymap "}" 'kogut-electric-closing)
    (setq kogut-mode-keymap keymap)))

;; Mode setup

(defun kogut-mode ()
  "Major mode for editing Kogut code."
  (interactive)
  (kill-all-local-variables)
  ;; Syntax highlighting
  (set-syntax-table kogut-mode-syntax-table)
  (make-local-variable 'font-lock-defaults)
  (setq font-lock-defaults '(kogut-font-lock-keywords))
  ;; Comments
  (make-local-variable 'comment-start)
  (setq comment-start "// ")
  (make-local-variable 'comment-end)
  (setq comment-end "")
  (make-local-variable 'comment-start-skip)
  (setq comment-start-skip "/\\*+ *\\|//+ *")
  ;; Indentation
  (make-local-variable 'indent-line-function)
  (setq indent-line-function 'kogut-indent-line)
  (make-local-variable 'indent-region-function)
  (setq indent-region-function 'kogut-indent-region)
  ;; Filling
  (make-local-variable 'paragraph-start)
  (setq paragraph-start "[ \t]*\\(//+[ \t]*\\)?$")
  (make-local-variable 'paragraph-separate)
  (setq paragraph-separate paragraph-start)
  (make-local-variable 'paragraph-ignore-fill-prefix)
  (setq paragraph-ignore-fill-prefix t)
  (make-local-variable 'adaptive-fill-regexp)
  (setq adaptive-fill-regexp
        (concat "\\([ \t]*//+\\)?"
                (if adaptive-fill-regexp
                    (concat "\\(" adaptive-fill-regexp "\\)")
                  "")))
  (make-local-variable 'fill-paragraph-function)
  (setq fill-paragraph-function 'kogut-fill-paragraph)
  (make-local-variable 'fill-column)
  (setq fill-column kogut-fill-column)
  ;; Keymap
  (use-local-map kogut-mode-keymap)
  ;; Compile errors
  (require 'compile)
  (let ((kokogut-errors (concat "\\(?:\\(?:Impossible \\)?Error\\|"
                                "\\(Warning\\)\\|\\(Notice\\)\\) "
                                "at \\([^:\n]+\\):"
                                "\\([[:digit:]]+\\),\\([[:digit:]]+\\)")))
    (unless (assoc kokogut-errors compilation-error-regexp-alist)
      (setq compilation-error-regexp-alist
            (cons (cons kokogut-errors '(3 4 5 (1 . 2)))
                  compilation-error-regexp-alist))))
  ;; Register the mode
  (setq mode-name "Kogut")
  (setq major-mode 'kogut-mode)
  (run-hooks 'kogut-mode-hook))
